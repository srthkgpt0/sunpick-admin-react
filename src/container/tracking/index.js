import React, { useEffect, useState } from 'react'
import {
  getTrackingListService,
  getSingleTrackingDetailService
} from '../../services/tracking'
import { message } from 'antd'
import TrackingList from './trackingList'
import TrackingDetail from './trackingDetail'
import TrackingMap from './trackingMap'
import { Link } from 'react-router-dom'
import logger from '../../utilities/logger'
import { filterDataObj } from '../../utilities/common'
import { useTranslation } from 'react-i18next'
import { LoadingSpinner } from '../../components/common'

function Tracking() {
  const [sizePerPage] = useState(10)
  const [page] = useState(1)
  const [filterData, setFilterData] = useState({})
  const [ride, setRide] = useState([])
  const [totalRide, setTotalRide] = useState(0)
  const [isLoading, setIsLoading] = useState(true)
  const [loading, setLoading] = useState(false)
  const [hasMore, setHasMore] = useState(true)
  const [showDetails, setShowDetails] = useState(false)
  const [rideDetail, setRideDetail] = useState({})

  const [trackData, setTrackData] = useState({})
  const [directions, setDirections] = useState()

  const formRef = React.createRef()
  const { t } = useTranslation()

  useEffect(() => {
    fetchNowRidesDetails()
  }, [filterData]) // eslint-disable-line react-hooks/exhaustive-deps

  const fetchNowRidesDetails = async (
    queryParams = {
      offset: (page - 1) * sizePerPage,
      limit: sizePerPage,
      status: 'started'
    }
  ) => {
    // setIsLoading(true)
    try {
      queryParams = {
        ...queryParams,
        ...filterData
      }

      const res = await getTrackingListService({ queryParams })
      setRide(res.data.rows)
      setTotalRide(res.data.total)
      setIsLoading(false)
    } catch {}
  }

  const handleInfiniteOnLoad = () => {
    setLoading(true)
    if (ride.length > 14) {
      message.warning('Infinite List loaded all')
      setLoading(false)
      setHasMore(false)
      return
    }
    fetchNowRidesDetails()
    setLoading(false)
  }

  const onClickTrackRide = async (id) => {
    setIsLoading(true)
    try {
      const res = await getSingleTrackingDetailService(id)
      setRideDetail(res.data)
      setIsLoading(false)
      setShowDetails(true)
    } catch {}
  }

  const onClickBack = () => {
    setTrackData({})
    setRideDetail({})
    setDirections()
    setShowDetails(false)
  }

  const onFinishSearch = (values) => {
    console.log('VALUES', values)
    const { filterData } = filterDataObj(values)
    console.log('FilterData', filterData)
    setFilterData(filterData)
    // fetchNowRidesDetails(filterData)
  }

  const onFinishFailed = (errorInfo) => {
    logger({ 'Failed:': errorInfo })
  }

  const onClickReset = () => {
    setFilterData({})
    formRef.current.resetFields()
    // fetchNowRidesDetails()
  }

  const updateTrackData = (data) => {
    setTrackData(data)
  }

  const updateDirections = (dir) => {
    setDirections(dir)
  }

  return (
    <main className='maincontent manage-ride dashboard-page'>
      <section className='page_header'>
        <div className='page_header_overlay'>
          <h2>{t('tracking.title')}</h2>
        </div>
      </section>
      <section className='page_content mb-0'>
        <div className='container-fluid'>
          <div className='content_sec'>
            <div id='toggleMsgBar'>
              <div className='row custom-gutters'>
                <div className='col-lg-4'>
                  <div className='card left bg-white box-shadow border-0'>
                    <div className='ride_list ride_user_details'>
                      {showDetails ? (
                        <div className='card-header border-0 bg-white d-flex align-items-center justify-content-between justify-content-lg-start'>
                          {/* <div className='toggle-icon'>
                            <a>
                              <span></span> <span></span>
                              <span></span>
                            </a>
                          </div> */}
                          <Link
                            to='#'
                            className='h-18 mb-0'
                            onClick={onClickBack}
                          >
                            <i class='icon-navigate_before'></i>
                            {t('common.back')}
                          </Link>
                        </div>
                      ) : (
                        <div className='card-header border-0 bg-white d-flex align-items-center justify-content-between'>
                          <h6 className='h-18 left-border mb-0'>
                            <b>
                              {t('tracking.rides')}{' '}
                              <span className='badge '>{totalRide}</span>
                            </b>
                          </h6>
                          <div className='toggle-icon'>
                            <Link to='#'>
                              <i className='icon-cross'></i>
                            </Link>
                          </div>
                        </div>
                      )}
                      {showDetails ? (
                        // isLoading? <LoadingSpinner/> :<TrackingDetail rideDetail={rideDetail} />
                        <TrackingDetail rideDetail={rideDetail} />
                      ) : isLoading ? (
                        <LoadingSpinner />
                      ) : (
                        <TrackingList
                          handleInfiniteOnLoad={handleInfiniteOnLoad}
                          loading={loading}
                          hasMore={hasMore}
                          ride={ride}
                          onClickTrackRide={onClickTrackRide}
                          onFinishSearch={onFinishSearch}
                          onClickReset={onClickReset}
                          onFinishFailed={onFinishFailed}
                          formRef={formRef}
                        />
                      )}
                    </div>
                  </div>
                </div>
                <div className='col-lg-8'>
                  <div className='overlay'></div>
                  <div className='  box-shadow border-0'>
                    <div className=' border-0  d-sm-flex align-items-center justify-content-between pl-0'></div>
                    <TrackingMap
                      rideDetail={rideDetail}
                      trackData={trackData}
                      updateTrackData={updateTrackData}
                      directions={directions}
                      updateDirections={updateDirections}
                    ></TrackingMap>
                    <div className='tab-content' id='myTabContent'>
                      <div
                        className='tab-pane fade show active'
                        id='user-location'
                        role='tabpanel'
                        aria-labelledby='user-location-tab'
                      >
                        <div className='card-body'>
                          <div className='map mapheight'></div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
    </main>
  )
}

export default Tracking
