import React, { Component, useEffect, useState } from 'react'
import { compose } from 'recompose'
import { getSingleTrackerService } from '../../services/tracking'
import {
  withScriptjs,
  withGoogleMap,
  GoogleMap,
  DirectionsRenderer,
  Marker,
  InfoWindow
} from 'react-google-maps'
import config from '../../config'
const MapWithAPolyLine = compose(
  withScriptjs,
  withGoogleMap
)((props) => {
  const [directions, setDirections] = useState()
  const [center, setCenter] = useState(
    new window.google.maps.LatLng(9.145, 40.489673)
  )

  // const [riderID, setRiderID] = useState(props.rideData && props.rideData.id ? props.rideData.id : '');
  const [trackData, setTrackData] = useState({})
  const [map, setMap] = React.useState(null)
  var bounds = new window.google.maps.LatLngBounds()
  const [isStartPoint, setIsStartPoint] = useState(false)
  useEffect(() => {
    // props.updateZoom(7)
    if (props.rideData && Object.keys(props.rideData).length != 0) {
      const directionsService = new window.google.maps.DirectionsService()

      const origin = {
        lat: props.rideData.pick_up_latitude,
        lng: props.rideData.pick_up_longitude
      }
      const destination = {
        lat: props.rideData.drop_off_latitude,
        lng: props.rideData.drop_off_longitude
      }

      directionsService.route(
        {
          origin: origin,
          destination: destination,
          travelMode: window.google.maps.TravelMode.DRIVING
        },
        (result, status) => {
          if (status === window.google.maps.DirectionsStatus.OK) {
            setDirections(result)
            // props.updateZoom(7)
          } else {
            console.error(`error fetching directions ${result}`)
          }
        }
      )
    } else {
      // window.google.maps.event.addListenerOnce(map, 'bounds_changed', function(event) {
      //   console.log("dddddddddd")
      //   if (this.getZoom()) {
      //   this.setZoom(7);
      //   }
      // });
      // props.updateZoom(7)// setZoom1(7)
      setCenter({ lat: 9.145, lng: 40.489673 })
      setDirections(null)
    }
  }, [props.rideData])

  useEffect(() => {
    const interval = setInterval(() => {
      opensnack()
    }, 10000)
    // console.log("interval", interval)
    return () => clearInterval(interval)
  }, [props.rideData])

  const onStartMarkerClick = (evt) => {
    setIsStartPoint(!isStartPoint)
  }
  function handleZoomChanged() {
    //  console.log("sidee",directions)
    // this.setZoom(7)
    // console.log(this.getZoom(), 'zom') //this refers to Google Map instance
  }

  function updateMapCenter() {
    // console.log('sidee', directions)
    // setCenter({ lat: 9.1450, lng: 40.489673 })
    //this refers to Google Map instance
  }

  const opensnack = async (text) => {
    // console.log("opensnack", props.rideData)
    if (props.rideData && props.rideData.id !== '') {
      // const res = await getSingleTrackerService(props.rideData.id)
      // console.log("res",res)
      // setTrackData(res.data)
    }
  }
  // console.log('props.rideData', props.rideData, 'directions', props.zoom1)
  //  window.google.maps.Map.setCenter(bounds.getCenter())
  return (
    <>
      <GoogleMap
        ref={(map) => map}
        center={center}
        defaultZoom={props.zoom1}
        onZoomChanged={handleZoomChanged}
        onCenterChanged={updateMapCenter}
      >
        {props.rideData && directions && (
          <DirectionsRenderer directions={directions} />
        )}
        {trackData &&
          trackData.status === 'started' &&
          trackData.driver &&
          trackData.driver.latitude &&
          trackData.driver.longitude && (
            <Marker
              position={{ lat: 22.7196, lng: 75.8577 }}
              onClick={onStartMarkerClick}
              label={{ text: 'A', color: 'white' }}
            >
              {isStartPoint && (
                <InfoWindow onClick={onStartMarkerClick}>
                  <div>
                    <div>
                      Name:{' '}
                      <b>
                        {' '}
                        ${trackData.driver.first_name} $
                        {trackData.driver.last_name}{' '}
                      </b>
                    </div>
                    <div>
                      Phone:<b> ${trackData.driver.phone_number} </b>
                    </div>
                    <div>
                      Registration No.:{' '}
                      <b> ${trackData.driver.car_registration_number} </b>
                    </div>
                  </div>
                </InfoWindow>
              )}
            </Marker>
          )}
      </GoogleMap>
    </>
  )
})

export default class TrackingMap extends Component {
  constructor(props) {
    super(props)
  }

  render() {
    return (
      <>
        {
          <MapWithAPolyLine
            googleMapURL={`https://maps.googleapis.com/maps/api/js?key=${config.GOOGLE_API_KEY}&v=3.exp&libraries=geometry,drawing,places`}
            loadingElement={<div style={{ height: `100%` }} />}
            containerElement={<div style={{ height: `400px` }} />}
            mapElement={<div style={{ height: `500px` }} />}
            rideData={this.props.rideDetail}
            zoom1={this.props.zoom1}
            // updateZoom ={this.props.updateZoom}
          />
        }
      </>
    )
  }
}
