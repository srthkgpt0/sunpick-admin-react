import React from 'react'
import { dateFormatter } from "../../utilities/common"
import { useTranslation } from 'react-i18next'


export default function TrackingDetail({
    rideDetail
}) { 
  const { t } = useTranslation()

  return (
    <>
      {/* <div classNameName="card-body p-0 left_cardbody">
        <h1>Hello</h1>
      </div> */}
      <div className="card-body p-0" style={{overflow: "inherit!important"}}>
           
           <div className="common_border">
             <div className="profile ">
               <div className="p_img">
                 <img src='assets/images/default-userNew.jpg' className="rounded-circle"
                   alt="user img" />
               </div>
               <div className="p_content">
                 <div className="info">
                   <h6 className="text-truncate text-capitalize font-heavy">
                    {rideDetail && rideDetail.customer.first_name} {rideDetail && rideDetail.customer.last_name}
                   </h6>
                   {/* <span className="date_time h-14 color-blue-light">booking date time</span> */}
                 </div>
                 <div className="view_info d-flex align-items-center justify-content-between">
                   <p className="mb-0 font-book h-14">{t('rides.rideId')}: <span
                       className="font-heavy">{rideDetail.id}</span></p>
                 </div>
                 <div className="view_info d-flex align-items-center justify-content-between">
                   <p className="mb-0 font-book h-14">{t('tracking.contactNo')}: <span
                       className="font-heavy">{rideDetail.customer.phone_number_country_code}-{rideDetail.customer.phone_number}
                     </span></p>
                 </div>
               </div>
             </div>
           </div>

          
           {/* <div className="book_for_someone common_border">
             <ul className="list-unstyled mb-0">
               <li className=" mb-3">
                 <h4 className="h-14">Passanger Name</h4>
                 <p className="mb-0">passanger name</p>
               </li>
               <li>
                 <h4>Contact Number</h4>
                 <p className="mb-0">
                   passenger_phone_number_country_code passenger_mobile 
                 </p>

               </li>
             </ul>
           </div> */}
          
           <div className="location common_border">
             <ul className="list-unstyled mb-0">
               <li>
                 <h4 className="h-14 ">{t('tracking.pickupLocation')}</h4>
                 <p className="mb-0">{rideDetail.pick_up_address}</p>
               </li>
               {/* <li  >
                 <h4>Way-Point Location</h4>
                 <p className="mb-0">address</p>
               </li> */}
               <li>
                 <h4>{t('tracking.dropOffLocation')}</h4>
                 <p className="mb-0">{rideDetail.end_address} {rideDetail.drop_off_address}</p>
               </li>
             </ul>
           </div>
           <div className="ride_info pb-0 common_border border-0">

             <h4>{t('tracking.bookingDateTime')}</h4>
             <div >
               <p className="mb-0">{dateFormatter(rideDetail.booking_date)} </p>
             </div>
          

           </div>
           <div className="fare_info common_border">
             <div className="row">
               <div className="col-sm-4 mb-sm-0">
                 <h4>{t('tracking.fare')}</h4>
                 <p className="mb-0">ETB{rideDetail.ride_amount}</p>
               </div>
               <div className="col-sm-4 mb-3 mb-sm-0">
                 <h4>{t('tracking.distance')}</h4>
                 <p className="mb-0">{rideDetail.ride_km} km</p>
               </div>
               <div className="col-sm-4 mb-3 mb-sm-0">
                 <h4>{t('tracking.time')}</h4>
                 <p className="mb-0">{rideDetail.estimate_time} min</p>
               </div>
             </div>
           </div>
           <div className="car_info common_border ">
             <div className="row">
               <div className="col-sm-6 mb-3 mb-sm-0">
                 <h4>{t('tracking.category')}</h4>
                 <p className="mb-0 text-capitalize">{rideDetail.car_category}</p>
               </div>
               <div className="col-sm-6 mb-3 mb-sm-0">
                 <h4>{t('tracking.couponCode')}</h4>
                 <p className="mb-0">
                 {/* {rideDetail.promo_code} */}
                 {(rideDetail.promo_code)? rideDetail.promo_code : '-'}
                 </p>
               </div>
             </div>
           </div>
           <div className="ride_info  common_border">
             <div className="row">
               <div className="col-sm-6 mb-3 mb-sm-0">
                 <h4>{t('tracking.rideStartTime')}</h4>
                 <p className="mb-0">{dateFormatter(rideDetail.start_date)}</p>
               </div>
               <div className="col-sm-6 ">
                 <h4>{t('tracking.rideEndTime')}</h4>
                 <p className="mb-0">{(rideDetail.end_date)? dateFormatter(rideDetail.end_date) : '-'}</p>
               </div>
             </div>
           </div>
           <div className="ride_info pb-0 common_border border-0">

             <h4>{t('storyLib.description')}</h4>
             <div >
               <div >-</div>
             </div>
           </div>
       </div>
    </>
  )
}

TrackingDetail.propTypes = {
//   form: PropTypes.any.isRequired,
//   handleInfiniteOnLoad: PropTypes.func.isRequired,
//   hasMore: PropTypes.bool.isRequired,
//  loading: PropTypes.bool.isRequired,
//   onFinish: PropTypes.func.isRequired,
//   onFinishFailed: PropTypes.func.isRequired,
//   onFileUploaded: PropTypes.func.isRequired,
//   ride: PropTypes.object.isRequired,
//   media: PropTypes.any.isRequired,
//   submitButtonText: PropTypes.string.isRequired,
//   errorMsg: PropTypes.string.isRequired
}
