import React, { Component, useState, useEffect } from 'react'
import { compose } from 'recompose'
// import { currentTimeStamp } from '../../utilities/common'
import { getSingleTrackerService } from '../../services/tracking'

import {
  withScriptjs,
  withGoogleMap,
  GoogleMap,
  Marker,
  InfoWindow,
  DirectionsRenderer
} from 'react-google-maps'
import config from '../../config'
const MapWithAPolyLine = compose(
  withScriptjs,
  withGoogleMap
)((props) => {
  const data = props.rideData
  const [isStartPoint, setIsStartPoint] = useState(false)
  // const [isEndPoint, setIsEndPoint] = useState(false)
  const [direction, setDirection] = useState(null)
  const [trackData, setTrackData] = useState({})
  const onStartMarkerClick = (evt) => {
    setIsStartPoint(!isStartPoint)
  }
  useEffect(() => {
    // props.updateZoom(7)

    if (data && Object.keys(data).length !== 0) {
      const directionsService = new window.google.maps.DirectionsService()

      const origin = {
        lat: props.rideData.pick_up_latitude,
        lng: props.rideData.pick_up_longitude
      }
      const destination = {
        lat: props.rideData.drop_off_latitude,
        lng: props.rideData.drop_off_longitude
      }

      directionsService.route(
        {
          origin: origin,
          destination: destination,
          travelMode: window.google.maps.TravelMode.DRIVING
        },
        (result, status) => {
          if (status === window.google.maps.DirectionsStatus.OK) {
            setDirection(result)
            // props.updateZoom(7)
          } else {
            console.error(`error fetching directions ${result}`)
          }
        }
      )
      const interval = setInterval(() => {
        opensnack()
      }, 10000)

      return () => clearInterval(interval)
    } else {
      setDirection(null)
    }
    // }
  }, [data]) // eslint-disable-line react-hooks/exhaustive-deps

  // const onEndMarkerClick = (evt) => {
  //   setIsEndPoint(!isEndPoint)
  // }
  // useEffect(() => {

  //   const interval = setInterval(() => {
  //     opensnack()
  //   }, 10000);

  //   return () => clearInterval(interval);
  // }, [props.rideData]);

  const opensnack = async (text) => {
    if (props.rideData && props.rideData.id) {
      const res = await getSingleTrackerService(props.rideData.id)
      // console.log("res",res)
      if (
        res.data.driver &&
        res.data.driver.latitude &&
        res.data.driver.longitude
      ) {
        setTrackData(res.data)
      }
    } else {
      setTrackData({})
    }
  }
  var langList = []

  if (data && Object.keys(data).length !== 0) {
    langList.push(
      new window.google.maps.LatLng(
        data.pick_up_latitude,
        data.pick_up_longitude
      )
    )

    langList.push(
      new window.google.maps.LatLng(data.end_latitude, data.end_longitude)
    )
  }
  // console.log("langList", langList)
  var bounds = new window.google.maps.LatLngBounds()
  langList.forEach(function (n) {
    bounds.extend(n)
  })

  // console.log("direction", bounds.getCenter())
  //  window.google.maps.Map.setCenter(bounds.getCenter())
  return (
    <>
      {data && Object.keys(data).length !== 0 ? (
        <GoogleMap
          ref={(map) =>
            map &&
            data &&
            Object.keys(data).length !== 0 &&
            map.fitBounds(bounds)
          }
          defaultCenter={bounds.getCenter()}
          defaultZoom={7}
        >
          {data && Object.keys(data).length !== 0 && (
            <>
              <DirectionsRenderer directions={direction} />
              {trackData &&
                trackData.status === 'started' &&
                trackData.driver &&
                trackData.driver.latitude &&
                trackData.driver.longitude && (
                  <Marker
                    position={{
                      lat: trackData.driver.latitude,
                      lng: trackData.driver.longitude
                    }}
                    onClick={onStartMarkerClick}
                    defaultIcon={'assets/images/booked-cab-new.png'}
                  >
                    {isStartPoint && (
                      <InfoWindow onClick={onStartMarkerClick}>
                        <div>
                          <div>
                            Name:{' '}
                            <b>
                              {' '}
                              ${trackData.driver.first_name} $
                              {trackData.driver.last_name}{' '}
                            </b>
                          </div>
                          <div>
                            Phone:<b> ${trackData.driver.phone_number} </b>
                          </div>
                          <div>
                            Registration No.:{' '}
                            <b> ${trackData.driver.car_registration_number} </b>
                          </div>
                        </div>
                      </InfoWindow>
                    )}
                  </Marker>
                )}
            </>
          )}
        </GoogleMap>
      ) : (
        <GoogleMap
          // ref={(map) => (map)}
          center={{ lat: 9.145, lng: 40.489673 }}
          defaultZoom={7}
          zoom={7}
        ></GoogleMap>
      )}
    </>
  )
})

export default class TrackingMap extends Component {
  render() {
    return (
      <>
        {
          <MapWithAPolyLine
            googleMapURL={`https://maps.googleapis.com/maps/api/js?key=${config.GOOGLE_API_KEY}&v=3.exp&libraries=geometry,drawing,places`}
            loadingElement={<div style={{ height: `100%` }} />}
            containerElement={<div style={{ height: `400px` }} />}
            mapElement={<div style={{ height: `500px` }} />}
            rideData={this.props.rideDetail}
          />
        }
      </>
    )
  }
}
