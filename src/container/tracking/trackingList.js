import React from 'react'
import { List, Spin, Button, Form, Input } from 'antd'
import InfiniteScroll from 'react-infinite-scroller'
import { dateFormatter } from '../../utilities/common'
import { Link } from 'react-router-dom'
import { useTranslation } from 'react-i18next'

export default function TrackingList({
  handleInfiniteOnLoad,
  loading,
  hasMore,
  ride,
  onClickTrackRide,
  onFinishSearch,
  onClickReset,
  onFinishFailed,
  formRef
}) {
  const { t } = useTranslation()

  return (
    <>
      <div className='card-body p-0 left_cardbody'>
        <div className='search '>
          <Form
            name='basic'
            ref={formRef}
            onFinish={onFinishSearch}
            onFinishFailed={onFinishFailed}
          >
            <Form.Item name='search_text' className='mb-0'>
              <Input
                type='text'
                className='form-control'
                placeholder='Search by Ride ID, Ride No., Ph. No.'
              />
            </Form.Item>
            {/* <Form.Item> */}
            <Link className='search_btn' to='#' onClick={onClickReset}>
              <i className='fa fa-refresh' aria-hidden='true'></i>
            </Link>
            {/* </Form.Item> */}
            {/* <Form.Item> */}
            <Button className='search_btn' to='#' htmlType='submit'>
              <i className='fa fa-search' aria-hidden='true'></i>
            </Button>
            {/* </Form.Item> */}
          </Form>
        </div>
        <div className='demo-infinite-container'>
          <InfiniteScroll
            initialLoad={false}
            pageStart={0}
            loadMore={handleInfiniteOnLoad}
            hasMore={!loading && hasMore}
            useWindow={false}
          >
            <List
              dataSource={ride}
              renderItem={(item) => (
                <List.Item key={item.id}>
                  <div className='profile'>
                    <div className='p_img'>
                      <img
                        src={'assets/images/default-userNew.jpg'}
                        className='rounded-circle'
                        alt='not found'
                      />
                    </div>
                    <div className='p_content'>
                      <div className='info'>
                        <h6 className='text-truncate text-capitalize font-heavy'>
                          {item.customer.first_name} {item.customer.last_name}
                        </h6>
                        <span className='h-14 color-blue-light'>
                          {item.customer.phone_number_country_code}{' '}
                          {item.customer.phone_number}
                        </span>
                        <span className='price font-black'>
                          ETB {item.ride_amount}
                        </span>
                        <div className='d-lg-flex align-items-center justify-content-between'>
                          <span className='date_time h-14 color-blue-light'>
                            {dateFormatter(item.booking_date)}
                          </span>
                          <span className='km h-12 color-blue-light'>
                            {item.ride_km}Km
                          </span>
                        </div>
                      </div>
                      <div className='view_info d-lg-flex align-items-center justify-content-between'>
                        <p className='mb-0 font-book h-14'>
                          {t('rides.rideId')}:{' '}
                          <span className='font-heavy'>{item.id}</span>
                          <br />
                          {t('tracking.rideNo')}:{' '}
                          <span className='font-heavy'>{item.ride_number}</span>
                        </p>
                        <Link
                          to='#'
                          className='btn btn-dark'
                          onClick={() => onClickTrackRide(item.id)}
                        >
                          {t('tracking.trackRide')}
                        </Link>
                      </div>
                    </div>
                  </div>
                </List.Item>
              )}
            >
              {loading && hasMore && (
                <div className='demo-loading-container'>
                  <Spin />
                </div>
              )}
            </List>
          </InfiniteScroll>
        </div>
      </div>
    </>
  )
}

TrackingList.propTypes = {
  //   form: PropTypes.any.isRequired,
  //   handleInfiniteOnLoad: PropTypes.func.isRequired,
  //   hasMore: PropTypes.bool.isRequired,
  //  loading: PropTypes.bool.isRequired,
  //   onFinish: PropTypes.func.isRequired,
  //   onFinishFailed: PropTypes.func.isRequired,
  //   onFileUploaded: PropTypes.func.isRequired,
  //   ride: PropTypes.object.isRequired,
  //   media: PropTypes.any.isRequired,
  //   submitButtonText: PropTypes.string.isRequired,
  //   errorMsg: PropTypes.string.isRequired
}
