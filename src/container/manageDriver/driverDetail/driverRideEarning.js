import React, { PureComponent } from 'react'
import { driverRidesEarningList } from '../../../services/driver'
// import {
//   getRideStatusClass,
//   getRideStatusCustomName
// } from '../../../services/common'
import logger from '../../../utilities/logger'
// import { MDBDataTable } from 'mdbreact'
// import Moment from 'moment'
import { withTranslation } from 'react-i18next'

// import InfiniteScroll from 'react-infinite-scroll-component'
import {
  addPageSizeInURL,
  addressFormatter,
  amountFormatter,
  customerNameFormatter,
  dateFormatter,
  // DEFAULT_CURRENCY_SYMBOL,
  getPageSizeFromURL,
  ratingFormatter,
  rideStatusFormatter
  // statusStringFormatter
} from '../../../utilities/common'
// import { Tooltip } from 'antd'
// import ToolTipComponent from '../../../../components/tooltip'
// import RatingComponent from '../../../../components/rating'
// import LoaderComponent from '../../../../components/loader'
import RemoteDataTable from '../../../components/dataTable'
import { withRouter } from 'react-router'

// import ConfirmationAndInfo from '../../../../components/modals/confirmationAndInfo'
// import modalNotification from '../../../../utilities/notifications'
// import PropTypes from 'prop-types'
class DriverRideEarningComponent extends PureComponent {
  constructor(props) {
    super(props)
    // Sets up our initial state

    this.state = {
      data: [],
      totalSize: 0,
      page: 1,
      sizePerPage: 10,
      mapData: {},
      formData: '',
      toData: '',
      error: false,
      hasMore: true,
      isLoading: false,
      users: [],
      driverData: [],
      dataList: [],
      driverId: this.props.driver_id,

      columns: [
        {
          text: this.props.t('drivers.id'),
          dataField: 'id'
        },
        {
          text: this.props.t('drivers.rideDateAndTime'),
          dataField: 'booking_date',
          formatter: dateFormatter
        },
        {
          text: this.props.t('drivers.bookingFromTo'),
          dataField: 'dummyPickupDropOf',
          formatter: addressFormatter
        },
        {
          text: this.props.t('rides.riderListing'),
          dataField: 'customer_name',
          formatter: customerNameFormatter
        },
        {
          text: this.props.t('rides.rideFare'),
          dataField: 'ride_fare',
          formatter: amountFormatter
        },
        {
          text: this.props.t('common.ratings'),
          dataField: 'rating',
          formatter: ratingFormatter
        },
        {
          text: this.props.t('rides.rideStatusListing'),
          dataField: 'status',
          formatter: rideStatusFormatter
        }
      ]
    }
  }
  // when props change from parent component

  // Loads some driverData on initial load

  componentDidMount() {
    this.fetchData()
  }

  // to fetch more data at the time of scrolling
  fetchMoreData = () => {
    if (this.state.data.length < this.state.totalSize) {
      this.setState({
        page: this.state.page + 1
      })
      const queryParams = {
        offset: (this.state.page - 1) * this.state.sizePerPage,
        limit: this.state.sizePerPage
      }
      this.fetchData(queryParams)
    } else {
      this.setState({
        hasMore: false
      })
    }
  }

  // to fetch driver data
  fetchData = async (
    queryParams = {
      offset: (this.state.page - 1) * this.state.sizePerPage,
      limit: this.state.sizePerPage,
      reset: false
    }
  ) => {
    if (!queryParams['reset']) {
      queryParams = {
        ...queryParams
      }
    }

    try {
      const res = await driverRidesEarningList(
        { queryParams },
        this.state.driverId
      )
      this.setState({
        isLoading: true
      })
      if (res.data.rows.length !== 0 && this.state.data.length !== 0) {
        this.setState({
          dataList: this.state.data.concat(res.data.rows)
        })
      } else if (res.data.rows.length !== 0) {
        this.setState({
          dataList: res.data.rows
        })
      }
      this.setState({
        isLoading: false,
        dataTemp: this.state.dataList,
        totalSize: res.data.total,
        hasMore: this.state.users.length < 800,
        data: this.state.dataList,
        driverData: []
      })
    } catch (error) {
      logger({ 'error:': error })
    }
  }
  handleTableChange = (
    type,
    { page, sizePerPage, filters, sortField, sortOrder, cellEdit }
  ) => {
    this.setState(
      {
        page: page,
        sizePerPage: sizePerPage,
        isLoading: true,
        data: [],
        totalSize: 0
      },
      () => {
        if (sortField === 'isDummySno') {
          sortField = 'id'
        }

        const queryParams = {
          offset: (page - 1) * sizePerPage,
          limit: sizePerPage,
          sortBy: sortField,
          sortType: sortOrder
        }

        if (!this.state.isFirstTimeFetching) {
          addPageSizeInURL(page, sizePerPage, this.props.history)
        }
        this.fetchOnHandleTableChange(queryParams)
      }
    )
  }
  fetchOnHandleTableChange = (queryParams) => {
    if (this.state.isFirstTimeFetching) {
      const { location } = this.props

      if (location) {
        const query = location.search
        const res = getPageSizeFromURL(query, location)
        if (res) {
          this.reFetchOnUrlBasis(query)
        } else {
          this.fetchData(queryParams)
        }
      }
    } else {
      this.fetchData(queryParams)
    }
  }

  render() {
    const {
      columns,
      isLoading,
      data,
      totalSize,
      page,
      sizePerPage
    } = this.state
    // const tableData = { columns: columns, rows: users }
    return (
      <>
        <div
          className='tab-pane fade show active'
          id='ridesEarning'
          role='tabpanel'
          aria-labelledby='forridesEarning'
        >
          <RemoteDataTable
            columns={columns}
            data={data}
            totalSize={totalSize}
            page={page}
            sizePerPage={sizePerPage}
            loading={isLoading}
            onTableChange={this.handleTableChange}
          />
        </div>
      </>
    )
  }
}

export default withRouter(withTranslation()(DriverRideEarningComponent))
