import React, { useEffect, useState } from 'react'
import { Form } from 'antd'
import modalNotification from '../../utilities/notifications'
import {
  getMakeList,
  getModelList,
  getCategoryList,
  getSateList,
  addUpdateDriver,
  getDriverDetail
} from '../../services/driver'
import logger from '../../utilities/logger'
import AddEditDriverForm from '../../components/forms/drivers/addeditDriver'
import { connect } from 'react-redux'
import { Link } from 'react-router-dom'
import PropTypes from 'prop-types'
import {
  PHONE_NUMBER_COUNTRY_CODE,
  PHONE_NUMBER_COUNTRY
} from '../../utilities/common'
import path from 'path'
import queryString from 'query-string'
import moment from 'moment'
// import GlobalLoader from '../../../components/subComponent/globalLoader'

const AddEditDriver = (props) => {
  const [form] = Form.useForm()
  const [isSpin, setIsSpin] = useState(false)
  // const [isAlert, setIsAlert] = useState(false)
  // const [errorMsg, setErrorMsg] = useState('')
  const [makeList, setmakeList] = useState([])
  const [modelList, setmodelList] = useState([])
  const [stateList, setstateList] = useState([])
  const [categoryId, setCategoryId] = useState()
  const { id } = queryString.parse(props.location.search)
  const history = props.history

  useEffect(() => {
    if (id) {
      fetchDriverDetail(id)
    } else {
      form.setFieldsValue({
        first_name: '',
        lastName: '',
        email: '',
        photo: '',
        commercial_insurance: '',
        license_photo_front: '',
        license_photo_back: '',
        registration_photo: '',
        owner_tin: '',
        employment_agreement: '',
        brand_id: '',
        model_id: '',
        state: '',
        category_id: '',
        date_of_birth: ''
      })
    }
    fetchvehicleMake()
    fetchstateList()
  }, []) // eslint-disable-line react-hooks/exhaustive-deps

  const fetchvehicleMake = async (values) => {
    try {
      const res = await getMakeList({})
      // console.log(res, 'fetchvehicleMake')
      if (res && res.success) {
        setmakeList(res.data.rows)
        if (!id) {
          form.setFieldsValue({
            model_id: '',
            category_id: ''
          })
        }
      }
    } catch (error) {
      logger({ 'error:': error })
    }
  }
  const fetchDriverDetail = async (id) => {
    try {
      const res = await getDriverDetail(id)
      // console.log(res.data, 'response from driverDetail')
      if (res && res.success) {
        const { data } = res
        // console.log(
        //   moment(data.date_of_birth).format('YYYY-MM-DD'),
        //   'data of birth'
        // )
        fetchvehicleModel(data.brand_id)
        form.setFieldsValue({
          first_name: data.first_name,
          last_name: data.last_name,
          email: data.email,
          photo: data.photo,
          commercial_insurance: data.commercial_insurance,
          license_photo_front: data.license_photo_front,
          license_photo_back: data.license_photo_back,
          registration_photo: data.registration_photo,
          owner_tin: data.owner_tin,
          employment_agreement: data.employment_agreement,
          model_id: { value: data.model_id, label: data.model_id },
          brand_id: { value: data.brand_id, label: data.brand_id },
          state: { value: data.state, label: data.state },
          category_id: data.category_name,
          date_of_birth: moment(data.date_of_birth).format('YYYY-MM-DD'),
          vehicle_year: data.vehicle_year.toString(),
          car_registration_number: data.car_registration_number,
          blood_group: data.blood_group,
          emergency_number: data.emergency_number,
          emergency_contact_name: data.emergency_contact_name,
          city: data.city,
          postal_code: data.postal_code,
          phone_number: data.phone_number,
          address: data.address
        })
        setCategoryId(data.category_id)
      }
    } catch (error) {
      logger({ 'error:': error })
    }
  }
  const fetchvehicleModel = async (brand_id_data) => {
    // console.log(brand_id_data, 'fetchvehicleModel')
    try {
      if (brand_id_data) {
        form.setFieldsValue({
          brand_id: brand_id_data,
          model_id: '',
          category_id: ''
        })
        const queryParams = {
          brand_id: brand_id_data.value ? brand_id_data.value : brand_id_data,
          limit: 2000,
          offset: 0
        }
        const res = await getModelList({ queryParams })
        console.log(res, 'Response of MODELLIST')
        if (res && res.success) {
          setmodelList(res.data.rows)
        }
      }
    } catch (error) {
      logger({ 'error:': error })
    }
  }
  const fetchCategory = async (model_id_data) => {
    try {
      form.setFieldsValue({
        model_id: model_id_data
      })

      if (model_id_data) {
        const queryParams = {
          brand_id: form.getFieldValue('brand_id').value,
          model_id: model_id_data.value,
          limit: 2000,
          offset: 0
        }
        const res = await getCategoryList({ queryParams })
        if (res && res.success) {
          // console.log('res.data.id', res.data.id)
          setCategoryId(res.data.id)
          form.setFieldsValue({
            category_id: res.data.name
          })
          // setmodelList(res.data.rows);
        }
      }
    } catch (error) {
      logger({ 'error:': error })
    }
  }
  const fetchstateList = async () => {
    try {
      const queryParams = {
        limit: 2000,
        offset: 0
      }
      const res = await getSateList({ queryParams })
      if (res && res.success) {
        setstateList(res.data)
        // setmodelList(res.data.rows);
      }
    } catch (error) {
      logger({ 'error:': error })
    }
  }

  const onFinish = async (values) => {
    setIsSpin(true)
    try {
      // console.log(values, 'values')
      const formData = {}
      formData['phone_number_country_code'] = PHONE_NUMBER_COUNTRY_CODE
      formData['phone_number_country'] = PHONE_NUMBER_COUNTRY
      formData['photo'] = path.basename(values.photo)
      formData['commercial_insurance'] = path.basename(
        values.commercial_insurance
      )
      formData['license_photo_front'] = path.basename(
        values.license_photo_front
      )
      formData['license_photo_back'] = path.basename(values.license_photo_back)
      formData['owner_tin'] = path.basename(values.owner_tin)
      formData['employment_agreement'] = path.basename(
        values.employment_agreement
      )
      formData['registration_photo'] = path.basename(values.registration_photo)
      formData['model_id'] = values.model_id.value
      formData['brand_id'] = values.brand_id.value
      formData['category_id'] = categoryId
      formData['state'] = values.state.value
      formData['city'] = values.city
      formData['address'] = values.address
      formData['first_name'] = values.first_name
      formData['last_name'] = values.last_name
      formData['phone_number'] = values.phone_number
      formData['email'] = values.email
      formData['vehicle_year'] = values.vehicle_year
      formData['car_registration_number'] = values.car_registration_number
      formData['date_of_birth'] = values.date_of_birth
      formData['blood_group'] = values.blood_group
      formData['emergency_number'] = values.emergency_number
      formData['emergency_contact_name'] = values.emergency_contact_name
      formData['postal_code'] = values.postal_code
      formData['car_passenger_capacity'] = '1'

      const res = await addUpdateDriver(formData, id)
      if (res && res.success) {
        modalNotification({
          type: 'success',
          message: 'Success',
          description: res.message
        })
        history.push('/drivers')
      } else {
        modalNotification({
          type: 'error',
          message: 'Error',
          description: res.message
        })
      }
    } catch (error) {
      // setIsAlert(true)
      setIsSpin(false)
      // setErrorMsg(error.message)
      logger({ 'error:': error })
    }
  }

  const onFinishFailed = (errorInfo) => {
    logger({ 'Failed:': errorInfo })
  }

  const onFileUploaded = (photo, mediaType) => {
    // console.log('dddd', photo)
    if (mediaType === 'commercial_insurance') {
      form.setFieldsValue({
        commercial_insurance: photo.commercial_insurance
      })
    } else if (mediaType === 'photo') {
      form.setFieldsValue({
        photo: photo.photo
      })
    } else if (mediaType === 'license_photo_front') {
      form.setFieldsValue({
        license_photo_front: photo.license_photo_front
      })
    } else if (mediaType === 'license_photo_back') {
      form.setFieldsValue({
        license_photo_back: photo.license_photo_back
      })
    } else if (mediaType === 'registration_photo') {
      form.setFieldsValue({
        registration_photo: photo.registration_photo
      })
    } else if (mediaType === 'owner_tin') {
      form.setFieldsValue({
        owner_tin: photo.owner_tin
      })
    } else if (mediaType === 'employment_agreement') {
      form.setFieldsValue({
        employment_agreement: photo.employment_agreement
      })
    }
  }

  const onFileRemoved = () => {
    form.setFieldsValue({
      icon: ''
    })
  }
  const onFromDateChange = (value, dateString) => {
    form.setFieldsValue({
      date_of_birth: dateString
    })
  }
  return (
    <main className='maincontent form_box'>
      <section className='page_header'>
        <div className='page_header_overlay'>
          <h2>{id ? 'Edit Driver' : 'Add Driver'}</h2>
          <Link className='back-btn' to='/drivers'>
            <i className='ti-hand-point-left' aria-hidden='true'></i>BACK
          </Link>
        </div>
      </section>
      <section className='page_content'>
        <div className='container-fluid'>
          <div className='card' id='card_height'>
            <div className='card-header clearfix'>
              <h3>{id ? 'Edit Driver' : 'Add Driver'}</h3>
            </div>
            <div className='card-block'>
              <div>
                <div className='row'>
                  <div className='col-sm-12 col-xl-8'>
                    <AddEditDriverForm
                      form={form}
                      isSpin={isSpin}
                      onFinish={onFinish}
                      onFinishFailed={onFinishFailed}
                      onFileUploaded={onFileUploaded}
                      onFileRemoved={onFileRemoved}
                      onFromDateChange={onFromDateChange}
                      submitButtonText={'submit'}
                      media={['']}
                      makeList={makeList}
                      modelList={modelList}
                      stateList={stateList}
                      fetchvehicleModel={fetchvehicleModel}
                      fetchCategory={fetchCategory}
                      id={id}
                    />
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
    </main>
  )
}

const mapStateToProps = (state) => {
  return {
    userData: state.auth.userData,
    selectedVendorId: state.common.selectedVendor
  }
}

const mapDispatchToProps = () => {
  return {}
}
AddEditDriver.propTypes = {
  userData: PropTypes.object.isRequired
}
export default connect(mapStateToProps, mapDispatchToProps)(AddEditDriver)
