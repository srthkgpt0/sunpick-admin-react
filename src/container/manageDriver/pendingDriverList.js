import React, { useEffect, useState } from 'react'
import RemoteDataTable from '../../components/dataTable/index'
import { Link, useHistory, useLocation } from 'react-router-dom'
import { useTranslation } from 'react-i18next'
import { Menu, Dropdown } from 'antd'
import {
  phoneNumberFormatter,
  getPageSizeFromURL,
  addPageSizeInURL,
  customerNameFormatter,
  emailFormatter,
  documentStatusFormatter,
  showDateOnlyInBrowser,
  statusFormatter
} from '../../utilities/common'
import {
  getDriverListService,
  driverStatusUpdateService
} from '../../services/driver'
import modalNotification from '../../utilities/notifications'
import enValidationMsg from '../../utilities/lang/validation-en'
import ConfirmationAndInfo from '../../components/modals/confirmationAndInfo'

function PendingDriverList({ isFinish, filterData }) {
  const { t } = useTranslation()
  const [data, setData] = useState('')
  const [sizePerPage, setSizePerPage] = useState(10)
  const [page, setPage] = useState(1)
  const [totalSize, setTotalSize] = useState(0)
  const [isLoading, setIsLoading] = useState(true)
  const [isFirstTimeFetching, setIsFirstTimeFetching] = useState(true)
  const [statusChangeIntermediate, setStatusChangeIntermediate] = useState({})
  const [visible, setVisible] = useState(false)
  const [newState, setNewState] = useState(null)

  const history = useHistory()
  const location = useLocation()

  useEffect(() => {
    fetchPendingDriverDetails()
  }, []) // eslint-disable-line react-hooks/exhaustive-deps
  useEffect(() => {
    const queryParams = {
      offset: (page - 1) * sizePerPage,
      limit: sizePerPage
    }
    if (!isFirstTimeFetching) {
      addPageSizeInURL(page, sizePerPage, history)
    }
    fetchOnHandleTableChange(queryParams)
  }, [page]) // eslint-disable-line react-hooks/exhaustive-deps

  useEffect(() => {
    formResponse()
  }, [isFinish]) // eslint-disable-line react-hooks/exhaustive-deps
  useEffect(() => {
    fetchPendingDriverDetails()
  }, [filterData]) // eslint-disable-line react-hooks/exhaustive-deps

  const formResponse = () => {
    setPage(1)
    setSizePerPage(sizePerPage)
    setIsLoading(true)
    setData([])
    setTotalSize(0)
    // setFilterData(filterData)
  }
  // const handleConfirmStatus = (id,currentState) => {
  //   setVisible(true)
  //   setNewState(currentState)
  // }

  // const handleConfirmStatus = (id, currentStatus) => {
  //   setExecutiveId(id)
  //   setVisible(true)
  //   console.log('currentStatus', currentStatus, 'id', id)
  //   if (currentStatus === 'active') {
  //     setNewStatus('inactive')
  //   } else if (currentStatus === 'inactive') {
  //     setNewStatus('active')
  //   } else {
  //     setNewStatus('delete')
  //   }
  // }

  // const actionFormatter = (cell, row) => {
  //   return (
  //     <div>
  //       <Link to={`/rides/detail/${row.id}?tab_type=pre_book`}>
  //         <span className='ti-eye'></span>
  //       </Link>
  //       <span className='ti-pencil'></span>
  //       <span className='ti-trash'></span>
  //     </div>
  //   )
  // }

  const actionFormatter = (cell, row) => {
    const menu = (
      <Menu>
        <div aria-labelledby='dropdownMenuButton'>
          <Menu>
            <Menu.Item key='1'>
              <Link
                className='dropdown-item'
                // onClick={() => handleStatus(row.id, 'delete')}
                to={{ pathname: '/driver-detail', search: `?id=${row.id}` }}
              >
                {t('common.view')}
              </Link>
            </Menu.Item>
            <Menu.Item key='0'>
              <Link
                className='dropdown-item'
                onClick={() => {
                  // handleEditStatus(row, 'edit')
                }}
                to={{ pathname: '/add-driver', search: `?id=${row.id}` }}
              >
                {t('common.edit')}
              </Link>
            </Menu.Item>
            <Menu.Item key='2'>
              <Link
                className='dropdown-item'
                onClick={() => onOpenStatus('delete', row)}
                to='#'
              >
                {t('common.delete')}
              </Link>
            </Menu.Item>
          </Menu>
        </div>
      </Menu>
    )
    return (
      <Dropdown overlay={menu} trigger={['click']}>
        <div className='dropdown mx-auto'>
          <Link
            to='#'
            onClick={(e) => e.preventDefault()}
            className='dropdown-toggle'
            id={`dropdownMenuButton_${row.id}`}
            data-toggle='dropdown'
            aria-haspopup='true'
            aria-expanded='false'
          >
            <span className='icon-more_vert'></span>
          </Link>
        </div>
      </Dropdown>
    )
  }

  const [columns] = useState([
    {
      text: t('drivers.id'),
      dataField: 'id'
    },
    {
      text: t('drivers.fullName'),
      dataField: 'first_name',
      formatter: customerNameFormatter
    },
    {
      text: t('drivers.number'),
      dataField: 'phone_number',
      formatter: phoneNumberFormatter
    },
    {
      text: t('drivers.email'),
      dataField: 'email',
      formatter: emailFormatter
    },
    {
      text: t('drivers.dateRegistered'),
      dataField: 'created_at',
      formatter: showDateOnlyInBrowser
    },
    {
      text: t('drivers.category'),
      dataField: 'category.name'
    },
    {
      text: t('drivers.documentStatus'),
      dataField: 'dummyDocumentStatus',
      formatter: documentStatusFormatter
    },
    {
      text: t('drivers.status'),
      dataField: 'status',
      formatter: (cell, row) => statusFormatter(cell, row, onOpenStatus)
    },
    {
      dataField: 'isDummyAction',
      text: t('common.action'),
      formatter: actionFormatter
    }
  ])
  const onConfirmation = async () => {
    if (visible) {
      let status = ''
      if (statusChangeIntermediate.val) {
        if (statusChangeIntermediate.val === 'delete') {
          status = 'delete'
          setNewState(status)
        } else {
          status = 'active'
          setNewState(status)
        }
      } else {
        status = 'inactive'
        setNewState(status)
      }

      // console.log('STATUS', status, newState)

      const res = await driverStatusUpdateService(
        statusChangeIntermediate.row.id,
        status
      )

      if (res.success) {
        openHideModal()
        fetchPendingDriverDetails()
        statusChangeIntermediate.resHandleChange(status)
        setStatusChangeIntermediate({})
        setData([])
        modalNotification({
          type: 'success',
          message: 'Success',
          description: res.message || enValidationMsg.statusUpdate
        })
      }
    }
  }
  const onOpenStatus = async (val, row, resHandleChange) => {
    try {
      setVisible(true)
      setStatusChangeIntermediate({
        val: val,
        row: row,
        resHandleChange: resHandleChange
      })
    } catch {}
  }
  const fetchOnHandleTableChange = (queryParams) => {
    if (isFirstTimeFetching) {
      if (location) {
        const query = location.search
        const res = getPageSizeFromURL(query, location)
        if (res) {
          // reFetchOnUrlBasis(query)
        } else {
          fetchPendingDriverDetails()
        }
      }
    } else {
      fetchPendingDriverDetails()
    }
  }
  const openHideModal = () => {
    let status = ''
    if (statusChangeIntermediate.val) {
      status = 'inactive'
    } else {
      status = 'active'
    }
    statusChangeIntermediate.resHandleChange(status)
    setVisible(false)
  }
  const handleTableChange = (
    type,
    { page, sizePerPage, filters, sortField, sortOrder, cellEdit }
  ) => {
    setPage(page)
    setSizePerPage(sizePerPage)
    setIsLoading(true)
    setData([])
    setTotalSize(0)
  }
  const fetchPendingDriverDetails = async (
    queryParams = {
      offset: (page - 1) * sizePerPage,
      limit: sizePerPage,
      status: 'pending'
    }
  ) => {
    try {
      queryParams = {
        ...queryParams,
        ...filterData
      }

      const res = await getDriverListService({ queryParams })
      // console.log(res)
      setData(res.data.rows)
      setTotalSize(res.data.rows.length > 0 ? res.data.total : 0)
      setIsFirstTimeFetching(false)
      setIsLoading(false)
    } catch {}
  }
  return (
    <div>
      <RemoteDataTable
        columns={columns}
        data={data}
        totalSize={totalSize}
        page={page}
        sizePerPage={sizePerPage}
        loading={isLoading}
        onTableChange={handleTableChange}
        // defaultSorted={defaultSorted}
      />
      <ConfirmationAndInfo
        show={visible}
        onHide={() => openHideModal()}
        title={'Confirmation Box'}
        message={`Are you sure you want to ${newState} ?`}
        textOnConfirmBtn={'Confirm'}
        textOnCancelBtn={'Cancel'}
        showLoading={false}
        onConfirmation={onConfirmation}
      />
    </div>
  )
}

export default PendingDriverList
