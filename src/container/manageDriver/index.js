import React, { PureComponent, Fragment } from 'react'
import { connect } from 'react-redux'
import PropTypes from 'prop-types'
// import { currentTimeStamp } from '../../utilities/common'
import {
  categoryListService,
  driverAvailabilityService
} from '../../services/driver'
import logger from '../../utilities/logger'
import { Link } from 'react-router-dom'
// import { Form, Input, Button } from 'antd'
// import { Select } from 'antd'
// import Autocomplete from 'react-google-autocomplete'
import RegisteredDriverList from './activeDriverList'
import PendingDriverList from './pendingDriverList'
import DriverImportModal from '../../components/modals/driver/importModal'
import { withTranslation } from 'react-i18next'
import DriversFilterForm from '../../components/forms/drivers/driversFilterForm'
import ExportCsvPdfComponent from '../../utilities/export-csv-pdf'
import ApiEndPoints from '../../utilities/apiEndPoints'


// const { Option } = Select

class ManageDriver extends PureComponent {
  constructor(props) {
    super(props)
    // Sets up our initial state

    this.state = {
      filterData: {},
      page: 1,
      sizePerPage: 10,
      mapData: {},
      formData: '',
      toData: '',
      error: false,
      taxiCategoryList: [],
      driverAvailability: [],
      activeTabValue: 'Approved',
      tabActive: true,
      show: true,
      isFinish: false,
      isLoading: true,
      showImportModal: false
    }
  }

  onFinish = (values) => {
    this.setState({ isFinish: true })
    values['latitude'] = this.state.mapData.latitude
    values['longitude'] = this.state.mapData.longitude
    values['from_date'] = this.state.formData
    values['to_date'] = this.state.toData
    values['category_id'] =
      values.category_id && values.category_id.value
        ? values.category_id.value
        : ''
    values['availability'] =
      values.availability && values.availability.key
        ? values.availability.key
        : ''

    this.setState({
      filterData: values
    })
  }
  onFromDateChange = (value, dateString) => {
    this.setState({ formData: dateString })
  }
  onToDateChange = (value, dateString) => {
    this.setState({ toData: dateString })
  }
  onAutoComplete = (values) => {
    const mapData = {}
    mapData['latitude'] = values.geometry.location.lat()
    mapData['longitude'] = values.geometry.location.lng()
    this.setState({ mapData: mapData })
  }

  onFinishFailed = (errorInfo) => {}

  onReset = () => {
    this.formRef.current.resetFields()
    let data = {}
    if (this.state.tabActive) {
      data['status'] = ''
      data['reset'] = true
      this.setState({
        filterData: data
      })
    } else if (!this.state.tabActive) {
      // console.log('this.state.tabActive', this.state.tabActive)
      data['status'] = 'pending'
      data['reset'] = true
      this.setState({
        filterData: data
      })
    }
  }
  formRef = React.createRef()

  componentDidMount() {
    // Loads some users on initial load
    this.setState({
      isFirstTimeFetching: true
    })
    this.fetchCategoryList()
    this.fetchDriverAvailability()
  }

  // update status
  filterOpen = (event, show) => {
    if (show === true) {
      this.setState({
        show: false
      })
    } else {
      this.setState({
        show: true
      })
    }

    event.preventDefault()
  }

  openImportModal = () => {
    this.setState({ showImportModal: true })
  }

  hideImportModal = () => {
    this.setState({ showImportModal: false })
  }

  fetchCategoryList = async (
    queryParams = {
      dropdown: 1,
      limit: 2000,
      offset: 0
    }
  ) => {
    try {
      const res = await categoryListService({ queryParams })
      if (res.success && res.data && res.data.rows) {
        this.setState({
          taxiCategoryList: res.data.rows
        })
      }
    } catch (error) {
      logger({ 'error:': error })
    }
  }

  fetchDriverAvailability = async () => {
    try {
      const data = await driverAvailabilityService()
      // console.log(data, 'driverAvailabilityService')
      this.setState({
        driverAvailability: data
      })
    } catch (error) {
      logger({ 'error:': error })
    }
  }

  onTabClick(tab) {
    // this.state.activeTabValue = tab
    const data = this.state.filterData
    if (tab === 'Approved') {
      data['status'] = ''
      this.setState({
        tabActive: true,
        filterData: data
      })
    } else if (tab === 'Pending') {
      data['status'] = 'pending'
      this.setState({
        tabActive: false,
        filterData: data
      })
    }
  }

  render() {
    const {
      taxiCategoryList,
      driverAvailability,
      filterData,
      tabActive,
      show
    } = this.state
    const activeClass = tabActive ? 'active' : ''
    const pengingClass = !tabActive ? 'active' : ''

    const activeclassName = `${'nav-link'} ${activeClass}`
    const pengingclassName = `${'nav-link'} ${pengingClass}`

    const { t } = this.props
    const { userType } = this.props.userData

    return (
      <>
        <main className='maincontent setting-page'>
          <section className='page_header'>
            <div className='page_header_overlay'>
              <h2>{t('drivers.manageDriver')}</h2>
            </div>
          </section>
          <section className='page_content'>
            <div className='container-fluid'>
              <div className='row'>
                <div className='col-md-12'>
                  <div className='card' id='card_height'>
                    <div className='card-header clearfix'>
                      <h3>{t('drivers.manageDriver')}s</h3>
                      <div className='action'>
                        <div className='d-inline-block addbtndiv'>
                          <Link
                            className='btn btn-warning'
                            id='btnSearch'
                            to='#'
                            onClick={(e) => this.filterOpen(e, show)}
                          >
                            <i className='fa fa-search'></i>
                          </Link>
                          {(userType === 'admin') &&
                            <>
                            <Link className='btn btn-warning' to='/add-driver'>
                              Add
                            </Link>
                            <Link
                              to='#'
                              className='btn btn-warning'
                              onClick={() => {
                                this.openImportModal()
                              }}
                            >
                              Import
                            </Link>
                            </>
                          }
                        </div>
                      </div>
                    </div>
                    {show && (
                      <div className='filter_section' id='driverSearch'>
                        <div className='container-fluid'>
                          <DriversFilterForm
                            taxiCategoryList={taxiCategoryList}
                            driverAvailability={driverAvailability}
                            onFromDateChange={this.onFromDateChange}
                            onToDateChange={this.onToDateChange}
                            onAutoComplete={this.onAutoComplete}
                            onFinish={this.onFinish}
                            onFinishFailed={this.onFinishFailed}
                            onReset={this.onReset}
                            formRef={this.formRef}
                          />
                        </div>
                      </div>
                    )}
                    
                    <div className='card-block'>
                      {(userType === 'admin') && 
                        <ul
                          className='nav nav-tabs mb-20'
                          id='myTab'
                          role='tablist'
                        >
                          <li className='nav-item'>
                            <Link
                              className={activeclassName}
                              role='tab'
                              aria-controls='home'
                              to='#'
                              aria-selected='true'
                              onClick={(e) => {
                                e.preventDefault()
                                this.onTabClick('Approved')
                              }}
                            >
                              {t('drivers.activeDrivers')}
                            </Link>
                          </li>
                          <li className='nav-item'>
                            <Link
                              className={pengingclassName}
                              data-toggle='tab'
                              to='#'
                              aria-controls='profile'
                              aria-selected='false'
                              onClick={(e) => {
                                e.preventDefault()
                                this.onTabClick('Pending')
                              }}
                            >
                              {t('drivers.pendingRequests')}
                            </Link>
                          </li>
                        </ul>
                      }
                      <div className='tab-content'>
                        {(userType === 'admin') &&
                          <ExportCsvPdfComponent
                            path={ApiEndPoints.DRIVER_LIST_FILE_DOWNLOAD}
                            queryParams={filterData}
                        ></ExportCsvPdfComponent>
                        }
                        {tabActive && (
                          <RegisteredDriverList
                            isFinish={this.state.isFinish}
                            filterData={filterData}
                          ></RegisteredDriverList>
                        )}
                        {!tabActive && (
                          <PendingDriverList
                            isFinish={this.state.isFinish}
                            filterData={filterData}
                          ></PendingDriverList>
                        )}
                      </div>
                      <DriverImportModal
                        show={this.state.showImportModal}
                        // data={executive}
                        onHide={() => this.hideImportModal()}
                      />
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </section>
        </main>
      </>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    userData: state.auth.userData,
    selectedVendorId: state.common.selectedVendor
  }
}

const mapDispatchToProps = () => {
  return {}
}

ManageDriver.propTypes = {
  userData: PropTypes.object.isRequired
}

export default withTranslation()(
  connect(mapStateToProps, mapDispatchToProps)(ManageDriver)
)
