import React, { PureComponent, Fragment } from 'react';
import { driverDocumentList } from '../../../services/driver';
import logger from '../../../utilities/logger';
import { Link } from 'react-router-dom';


class PendingDriverDocument extends PureComponent {

  constructor(props) {
    super(props);
    // Sets up our initial state

    this.state = {
      page: 1,
      sizePerPage: 10,
      documentModel: [],
      driverId: this.props.driver_id,
    }
  }


  // Loads some driverData on initial load

  componentDidMount() {
    this.fetchData();
  }


  // to fetch driver data
  fetchData = async (
    queryParams = {
      offset: ((this.state.page - 1) * this.state.sizePerPage),
      limit: this.state.sizePerPage
    }
  ) => {

    queryParams = {
      ...queryParams,

    }

    try {

      if (this.state.driverId) {
        const res = await driverDocumentList({ queryParams }, this.state.driverId);

        this.setState({
          documentModel: res.data,
          isLoading: false
        })
      }

    } catch (error) {

      logger({ 'error:': error })
    }
  };




  render() {

    const {
      documentModel
    } = this.state;
    return (
      <>

        <div className="tab-pane fade show active" id="DocTab" role="tabpanel" aria-labelledby="forDocTab">
          <div className="doc-wrapper">
            
               <div className="row">

                <div className="col-sm-6 col-md-3">
                  <div className="doc-set">
                    <p>Driver’s license Front Side</p>
                    <Link className="example-image-link" title="Driver’s license Front Side"> <img src={documentModel.license_photo_front} className="img-fluid" alt="license_front" /></Link>

                  </div>
                </div>

                <div className="col-sm-6 col-md-3">
                  <div className="doc-set">
                    <p>Driver’s license Back Side</p>
                    <Link className="example-image-link" title="Driver’s license Back Side"> <img src={documentModel.license_photo_back} className="img-fluid" alt="license_back"/></Link>

                  </div>
                </div>

                <div className="col-sm-6 col-md-3">
                  <div className="doc-set">
                    <p>Owner's Tin</p>
                    <Link className="example-image-link" title="Owner's Tin">  <img src={documentModel.owner_tin} className="img-fluid" alt="owner"/></Link>

                  </div>
                </div>



                <div className="col-sm-6 col-md-3">
                  <div className="doc-set">
                    <p>Commercial Insurance</p>
                    <Link className="example-image-link" title="Commercial Insurance">  <img src={documentModel.commercial_insurance} className="img-fluid" alt="insurance"/></Link>

                  </div>
                </div>

                <div className="col-sm-6 col-md-3">
                  <div className="doc-set">
                    <p>Driver Employment Agreement</p>
                    <Link className="example-image-link" title="Driver Employment Agreement">   <img src={documentModel.employment_agreement } className="img-fluid" alt="agreement" /></Link>

                  </div>
                </div>



                <div className="col-sm-6 col-md-3">
                  <div className="doc-set">
                    <p>Vehicle Registration </p>
                    <Link className="example-image-link" title="Vehicle Registration">   <img src={documentModel.registration_photo} className="img-fluid" alt="registration" /></Link>

                  </div>
                </div>

              </div>
            

          </div>
        </div>

      </>
    )
  }
}



export default PendingDriverDocument