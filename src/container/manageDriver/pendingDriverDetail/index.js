import React, { useEffect, useState } from 'react'
// import modalNotification from '../../../utilities/notifications';
import { getDriverDetail ,changeStatusService} from '../../../services/driver'
import logger from '../../../utilities/logger'
import { connect } from 'react-redux';
import { Link } from 'react-router-dom'
import PropTypes from 'prop-types';
import Moment from 'moment';
import PendingDriverDocument from './pendingDriverDocs';
import queryString from 'query-string';
import modalNotification from '../../../utilities/notifications'
// import {
//   PHONE_NUMBER_COUNTRY_CODE,
//   PHONE_NUMBER_COUNTRY
// } from '../../../utilities/common';
// import notification_msg from '../../../utilities/textMessages';

const PendingDriverDetail = (props) => {
  const [isDucument] = useState(true)
  const [driverModel, setdriverDetail] = useState([])
  const { id } = queryString.parse(props.location.search);
  // const history = props.history
  useEffect(() => {
    fetchDriverDetail(id)

  }, []) // eslint-disable-line react-hooks/exhaustive-deps


  const fetchDriverDetail = async (id) => {
    try {
      const res = await getDriverDetail(id);

      if (res && res.success) {
        setdriverDetail(res.data);
      }

    } catch (error) {
      logger({ 'error:': error })
    }
  }
 const ridesDocComponent = (event, type) => {
    try {
      

    } catch (error) {
      logger({ 'error:': error })
    }

    event.preventDefault();
  };
 const updateStatusById = async (event, id, status, visible) => {
    let newStatus;
    if (status === 'pending') {
      newStatus = 'active';
  
    }else if (status === 'active'){
      newStatus = 'pending';
 
    }
    try {
      const body = { status: newStatus };
      const res = await changeStatusService(body, id);

      if (res.success) {
        
        modalNotification({
          type: 'success',
          message: 'Success',
          description: res.message
        })
      }
      event.preventDefault();
    } catch (error) {

      logger({ 'error:': error })
    }
    
  };

  
 
  return (

  
    <main className="maincontent view-p">
  <section className="page_header">
    <div className="page_header_overlay">
      <h2>DRIVER DETAIL</h2>
      <Link className="back-btn" to="/drivers"><i className="ti-hand-point-left" aria-hidden="true"></i>BACK </Link>
    </div>
  </section>
  <section className="page_content">
    <div className="top-info">
      <div className="container-fluid">
        <div className="row">
          <div className="col-sm-6 col-lg-4">
            <div className="box user-detail">
              <h2 className="view-heading">USER BASIC INFO :</h2>
              <div className="row">
                <div className="col-5 custom-col col-sm-4">
                  <div className="left-side">
                    <Link className="example-image-link user-img rounded-circle" to=""  onClick={(e) => e.preventDefault()}>
                    <img src={driverModel?.photo?driverModel?.photo:'assets/images/default-userNew.jpg'} className="" alt="logo" />
                    </Link>
              
                  
                    {/* <div className="review-text">
                      <small>Average rating</small>
                    </div> */}
                  </div>
                </div>
                <div className="col-7 custom-col col-sm-8">
                  <div className="right-side">
                    <div className="info-set">
                      <p>Name</p>
                      <span>{driverModel?.first_name?driverModel?.first_name:'-'}
                        {driverModel?.last_name?driverModel?.last_name:'-'}</span>
                    </div>
                    <div className="info-set">
                      <p>Mobile Number</p>
                      <span>{driverModel?.phone_number_country_code} {driverModel?.phone_number}</span>
                    </div>
                    <div className="info-set">
                      <p>Email</p>
                      <span>{driverModel?.email} </span>
                    </div>
                    <div className="info-set">
                      <p>Razorpay Account ID</p>
                      <span>{driverModel?.razorpay_account_id?driverModel?.razorpay_account_id:'-'} </span>
                    </div>
                   

                  </div>
                </div>
              </div>
            </div>
          </div>

          <div className="col-sm-6 col-lg-4">
            <div className="box user-detail ">
              <h2 className="view-heading">VEHICLE INFO :</h2>
              <div className="row">
                <div className="col-5 custom-col col-sm-4">
                  <div className="left-side">
                  <Link className="example-image-link user-img rounded-circle" to=""  onClick={(e) => e.preventDefault()}>
                  <img className="" src={driverModel?.category_icon?driverModel?.category_icon:'assets/images/default-userNew.jpg'} alt="logo"/> 
                    </Link>
                
                    <div className="vehicle-text">
                      <p className="mb-0">{driverModel?.brand_name?driverModel?.brand_name:'-'}</p>
                      <small className="subtext">{driverModel?.vehicle_year?driverModel?.vehicle_year:'-'}</small>
                    </div>
                  </div>
                </div>

                <div className="col-7 custom-col col-sm-8">
                  <div className="right-side">
                    <div className="info-set">
                      <p>Vehicle Category</p>
                      <span>{driverModel?.category_name?driverModel?.category_name:'-'}</span>
                    </div>
                    <div className="info-set">
                     
                      <p> Vehicle Registration Number</p>
                      <span>{driverModel?.car_registration_number?driverModel?.car_registration_number:'-'}</span>
                    </div>
                   
                    <div className="info-set">
                      <p>Capacity</p>
                      <span>{driverModel?.category_capacity?driverModel?.category_capacity:'-'}</span>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        
          <div className="col-sm-12 col-lg-4">
            <div className="box user-detail m-t-15">
              <h2 className="view-heading">OTHER DETAILS :</h2>
              <div className="row">
                <div className="col-6 custom-col">
                  <div className="info-set">
                    <p>Total Trips</p>
                    <span>{driverModel?.total_rides?driverModel?.total_rides:'0'}
                    </span>
                  </div>

                  <div className="info-set mb-xl--0">
                    <p>Status</p>
                   
                  </div>
                  <label className="commoncheck-wrap mb-1">
                  <input type="checkbox" defaultChecked={false} onClick={(e) => updateStatusById(e, driverModel.id, driverModel.status, true)} />
                  <span className="slider round"></span>
                </label>
                    
                  <div className="info-set">
                    <p>Emergency Number {driverModel.status}</p>
                    <span>{driverModel?.emergency_number_country_code}
                      {driverModel?.emergency_number?driverModel?.emergency_number:'-'}</span>
                  </div>

                  <div className="info-set">
                    <p>City </p>
                    <span>{driverModel?.city?driverModel?.city:'-'}</span>
                  </div>
             
                </div>

                <div className="col-6 custom-col">
                  <div className="info-set">
                    <p>DOB </p>
                    <span>{driverModel?.date_of_birth==null ?"-": Moment(driverModel?.date_of_birth).format('YYYY-MM-DD') }</span>
                  </div>

                  <div className="info-set">
                    <p>Blood Group </p>
                    <span>{driverModel?.blood_group?driverModel?.blood_group:'-'}</span>
                  </div>

              
                  <div className="info-set">
                    <p>Postal Code</p>
                    <span>{driverModel?.postal_code?driverModel?.postal_code:'-'}</span>
                  </div>
                  <div className="info-set">
                    <p>State</p>
                    <span>{driverModel?.state?driverModel?.state:'-'}</span>
                  </div>
                </div>
              </div>
              <div className="row">
                <div className="col-md-12">
                  
                  <div className="info-set">
                    <p>Address </p>
                    <span>{driverModel?.address?driverModel?.address:'-'}</span>
                  </div>
                </div>

              </div>
            </div>
          </div>
        </div>
      </div>
    </div>



    <div className="container-fluid">
      <div className="view-grid">
        <div className="tab-view">
          <ul className="nav nav-tabs mb-20" role="tablist">
            
           
            <li className="nav-item ">
            <Link className="nav-link active "  to="" onClick={(e) => ridesDocComponent(e, 'document')}> Documents </Link>
            </li>
          </ul>
        </div>
        <div className="tab-content">
              { isDucument && <PendingDriverDocument driver_id={id}></PendingDriverDocument>}
        </div>
      </div>
    </div>
  </section>
</main>
  )
}


const mapStateToProps = (state) => {
  return {
    userData: state.auth.userData,
    selectedVendorId: state.common.selectedVendor
  }
}

const mapDispatchToProps = () => {
  return {

  }
}
PendingDriverDetail.propTypes = {
  userData: PropTypes.object.isRequired
}
export default connect(mapStateToProps, mapDispatchToProps)(PendingDriverDetail)

