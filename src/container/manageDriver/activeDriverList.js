import React, { useEffect, useState } from 'react'
import RemoteDataTable from '../../components/dataTable/index'
import { Menu, Dropdown, Switch } from 'antd'
import { Link, useHistory, useLocation } from 'react-router-dom'
import enValidationMsg from '../../utilities/lang/validation-en'
import { useTranslation } from 'react-i18next'
import {
  phoneNumberFormatter,
  getPageSizeFromURL,
  addPageSizeInURL,
  customerNameFormatter,
  emailFormatter,
  availabilityFormatter,
  showDateOnlyInBrowser,
  locationStatusFormatter,
  statusFormatter
} from '../../utilities/common'
import {
  getDriverListService,
  driverStatusUpdateService
} from '../../services/driver'
import ConfirmationAndInfo from '../../components/modals/confirmationAndInfo'
import modalNotification from '../../utilities/notifications'
import MapMarker from '../../components/defaultMap'

function RegisteredDriverList({ isFinish, filterData }) {
  const { t } = useTranslation()
  const [data, setData] = useState('')
  const [sizePerPage, setSizePerPage] = useState(10)
  const [page, setPage] = useState(1)
  const [totalSize, setTotalSize] = useState(0)
  const [isLoading, setIsLoading] = useState(true)
  const [isFirstTimeFetching, setIsFirstTimeFetching] = useState(true)
  const [visible, setVisible] = useState(false)
  const [newStatus, setNewStatus] = useState(null)
  const [changeData, setChangeData] = useState({})
  const [showMap, setShowMap] = useState(false)

  const history = useHistory()
  const location = useLocation()

  useEffect(() => {
    fetchActiveDriverDetails()
  }, []) // eslint-disable-line react-hooks/exhaustive-deps
  useEffect(() => {
    const queryParams = {
      offset: (page - 1) * sizePerPage,
      limit: sizePerPage
    }
    if (!isFirstTimeFetching) {
      addPageSizeInURL(page, sizePerPage, history)
    }
    fetchOnHandleTableChange(queryParams)
  }, [page]) // eslint-disable-line react-hooks/exhaustive-deps

  useEffect(() => {
    formResponse()
  }, [isFinish]) // eslint-disable-line react-hooks/exhaustive-deps
  useEffect(() => {
    fetchActiveDriverDetails()
  }, [filterData]) // eslint-disable-line react-hooks/exhaustive-deps

  const formResponse = () => {
    setPage(1)
    setSizePerPage(sizePerPage)
    setIsLoading(true)
    setData([])
    setTotalSize(0)
    // setFilterData(filterData)
  }

  const actionFormatter = (cell, row) => {
    const menu = (
      <Menu>
        <div aria-labelledby='dropdownMenuButton'>
          <Menu>
            <Menu.Item key='1'>
              <Link
                className='dropdown-item'
                // onClick={() => handleStatus(row.id, 'delete')}
                to={{ pathname: '/driver-detail', search: `?id=${row.id}` }}
              >
                {t('common.view')}
              </Link>
            </Menu.Item>
            <Menu.Item key='0'>
              <Link
                className='dropdown-item'
                onClick={() => {
                  // handleEditStatus(row, 'edit')
                }}
                to={{ pathname: '/add-driver', search: `?id=${row.id}` }}
              >
                {t('common.edit')}
              </Link>
            </Menu.Item>
          </Menu>
        </div>
      </Menu>
    )
    return (
      <Dropdown overlay={menu} trigger={['click']}>
        <div className='dropdown mx-auto'>
          <Link
            to='#'
            onClick={(e) => e.preventDefault()}
            className='dropdown-toggle'
            id={`dropdownMenuButton_${row.id}`}
            data-toggle='dropdown'
            aria-haspopup='true'
            aria-expanded='false'
          >
            <span className='icon-more_vert'></span>
          </Link>
        </div>
      </Dropdown>
    )
  }

  const [columns] = useState([
    {
      text: t('drivers.id'),
      dataField: 'id'
    },
    {
      text: t('drivers.fullName'),
      dataField: 'first_name',
      formatter: customerNameFormatter
    },
    {
      text: t('drivers.number'),
      dataField: 'phone_number',
      formatter: phoneNumberFormatter
    },
    {
      text: t('drivers.email'),
      dataField: 'email',
      formatter: emailFormatter
    },
    {
      text: t('drivers.dateRegistered'),
      dataField: 'created_at',
      formatter: showDateOnlyInBrowser
    },
    {
      text: t('drivers.category'),
      dataField: 'category.name'
    },
    {
      text: t('drivers.availability'),
      dataField: 'is_available',
      formatter: availabilityFormatter
    },
    {
      text: t('drivers.location_status'),
      dataField: 'socket_status',
      formatter: locationStatusFormatter
    },
    {
      text: t('drivers.status'),
      dataField: 'status',
      formatter: (cell, row) => statusFormatter(cell, row, onOpenStatus)
    },
    {
      dataField: 'isDummyAction',
      text: t('common.action'),
      formatter: actionFormatter
    }
  ])
  const onConfirmation = async () => {
    if (visible) {
      let status = ''
      if (changeData.val) {
        status = 'active'
        // setNewState(status)
      } else {
        status = 'inactive'
        // setNewState(status)
      }

      const res = await driverStatusUpdateService(changeData.row.id, status)

      if (res.success) {
        openHideModal()
        changeData.resHandleChange(status)
        setChangeData({})
        modalNotification({
          type: 'success',
          message: 'Success',
          description: res.message || enValidationMsg.statusUpdate
        })
      }
    }
  }
  const onOpenStatus = async (val, row, resHandleChange) => {
    try {
      if (row.status === 'active') {
        setNewStatus('inactive')
      } else if (row.status === 'inactive') {
        setNewStatus('active')
      }
      setVisible(true)
      setChangeData({ val: val, row: row, resHandleChange: resHandleChange })
    } catch {}
  }

  const fetchOnHandleTableChange = (queryParams) => {
    if (isFirstTimeFetching) {
      if (location) {
        const query = location.search
        const res = getPageSizeFromURL(query, location)
        if (res) {
          // reFetchOnUrlBasis(query)
        } else {
          fetchActiveDriverDetails()
        }
      }
    } else {
      fetchActiveDriverDetails()
    }
  }
  const openHideModal = () => {
    let status = ''
    if (changeData.val) {
      status = 'inactive'
    } else {
      status = 'active'
    }
    changeData.resHandleChange(status)

    setVisible(false)
  }
  const handleTableChange = (
    type,
    { page, sizePerPage, filters, sortField, sortOrder, cellEdit }
  ) => {
    setPage(page)
    setSizePerPage(sizePerPage)
    setIsLoading(true)
    setData([])
    setTotalSize(0)
  }
  const fetchActiveDriverDetails = async (
    queryParams = {
      offset: (page - 1) * sizePerPage,
      limit: sizePerPage
    }
  ) => {
    try {
      queryParams = {
        ...queryParams,
        ...filterData
      }

      const res = await getDriverListService({ queryParams })
      // console.log(res)
      setData(res.data.rows)
      setTotalSize(res.data.rows.length > 0 ? res.data.total : 0)
      setIsFirstTimeFetching(false)
      setIsLoading(false)
    } catch {}
  }

  const onChangeSwitch = () => {
    if (!showMap) {
      setShowMap(true)
      document
        .getElementsByClassName('width-full')[0]
        .classList.remove('col-md-12')
    } else {
      setShowMap(false)
      document
        .getElementsByClassName('width-full')[0]
        .classList.add('col-md-12')
    }
  }
  return (
    <div>
      <div className='tab-pane fade show active' id='drivTab' role='tabpanel'>
        <div className='text-right mapSwitch'>
          {t('drivers.showMap')}
          <label className='commoncheck-wrap'>
            <Switch onChange={onChangeSwitch} />
            {/* <span className="slider round"></span> */}
          </label>
        </div>
      </div>
      <div className='row'>
        <div className='col-md-6 width-full col-md-12'>
          <RemoteDataTable
            columns={columns}
            data={data}
            totalSize={totalSize}
            page={page}
            sizePerPage={sizePerPage}
            loading={isLoading}
            onTableChange={handleTableChange}
            // defaultSorted={defaultSorted}
          />
        </div>
        {showMap && (
          <div className='col-md-6 news'>
            <div className=''>
              <div id='amMap'>
                <MapMarker driverData={data}></MapMarker>
              </div>
            </div>
          </div>
        )}
      </div>
      <ConfirmationAndInfo
        show={visible}
        onHide={() => openHideModal()}
        title={'Confirmation Box'}
        message={`Are you sure you want to ${newStatus} ?`}
        textOnConfirmBtn={'Confirm'}
        textOnCancelBtn={'Cancel'}
        showLoading={false}
        onConfirmation={onConfirmation}
      />
    </div>
  )
}

export default RegisteredDriverList
