import React, { useEffect, useState } from 'react'
import { useHistory, useLocation } from 'react-router-dom'
import { useTranslation } from 'react-i18next'
import {
  getPageSizeFromURL,
  addPageSizeInURL,
  showDateOnlyInBrowser,
  commonRideStatusFormatter,
  commonStatusFormatter,
  timeExtracter
} from '../../utilities/common'
import RemoteDataTable from '../../components/dataTable'
import { getCompletedRideListService } from '../../services/report/index'

export default function CompleteRideListing({ isFinish, filterData }) {
  const { t } = useTranslation()
  const [data, setData] = useState('')
  const [sizePerPage, setSizePerPage] = useState(10)
  const [page, setPage] = useState(1)
  const [totalSize, setTotalSize] = useState(0)
  const [isLoading, setIsLoading] = useState(true)
  const [isFirstTimeFetching, setIsFirstTimeFetching] = useState(true)
  const history = useHistory()
  const location = useLocation()
  useEffect(() => {
    fetchCompleteRideDetail()
  }, []) // eslint-disable-line react-hooks/exhaustive-deps

  useEffect(() => {
    const queryParams = {
      offset: (page - 1) * sizePerPage,
      limit: sizePerPage
    }
    if (!isFirstTimeFetching) {
      addPageSizeInURL(page, sizePerPage, history)
    }
    fetchOnHandleTableChange(queryParams)
  }, [page]) // eslint-disable-line react-hooks/exhaustive-deps

  useEffect(() => {
    formResponse()
  }, [isFinish]) // eslint-disable-line react-hooks/exhaustive-deps
  useEffect(() => {
    fetchCompleteRideDetail()
  }, [filterData]) // eslint-disable-line react-hooks/exhaustive-deps

  const formResponse = () => {
    setPage(1)
    setSizePerPage(sizePerPage)
    setIsLoading(true)
    setData([])
    setTotalSize(0)
    // setFilterData(filterData)
  }

  const [columns] = useState([
    {
      dataField: 'booking_id',
      text: t('reports.rideId')
    },
    {
      dataField: 'ride_number',
      text: t('reports.rideNo')
    },
    {
      dataField: 'rider_name',
      text: t('reports.rider')
    },
    {
      dataField: 'rider_email',
      text: t('reports.riderEmail')
    },
    {
      dataField: 'rider_id',
      text: t('riders.id')
    },
    {
      dataField: 'booking_date',
      text: t('reports.bookingDate'),
      formatter: showDateOnlyInBrowser
    },
    {
      dataField: 'booking_date_dummy',
      text: t('reports.bookingTime'),
      formatter: (cell, row) => timeExtracter(row.booking_date)
    },
    {
      dataField: 'driver_name',
      text: t('reports.driverName')
    },
    {
      dataField: 'driver_razorpay_account_id',
      text: t('reports.driverRazorpayAcc')
    },
    {
      dataField: 'driver_id',
      text: t('drivers.id')
    },
    {
      dataField: 'fare_paid',
      text: t('reports.rideFare')
    },
    {
      dataField: 'toll_taxes',
      text: t('reports.tollTax')
    },
    {
      dataField: 'ride_status',
      text: t('reports.rideStatus'),
      formatter: commonRideStatusFormatter
    },
    {
      dataField: 'payment_status',
      text: t('reports.paymentStatus'),
      formatter: commonStatusFormatter
    },
    {
      dataField: 'payment_id',
      text: t('reports.paymentId')
    },
    {
      dataField: 'payment_mode',
      text: t('reports.paymentMode')
    },
    {
      dataField: 'ride_date',
      text: t('reports.rideDate'),
      formatter: showDateOnlyInBrowser
    },
    {
      dataField: 'ride_date_dummy',
      text: t('reports.rideTime'),
      formatter: (cell, row) => timeExtracter(row.ride_date)
    },
    {
      dataField: 'from',
      text: t('reports.from')
    },
    {
      dataField: 'to',
      text: t('reports.to')
    },
    {
      dataField: 'actual_time',
      text: t('reports.timeTaken')
    },
    {
      dataField: 'distance',
      text: t('reports.kms')
    }
  ])
  const fetchOnHandleTableChange = (queryParams) => {
    if (isFirstTimeFetching) {
      if (location) {
        const query = location.search
        const res = getPageSizeFromURL(query, location)
        if (res) {
          // reFetchOnUrlBasis(query)
        } else {
          fetchCompleteRideDetail()
        }
      }
    } else {
      fetchCompleteRideDetail()
    }
  }
  const handleTableChange = (
    type,
    { page, sizePerPage, filters, sortField, sortOrder, cellEdit }
  ) => {
    setPage(page)
    setSizePerPage(sizePerPage)
    setIsLoading(true)
    setData([])
    setTotalSize(0)
  }
  const fetchCompleteRideDetail = async (
    queryParams = {
      offset: (page - 1) * sizePerPage,
      limit: sizePerPage
    }
  ) => {
    try {
      queryParams = {
        ...queryParams,
        ...filterData
      }

      const res = await getCompletedRideListService({ queryParams })
      setData(res.data.rows)
      setTotalSize(res.data.rows.length > 0 ? res.data.total : 0)
      setIsFirstTimeFetching(false)
      setIsLoading(false)
    } catch {}
  }
  return (
    <div>
      <RemoteDataTable
        columns={columns}
        data={data}
        totalSize={totalSize}
        page={page}
        sizePerPage={sizePerPage}
        loading={isLoading}
        onTableChange={handleTableChange}
        // defaultSorted={defaultSorted}
      />
    </div>
  )
}
