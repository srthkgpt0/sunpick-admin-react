import React, { useEffect, useState } from 'react'
import { useHistory, useLocation } from 'react-router-dom'
import { useTranslation } from 'react-i18next'
import {
  getPageSizeFromURL,
  addPageSizeInURL,
  showDateOnlyInBrowser,
  customerNameFormatter,
  phoneNumberFormatter,
  documentStatusFormatter
} from '../../utilities/common'
import RemoteDataTable from '../../components/dataTable'
import { getDriverParticularListService } from '../../services/report'

export default function DriverParticularListing({ isFinish, filterData }) {
  const { t } = useTranslation()
  const [data, setData] = useState('')
  const [sizePerPage, setSizePerPage] = useState(10)
  const [page, setPage] = useState(1)
  const [totalSize, setTotalSize] = useState(0)
  const [isLoading, setIsLoading] = useState(true)
  const [isFirstTimeFetching, setIsFirstTimeFetching] = useState(true)
  const history = useHistory()
  const location = useLocation()
  useEffect(() => {
    fetchDriverParticular()
  }, []) // eslint-disable-line react-hooks/exhaustive-deps

  useEffect(() => {
    const queryParams = {
      offset: (page - 1) * sizePerPage,
      limit: sizePerPage
    }
    if (!isFirstTimeFetching) {
      addPageSizeInURL(page, sizePerPage, history)
    }
    fetchOnHandleTableChange(queryParams)
  }, [page]) // eslint-disable-line react-hooks/exhaustive-deps

  useEffect(() => {
    formResponse()
  }, [isFinish]) // eslint-disable-line react-hooks/exhaustive-deps
  useEffect(() => {
    fetchDriverParticular()
  }, [filterData]) // eslint-disable-line react-hooks/exhaustive-deps

  const formResponse = () => {
    setPage(1)
    setSizePerPage(sizePerPage)
    setIsLoading(true)
    setData([])
    setTotalSize(0)
    // setFilterData(filterData)
  }

  const [columns] = useState([
    {
      dataField: 'id',
      text: t('common.id')
    },
    {
      dataField: 'razorpay_account_id',
      text: t('reports.razorpayAccountId')
    },
    {
      dataField: 'pan_card',
      text: t('reports.panCard')
    },
    {
      dataField: 'first_name',
      text: t('reports.name'),
      formatter: customerNameFormatter
    },
    {
      dataField: 'phone_number',
      text: t('reports.contact'),
      formatter: phoneNumberFormatter
    },
    {
      dataField: 'email',
      text: t('common.emailAddress')
    },
    {
      dataField: 'address',
      text: t('common.address')
    },
    {
      dataField: 'city',
      text: t('common.city')
    },
    {
      dataField: 'state',
      text: t('common.state')
    },
    {
      dataField: 'country',
      text: t('common.country')
    },
    {
      dataField: 'postalCode',
      text: t('reports.postalCode')
    },
    {
      dataField: 'status',
      text: t('common.status'),
      formatter: documentStatusFormatter
    },
    {
      dataField: 'created_at',
      text: t('reports.dateJoin'),
      formatter: showDateOnlyInBrowser
    },

    {
      dataField: 'left_at',
      text: t('reports.dateLeft')
    },
    {
      dataField: 'suspended_at',
      text: t('reports.dateSuspended'),
      formatter: showDateOnlyInBrowser
    }
  ])
  const fetchOnHandleTableChange = (queryParams) => {
    if (isFirstTimeFetching) {
      if (location) {
        const query = location.search
        const res = getPageSizeFromURL(query, location)
        if (res) {
          // reFetchOnUrlBasis(query)
        } else {
          fetchDriverParticular()
        }
      }
    } else {
      fetchDriverParticular()
    }
  }
  const handleTableChange = (
    type,
    { page, sizePerPage, filters, sortField, sortOrder, cellEdit }
  ) => {
    setPage(page)
    setSizePerPage(sizePerPage)
    setIsLoading(true)
    setData([])
    setTotalSize(0)
  }
  const fetchDriverParticular = async (
    queryParams = {
      offset: (page - 1) * sizePerPage,
      limit: sizePerPage
    }
  ) => {
    try {
      queryParams = {
        ...queryParams,
        ...filterData
      }
      const res = await getDriverParticularListService({ queryParams })
      setData(res.data.rows)
      setTotalSize(res.data.rows.length > 0 ? res.data.total : 0)
      setIsFirstTimeFetching(false)
      setIsLoading(false)
    } catch {}
  }
  return (
    <div>
      <RemoteDataTable
        columns={columns}
        data={data}
        totalSize={totalSize}
        page={page}
        sizePerPage={sizePerPage}
        loading={isLoading}
        onTableChange={handleTableChange}
        // defaultSorted={defaultSorted}
      />
    </div>
  )
}
