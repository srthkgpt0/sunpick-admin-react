import React, { useState } from 'react'
import { useTranslation } from 'react-i18next'
import { Link } from 'react-router-dom'
import ManageReportForm from '../../components/forms/report/manageReportForm'
import ApiEndPoints from '../../utilities/apiEndPoints'
import { filterDataObj } from '../../utilities/common'
import ExportCsvPdfComponent from '../../utilities/export-csv-pdf'
import DriverParticularListing from './driverParticularListing'

export default function DriverParticular() {
  const { t } = useTranslation()
  const [showFilter, setShowFilter] = useState(true)
  const [isFinish, setIsFinish] = useState(false)
  const [filterData, setFilterData] = useState({})
  const [fromDate, setFromDate] = useState('')
  const [toDate, setToDate] = useState('')
  const formRef = React.createRef()

  const onFinish = (values) => {
    const { filterData } = filterDataObj(values)
    filterData.from_date = fromDate
    filterData.to_date = toDate
    setIsFinish(true)
    setFilterData(filterData)
  }

  const onFromDateChange = (value, dateString) => {
    setFromDate(dateString)
  }

  const onToDateChange = (value, dateString) => {
    setToDate(dateString)
  }

  const onFinishFailed = () => {
    // console.log('values')
  }
  const onReset = () => {
    setIsFinish(false)
    setFilterData({})

    formRef.current.resetFields()
  }

  return (
    <main className='maincontent setting-page'>
      <section className='page_header'>
        <div className='page_header_overlay'>
          <h2>{t('reports.driverParticulars')}</h2>
        </div>
      </section>
      <section className='page_content'>
        <div className='container-fluid'>
          <div className='row'>
            <div className='col-md-12'>
              <div className='card' id='card_height'>
                <div className='card-header clearfix'>
                  <h3>{t('reports.driverParticulars')}</h3>
                  <div className='action'>
                    <div className='d-inline-block addbtndiv'>
                      <Link
                        className='btn btn-warning'
                        id='btnSearch'
                        onClick={() => setShowFilter((state) => !state)}
                        // onClick={() => filterToggle()}
                        to='#'
                      >
                        <i className='fa fa-search'></i>
                      </Link>
                    </div>
                  </div>
                </div>
                {showFilter && (
                  <div className='filter_section' id='executiveSearch'>
                    <div className='container-fluid'>
                      <ManageReportForm
                        onFinish={onFinish}
                        onFinishFailed={onFinishFailed}
                        onFromDateChange={onFromDateChange}
                        onToDateChange={onToDateChange}
                        formRef={formRef}
                        onReset={onReset}
                      />
                    </div>
                  </div>
                )}
                <div className='card-block' infinitescroll=''>
                  {/* <div className='action-btn'>
                      Export via :
                      <div className='d-inline-block addbtndiv'>
                        <a className='btn btn-warning'>
                          <i
                            aria-hidden='true'
                            className='fa fa-file-excel-o'
                          ></i>
                          CSV
                        </a>
                      </div>
                    </div> */}
                  <ExportCsvPdfComponent
                    path={ApiEndPoints.DRIVER_PARTICULARS_LIST_FILE_DOWNLOAD}
                    filterData={filterData}
                  />
                  <DriverParticularListing
                    isFinish={isFinish}
                    filterData={filterData}
                  />
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
    </main>
  )
}
