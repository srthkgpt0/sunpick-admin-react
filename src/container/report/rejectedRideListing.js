import React, { useEffect, useState } from 'react'
import { useHistory, useLocation } from 'react-router-dom'
import { useTranslation } from 'react-i18next'
import {
  getPageSizeFromURL,
  addPageSizeInURL,
  showDateOnlyInBrowser,
  timeExtracter
} from '../../utilities/common'
import RemoteDataTable from '../../components/dataTable'
import { getRejectedRideListService } from '../../services/report'

export default function RejectedRideListing({ isFinish, filterData }) {
  const { t } = useTranslation()
  const [data, setData] = useState('')
  const [sizePerPage, setSizePerPage] = useState(10)
  const [page, setPage] = useState(1)
  const [totalSize, setTotalSize] = useState(0)
  const [isLoading, setIsLoading] = useState(true)
  const [isFirstTimeFetching, setIsFirstTimeFetching] = useState(true)
  const history = useHistory()
  const location = useLocation()
  useEffect(() => {
    fetchRejectedRide()
  }, []) // eslint-disable-line react-hooks/exhaustive-deps

  useEffect(() => {
    const queryParams = {
      offset: (page - 1) * sizePerPage,
      limit: sizePerPage
    }
    if (!isFirstTimeFetching) {
      addPageSizeInURL(page, sizePerPage, history)
    }
    fetchOnHandleTableChange(queryParams)
  }, [page]) // eslint-disable-line react-hooks/exhaustive-deps

  useEffect(() => {
    formResponse()
  }, [isFinish]) // eslint-disable-line react-hooks/exhaustive-deps
  useEffect(() => {
    fetchRejectedRide()
  }, [filterData]) // eslint-disable-line react-hooks/exhaustive-deps

  const formResponse = () => {
    setPage(1)
    setSizePerPage(sizePerPage)
    setIsLoading(true)
    setData([])
    setTotalSize(0)
    // setFilterData(filterData)
  }

  // const defaultSorted = [
  //   {
  //     dataField: 'id',
  //     order: 'desc'
  //   }
  // ]
  const [columns] = useState([
    {
      dataField: 'booking_id',
      text: t('reports.rideId')
    },
    {
      dataField: 'ride_number',
      text: t('reports.rideNo')
    },
    {
      dataField: 'ride_type',
      text: t('reports.rideType')
    },
    {
      dataField: 'booking_date',
      text: t('reports.bookingDate'),
      formatter: showDateOnlyInBrowser
    },
    {
      dataField: 'booking_date_dummy',
      text: t('reports.bookingTime'),
      formatter: (cell, row) => timeExtracter(row.booking_date)
    },
    {
      dataField: 'payment_mode',
      text: t('reports.paymentMode')
    },
    {
      dataField: 'from',
      text: t('reports.from')
    },
    {
      dataField: 'to',
      text: t('reports.to')
    },
    {
      dataField: 'estimate_time',
      text: t('reports.estimatedTime')
    },
    {
      dataField: 'distance',
      text: t('reports.estimatedKm')
    },
    {
      dataField: 'rider_id',
      text: t('reports.ridesId')
    },
    {
      dataField: 'rider_name',
      text: t('reports.riderName')
    },
    {
      dataField: 'driver_id',
      text: t('drivers.id')
    },
    {
      dataField: 'driver_name',
      text: t('reports.driverNames')
    }
  ])
  const fetchOnHandleTableChange = (queryParams) => {
    if (isFirstTimeFetching) {
      if (location) {
        const query = location.search
        const res = getPageSizeFromURL(query, location)
        if (res) {
          // reFetchOnUrlBasis(query)
        } else {
          fetchRejectedRide()
        }
      }
    } else {
      fetchRejectedRide()
    }
  }
  const handleTableChange = (
    type,
    { page, sizePerPage, filters, sortField, sortOrder, cellEdit }
  ) => {
    setPage(page)
    setSizePerPage(sizePerPage)
    setIsLoading(true)
    setData([])
    setTotalSize(0)
  }
  const fetchRejectedRide = async (
    queryParams = {
      offset: (page - 1) * sizePerPage,
      limit: sizePerPage
    }
  ) => {
    try {
      queryParams = {
        ...queryParams,
        ...filterData
      }
      const res = await getRejectedRideListService({ queryParams })
      setData(res.data.rows)
      setTotalSize(res.data.rows.length > 0 ? res.data.total : 0)
      setIsFirstTimeFetching(false)
      setIsLoading(false)
    } catch {}
  }
  return (
    <div>
      <RemoteDataTable
        columns={columns}
        data={data}
        totalSize={totalSize}
        page={page}
        sizePerPage={sizePerPage}
        loading={isLoading}
        onTableChange={handleTableChange}
        // defaultSorted={defaultSorted}
      />
    </div>
  )
}
