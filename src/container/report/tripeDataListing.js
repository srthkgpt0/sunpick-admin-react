import React, { useEffect, useState } from 'react'
import { useHistory, useLocation } from 'react-router-dom'
import { useTranslation } from 'react-i18next'
import {
  getPageSizeFromURL,
  addPageSizeInURL,
  showDateOnlyInBrowser,
  rideStatusFormatter
} from '../../utilities/common'
import RemoteDataTable from '../../components/dataTable'
import { getTripDataListService } from '../../services/report'

export default function TripeDataListing({ isFinish, filterData }) {
  const { t } = useTranslation()
  const [data, setData] = useState('')
  const [sizePerPage, setSizePerPage] = useState(10)
  const [page, setPage] = useState(1)
  const [totalSize, setTotalSize] = useState(0)
  const [isLoading, setIsLoading] = useState(true)
  const [isFirstTimeFetching, setIsFirstTimeFetching] = useState(true)
  const history = useHistory()
  const location = useLocation()
  useEffect(() => {
    fetchTripData()
  }, []) // eslint-disable-line react-hooks/exhaustive-deps

  useEffect(() => {
    const queryParams = {
      offset: (page - 1) * sizePerPage,
      limit: sizePerPage
    }
    if (!isFirstTimeFetching) {
      addPageSizeInURL(page, sizePerPage, history)
    }
    fetchOnHandleTableChange(queryParams)
  }, [page]) // eslint-disable-line react-hooks/exhaustive-deps

  useEffect(() => {
    formResponse()
  }, [isFinish]) // eslint-disable-line react-hooks/exhaustive-deps
  useEffect(() => {
    fetchTripData()
  }, [filterData]) // eslint-disable-line react-hooks/exhaustive-deps

  const formResponse = () => {
    setPage(1)
    setSizePerPage(sizePerPage)
    setIsLoading(true)
    setData([])
    setTotalSize(0)
    // setFilterData(filterData)
  }

  const [columns] = useState([
    {
      dataField: 'date',
      text: t('reports.date'),
      formatter: showDateOnlyInBrowser
    },
    {
      dataField: 'vehicle_id',
      text: t('reports.vehicleId')
    },
    {
      dataField: 'rider_id',
      text: t('reports.riderId')
    },
    {
      dataField: 'rider_name',
      text: t('reports.riderName')
    },
    {
      dataField: 'driver_id',
      text: t('drivers.id')
    },
    {
      dataField: 'driver_name',
      text: t('reports.driverNames')
    },
    {
      dataField: 'ride_id',
      text: t('reports.requestId')
    },
    {
      dataField: 'dt_sent',
      text: t('reports.dtSent'),
      formatter: showDateOnlyInBrowser
    },

    {
      dataField: 'dt_accepted',
      text: t('reports.dtAccepted'),
      formatter: showDateOnlyInBrowser
    },
    {
      dataField: 'dt_cancelled',
      text: t('reports.dtCancelled'),
      formatter: showDateOnlyInBrowser
    },
    {
      dataField: 'cancelled_by',
      text: t('reports.cancelledBy')
    },
    {
      dataField: 'reason',
      text: t('reports.reason')
    },
    {
      dataField: 'pro_nam',
      text: t('reports.category')
    },
    {
      dataField: 'dt_start',
      text: t('reports.dtStart'),
      formatter: showDateOnlyInBrowser
    },
    {
      dataField: 'post_pickup',
      text: t('reports.postPickup')
    },
    {
      dataField: 'add_pickup',
      text: t('reports.addPickup')
    },
    {
      dataField: 'longitude_driver',
      text: t('reports.longitudeDriver')
    },

    {
      dataField: 'latitude_driver',
      text: t('reports.latitudeDriver')
    },

    {
      dataField: 'longitude_pickup',
      text: t('reports.longitudePickup')
    },
    {
      dataField: 'latitude_pickup',
      text: t('reports.latitudePickup')
    },

    {
      dataField: 'dt_end',
      text: t('reports.dtEnd'),
      formatter: showDateOnlyInBrowser
    },
    {
      dataField: 'post_end',
      text: t('reports.postEnd')
    },
    {
      dataField: 'add_end',
      text: t('reports.addEnd')
    },
    {
      dataField: 'longitude_end',
      text: t('reports.longitudeEnd')
    },

    {
      dataField: 'latitude_end',
      text: t('reports.latitudeEnd')
    },
    {
      dataField: 'distance',
      text: t('reports.distance')
    },

    {
      dataField: 'fare',
      text: t('reports.fare')
    },

    {
      dataField: 'discount',
      text: t('reports.discount')
    },

    {
      dataField: 'status',
      text: t('common.status'),
      formatter: rideStatusFormatter
    }
  ])
  const fetchOnHandleTableChange = (queryParams) => {
    if (isFirstTimeFetching) {
      if (location) {
        const query = location.search
        const res = getPageSizeFromURL(query, location)
        if (res) {
          // reFetchOnUrlBasis(query)
        } else {
          fetchTripData()
        }
      }
    } else {
      fetchTripData()
    }
  }
  const handleTableChange = (
    type,
    { page, sizePerPage, filters, sortField, sortOrder, cellEdit }
  ) => {
    setPage(page)
    setSizePerPage(sizePerPage)
    setIsLoading(true)
    setData([])
    setTotalSize(0)
  }
  const fetchTripData = async (
    queryParams = {
      offset: (page - 1) * sizePerPage,
      limit: sizePerPage
    }
  ) => {
    try {
      queryParams = {
        ...queryParams,
        ...filterData
      }
      const res = await getTripDataListService({ queryParams })
      setData(res.data.rows)
      setTotalSize(res.data.rows.length > 0 ? res.data.total : 0)
      setIsFirstTimeFetching(false)
      setIsLoading(false)
    } catch {}
  }
  return (
    <div>
      <RemoteDataTable
        columns={columns}
        data={data}
        totalSize={totalSize}
        page={page}
        sizePerPage={sizePerPage}
        loading={isLoading}
        onTableChange={handleTableChange}
        // defaultSorted={defaultSorted}
      />
    </div>
  )
}
