import React, { useEffect, useState } from 'react'
// import {
//   changeStatusService,
//   getDriverAvailabilityDetails,
//   getDriverLocationAvailabilityDetails
// } from '../../services/driver'
// import logger from '../../utilities/logger'
import { useTranslation } from 'react-i18next'
import { Link, useHistory, useLocation } from 'react-router-dom'
import ConfirmationAndInfo from '../../components/modals/confirmationAndInfo'
import modalNotification from '../../utilities/notifications'
// import ExportCsvPdfComponent from '../../utilities/export-csv-pdf'
import RemoteDataTable from '../../components/dataTable'
import AddEditRidersModal from '../../components/modals/riders/addEditRidersModal'
import {
  addPageSizeInURL,
  customerNameFormatter,
  emailFormatter,
  getPageSizeFromURL,
  OnlyDateFormatter,
  phoneNumberFormatter,
  statusFormatter,
  statusStringFormatter
} from '../../utilities/common'
import { Dropdown, Menu } from 'antd'
import enValidationMsg from '../../utilities/lang/validation-en'
import {
  getRiderListingService,
  updateRiderStatusService
} from '../../services/riders'
// class RidersList extends PureComponent {
//   constructor(props) {
//     super(props)

//     // Sets up our initial state

//     this.state = {
//       data: [],
//       totalSize: 0,
//       page: 1,
//       sizePerPage: 10,
//       mapData: {},
//       formData: '',
//       toData: '',
//       error: false,
//       hasMore: true,
//       isLoading: true,
//       users: [],
//       driverData: [],
//       dataList: [],
//       filterData: this.props.filterData,
//       visible: false,
//       newStatus: 'pending',
//       toggleButton: false,
//       statusIntermediateData: {},
//       editVisible: false,
//       rider: {},

//
//     }
//   }
//   // when props change from parent component

//   componentWillReceiveProps(newProps) {
//     if (newProps.filterData !== this.props.filterData) {
//       let data = newProps.filterData

//       if (newProps.filterData.reset) {
//         this.setState({
//           filterData: newProps.filterData,
//           driverData: [],
//           data: [],
//           dataList: [],
//           hasMore: false,
//           totalSize: 0,
//           sizePerPage: 10,
//           page: 1
//         })
//         const queryParams = {
//           offset: 0,
//           limit: 10,
//           reset: false,
//           filter: 'filter'
//         }
//         this.fetchData(queryParams)
//       } else {
//         data['reset'] = false
//         this.setState({
//           filterData: data,
//           driverData: [],
//           data: [],
//           dataList: [],
//           hasMore: false,
//           totalSize: 0,
//           sizePerPage: 10,
//           page: 1
//         })
//         const queryParams = {
//           offset: 0,
//           limit: 10,
//           reset: false,
//           filter: 'filter'
//         }
//         this.fetchData(queryParams)
//       }
//     }
//   }
//   handleEditStatus = (row, action) => {
//     row && this.setState({ editVisible: true, rider: row })
//   }
//   hideRidersAddEdit = (row = {}) => {
//     this.setState({
//       isLoading: true,
//       data: [],
//       editVisible: false,
//       rider: {}
//     })
//     this.fetchData()
//   }

//   onOpenStatus = async (val, row, resHandleChange) => {
//     try {
//       console.log(row, 'row.status')
//       if (row.status === 'active') {
//         this.setState({ newStatus: 'inactive' })
//       } else if (row.status === 'inactive') {
//         this.setState({ newStatus: 'active' })
//       }
//       this.setState({
//         visible: true,
//         statusIntermediateData: {
//           val: val,
//           row: row,
//           resHandleChange: resHandleChange
//         }
//       })
//     } catch {}
//   }
//   // Loads some driverData on initial load

//   componentDidMount() {
//     this.fetchData()
//   }

//   // to get avaible value and class name
//   getDriverAvailabilityString = (is_available, is_shift_started) => {
//     const data = getDriverAvailabilityDetails(is_available, is_shift_started)
//     return data
//   }

//   actionFormatter = (cell, row) => {
//     const menu = (
//       <Menu>
//         <div aria-labelledby='dropdownMenuButton'>
//           <Menu>
//             <Menu.Item key='0'>
//               <Link className='dropdown-item' to={`/rider/detail/${row.id}`}>
//                 {this.props.t('common.view')}
//               </Link>
//             </Menu.Item>
//             <Menu.Item key='1'>
//               <Link
//                 className='dropdown-item'
//                 to='#'
//                 onClick={() => this.handleEditStatus(row, 'Edit')}
//               >
//                 {this.props.t('common.edit')}
//               </Link>
//             </Menu.Item>
//           </Menu>
//         </div>
//       </Menu>
//     )
//     return (
//       <Dropdown overlay={menu} trigger={['click']}>
//         <div className='dropdown mx-auto'>
//           <a
//             href='/'
//             onClick={(e) => e.preventDefault()}
//             className='dropdown-toggle'
//             id={`dropdownMenuButton_${row.id}`}
//             data-toggle='dropdown'
//             aria-haspopup='true'
//             aria-expanded='false'
//           >
//             <span className='icon-more_vert'></span>
//           </a>
//         </div>
//       </Dropdown>
//     )
//   }

//   // to get Location status value and class name
//   getDriverLocationAvailabilityString = (socket_status) => {
//     const data = getDriverLocationAvailabilityDetails(socket_status)
//     return data
//   }
//   handleTableChange = (
//     type,
//     { page, sizePerPage, filters, sortField, sortOrder, cellEdit }
//   ) => {
//     this.setState(
//       {
//         page: page,
//         sizePerPage: sizePerPage,
//         isLoading: true,
//         data: [],
//         totalSize: 0
//       },
//       () => {
//         if (sortField === 'isDummySno') {
//           sortField = 'id'
//         }

//         const queryParams = {
//           offset: (page - 1) * sizePerPage,
//           limit: sizePerPage,
//           sortBy: sortField,
//           sortType: sortOrder
//         }

//         if (!this.state.isFirstTimeFetching) {
//           addPageSizeInURL(page, sizePerPage, this.props.history)
//         }
//         this.fetchOnHandleTableChange(queryParams)
//       }
//     )
//   }
//   fetchOnHandleTableChange = (queryParams) => {
//     if (this.state.isFirstTimeFetching) {
//       const { location } = this.props

//       if (location) {
//         const query = location.search
//         const res = getPageSizeFromURL(query, location)
//         if (res) {
//           this.reFetchOnUrlBasis(query)
//         } else {
//           this.fetchData(queryParams)
//         }
//       }
//     } else {
//       this.fetchData(queryParams)
//     }
//   }

//   // to hide model
//   openHideModal = () => {
//     this.setState({ visible: false })
//   }

//   // model confirmation
//   onConfirmation = async () => {
//     try {
//       if (this.state.visible) {
//         let status = ''
//         if (this.state.statusIntermediateData.val) {
//           status = 'active'
//         } else {
//           status = 'inactive'
//         }
//         const payload = {
//           ...ApiEndPoints.updateRiderStatus(
//             this.state.statusIntermediateData.row.id
//           ),
//           bodyData: { status }
//         }
//         const res = await APIrequest(payload)

//         if (res.success) {
//           this.openHideModal()
//           const dataTemp = this.state.data
//           const indexData = dataTemp.findIndex(
//             (d) => d.id === this.state.statusIntermediateData.row.id
//           )
//           if (indexData > -1) {
//             dataTemp[indexData].status = status
//           }
//           this.state.statusIntermediateData.resHandleChange(status)

//           this.setState({
//             statusIntermediateData: {},
//             data: dataTemp
//           })
//           modalNotification({
//             type: 'success',
//             message: 'Success',
//             description: res.message || textMessages.statusUpdate
//           })
//         }
//       }
//     } catch (error) {}
//   }

//   // to open and close map sidewar
//   clickMap = () => {
//     if (!this.state.toggleButton) {
//       this.setState({
//         toggleButton: true
//       })
//       document
//         .getElementsByClassName('width-full')[0]
//         .classList.remove('col-md-12')
//     } else {
//       this.setState({
//         toggleButton: false
//       })
//       document
//         .getElementsByClassName('width-full')[0]
//         .classList.add('col-md-12')
//     }
//   }

//   // to fetch driver data
//   fetchData = async (
//     queryParams = {
//       offset: (this.state.page - 1) * this.state.sizePerPage,
//       limit: this.state.sizePerPage,
//       reset: false
//     }
//   ) => {
//     if (!queryParams['reset']) {
//       queryParams = {
//         ...queryParams,
//         ...this.state.filterData
//       }
//     }
//     console.log(queryParams, 'queryParamsqueryParamsqueryParams')

//     try {
//       const payload = {
//         ...ApiEndPoints.getRidersDetails,
//         queryParams
//       }
//       const res = await APIrequest(payload)
//       this.setState({
//         isLoading: true
//       })
//       if (res.data.rows.length !== 0 && this.state.data.length !== 0) {
//         this.setState({
//           dataList: this.state.data.concat(res.data.rows)
//         })
//       } else if (res.data.rows.length !== 0) {
//         this.setState({
//           dataList: res.data.rows
//         })
//       }

//       this.setState({
//         isLoading: false,
//         dataTemp: this.state.dataList,
//         totalSize: res.data.total,
//         hasMore: this.state.users.length < 800,
//         data: this.state.dataList,
//         driverData: []
//       })
//     } catch (error) {
//       logger({ 'error:': error })
//     }
//   }

//   render() {
//     const {
//       hasMore,
//       isLoading,
//       users,
//       data,
//       columns,
//       toggleButton,
//       filterData
//     } = this.state
//     const { t } = this.props

//     return (
//       <>
//         <ExportCsvPdfComponent queryParams={filterData}></ExportCsvPdfComponent>

//         <div className='table-responsive'>
//           <RemoteDataTable
//             columns={this.state.columns}
//             data={this.state.data}
//             totalSize={this.state.totalSize}
//             page={this.state.page}
//             sizePerPage={this.state.sizePerPage}
//             loading={this.state.isLoading}
//             onTableChange={this.handleTableChange}
//           />
//         </div>
//         <AddEditRidersModal
//           show={this.state.editVisible}
//           data={this.state.rider}
//           onHide={this.hideRidersAddEdit}
//         />
//         <ConfirmationAndInfo
//           show={this.state.visible}
//           onHide={() => this.openHideModal()}
//           title={'Confirmation Box'}
//           message={`Are you sure you want to ${this.state.newStatus} ?`}
//           textOnConfirmBtn={'Confirm'}
//           textOnCancelBtn={'Cancel'}
//           showLoading={false}
//           onConfirmation={this.onConfirmation}
//         />
//       </>
//     )
//   }
// }

// export default withRouter(withTranslation()(RidersList))
function NowListing({ isFinish, filterData, setIsComponentLoading }) {
  const { t } = useTranslation()
  const [data, setData] = useState([])
  const [sizePerPage, setSizePerPage] = useState(10)
  const [page, setPage] = useState(1)
  const [totalSize, setTotalSize] = useState(0)
  const [visible, setVisible] = useState(false)
  const [editVisible, setEditVisible] = useState(false)
  const [rider, setRider] = useState({})
  const [newStatus, setNewStatus] = useState('pending')
  const [statusChangeIntermediate, setStatusChangeIntermediate] = useState({})
  const [isLoading, setIsLoading] = useState(true)
  const [isFirstTimeFetching, setIsFirstTimeFetching] = useState(true)
  // const [filterData, setFilterData] = useState({})
  const history = useHistory()
  const location = useLocation()
  useEffect(() => {
    fetchData()
  }, []) // eslint-disable-line react-hooks/exhaustive-deps

  useEffect(() => {
    const queryParams = {
      offset: (page - 1) * sizePerPage,
      limit: sizePerPage
    }
    if (!isFirstTimeFetching) {
      addPageSizeInURL(page, sizePerPage, history)
    }
    fetchOnHandleTableChange(queryParams)
  }, [page]) // eslint-disable-line react-hooks/exhaustive-deps

  useEffect(() => {
    formResponse()
  }, [isFinish]) // eslint-disable-line react-hooks/exhaustive-deps
  useEffect(() => {
    fetchData()
  }, [filterData]) // eslint-disable-line react-hooks/exhaustive-deps

  const formResponse = () => {
    setPage(1)
    setSizePerPage(sizePerPage)
    setIsLoading(true)
    setData([])
    setTotalSize(0)
    // setFilterData(filterData)
  }
  const onOpenStatus = async (val, row, resHandleChange) => {
    try {
      if (row.status === 'active') {
        setNewStatus('inactive')
      } else if (row.status === 'inactive') {
        setNewStatus('active')
      }
      setVisible(true)
      setStatusChangeIntermediate({
        val: val,
        row: row,
        resHandleChange: resHandleChange
      })
    } catch {}
  }
  const handleEditStatus = (row, action) => {
    row ? setRider(row) : setRider({})
    setEditVisible(true)
  }
  // const handleStatus = () => {}
  const actionFormatter = (cell, row) => {
    const menu = (
      <Menu>
        <div aria-labelledby='dropdownMenuButton'>
          <Menu>
            <Menu.Item key='0'>
              <Link className='dropdown-item' to={`/rider/detail/${row.id}`}>
                {t('common.view')}
              </Link>
            </Menu.Item>
            <Menu.Item key='1'>
              <Link
                className='dropdown-item'
                to='#'
                onClick={() => handleEditStatus(row, 'Edit')}
              >
                {t('common.edit')}
              </Link>
            </Menu.Item>
          </Menu>
        </div>
      </Menu>
    )
    return (
      <Dropdown overlay={menu} trigger={['click']}>
        <div className='dropdown mx-auto'>
          <Link
            to='#'
            onClick={(e) => e.preventDefault()}
            className='dropdown-toggle'
            id={`dropdownMenuButton_${row.id}`}
            data-toggle='dropdown'
            aria-haspopup='true'
            aria-expanded='false'
          >
            <span className='icon-more_vert'></span>
          </Link>
        </div>
      </Dropdown>
    )
  }
  const [columns] = useState([
    {
      text: t('riders.id'),
      dataField: 'id'
    },
    {
      text: t('riders.fullName'),
      dataField: 'first_name',
      formatter: customerNameFormatter
    },
    {
      text: t('riders.phoneNumber'),
      dataField: 'phone_number',
      formatter: phoneNumberFormatter
    },
    {
      text: t('riders.email'),
      dataField: 'email',
      formatter: emailFormatter
    },
    {
      text: t('riders.dateRegistered'),
      dataField: 'created_at',
      formatter: OnlyDateFormatter
    },
    {
      text: t('riders.totalRides'),
      dataField: 'total_rides'
    },

    {
      text: t('riders.accountStatus'),
      dataField: 'isDummyStatus',
      formatter: statusStringFormatter
    },
    {
      text: t('common.status'),
      dataField: 'status',
      formatter: (cell, row) => statusFormatter(cell, row, onOpenStatus)
    },
    {
      dataField: t('common.action'),
      text: 'Action',
      formatter: actionFormatter
    }
  ])
  const hideRiderAddEdit = (row = {}) => {
    fetchData()
    setEditVisible(false)
    setRider({})
  }
  const openHideModal = () => {
    let status = ''
    if (statusChangeIntermediate.val) {
      status = 'inactive'
    } else {
      status = 'active'
    }
    statusChangeIntermediate.resHandleChange(status)
    setVisible(false)
  }
  const onConfirmation = async () => {
    try {
      if (visible) {
        let status = ''
        if (statusChangeIntermediate.val) {
          status = 'active'
        } else {
          status = 'inactive'
        }

        const res = await updateRiderStatusService(
          statusChangeIntermediate.row.id,
          status
        )

        if (res.success) {
          openHideModal()
          const dataTemp = data
          const indexData = dataTemp.findIndex(
            (d) => d.id === statusChangeIntermediate.row.id
          )
          if (indexData > -1) {
            dataTemp[indexData].status = status
          }
          statusChangeIntermediate.resHandleChange(status)
          setStatusChangeIntermediate({})
          console.log(dataTemp, 'dataTemp HELOOOOO')
          setData([])
          setIsLoading(true)
          fetchData()
          // setData(dataTemp)
          modalNotification({
            type: 'success',
            message: 'Success',
            description: res.message || enValidationMsg.statusUpdate
          })
        }
      }
    } catch (error) {}
  }
  const fetchOnHandleTableChange = (queryParams) => {
    if (isFirstTimeFetching) {
      if (location) {
        const query = location.search
        const res = getPageSizeFromURL(query, location)
        if (res) {
          // reFetchOnUrlBasis(query)
        } else {
          fetchData()
        }
      }
    } else {
      fetchData()
    }
  }
  const handleTableChange = (
    type,
    { page, sizePerPage, filters, sortField, sortOrder, cellEdit }
  ) => {
    setPage(page)
    setSizePerPage(sizePerPage)
    setIsLoading(true)
    setData([])
    setTotalSize(0)
  }
  const fetchData = async (
    queryParams = {
      offset: (page - 1) * sizePerPage,
      limit: sizePerPage
    }
  ) => {
    try {
      queryParams = {
        ...queryParams,
        ...filterData
      }
      const res = await getRiderListingService(queryParams)
      setData(res.data.rows)
      setTotalSize(res.data.rows.length > 0 ? res.data.total : 0)
      setIsLoading(false)
      setIsComponentLoading(false)
      setIsFirstTimeFetching(false)
    } catch {}
  }

  return (
    <div>
      <RemoteDataTable
        columns={columns}
        data={data}
        totalSize={totalSize}
        page={page}
        sizePerPage={sizePerPage}
        loading={isLoading}
        onTableChange={handleTableChange}
        // defaultSorted={defaultSorted}
      />
      <AddEditRidersModal
        show={editVisible}
        data={rider}
        onHide={() => hideRiderAddEdit()}
      />
      <ConfirmationAndInfo
        show={visible}
        onHide={() => openHideModal()}
        title={'Confirmation Box'}
        message={`Are you sure you want to ${newStatus} ?`}
        textOnConfirmBtn={'Confirm'}
        textOnCancelBtn={'Cancel'}
        showLoading={false}
        onConfirmation={() => onConfirmation()}
      />
    </div>
  )
}

export default NowListing
