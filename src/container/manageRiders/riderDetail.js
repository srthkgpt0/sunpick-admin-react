import React, { useEffect, useState } from 'react'
import { useTranslation } from 'react-i18next'
import { useHistory } from 'react-router'
import { Link } from 'react-router-dom'
import { RatingComponent } from '../../components/common'
import RemoteDataTable from '../../components/dataTable'
import {
  getRiderDetailsService,
  getRiderListingDetailsService
} from '../../services/riders'
import {
  addressFormatter,
  amountFormatter,
  currency_symbol,
  dateFormatter,
  ratingFormatter,
  rideStatusFormatter,
  titleCase
} from '../../utilities/common'

function RiderDetail(props) {
  const id = props.match.params.id
  const { t } = useTranslation()
  const [data, setData] = useState('')
  const [listingData, setListingData] = useState('')
  const [sizePerPage, setSizePerPage] = useState(10)
  const [page, setPage] = useState(1)
  const [totalSize, setTotalSize] = useState(0)
  const [isLoading, setIsLoading] = useState(true)
  // const [isFirstTimeFetching, setIsFirstTimeFetching] = useState(true)
  // const [filterData, setFilterData] = useState({})
  const history = useHistory()
  // const location = useLocation()

  useEffect(() => {
    console.log(id, 'sdfsdfsdf')
    fetchRiderDetails()
    fetchRiderListingDetail()
  }, []) // eslint-disable-line react-hooks/exhaustive-deps
  const fetchRiderListingDetail = async (
    queryParams = {
      offset: (page - 1) * sizePerPage,
      limit: sizePerPage
    }
  ) => {
    try {
      queryParams = {
        ...queryParams
      }
      const res = await getRiderListingDetailsService(id, { queryParams })
     // console.log(res, 'getRiderListingDetails')
      setListingData(res.data.rows)
      setTotalSize(res.data.rows.length > 0 ? res.data.total : 0)
      setIsLoading(false)
    } catch {}
  }
  const fetchRiderDetails = async () => {
    try {
      const res = await getRiderDetailsService(id)
      // console.log(res, 'resresresres')
      setData(res.data)
      setIsLoading(false)
    } catch {}
  }
  const handleTableChange = (
    type,
    { page, sizePerPage, filters, sortField, sortOrder, cellEdit }
  ) => {
    setPage(page)
    setSizePerPage(sizePerPage)
    setIsLoading(true)
    setData([])
    setTotalSize(0)
  }
  const [columns] = useState([
    {
      dataField: 'id',
      text: t('rides.id'),
      sort: true
    },
    {
      dataField: 'booking_date',
      text: t('drivers.rideDateAndTime'),
      formatter: dateFormatter
    },

    {
      dataField: 'isDummyBookingDate',
      text: t('drivers.bookingFromTo'),
      formatter: addressFormatter
    },
    {
      dataField: 'ride_amount',
      text: t('rides.rideFare'),
      formatter: amountFormatter
    },
    {
      dataField: 'customer_rating',
      text: t('common.ratings'),
      formatter: ratingFormatter
    },
    {
      dataField: 'status',
      text: t('rides.rideStatus'),
      formatter: rideStatusFormatter
    },
    {
      dataField: 'isDummyAction',
      text: t('common.action')
      // formatter: actionFormatter
    }
  ])
  return (
    <main className='maincontent view-p'>
      <section className='page_header'>
        <div className='page_header_overlay'>
          <h2>{t('riders.riderDetail')}</h2>
          <Link to='#' className='back-btn' onClick={() => history.goBack()}>
            <i className='ti-hand-point-left' aria-hidden='true'></i>
            {t('common.back')}
          </Link>
        </div>
      </section>
      <section className='page_content'>
        <div className='top-info'>
          <div className='container-fluid'>
            <div className='row'>
              <div className='col-sm-6'>
                <div className='box user-detail'>
                  <h2 className='view-heading'>
                    {t('riders.riderBasicInfo')} :
                  </h2>
                  <div className='row'>
                    <div className='col-5 custom-col col-sm-5'>
                      <div className='left-side'>
                        <Link
                          to='#'
                          className='example-image-link user-img rounded-circle'
                        >
                          <img alt='' src={data.photo} />
                        </Link>

                        <div className='review-text'>
                          <RatingComponent ratingValue={data.avg_rating} />
                          <small>{t('riders.averageRating')}</small>
                        </div>
                      </div>
                    </div>

                    <div className='col-6 custom-col col-sm-7'>
                      <div className='right-side'>
                        <div className='info-set'>
                          <p>{t('common.name')}</p>
                          <span>
                            {data?.first_name || '-'} {data?.last_name || '-'}
                          </span>
                        </div>

                        <div className='info-set'>
                          <p>{t('common.mobileNumber')}</p>
                          <span>
                            {data?.phone_number_country_code}
                            {data?.phone_number || '-'}
                          </span>
                        </div>

                        <div className='info-set'>
                          <p>{t('common.email')}</p>
                          <span>{data?.email || '-'}</span>
                        </div>

                        <div className='info-set'>
                          <p>{t('common.status')}</p>
                          <span>{titleCase(data?.status) || '-'}</span>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>

              <div className='col-sm-6'>
                <div className='box user-detail'>
                  <h2 className='view-heading'>
                    {t('riders.spendingEarning')} :
                  </h2>
                  <div className='row'>
                    <div className='col-6 custom-col col-sm-6'>
                      <div className='info-set'>
                        <p>{t('riders.totalRidesTaken')}</p>
                        <span>{data?.total_rides || '0'}</span>
                      </div>
                    </div>

                    <div className='col-6 custom-col col-sm-6'>
                      <div className='info-set'>
                        <p>{t('riders.totalAmountSpentOnRides')}</p>
                        <span>
                          {currency_symbol}
                          {data?.earning_paid || '0'}
                        </span>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>

        <div className='container-fluid'>
          <div className='view-grid'>
            <div className='tab-view'>
              <ul className='nav nav-tabs mb-20' role='tablist'>
                <li className='nav-item'>
                  <a
                    className='nav-link active'
                    id='forridesEarning'
                    data-toggle='tab'
                    href='#ridesEarning'
                    role='tab'
                    aria-controls='ridesEarning'
                    aria-selected='true'
                  >
                    Rides
                  </a>
                </li>
              </ul>
            </div>

            <div className='tab-content'>
              <RemoteDataTable
                columns={columns}
                data={listingData}
                totalSize={totalSize}
                page={page}
                sizePerPage={sizePerPage}
                loading={isLoading}
                onTableChange={handleTableChange}
              />
            </div>
          </div>
        </div>
      </section>
    </main>
  )
}

export default RiderDetail
