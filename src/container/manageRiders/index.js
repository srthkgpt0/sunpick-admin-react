// import React, { PureComponent, Fragment } from 'react'
// import { connect } from 'react-redux'
// import PropTypes from 'prop-types'
// import { Link, withRouter } from 'react-router-dom'

// import RidersList from './riders-list'
// import ManageRiderForm from '../../components/forms/manageRiders'
// import { withTranslation } from 'react-i18next'

// class ManageRiders extends PureComponent {
//   constructor(props) {
//     super(props)
//     // Sets up our initial state

//     this.state = {
//       filterData: {},
//       page: 1,
//       sizePerPage: 10,
//       mapData: {},
//       formData: '',
//       toData: '',
//       error: false,
//       taxiCategoryList: [],
//       driverAvailability: [],
//       activeTabValue: 'Approved',
//       tabActive: true,
//       show: true
//     }
//   }
//   onFinish = (values) => {
//     values['from_date'] = this.state.formData
//     values['to_date'] = this.state.toData

//     this.setState({
//       filterData: values
//     })
//   }

//   onFromDateChange = (value, dateString) => {
//     this.setState({ formData: dateString })
//   }
//   onToDateChange = (value, dateString) => {
//     this.setState({ toData: dateString })
//   }

//   onFinishFailed = (errorInfo) => {}

//   onReset = () => {
//     this.formRef.current.resetFields()
//     let data = {}

//     this.setState({
//       filterData: data
//     })
//   }
//   formRef = React.createRef()

//   // update status
//   filterOpen = (event, show) => {
//     if (show === true) {
//       this.setState({
//         show: false
//       })
//     } else {
//       this.setState({
//         show: true
//       })
//     }

//     event.preventDefault()
//   }

//   render() {
//     const { filterData, show } = this.state
//     const { t } = this.props
//     return (
//       <>
//         <main className='maincontent setting-page'>
//           <section className='page_header'>
//             <div className='page_header_overlay'>
//               <h2>{t('riders.manageRider')}</h2>
//             </div>
//           </section>
//           <section className='page_content'>
//             <div className='container-fluid'>
//               <div className='row'>
//                 <div className='col-md-12'>
//                   <div className='card' id='card_height'>
//                     <div className='card-header clearfix'>
//                       <h3>{t('riders.manageRider')}</h3>
//                       <div className='action'>
//                         <div className='d-inline-block addbtndiv'>
//                           <Link
//                             className='btn btn-warning'
//                             id='btnSearch'
//                             to='/'
//                             onClick={(e) => this.filterOpen(e, show)}
//                           >
//                             <i className='fa fa-search'></i>
//                           </Link>
//                         </div>
//                       </div>
//                     </div>
//                     {show && (
//                       <div className='filter_section' id='driverSearch'>
//                         <div className='container-fluid'>
//                           <ManageRiderForm
//                             onFinish={this.onFinish}
//                             onFinishFailed={this.onFinishFailed}
//                             onFromDateChange={this.onFromDateChange}
//                             onToDateChange={this.onToDateChange}
//                             formRef={this.formRef}
//                             onReset={this.onReset}
//                           />
//                         </div>
//                       </div>
//                     )}

//                     <div className='card-block'>
//                       <RidersList filterData={filterData}></RidersList>
//                     </div>
//                   </div>
//                 </div>
//               </div>
//             </div>
//           </section>
//         </main>
//       </>
//     )
//   }
// }

// const mapStateToProps = (state) => {
//   return {
//     userData: state.auth.userData,
//     selectedVendorId: state.common.selectedVendor
//   }
// }

// const mapDispatchToProps = () => {
//   return {}
// }

// ManageRiders.propTypes = {
//   userData: PropTypes.object.isRequired
// }

// export default withTranslation()(
//   connect(mapStateToProps, mapDispatchToProps)(ManageRiders)
// )
import React, { useState } from 'react'
import { Link } from 'react-router-dom'
import { useTranslation } from 'react-i18next'
import ManageRiderForm from '../../components/forms/riders/manageRidersFilterForm'
import RidersList from './ridersList'

// import ManageRidesForm from '../../components/forms/manageRidesform'
import { filterDataObj } from '../../utilities/common'
import GlobalLoader from '../../components/subComponent/globalLoader'
import ExportCsvPdfComponent from '../../utilities/export-csv-pdf'
import ApiEndPoints from '../../utilities/apiEndPoints'
import { useSelector } from 'react-redux'
import { selectUserData } from '../../redux/auth/authSlice'

function ManageRiders() {
  const { t } = useTranslation()
  const [showFilter, setShowFilter] = useState(true)
  // const [tabActive, setTabActive] = useState(true)
  const [isFinish, setIsFinish] = useState(false)
  const [filterData, setFilterData] = useState({})
  const [fromDate, setFromDate] = useState('')
  const [toDate, setToDate] = useState('')
  const [isLoading, setIsLoading] = useState(false)

  const { userType } = useSelector(selectUserData)

  const formRef = React.createRef()
  const handleFilterToggle = () => {
    setShowFilter((state) => !state)
  }
  const onFinish = (values) => {
    values['from_date'] = fromDate
    values['to_date'] = toDate
    if (values['status']) {
      values.status = values.status.value
    }

    const { filterData } = filterDataObj(values)
    // console.log(filterData, 'filterData')
    setIsFinish(true)
    setFilterData(filterData)
  }
  const onFinishFailed = () => {
    // console.log('values')
  }
  const onFromDateChange = (value, dateString) => {
    setFromDate(dateString)
  }
  const onToDateChange = (value, dateString) => {
    setToDate(dateString)
  }
  const onReset = () => {
    setIsFinish(false)
    setFilterData({})

    formRef.current.resetFields()
  }
  return isLoading ? (
    <GlobalLoader />
  ) : (
    <>
      <main className='maincontent setting-page'>
        <section className='page_header'>
          <div className='page_header_overlay'>
            <h2>{t('riders.manageRider')}</h2>
          </div>
        </section>

        <section className='page_content'>
          <div className='container-fluid'>
            <div className='row'>
              <div className='col-md-12'>
                <div className='card' id='card_height'>
                  <div className='card-header clearfix'>
                    <h3>{t('riders.manageRider')}</h3>
                    <div className='action'>
                      <div className='d-inline-block addbtndiv'>
                        <Link
                          className='btn btn-warning'
                          id='btnSearch'
                          to='#'
                          onClick={(e) => handleFilterToggle()}
                        >
                          <i className='fa fa-search'></i>
                        </Link>
                      </div>
                    </div>
                  </div>
                  {showFilter && (
                    <div className='filter_section' id='driverSearch'>
                      <div className='container-fluid'>
                        <ManageRiderForm
                          onFinish={onFinish}
                          onFinishFailed={onFinishFailed}
                          onReset={onReset}
                          formRef={formRef}
                          onFromDateChange={onFromDateChange}
                          onToDateChange={onToDateChange}
                          // onFinish={onFinish}
                          // onFinishFailed={onFinishFailed}

                          // formRef={formRef}
                          // onReset={onReset}
                        />
                      </div>
                    </div>
                  )}
                  <div className='card-block'>
                    {userType === 'admin' && (
                      <ExportCsvPdfComponent
                        filterData={filterData}
                        path={ApiEndPoints.RIDERS_LIST_FILE_DOWNLOAD}
                      />
                    )}
                    <RidersList
                      isFinish={isFinish}
                      filterData={filterData}
                      setIsComponentLoading={setIsLoading}
                    ></RidersList>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>
      </main>
    </>
  )
}

export default ManageRiders
