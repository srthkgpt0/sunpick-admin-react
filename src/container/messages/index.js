import React, { useState } from 'react'
import { Link } from 'react-router-dom'
import { useTranslation } from 'react-i18next'
import DriverTab from './driverMessageTab'
import CustomerTab from './customerTab'

export default function Message() {
  const { t } = useTranslation()
  const [tabActive, setTabActive] = useState(true)

  const onTabClick = (clickedTab) => {
    if (clickedTab === 'driver') {
      setTabActive(true)
    }
    if (clickedTab === 'customer') {
      setTabActive(false)
    }
  }
  return (
    <main className='maincontent setting-page'>
      <section className='page_header'>
        <div className='page_header_overlay'>
          <h2>{t('notification.notification')}</h2>
        </div>
      </section>
      <section className='page_content'>
        <div className='container-fluid'>
          <div className='row'>
            <div className='col-md-12'>
              <div className='card' id='card_height'>
                <div className='card-header clearfix'>
                  <h3>{t('notification.notification')}</h3>
                  <div className='action'></div>
                </div>
                <div className='card-block'>
                  <ul className='nav nav-tabs mb-20' id='myTab' role='tablist'>
                    <li className='nav-item'>
                      <Link
                        className={`nav-link ${tabActive ? 'active' : ''}`}
                        role='tab'
                        aria-controls='home'
                        to='#'
                        aria-selected='true'
                        onClick={() => {
                          onTabClick('driver')
                        }}
                      >
                        DRIVER
                      </Link>
                    </li>
                    <li className='nav-item'>
                      <Link
                        className={`nav-link ${tabActive ? '' : 'active'}`}
                        data-toggle='tab'
                        to='#'
                        aria-controls='profile'
                        aria-selected='false'
                        onClick={() => {
                          onTabClick('customer')
                        }}
                      >
                        CUSTOMER
                      </Link>
                    </li>
                  </ul>
                  <div className='tab-content'>
                    {tabActive && <DriverTab />}
                    {!tabActive && <CustomerTab />}
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
    </main>
  )
}
