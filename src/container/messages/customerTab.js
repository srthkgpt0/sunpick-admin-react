import MessageForm from '../../components/forms/message'
import modalNotification from '../../utilities/notifications'
import { sendCustomerMessage } from '../../services/message/index'
import { useTranslation } from 'react-i18next'
import logger from '../../utilities/logger'

export default function DriverTab() {
  const { t } = useTranslation()
  const statusOptions = [
    {
      id: 1,
      label: t('common.active'),
      value: 'active'
    },
    {
      id: 2,
      label: t('common.inactive'),
      value: 'inactive'
    }
  ]
  const onFinish = async (values) => {
    try {
      values.is_all = true
      values.customers = null

      const res = await sendCustomerMessage({ bodyData: values })
      if (res && res.success) {
        modalNotification({
          type: 'success',
          message: 'Success',
          description: res.message
        })
      }
    } catch (error) {
      logger({ 'error:': error })
    }
  }

  const onFinishFailed = (value) => {
    // console.log('something wrong ', value);
  }
  return (
    <div>
      <MessageForm
        onFinish={onFinish}
        onFinishFailed={onFinishFailed}
        statusOptions={statusOptions}
      ></MessageForm>
    </div>
  )
}
