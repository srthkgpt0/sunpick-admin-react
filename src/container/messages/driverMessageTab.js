import { useTranslation } from 'react-i18next'
import MessageForm from '../../components/forms/message/index'
import { sendDriverMessage } from '../../services/message'
import logger from '../../utilities/logger'
import modalNotification from '../../utilities/notifications'
import { Form } from 'antd'

export default function DriverTab() {
  const { t } = useTranslation()
  const [form] = Form.useForm()
  const statusOptions = [
    {
      id: 1,
      label: t('common.active'),
      value: 'active'
    },
    {
      id: 2,
      label: t('common.pending'),
      value: 'pending'
    }
  ]
  const onFinish = async (values) => {
    try {
      values.is_all = true
      values.drivers = null
      const res = await sendDriverMessage({ bodyData: values })
      if (res && res.success) {
        modalNotification({
          type: 'success',
          message: 'Success',
          description: res.message
        })
      }
    } catch (error) {
      logger({ 'error:': error })
    }
  }

  const onFinishFailed = (value) => {
    // console.log('something wrong ', value)
  }
  return (
    <div>
      <MessageForm
        onFinish={onFinish}
        form={form}
        onFinishFailed={onFinishFailed}
        statusOptions={statusOptions}
      ></MessageForm>
    </div>
  )
}
