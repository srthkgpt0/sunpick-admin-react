import React, { useEffect } from 'react'
// import { useDispatch } from 'react-redux'
import { Link } from 'react-router-dom'
import { Form, Input, Button } from 'antd'
import validation from '../../utilities/validation'
import modalNotification from '../../utilities/notifications'
import logger from '../../utilities/logger'
import { forgetPassService } from '../../services/auth'
import { MailOutlined } from '@ant-design/icons'

export default function ForgetPassword() {
  // const dispatch = useDispatch()
  useEffect(() => {
    document.getElementsByTagName('body')[0].classList.add('loginpage')
    // document.getElementsByTagName("body")[0].classList.add("loginpage");
  }, [])
  const onFinish = async (values) => {
    try {
      let res = await forgetPassService(values)
      if (res && res.success) {
        // formRef.current.resetFields();
        modalNotification({
          type: 'success',
          message: 'Success',
          description: res.message
        })
      }
    } catch (error) {
      console.log('error', error)
      logger(error)
    }
  }

  const onFinishFailed = (errorInfo) => {
    // console.log('Failed:', errorInfo);
  }

  // const formRef = React.createRef();

  return (
    <main>
      <div className='loginwrap'>
        <Form name='basic' onFinish={onFinish} onFinishFailed={onFinishFailed}>
          <div className='content'>
            <div className='l_header'>
              <img
                src='/assets/images/logo-login.png'
                className='img-fluid'
                alt='logo'
              />
            </div>
            <div className='l_field'>
              <div className='form-group'>
                <div className='input-group'>
                  <span className='input-group-addon'>
                    <i className='material-icons'>mail_outline</i>
                  </span>
                  {/* <input className="form-control ng-untouched ng-pristine ng-invalid" formcontrolname="email" placeholder="Email" type="text"></input> */}

                  <Form.Item name='email' rules={validation.login.email}>
                    <Input
                      prefix={<MailOutlined className='site-form-item-icon' />}
                      placeholder='email'
                    />
                  </Form.Item>
                </div>
              </div>
              <div className='text-right forgot'>
                <Link to='/'>
                  {' '}
                  <i className='fa fa-long-arrow-left'></i> Back to Login{' '}
                </Link>
              </div>
            </div>
            <div className='l_footer'>
              <Form.Item name='button'>
                <Button className='btn-block loginbtn' htmlType='submit'>
                  SEND
                </Button>
              </Form.Item>
            </div>
          </div>
        </Form>
        );
      </div>
    </main>
  )
}
