import React, { useEffect, useState } from 'react'
import { useDispatch } from 'react-redux'
import { loginAction } from '../../redux/auth/authSlice'
import { Link } from 'react-router-dom'
import { Form, Input, Button } from 'antd'
import validation from '../../utilities/validation'
import { authListService } from '../../services/auth'
import { setSessionStorageToken } from '../../utilities/common'
import modalNotification from '../../utilities/notifications'
import { LoadingSpinner } from '../../components/common'

export default function Login() {
  const [isSpin, setIsSpin] = useState(false)
  const dispatch = useDispatch()
  useEffect(() => {
    document.getElementsByTagName('body')[0].classList.add('loginpage')
    // document.getElementsByTagName("body")[0].classList.add("loginpage");
  }, [])
  // const onFinish = (values) => {
  //   dispatch(
  //     onlogin({
  //       userDetails: {
  //         email: values.email,
  //         password: values.password
  //       }
  //     })
  //   )
  // }
  const onFinish = async (values) => {
    setIsSpin(true)
    const userDetail = {
      email: values.email,
      password: values.password
    }
    const res = await authListService(userDetail)

    if (res.success) {
      setSessionStorageToken(res.data.user_token.access_token)
      modalNotification({
        type: 'success',
        message: 'Login',
        description: res.message
      })
      setIsSpin(false)
      dispatch(
        loginAction({
          userId: res.data.id,
          userName: `${res.data ? res.data.first_name : '-'} ${
            res.data && res.data.last_name != null ? res.data.last_name : ''
          }`,
          userType: res.data.user_type,
          status: res.data.success,
          isActive: res.data.can_user_login,
          photo: res.data.photo
        })
      )
    }
  }

  const onFinishFailed = (errorInfo) => {
    // console.log('Failed:', errorInfo);
  }
  return (
    <main>
      <div className='loginwrap'>
        <Form name='basic' onFinish={onFinish} onFinishFailed={onFinishFailed}>
          <div className='content'>
            <div className='l_header'>
              <img
                src='/assets/images/logo-login.png'
                className='img-fluid'
                alt='logo'
              />
            </div>
            <div className='l_field'>
              <div className='form-group'>
                <div className='input-group'>
                  <span className='input-group-addon'>
                    <i className='ti-email icon'></i>
                  </span>
                  <Form.Item name='email' rules={validation.login.email}>
                    <Input placeholder='USER NAME' />
                  </Form.Item>
                </div>
              </div>
              <div className='form-group'>
                <div className='input-group'>
                  <span className='input-group-addon'>
                    <i className='ti-lock icon'></i>
                  </span>
                  <Form.Item name='password' rules={validation.login.password}>
                    <Input placeholder='PASSWORD' type='password' />
                  </Form.Item>
                </div>
              </div>
              <div className='text-right forgot'>
                {/* <a [routerLink]="['/forget-password']"> Forgot Password</a> */}
                <Link to='/forgot-password'>
                  {' '}
                  <i className='ti-lock'></i> Forgot Password{' '}
                </Link>
              </div>
            </div>
            <div className='l_footer'>
              <Form.Item name='button'>
                <Button className='loginbtn btn-block' htmlType='submit'>
                  {isSpin ? <LoadingSpinner /> : 'LOGIN'}
                </Button>
              </Form.Item>
            </div>
          </div>
        </Form>
        );
      </div>
    </main>
  )
}
