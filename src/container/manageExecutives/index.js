import { Menu, Dropdown } from 'antd'
import React, { useState, useEffect } from 'react'
import { useTranslation } from 'react-i18next'
import { Link, useHistory, useLocation } from 'react-router-dom'
import RemoteDataTable from '../../components/dataTable'
import {
  getExecutiveListService,
  deleteUpdateExecutiveStatusService
} from '../../services/executives'
import AddEditExecutiveModal from '../../components/modals/excutive/addEditExecutiveModal'
import ChangeExecutiveModal from '../../components/modals/excutive/changeExecutiveModal'
import ExecutivesFilterForm from '../../components/forms/excutive/executivesFilterForm'
import modalNotification from '../../utilities/notifications'
import logger from '../../utilities/logger'
import ConfirmationAndInfo from '../../components/modals/confirmationAndInfo'
import {
  phoneNumberFormatter,
  statusStringFormatter,
  getPageSizeFromURL,
  addPageSizeInURL,
  filterDataObj,
  nameWithImageFormatter
} from '../../utilities/common'
import ExportCsvPdfComponent from '../../utilities/export-csv-pdf'
import ApiEndPoints from '../../utilities/apiEndPoints'

function ManageExecutives() {
  //////////////STATES////////////////////
  const { t } = useTranslation()
  const [showFilter, setShowFilter] = useState(true)
  const [data, setData] = useState('')
  const [executive, setExecutive] = useState({})
  const [sizePerPage, setSizePerPage] = useState(10)
  const [page, setPage] = useState(1)
  const [totalSize, setTotalSize] = useState(0)
  const [isLoading, setIsLoading] = useState(true)
  const [isFirstTimeFetching, setIsFirstTimeFetching] = useState(true)
  const [editVisible, setEditVisible] = useState(false)
  const [passwordVisible, setPasswordVisible] = useState(false)
  const [confirmVisible, setConfirmVisible] = useState(false)
  const [filterData, setFilterData] = useState({})
  const [newStatus, setNewStatus] = useState({})
  const [executiveId, setExecutiveId] = useState({})
  const history = useHistory()
  const location = useLocation()

  useEffect(() => {
    fetchExecutiveDetails()
  }, []) // eslint-disable-line react-hooks/exhaustive-deps
  useEffect(() => {
    const queryParams = {
      offset: (page - 1) * sizePerPage,
      limit: sizePerPage
      // sortBy: sortField,
    }

    if (!isFirstTimeFetching) {
      addPageSizeInURL(page, sizePerPage, history)
    }
    fetchOnHandleTableChange(queryParams)
  }, [page]) // eslint-disable-line react-hooks/exhaustive-deps
  useEffect(() => {
    fetchExecutiveDetails()
  }, [filterData]) // eslint-disable-line react-hooks/exhaustive-deps

  const hideExecutiveAddEdit = (row = {}) => {
    fetchExecutiveDetails()
    setEditVisible(false)
    setExecutive({})
  }
  const hideChangePassword = (row = {}) => {
    setPasswordVisible(false)
    setExecutive({})
  }
  const hideConfirmStatus = (row = {}) => {
    setConfirmVisible(false)
    setExecutive({})
  }

  const handleEditStatus = (row, action) => {
    row ? setExecutive(row) : setExecutive({})
    setEditVisible(true)
  }
  const handleCahngePassword = (row) => {
    setExecutive(row)
    setPasswordVisible(true)
  }
  const handleConfirmStatus = (id, currentStatus) => {
    setExecutiveId(id)
    setConfirmVisible(true)
    // console.log('currentStatus', currentStatus, 'id', id)
    if (currentStatus === 'active') {
      setNewStatus('inactive')
    } else if (currentStatus === 'inactive') {
      setNewStatus('active')
    } else {
      setNewStatus('delete')
    }
  }

  const onConfirmation = async () => {
    try {
      const body = { status: newStatus }

      const res = await deleteUpdateExecutiveStatusService(executiveId, body)
      if (res.success) {
        modalNotification({
          type: 'success',
          message: 'Success',
          description: res.message
        })
      }

      fetchExecutiveDetails()
      // this.fetchData()
      return true
    } catch (error) {
      logger({ 'error:': error })
    }
  }

  const actionFormatter = (cell, row) => {
    const menu = (
      <Menu>
        <div aria-labelledby='dropdownMenuButton'>
          <Menu>
            <Menu.Item key='0'>
              <Link
                className='dropdown-item'
                onClick={() => {
                  handleEditStatus(row, 'edit')
                }}
                to='#'
              >
                {t('common.edit')}{' '}
              </Link>
            </Menu.Item>
            <Menu.Item key='1'>
              <Link
                className='dropdown-item'
                onClick={() => handleConfirmStatus(row.id, 'delete')}
                to='#'
              >
                {t('common.delete')}
              </Link>
            </Menu.Item>
            <Menu.Item key='2'>
              <Link
                className='dropdown-item'
                onClick={() => handleConfirmStatus(row.id, row.status)}
                to='#'
              >
                {row.status === 'active'
                  ? t('common.inactive')
                  : t('common.active')}
              </Link>
            </Menu.Item>
            <Menu.Item key='3'>
              <Link
                className='dropdown-item'
                onClick={() => {
                  handleCahngePassword(row)
                }}
                to='#'
              >
                {t('common.changePassword')}
              </Link>
            </Menu.Item>
          </Menu>
        </div>
      </Menu>
    )
    return (
      <Dropdown overlay={menu} trigger={['click']}>
        <div className='dropdown mx-auto'>
          <Link
            to='#'
            onClick={(e) => e.preventDefault()}
            className='dropdown-toggle'
            id={`dropdownMenuButton_${row.id}`}
            data-toggle='dropdown'
            aria-haspopup='true'
            aria-expanded='false'
          >
            <span className='icon-more_vert'></span>
          </Link>
        </div>
      </Dropdown>
    )
  }
  const [columns] = useState([
    {
      dataField: 'id',
      text: t('common.id'),
      sort: true,
      hidden: true
    },
    {
      dataField: 'first_name',
      text: t('common.name'),
      formatter: nameWithImageFormatter
    },

    {
      dataField: 'phoneNumber',
      text: t('common.phoneNumber'),
      headerAlign: 'left',
      align: 'left',
      sort: true,
      formatter: phoneNumberFormatter
    },
    {
      dataField: 'email',
      text: t('common.email')
    },
    {
      dataField: '',
      text: t('executives.numberOfRequestAssigned')
    },
    {
      dataField: 'manual_bookings',
      text: t('executives.numberOfManualBooking')
    },
    {
      dataField: 'status',
      text: t('common.status'),
      formatter: statusStringFormatter
    },
    {
      dataField: 'isDummyAction',
      text: t('common.action'),
      formatter: actionFormatter
    }
  ])
  // const style = {
  //   display: 'initial'
  // }
  /////////////////////FUNCTIONS//////////////////////////
  const filterToggle = () => {
    setShowFilter((state) => !state)
  }
  const onFinish = (values) => {
    const { filterData } = filterDataObj(values)
    setPage(1)
    setSizePerPage(sizePerPage)
    setIsLoading(true)
    setData([])
    setTotalSize(0)
    setFilterData(filterData)
  }
  const onFinishFailed = () => {}

  const onReset = () => {
    setPage(1)
    setSizePerPage(sizePerPage)
    setIsLoading(true)
    setData([])
    setTotalSize(0)
    setFilterData({})
  }

  const fetchExecutiveDetails = async (
    queryParams = {
      offset: (page - 1) * sizePerPage,
      limit: sizePerPage
    }
  ) => {
    try {
      queryParams = {
        ...queryParams,
        ...filterData
      }
      const res = await getExecutiveListService({ queryParams })
      setData(res.data.rows)
      setTotalSize(res.data.rows.length > 0 ? res.data.total : 0)
      setIsFirstTimeFetching(false)
      setIsLoading(false)
    } catch {}
  }

  /**
   * To handle the DataTable on change
   */
  const handleTableChange = (
    type,
    { page, sizePerPage, filters, sortField, sortOrder, cellEdit }
  ) => {
    setPage(page)
    setSizePerPage(sizePerPage)
    setIsLoading(true)
    setData([])
    setTotalSize(0)
  }

  const fetchOnHandleTableChange = (queryParams) => {
    if (isFirstTimeFetching) {
      if (location) {
        const query = location.search
        const res = getPageSizeFromURL(query, location)
        if (res) {
          reFetchOnUrlBasis(query)
        } else {
          fetchExecutiveDetails()
        }
      }
    } else {
      fetchExecutiveDetails()
    }
  }

  const reFetchOnUrlBasis = (query) => {
    const res = getPageSizeFromURL(query, location)
    if (res) {
      // const {
      //   data: { page, sizePerPage },
      //   queryParams
      // } = res
      setPage(page)
      setSizePerPage(sizePerPage)
      setIsLoading(true)
      setData([])
      setTotalSize(0)
      // fetchExecutiveDetails(queryParams)
    }
  }
  return (
    <>
      <main className='maincontent setting-page'>
        <section className='page_header'>
          <div className='page_header_overlay'>
            <h2>{t('executives.manageTitle')}</h2>
          </div>
        </section>
        <section className='page_content'>
          <div className='container-fluid'>
            <div className='row'>
              <div className='col-md-12'>
                <div className='card' id='card_height'>
                  <div className='card-header clearfix'>
                    <h3>{t('executives.manageTitle')}</h3>
                    <div className='action'>
                      <div className='d-inline-block addbtndiv'>
                        <Link
                          className='btn btn-warning'
                          id='btnSearch'
                          onClick={() => filterToggle()}
                          to='#'
                        >
                          <i className='fa fa-search'></i>
                        </Link>
                        <Link
                          className='btn btn-warning'
                          to='#'
                          onClick={() => {
                            handleEditStatus()
                          }}
                        >
                          {t('executives.add')}
                        </Link>
                      </div>
                    </div>
                  </div>
                  {showFilter && (
                    <div className='filter_section' id='executiveSearch'>
                      <div className='container-fluid'>
                        <ExecutivesFilterForm
                          onFinish={onFinish}
                          onFinishFailed={onFinishFailed}
                          onReset={onReset}
                        ></ExecutivesFilterForm>
                      </div>
                    </div>
                  )}
                  <div className='card-block' infinitescroll=''>
                    <ExportCsvPdfComponent
                      filterData={filterData}
                      path={ApiEndPoints.EXECUTIVE_LIST_FILE_DOWNLOAD}
                    />

                    <RemoteDataTable
                      columns={columns}
                      data={data}
                      totalSize={totalSize}
                      page={page}
                      sizePerPage={sizePerPage}
                      loading={isLoading}
                      onTableChange={handleTableChange}
                      // defaultSorted={defaultSorted}
                    />
                  </div>
                  <AddEditExecutiveModal
                    show={editVisible}
                    data={executive}
                    onHide={() => hideExecutiveAddEdit()}
                  />
                  <ChangeExecutiveModal
                    show={passwordVisible}
                    onHide={() => hideChangePassword()}
                    data={executive}
                  />
                  <ConfirmationAndInfo
                    show={confirmVisible}
                    onHide={() => hideConfirmStatus()}
                    title={'Confirmation Box'}
                    message={`Are you sure you want to ${newStatus} ?`}
                    textOnConfirmBtn={'Confirm'}
                    textOnCancelBtn={'Cancel'}
                    showLoading={false}
                    onConfirmation={onConfirmation}
                  />
                  {/* <EditExecutiveModal editVisible = {editVisible} handleOk={handleOk} handleCancel={handleCancel} executive={executive} ></EditExecutiveModal> */}
                </div>
              </div>
            </div>
          </div>
        </section>
      </main>
    </>
  )
}

export default ManageExecutives
