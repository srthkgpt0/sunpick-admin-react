import React, { useState } from 'react'
import { useTranslation } from 'react-i18next'
import { Link } from 'react-router-dom'
import ManagePromoCodeForm from '../../components/forms/promoCode/managePromoCode'

import { filterDataObj } from '../../utilities/common'
import PromoCodeListing from './promoCodeListing'

export default function ManagePromoCode() {
  const { t } = useTranslation()
  const [showFilter, setShowFilter] = useState(true)
  const [isFinish, setIsFinish] = useState(false)
  const [filterData, setFilterData] = useState({})
  const [fromDate, setFromDate] = useState('')
  const [toDate, setToDate] = useState('')
  const formRef = React.createRef()

  const onFinish = (values) => {
    const { filterData } = filterDataObj(values)
    filterData.start_date = fromDate
    filterData.end_date = toDate
    setIsFinish(true)
    setFilterData(filterData)
  }

  const onFromDateChange = (value, dateString) => {
    setFromDate(dateString)
  }

  const onToDateChange = (value, dateString) => {
    setToDate(dateString)
  }

  const onFinishFailed = () => {
    // console.log('values')
  }
  const onReset = () => {
    setIsFinish(false)
    setFilterData({})

    formRef.current.resetFields()
  }

  return (
    <main className='maincontent setting-page'>
      <section className='page_header'>
        <div className='page_header_overlay'>
          <h2>{t('promoCode.promoSystem')}</h2>
        </div>
      </section>
      <section className='page_content'>
        <div className='container-fluid'>
          <div className='row'>
            <div className='col-md-12'>
              <div className='card' id='card_height'>
                <div className='card-header clearfix'>
                  <h3>{t('promoCode.title')}</h3>
                  <div className='action'>
                    <div className='d-inline-block addbtndiv'>
                      <Link
                        className='btn btn-warning'
                        id='btnSearch'
                        onClick={() => setShowFilter((state) => !state)}
                        // onClick={() => filterToggle()}
                        to='#'
                      >
                        <i className='fa fa-search'></i>
                      </Link>
                      <Link
                        className='btn btn-warning'
                        to={{ pathname: '/add-promo-code' }}
                      >
                        {t('promoCode.add')}
                      </Link>
                    </div>
                  </div>
                </div>
                {showFilter && (
                  <div className='filter_section' id='executiveSearch'>
                    <div className='container-fluid'>
                      <ManagePromoCodeForm
                        onFinish={onFinish}
                        onFinishFailed={onFinishFailed}
                        onFromDateChange={onFromDateChange}
                        onToDateChange={onToDateChange}
                        onReset={onReset}
                        formRef={formRef}
                      />
                    </div>
                  </div>
                )}
                <div className='card-block' infinitescroll=''>
                  <PromoCodeListing
                    isFinish={isFinish}
                    filterData={filterData}
                  />
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
    </main>
  )
}
