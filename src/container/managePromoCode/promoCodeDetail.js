import React, { useEffect, useState } from 'react'
import { promoCodeDetailService } from '../../services/promocode';
import logger from '../../utilities/logger'
import { connect } from 'react-redux';
import { Link } from 'react-router-dom'

import { showDateMonthlyFormat } from '../../utilities/common';

const PromoCodeDetails = (props) => {
    const id = props.match.params.id
    useEffect(() => {
    
        fetchPromoCodeDetail(id)
      });
  const [promoCodeDetail, setPromoCodeDetail] = useState('');
  
  const fetchPromoCodeDetail = async (id) => {
    try {
      const res = await promoCodeDetailService(id);
      if (res && res.success) {
       setPromoCodeDetail(res.data);
        }
    } catch (error) {
      logger({ 'error:': error })
    }
  }
   

  return (
    <main className="maincontent view-p">
  <section className="page_header">
    <div className="page_header_overlay">
      <h2>PROMO DETAIL</h2>
      <Link className="back-btn" to="/promo-code"><i className="ti-hand-point-left" aria-hidden="true"></i>BACK </Link>
    </div>
  </section>
  <section className="page_content">
    <div className="top-info">
      <div className="container-fluid">
        <div className="row">
             
          <div className="col-sm-12 col-lg-4">
            <div className="box user-detail m-t-15">
              <h2 className="view-heading">PROMO CODE :</h2>
              <div className="row">
                <div className="col-6 custom-col">
                  <div className="info-set">
                    <p>PROMO CODE TITLE</p>
                    <span>{promoCodeDetail?promoCodeDetail.title:''}
                    </span>
                  </div>

                  <div className="info-set">
                    <p>DISCOUNT TYPE</p>
                    <span>{promoCodeDetail? promoCodeDetail.discount_type:''}</span>
                  </div>

                  <div className="info-set">
                    <p>DISCOUNT</p>
                    <span>{promoCodeDetail? promoCodeDetail.discount:''}</span>
                  </div>

                  <div className="info-set">
                    <p>Date Range </p>
                    <span>{promoCodeDetail? showDateMonthlyFormat(promoCodeDetail.start_date) :''}</span>
                    <span>{promoCodeDetail?showDateMonthlyFormat(promoCodeDetail.end_date) :''}</span>
                  </div>
             
                </div>

                <div className="col-6 custom-col">
                  <div className="info-set">
                    <p>PROMO CODE</p>
                    <span>{promoCodeDetail? promoCodeDetail.code : '' }</span>
                  </div>

                  <div className="info-set">
                    <p>USED COUNT</p>
                    <span>{promoCodeDetail?promoCodeDetail.used_count:''}</span>
                  </div>

              
                  <div className="info-set">
                    <p>MAX DISCOUNT</p>
                    <span>{promoCodeDetail?promoCodeDetail.max_discount :''}</span>
                  </div>
                  <div className="info-set">
                    <p>LIMIT PER USER</p>
                    <span>{promoCodeDetail?promoCodeDetail.limit_per_user :''}</span>
                  </div>
                </div>
              </div>
              <div className="row">
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

    <div className="container-fluid">
      <div className="view-grid">
        <div className="box">
          <h2 className='view-heading'>TERMS & CONDITIONS</h2>
            <ul className="nav-item">
               <li>
               {promoCodeDetail?promoCodeDetail.term_condition:''}

               </li>
            </ul>
           <div>
           
           </div>
        </div>
      </div>
    </div>
  </section>
</main>
  )
}


const mapStateToProps = (state) => {
  return {
    userData: state.auth.userData,
    selectedVendorId: state.common.selectedVendor
  }
}

const mapDispatchToProps = () => {
  return {

  }
}
// PromoCodeDetails.propTypes = {
//   userData: PropTypes.object.isRequired
// }
export default connect(mapStateToProps, mapDispatchToProps)(PromoCodeDetails)
