import { Form } from 'antd'
import React, { useEffect, useState } from 'react'
import {
  promoCodeDetailService,
  updatePromoCode
} from '../../services/promocode'
import logger from '../../utilities/logger'
import { connect } from 'react-redux'
import { Link } from 'react-router-dom'
import AddEditPromoCodeForm from '../../components/forms/promoCode/addeditPromoCode'
// import { showDateMonthlyFormat } from '../../utilities/common';
import modalNotification from '../../utilities/notifications'
// import { notification_msg } from '../../utilities/textMessages';
import queryString from 'query-string'
import PropTypes from 'prop-types'
// import { GlobalLoader } from '../../components/common';
import { filterDataObj } from '../../utilities/common'
import { useTranslation } from 'react-i18next'
import moment from 'moment'

const AddEditPromoCode = (props) => {
  const [form] = Form.useForm()
  const { t } = useTranslation()
  // const [isLoading, setIsLoading] = useState(true)
  const { id } = queryString.parse(props.location.search)
  const [submit, setSubmit] = useState('submit')
  const history = props.history

  useEffect(() => {
    if (id) {
      fetchPromoCodeDetail(id)
    } else {
      form.setFieldsValue({
        code_type: '',
        code: '',
        title: '',
        discount_type: '',
        discount: '',
        max_discount: '',
        limit_per_user: '',
        usage_limit: '',
        code_rule: '',
        term_condition: '',
        status: '',
        used_count: '',
        start_date: '',
        end_date: ''
      })
    }
  }, []) // eslint-disable-line react-hooks/exhaustive-deps
  // const [promoCodeDetail, setPromoCodeDetail] = useState('');

  const fetchPromoCodeDetail = async (id) => {
    try {
      const res = await promoCodeDetailService(id)
      if (res && res.success) {
        setSubmit('update')
        // setIsLoading(false);
        const data = res.data
        form.setFieldsValue({
          code_type: data.code_type,
          code: data.code,
          title: data.title,
          discount_type: data.discount_type,
          discount: data.discount,
          max_discount: data.max_discount,
          limit_per_user: data.limit_per_user,
          usage_limit: data.usage_limit,
          code_rule: data.code_rule,
          term_condition: data.term_condition,
          status: data.status,
          used_count: data.used_count,
          start_date: moment(data.start_date).format('YYYY-MM-DD'),
          end_date: moment(data.end_date).format('YYYY-MM-DD')
        })
      }
    } catch (error) {
      logger({ 'error:': error })
    }
  }
  const onFinish = async (values) => {
    try {
      const { filterData } = filterDataObj(values)
      const res = await updatePromoCode(filterData, id)
      if (res && res.success) {
        modalNotification({
          type: 'success',
          message: 'Success',
          description: res.message
        })
        history.push('/promo-code')
      }
    } catch (error) {
      // setIsAlert(true)
      // setErrorMsg(error.message)
      logger({ 'error:': error })
    }
  }

  const onFinishFailed = (errorInfo) => {
    logger({ 'Failed:': errorInfo })
  }

  const onFromDateChange = (value, dateString) => {
    form.setFieldsValue({
      start_date: dateString
    })
  }
  const onToDateChange = (value, dateString) => {
    form.setFieldsValue({
      end_date: dateString
    })
  }
  return (
    <main className='maincontent form_box'>
      <section className='page_header'>
        <div className='page_header_overlay'>
          <h2>
            {id
              ? `${t('promoCode.updatePromo')}`
              : `${t('promoCode.addPromo')}`}
          </h2>
          <Link className='back-btn' to='/promo-code'>
            <i className='ti-hand-point-left' aria-hidden='true'></i>BACK{' '}
          </Link>
        </div>
      </section>
      <section className='page_content'>
        <div className='container-fluid'>
          <div className='card' id='card_height'>
            <div className='card-header clearfix'>
              <h3>
                {id
                  ? `${t('promoCode.editFirstTimeRider')}`
                  : `${t('promoCode.addFirstTimeRider')}`}
              </h3>
            </div>
            <div className='card-block'>
              <div>
                <div className='row'>
                  <div className='col-sm-12'>
                    <AddEditPromoCodeForm
                      form={form}
                      onFinish={onFinish}
                      onFinishFailed={onFinishFailed}
                      onFromDateChange={onFromDateChange}
                      onToDateChange={onToDateChange}
                      submitButtonText={submit}
                      id ={id}
                    />
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
    </main>
  )
}

const mapStateToProps = (state) => {
  return {
    userData: state.auth.userData,
    selectedVendorId: state.common.selectedVendor
  }
}

const mapDispatchToProps = () => {
  return {}
}
AddEditPromoCode.propTypes = {
  userData: PropTypes.object.isRequired
}
export default connect(mapStateToProps, mapDispatchToProps)(AddEditPromoCode)
