import { Menu, Dropdown } from 'antd'

import React, { useEffect, useState } from 'react'
import { Link, useHistory, useLocation } from 'react-router-dom'
import { useTranslation } from 'react-i18next'
import {
  getPageSizeFromURL,
  addPageSizeInURL,
  showDateOnlyInBrowser,
  statusFormatter,
  underScoreFormatter
} from '../../utilities/common'
import ApiEndPoints from '../../utilities/apiEndPoints'
import APIrequest from '../../services/apiRequest'
import RemoteDataTable from '../../components/dataTable'
import ConfirmationAndInfo from '../../components/modals/confirmationAndInfo'
import {
  deletePromoCode,
  promoCodeChangeStatusService
} from '../../services/promocode'
import modalNotification from '../../utilities/notifications'
import enValidationMsg from '../../utilities/lang/validation-en'

function PromoCodeListing({ isFinish, filterData }) {
  const [visible, setVisible] = useState(false)

  const { t } = useTranslation()
  const [data, setData] = useState('')
  const [sizePerPage, setSizePerPage] = useState(10)
  const [page, setPage] = useState(1)
  const [totalSize, setTotalSize] = useState(0)
  const [isLoading, setIsLoading] = useState(true)
  const [isFirstTimeFetching, setIsFirstTimeFetching] = useState(true)
  const [newStatus, setNewStatus] = useState('pending')
  const [statusChangeIntermediate, setStatusChangeIntermediate] = useState({})
  const [vehicle, setVehicle] = useState({})

  const history = useHistory()
  const location = useLocation()
  useEffect(() => {
    fetchPromoDetail()
  }, []) // eslint-disable-line react-hooks/exhaustive-deps

  useEffect(() => {
    const queryParams = {
      offset: (page - 1) * sizePerPage,
      limit: sizePerPage
    }
    if (!isFirstTimeFetching) {
      addPageSizeInURL(page, sizePerPage, history)
    }
    fetchOnHandleTableChange(queryParams)
  }, [page]) // eslint-disable-line react-hooks/exhaustive-deps

  useEffect(() => {
    formResponse()
  }, [isFinish]) // eslint-disable-line react-hooks/exhaustive-deps
  useEffect(() => {
    fetchPromoDetail()
  }, [filterData]) // eslint-disable-line react-hooks/exhaustive-deps

  const formResponse = () => {
    setPage(1)
    setSizePerPage(sizePerPage)
    setIsLoading(true)
    setData([])
    setTotalSize(0)
  }

  const openHideModal = () => {
    let status = ''
    if (statusChangeIntermediate.val) {
      status = 'inactive'
    } else {
      status = 'active'
    }
    statusChangeIntermediate.resHandleChange(status)
    setVisible(false)
  }
  const onOpenStatus = async (val, row, resHandleChange) => {
    try {
      if (row.status === 'active') {
        setNewStatus('inactive')
      } else if (row.status === 'inactive') {
        setNewStatus('active')
      }
      setVisible(true)
      setStatusChangeIntermediate({
        val: val,
        row: row,
        resHandleChange: resHandleChange
      })
    } catch {}
  }
  const onConfirmation = async () => {
    try {
      let res = null
      if (newStatus === 'delete') {
        res = await deletePromoCode(vehicle.id)
        if (res.success) {
          modalNotification({
            type: 'success',
            message: 'Success',
            description: res.message || enValidationMsg.statusUpdate
          })
          setData([])
          setIsLoading(true)
          fetchPromoDetail()
          setVisible(false)
        }
      } else {
        if (visible) {
          let status = ''
          if (statusChangeIntermediate.val) {
            status = 'active'
          } else {
            status = 'inactive'
          }
          let statusObject = {
            status
          }
          res = await promoCodeChangeStatusService(
            statusChangeIntermediate.row.id,
            { bodyData: statusObject }
          )
          if (res.success) {
            openHideModal()
            const dataTemp = data
            const indexData = dataTemp.findIndex(
              (d) => d.id === statusChangeIntermediate.row.id
            )
            if (indexData > -1) {
              dataTemp[indexData].status = status
            }
            statusChangeIntermediate.resHandleChange(status)
            setStatusChangeIntermediate({})
            // setData([])
            setIsLoading(true)
            // fetchPromoDetail()
            // setData(dataTemp)
            modalNotification({
              type: 'success',
              message: 'Success',
              description: res.message || enValidationMsg.statusUpdate
            })
          }
        }
      }
    } catch (error) {}
  }
  const handleAction = (action, row = {}) => {
    row ? setVehicle(row) : setVehicle({})
    setNewStatus('delete')
    setVisible(true)
  }
  const actionFormatter = (cell, row) => {
    const menu = (
      <Menu>
        <div aria-labelledby='dropdownMenuButton'>
          <Menu>
            <Menu.Item key='0'>
              <Link
                className='dropdown-item'
                to={`/promo-code/detail/${row.id}`}
              >
                {t('common.view')}
              </Link>
            </Menu.Item>
            <Menu.Item key='1'>
              {/* to={{ pathname: '/driver-detail', search: `?id=${row.id}` }} */}
              <Link
                className='dropdown-item'
                to={{ pathname: '/add-promo-code', search: `?id=${row.id}` }}
              >
                {t('common.edit')}
              </Link>
            </Menu.Item>
            <Menu.Item key='2'>
              {/* to={{ pathname: '/driver-detail', search: `?id=${row.id}` }} */}
              <Link
                to='#'
                className='dropdown-item'
                onClick={() => handleAction('delete', row)}
              >
                {t('common.delete')}
              </Link>
            </Menu.Item>
          </Menu>
        </div>
      </Menu>
    )
    return (
      <Dropdown overlay={menu} trigger={['click']}>
        <div className='dropdown mx-auto'>
          <Link
            to='#'
            onClick={(e) => e.preventDefault()}
            className='dropdown-toggle'
            id={`dropdownMenuButton_${row.id}`}
            data-toggle='dropdown'
            aria-haspopup='true'
            aria-expanded='false'
          >
            <span className='icon-more_vert'></span>
          </Link>
        </div>
      </Dropdown>
    )
  }

  const [columns] = useState([
    {
      dataField: 'id',
      text: t('promoCode.s.No'),
      sort: true,
      hidden: false
    },
    {
      dataField: 'title',
      text: t('promoCode.dataTitle')
    },
    {
      dataField: 'code',
      text: t('promoCode.promoCode')
    },
    {
      dataField: 'discount_type',
      text: t('promoCode.discountType'),
      formatter: underScoreFormatter
    },
    {
      dataField: 'discount',
      text: t('promoCode.discount')
    },
    {
      dataField: 'used_count',
      text: t('promoCode.usedCount')
    },
    {
      dataField: 'limit_per_user',
      text: t('promoCode.limitPerUser')
    },
    {
      dataField: 'start_date',
      text: t('promoCode.startDate'),
      formatter: showDateOnlyInBrowser
    },

    {
      dataField: 'end_date',
      text: t('promoCode.endDate'),
      formatter: showDateOnlyInBrowser
    },

    {
      dataField: 'status',
      text: t('common.status'),
      formatter: (cell, row) => statusFormatter(cell, row, onOpenStatus)
    },
    {
      dataField: 'isDummyAction',
      text: t('common.action'),
      formatter: actionFormatter
    }
  ])
  const fetchOnHandleTableChange = (queryParams) => {
    if (isFirstTimeFetching) {
      if (location) {
        const query = location.search
        const res = getPageSizeFromURL(query, location)
        if (res) {
          // reFetchOnUrlBasis(query)
        } else {
          fetchPromoDetail()
        }
      }
    } else {
      fetchPromoDetail()
    }
  }
  const handleTableChange = (
    type,
    { page, sizePerPage, filters, sortField, sortOrder, cellEdit }
  ) => {
    setPage(page)
    setSizePerPage(sizePerPage)
    setIsLoading(true)
    setData([])
    setTotalSize(0)
  }
  const fetchPromoDetail = async (
    queryParams = {
      offset: (page - 1) * sizePerPage,
      limit: sizePerPage
    }
  ) => {
    try {
      queryParams = {
        ...queryParams,
        ...filterData
      }
      const payload = {
        ...ApiEndPoints.getPromoCodeList,
        queryParams
      }
      const res = await APIrequest(payload)
      setData(res.data.rows)
      setTotalSize(res.data.rows.length > 0 ? res.data.total : 0)
      setIsFirstTimeFetching(false)
      setIsLoading(false)
    } catch {}
  }
  return (
    <div>
      <RemoteDataTable
        columns={columns}
        data={data}
        totalSize={totalSize}
        page={page}
        sizePerPage={sizePerPage}
        loading={isLoading}
        onTableChange={handleTableChange}
        // defaultSorted={defaultSorted}
      />
      <ConfirmationAndInfo
        show={visible}
        onHide={() => openHideModal()}
        title={'Confirmation Box'}
        message={`Are you sure you want to ${newStatus} ?`}
        textOnConfirmBtn={'Confirm'}
        textOnCancelBtn={'Cancel'}
        showLoading={false}
        onConfirmation={() => onConfirmation()}
      />
    </div>
  )
}

export default PromoCodeListing
