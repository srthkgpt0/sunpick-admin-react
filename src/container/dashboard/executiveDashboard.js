import React from 'react'
import { useTranslation } from 'react-i18next'
import PropTypes from 'prop-types'

export default function ExecutiveDashboard({ countData }) {
  const { t } = useTranslation()
  const style = {
    display: 'initial'
  }
  return (
    <>
      <div className='row'>
        <div className='col-sm-6 col-lg-4'>
          <div className='info-box box01'>
            <h3>{t('dashboard.rideAssign')}</h3>
            <h2>
              <i className='fa fa-road icon' style={style}></i>
              <span className='float-right'>
                {countData.requested_ride_count
                  ? countData.requested_ride_count
                  : 0}
              </span>
            </h2>
          </div>
        </div>

        <div className='col-sm-6 col-lg-4'>
          <div className='info-box box01'>
            <h3>{t('dashboard.rideGenerated')}</h3>
            <h2>
              <i className='fa fa-map-marker' style={style}></i>
              <span className='float-right'>
                {countData.completed_ride_count
                  ? countData.completed_ride_count
                  : 0}
              </span>
            </h2>
          </div>
        </div>

        <div className='col-sm-6 col-lg-4'>
          <div className='info-box box08'>
            <h3>{t('dashboard.avaiableDrivers')}</h3>
            <h2>
              <i className='ti-user'></i>
              <span className='float-right'>
                {' '}
                {countData.available_driver_count
                  ? countData.available_driver_count
                  : 0}
              </span>
            </h2>
          </div>
        </div>
      </div>
    </>
  )
}

ExecutiveDashboard.propTypes = {
  countData: PropTypes.any.isRequired
}
