import React, { PureComponent } from 'react'
import { connect } from 'react-redux'
import PropTypes from 'prop-types'
import {
  dashboardCountService,
  dashboardMapData,
  getUserGraph
} from '../../services/dashboard'
import logger from '../../utilities/logger'
import MapMarker from '../../components/defaultMap'
import { withTranslation } from 'react-i18next'
import ApexDashedGraph from '../../components/graph/apexDashedGraph'
import AdminDashboard from './adminDashboard'
import ExecutiveDashboard from './executiveDashboard'
class Dashboard extends PureComponent {
  constructor(props) {
    super(props)
    this.state = {
      activeDriverData: [],
      countData: [],
      userGraphData: []
    }
  }

  componentDidMount() {
    document.getElementsByTagName('body')[0].classList.remove('loginpage')
    this.fetchDashCountData()
    this.fetchDashMapData()
    this.fetchDashGraphData()
  }

  async fetchDashCountData() {
    try {
      const res = await dashboardCountService({})
      if (res && res.success) {
        this.setState({ countData: res.data })
      }
    } catch (error) {
      logger({ 'error:': error })
    }
  }
  async fetchDashMapData() {
    try {
      const res = await dashboardMapData({})
      if (res && res.success) {
        this.setState({ activeDriverData: res.data })
      }
    } catch (error) {
      logger({ 'error:': error })
    }
  }

  async fetchDashGraphData() {
    try {
      const res = await getUserGraph({})
      if (res && res.success) {
        this.setState({ userGraphData: res.data })
      }
    } catch (error) {
      logger({ 'error:': error })
    }
  }

  render() {
    const { countData, activeDriverData, userGraphData } = this.state
    const { t, userData } = this.props
    return (
      <>
        <main className='maincontent dashboard_page'>
          <section className='page_header'>
            <div className='page_header_overlay'>
              <h2>{t('dashboard.title')}</h2>
            </div>
          </section>
          <section className='page_content'>
            <div className='container-fluid'>
              {userData.userType === 'admin' ? (
                <AdminDashboard countData={countData}></AdminDashboard>
              ) : (
                <ExecutiveDashboard countData={countData}></ExecutiveDashboard>
              )}
            </div>
            <div className='container-fluid'>
              <div className='row'>
                {userData.userType === 'admin' ? (
                  <div className='col-lg-12'>
                    <div className='statistics-box box-04'>
                      <h2 className='view-heading'>
                        {t('dashboard.activeDrivers')} :
                      </h2>
                      <div className='map'>
                        <MapMarker driverData={activeDriverData}></MapMarker>
                      </div>
                    </div>
                  </div>
                ) : null}
                <div className='col-lg-12'>
                  <div className='statistics-box box-02 custom_height'>
                    <h2 className='view-heading'>
                      {t('dashboard.ratioOfRidersDriver')}:
                    </h2>
                    <p className='box-info'> {t('dashboard.lastThreeMonth')}</p>
                    <div id='bar_chart'>
                      <ApexDashedGraph
                        graphData={userGraphData}
                      ></ApexDashedGraph>
                    </div>
                    <ul className='list-inline tip'>
                      <li className='list-inline-item'>
                        <span className='color01'></span>
                        {t('dashboard.drivers')}
                      </li>
                      <li className='list-inline-item'>
                        <span className='color04'></span>
                        {t('dashboard.riders')}
                      </li>
                    </ul>
                  </div>
                </div>
              </div>
            </div>
          </section>
        </main>
      </>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    userData: state.auth.userData,
    selectedVendorId: state.common.selectedVendor
  }
}

const mapDispatchToProps = () => {
  return {}
}

Dashboard.propTypes = {
  userData: PropTypes.object.isRequired,
  t: PropTypes.func.isRequired,
  i18n: PropTypes.object.isRequired
}

export default withTranslation()(
  connect(mapStateToProps, mapDispatchToProps)(Dashboard)
)
// export default connect(mapStateToProps, mapDispatchToProps)(Dashboard)
