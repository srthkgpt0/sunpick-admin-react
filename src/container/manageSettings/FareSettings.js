import React, { useState, useEffect } from 'react'
import { Table, InputNumber, Form } from 'antd'
import {
  getFareListingDataService,
  updateFareSettingService,
  updateFareStatusService
} from '../../services/settings'
import { Link } from 'react-router-dom'
import ConfirmationAndInfo from '../../components/modals/confirmationAndInfo'
import enValidationMsg from '../../utilities/lang/validation-en'
import modalNotification from '../../utilities/notifications'
import { useTranslation } from 'react-i18next'
import {
  currency_symbol,
  fareInputFormatter,
  kmInputFormatter,
  statusFormatter
} from '../../utilities/common'
import LoadingSpinner from '../../components/subComponent/loadingSpinner'

const EditableCell = ({
  editing,
  dataIndex,
  title,
  inputType,
  record,
  index,
  children,
  ...restProps
}) => {
  return (
    <td {...restProps}>
      {editing ? (
        <>
          {currency_symbol}
          <Form.Item
            key={record.id}
            name={dataIndex}
            style={{
              margin: 0
            }}
            rules={[
              {
                required: true,
                message: `Please Input ${title}!`
              }
            ]}
          >
            <InputNumber />
          </Form.Item>
        </>
      ) : (
        children
      )}
    </td>
  )
}

const FareSettings = () => {
  const [form] = Form.useForm()
  const [data, setData] = useState([])
  const [editingKey, setEditingKey] = useState('')
  const [isLoading, setIsLoading] = useState(true)
  const [onSaveKey, setOnSaveKey] = useState(true)
  const [statusChangeIntermediate, setStatusChangeIntermediate] = useState({})
  const [visible, setVisible] = useState(false)
  const [newStatus, setNewStatus] = useState('')
  const { t } = useTranslation()

  const isEditing = (record) => record.id === editingKey

  useEffect(() => {
    fetchFareListingDetails()
  }, [])
  const fetchFareListingDetails = async () =>
    // queryParams = {
    //   offset: (page - 1) * sizePerPage,
    //   limit: sizePerPage
    // }
    {
      try {
        // queryParams = {
        //   ...queryParams
        // }
        const res = await getFareListingDataService()
        // console.log(res, 'res')
        setData(res.data.rows)
        // setTotalSize(res.data.rows.length > 0 ? res.data.total : 0)
        // setIsFirstTimeFetching(false)
        setIsLoading(false)
      } catch (error) {
        // console.log(error)
      }
    }
  const edit = (record) => {
    form.setFieldsValue({
      base_fare: data.base_fare,
      per_min_fare: data.per_min_fare,
      per_km_fare: data.per_km_fare,
      per_km_fare_slab1: data.per_km_fare_slab1,
      per_km_fare_slab2: data.per_km_fare_slab2,
      per_km_fare_slab3: data.per_km_fare_slab3,
      per_km_fare_slab4: data.per_km_fare_slab4,
      per_km_fare_slab5: data.per_km_fare_slab5,
      minimum_fare: data.minimum_fare,
      minimum_fare_km: data.minimum_fare_km,
      ...record
    })
    setEditingKey(record.id)
  }

  const cancel = () => {
    setEditingKey('')
  }

  const save = (record) => {
    setVisible(true)
    setOnSaveKey(record.id)
    setNewStatus('edit')
  }

  const columns = [
    {
      dataIndex: ['category', 'name'],
      title: t('settings.vehicleCategory')
    },
    {
      dataIndex: 'base_fare',
      title: t('settings.baseFare'),
      editable: true,
      render: fareInputFormatter
    },
    {
      dataIndex: 'per_min_fare',
      title: t('settings.perMinFare'),
      editable: true,
      render: fareInputFormatter
      // formatter: fareInputFormatter
    },
    {
      dataIndex: 'per_km_fare',
      title: t('settings.ratePerKM25'),
      editable: true,
      render: fareInputFormatter
      // formatter: fareInputFormatter
    },
    {
      dataIndex: 'per_km_fare_slab1',
      title: t('settings.ratePerKM2550'),
      editable: true,
      render: fareInputFormatter
      // formatter: fareInputFormatter
    },
    {
      dataIndex: 'per_km_fare_slab2',
      title: t('settings.ratePerKM50100'),
      editable: true,
      render: fareInputFormatter
      // formatter: fareInputFormatter
    },
    {
      dataIndex: 'per_km_fare_slab3',
      title: t('settings.ratePerKM100250'),
      editable: true,
      render: fareInputFormatter
      // formatter: fareInputFormatter
    },
    {
      dataIndex: 'per_km_fare_slab4',
      title: t('settings.ratePerKM250500'),
      editable: true,
      render: fareInputFormatter
      // formatter: fareInputFormatter
    },
    {
      dataIndex: 'per_km_fare_slab5',
      title: t('settings.ratePerKM500'),
      editable: true,
      render: fareInputFormatter
      // formatter: fareInputFormatter
    },
    {
      dataIndex: 'minimum_fare',
      title: t('settings.minimumFare'),
      editable: true,
      render: fareInputFormatter
      // formatter: fareInputFormatter
    },
    {
      dataIndex: 'minimum_fare_km',
      title: t('settings.minFareKM'),
      editable: true,
      render: kmInputFormatter
      // formatter: fareInputFormatter
    },
    {
      dataIndex: 'status',
      title: t('rides.rideStatus'),
      render: (cell, row) => statusFormatter(cell, row, onOpenStatus)
    },
    {
      title: 'Actions',
      dataIndex: 'action',
      render: (_, record) => {
        const editable = isEditing(record)
        return editable ? (
          <span key={record.id}>
            <Link
              to='#'
              className='action'
              onClick={() => save(record)}
              style={{
                marginRight: 8
              }}
            >
              <i className='ti-save'></i>
            </Link>

            <Link to='#' onClick={() => cancel()} className='action'>
              <i className='ti-na'></i>
            </Link>
          </span>
        ) : (
          <Link
            key={record.id}
            to='#'
            className='action'
            disabled={editingKey !== ''}
            onClick={() => edit(record)}
          >
            <i className='ti-pencil'></i>
          </Link>
        )
      }
    }
  ]

  const onOpenStatus = async (val, row, resHandleChange) => {
    // console.log(row, 'row', val, 'val')
    try {
      if (row.status === 'active') {
        setNewStatus('inactive')
      } else if (row.status === 'inactive') {
        setNewStatus('active')
      }
      setVisible(true)
      setStatusChangeIntermediate({
        val: val,
        row: row,
        resHandleChange: resHandleChange
      })
    } catch {}
  }

  const mergedColumns = columns.map((col) => {
    if (!col.editable) {
      return col
    }

    return {
      ...col,
      onCell: (record) => ({
        record,
        inputType: 'number',
        dataIndex: col.dataIndex,
        title: col.title,
        editing: isEditing(record)
      })
    }
  })
  // const checkNumber = (value) => isNaN(Number(value)) === false
  // const isTime = (value) => {
  //   if (value?.toString()?.includes(':')) {
  //     return checkNumber(value.replace(':', '1'))
  //   }
  // }
  const openHideModal = () => {
    if (newStatus !== 'edit') {
      let status = ''
      if (statusChangeIntermediate.val) {
        status = 'inactive'
      } else {
        status = 'active'
      }
      statusChangeIntermediate.resHandleChange(status)
    }
    setVisible(false)
  }
  const onConfirmation = async () => {
    try {
      if (newStatus === 'edit') {
        const row = await form.validateFields()
        // console.log(row, 'onConfirmation row')
        const res = await updateFareSettingService(row, onSaveKey)
        // console.log('onConfirmation', res)
        if (res.success) {
          const newData = [...data]
          const index = newData.findIndex((item) => onSaveKey === item.id)
          if (index > -1) {
            const item = newData[index]
            newData.splice(index, 1, { ...item, ...row })
            setData(newData)
            setEditingKey('')
            setVisible(false)
            modalNotification({
              type: 'success',
              message: 'Success',
              description: res.message || enValidationMsg.statusUpdate
            })
          } else {
            newData.push(row)
            setData(newData)
            setEditingKey('')
            setVisible(false)
            modalNotification({
              type: 'success',
              message: 'Success',
              description: res.message || enValidationMsg.statusUpdate
            })
          }
        }
      } else {
        if (visible) {
          let status = ''
          if (statusChangeIntermediate.val) {
            status = 'active'
          } else {
            status = 'inactive'
          }
          // console.log(status, 'status')
          const res = await updateFareStatusService(
            statusChangeIntermediate.row.id,
            status
          )

          if (res.success) {
            openHideModal()
            const dataTemp = data
            const indexData = dataTemp.findIndex(
              (d) => d.id === statusChangeIntermediate.row.id
            )
            if (indexData > -1) {
              dataTemp[indexData].status = status
            }
            statusChangeIntermediate.resHandleChange(status)
            setStatusChangeIntermediate({})
            //   console.log(dataTemp, 'dataTemp HELOOOOO')
            // setData([])
            setIsLoading(false)
            // fetchCategoryListingData()
            setData(dataTemp)
            modalNotification({
              type: 'success',
              message: 'Success',
              description: res.message || enValidationMsg.statusUpdate
            })
            setVisible(false)
          }
        }
      }
    } catch (errInfo) {
      // console.log('Validate Failed:', errInfo)
      setVisible(false)
      modalNotification({
        type: 'error',
        message: 'Error',
        description: errInfo.message || enValidationMsg.statusUpdate
      })
    }
  }
  const tableLoading = {
    spinning: isLoading,
    indicator: <LoadingSpinner />
  }
  // const indicationLoading = () => <LoadingSpinner />
  // const indicationNoRecords = (message) => (
  //   <div className='alert alert-danger text-center'>
  //     {message || 'No records found'}
  //   </div>

  return (
    <>
      <main className='maincontent setting-page'>
        <section className='page_header'>
          <div className='page_header_overlay'>
            <h2>Settings</h2>
          </div>
        </section>
        <section className='page_content'>
          <div className='container-fluid'>
            <div className='card' id='card_height'>
              <div className='card-header clearfix'>
                <h3>Settings</h3>
              </div>
              <div className='card-block'>
                <div className='table-responsive'>
                  <Form form={form} component={false}>
                    <Table
                      components={{
                        body: {
                          cell: EditableCell
                        }
                      }}
                      bordered
                      dataSource={data}
                      columns={mergedColumns}
                      rowClassName='editable-row'
                      loading={tableLoading}
                    />
                  </Form>
                </div>
              </div>
              <ConfirmationAndInfo
                show={visible}
                onHide={() => openHideModal()}
                title={'Confirmation Box'}
                message={`Are you sure you want to ${newStatus} ?`}
                textOnConfirmBtn={'Confirm'}
                textOnCancelBtn={'Cancel'}
                showLoading={false}
                onConfirmation={() => onConfirmation()}
              />
            </div>
          </div>
        </section>
      </main>
    </>
  )
}
export default FareSettings
