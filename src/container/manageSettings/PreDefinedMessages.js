import React, { useEffect, useState } from 'react'
import { useTranslation } from 'react-i18next'
import RemoteDataTable from '../../components/dataTable/index'
import {
  deleteMessageService,
  getPreDefinedListingService
} from '../../services/settings/index'
import { Menu, Dropdown } from 'antd'
import { Link } from 'react-router-dom'
import AddEditMessageModal from '../../components/modals/settings/AddEditMessageModal'
import ConfirmationAndInfo from '../../components/modals/confirmationAndInfo'
import modalNotification from '../../utilities/notifications'
import enValidationMsg from '../../utilities/lang/validation-en'
import { stringFormatter } from '../../utilities/common'

function PreDefinedSettings() {
  const [data, setData] = useState('')
  const [sizePerPage, setSizePerPage] = useState(10)
  const { t } = useTranslation()
  const [page, setPage] = useState(1)
  const [totalSize, setTotalSize] = useState(0)
  const [isLoading, setIsLoading] = useState(true)
  // const [isFirstTimeFetching, setIsFirstTimeFetching] = useState(true)
  // const [statusChangeIntermediate, setStatusChangeIntermediate] = useState({})
  const [editVisible, setEditVisible] = useState(false)
  const [visible, setVisible] = useState(false)
  const [newStatus, setNewStatus] = useState('')
  // const [relatedCategory, setRelatedCategory] = useState()
  const [message, setMessage] = useState({})

  useEffect(() => {
    getPreDefinedListingData()
  }, []) // eslint-disable-line react-hooks/exhaustive-deps
  useEffect(() => {
    getPreDefinedListingData()
  }, [page]) // eslint-disable-line react-hooks/exhaustive-deps
  const getPreDefinedListingData = async (
    queryParams = {
      offset: (page - 1) * sizePerPage,
      limit: sizePerPage
    }
  ) => {
    try {
      queryParams = {
        ...queryParams
      }

      const res = await getPreDefinedListingService(queryParams)
      // console.log(res, 'res')
      setData(res.data.rows)
      setTotalSize(res.data.rows.length > 0 ? res.data.total : 0)
      // setIsFirstTimeFetching(false)
      setIsLoading(false)
    } catch (error) {
      // console.log(error)
    }
  }
  const handleAction = (action, row = null) => {
    if (action === 'edit' || action === 'add') {
      row ? setMessage(row) : setMessage({})
      setEditVisible(true)
    }
    if (action === 'delete') {
      row ? setMessage(row) : setMessage({})
      setNewStatus('delete')
      setVisible(true)
    }
  }
  const actionFormatter = (cell, row) => {
    const menu = (
      <Menu>
        <div aria-labelledby='dropdownMenuButton'>
          <Menu>
            <Menu.Item key='0'>
              <Link
                to='#'
                className='dropdown-item'
                onClick={() => {
                  handleAction('edit', row)
                }}
              >
                {t('common.edit')}
              </Link>
            </Menu.Item>
            <Menu.Item key='1'>
              <Link
                to='#'
                className='dropdown-item'
                onClick={() => handleAction('delete', row)}
                // to={{ pathname: '/driver-detail', search: `?id=${row.id}` }}
              >
                {t('common.delete')}
              </Link>
            </Menu.Item>
          </Menu>
        </div>
      </Menu>
    )
    return (
      <Dropdown overlay={menu} trigger={['click']}>
        <div className='dropdown mx-auto'>
          <Link
            to='#'
            onClick={(e) => e.preventDefault()}
            className='dropdown-toggle'
            id={`dropdownMenuButton_${row.id}`}
            data-toggle='dropdown'
            aria-haspopup='true'
            aria-expanded='false'
          >
            <span className='icon-more_vert'></span>
          </Link>
        </div>
      </Dropdown>
    )
  }
  const handleTableChange = (
    type,
    { page, sizePerPage, filters, sortField, sortOrder, cellEdit }
  ) => {
    setPage(page)
    setSizePerPage(sizePerPage)
    setIsLoading(true)
    setData([])
    setTotalSize(0)
  }
  const [columns] = useState([
    {
      dataField: 'id',
      text: t('rides.id'),
      hidden: true
    },
    {
      dataField: 'message',
      text: t('settings.message')
    },
    {
      dataField: 'sort_order',
      text: t('settings.sequenceNumber'),
      sort: true
    },
    {
      dataField: 'user_type',
      text: t('settings.userType'),
      formatter: stringFormatter
    },
    {
      dataField: 'type',
      text: t('settings.messageType'),
      formatter: stringFormatter
    },
    {
      dataField: 'isDummyAction',
      text: t('common.action'),
      formatter: actionFormatter
    }
  ])
  const hideMessageAddEdit = (edited) => {
    if (edited) {
      setIsLoading(true)
      setData([])
      getPreDefinedListingData()
    }
    setEditVisible(false)
  }
  const openHideModal = () => {
    setVisible(false)
  }
  const onConfirmation = async () => {
    try {
      let res = await deleteMessageService(message.id)
      if (res.success) {
        modalNotification({
          type: 'success',
          message: 'Success',
          description: res.message || enValidationMsg.statusUpdate
        })
        setIsLoading(true)
        setData([])
        getPreDefinedListingData()
        setVisible(false)
      }
    } catch (error) {}
  }
  return (
    <div>
      <main className='maincontent settings_page text-template-page'>
        <section className='page_header'>
          <div className='page_header_overlay'>
            <h2>Predefined Message</h2>
          </div>
        </section>
        <section className='page_content'>
          <div className='container-fluid'>
            <div className='row'>
              <div className='col-md-12'>
                <div className='card' id='card_height'>
                  <div className='card-header clearfix'>
                    <h3>Predefined Messages</h3>
                  </div>
                  <div className='card-block'>
                    <div className='text-right mb-3'>
                      <button
                        className='btn btn-warning'
                        onClick={() => handleAction('add')}
                      >
                        ADD
                      </button>
                    </div>
                    <div>
                      <RemoteDataTable
                        columns={columns}
                        data={data}
                        totalSize={totalSize}
                        page={page}
                        sizePerPage={sizePerPage}
                        loading={isLoading}
                        onTableChange={handleTableChange}
                      />
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>
        <AddEditMessageModal
          show={editVisible}
          data={message}
          onHide={hideMessageAddEdit}
          // relatedCategory={relatedCategory}
        />
        <ConfirmationAndInfo
          show={visible}
          onHide={() => openHideModal()}
          title={'Confirmation Box'}
          message={`Are you sure you want to ${newStatus} ?`}
          textOnConfirmBtn={'Confirm'}
          textOnCancelBtn={'Cancel'}
          showLoading={false}
          onConfirmation={() => onConfirmation()}
        />
      </main>
    </div>
  )
}

export default PreDefinedSettings
