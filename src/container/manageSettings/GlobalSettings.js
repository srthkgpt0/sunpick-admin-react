import React, { useState, useEffect } from 'react'
import { Table, Input, InputNumber, Form } from 'antd'
import {
  getGlobalSettingDataService,
  updateSettingService
} from '../../services/settings'
import { Link } from 'react-router-dom'
import ConfirmationAndInfo from '../../components/modals/confirmationAndInfo'
import enValidationMsg from '../../utilities/lang/validation-en'
import modalNotification from '../../utilities/notifications'
import LoadingSpinner from '../../components/subComponent/loadingSpinner'

const EditableCell = ({
  editing,
  dataIndex,
  title,
  inputType,
  record,
  index,
  children,
  ...restProps
}) => {
  let inputNode
  if (inputType === 'number') {
    inputNode = <InputNumber />
  } else if (inputType === 'text') {
    inputNode = <Input />
  } else {
    inputNode = <Input type='time' />
  }
  return (
    <td {...restProps}>
      {editing ? (
        <Form.Item
          name={dataIndex}
          style={{
            margin: 0
          }}
          rules={[
            {
              required: true,
              message: `Please Input ${title}!`
            }
          ]}
        >
          {inputNode}
        </Form.Item>
      ) : (
        children
      )}
    </td>
  )
}

const GlobalSettings = () => {
  const [form] = Form.useForm()
  const [data, setData] = useState([])
  const [editingKey, setEditingKey] = useState('')
  const [isLoading, setIsLoading] = useState(true)
  const [onSaveKey, setOnSaveKey] = useState(true)
  const [visible, setVisible] = useState(false)

  const isEditing = (record) => record.key === editingKey
  useEffect(() => {
    fetchGlobalSettingDetails()
  }, [])
  const fetchGlobalSettingDetails = async () =>
    // queryParams = {
    //   offset: (page - 1) * sizePerPage,
    //   limit: sizePerPage
    // }
    {
      try {
        // queryParams = {
        //   ...queryParams
        // }
        const res = await getGlobalSettingDataService()
        // console.log(res, 'res')
        setData(res.data.rows)
        // setTotalSize(res.data.rows.length > 0 ? res.data.total : 0)
        // setIsFirstTimeFetching(false)
        setIsLoading(false)
      } catch (error) {
        // console.log(error)
      }
    }
  const edit = (record) => {
    form.setFieldsValue({
      value: data.value,
      ...record
    })
    setEditingKey(record.key)
  }

  const cancel = () => {
    setEditingKey('')
  }

  const save = (record) => {
    setVisible(true)
    setOnSaveKey(record.id)
  }

  const columns = [
    {
      title: 'Key',
      dataIndex: 'name'
    },
    {
      title: 'Value',
      dataIndex: 'value',
      editable: true
    },
    {
      title: 'Actions',
      dataIndex: 'action',
      render: (_, record) => {
        const editable = isEditing(record)
        return editable ? (
          <span>
            <Link
              to='#'
              className='action'
              onClick={() => save(record)}
              style={{
                marginRight: 8
              }}
            >
              <i className='ti-save'></i>
            </Link>

            <Link to='#' onClick={() => cancel()} className='action'>
              <i className='ti-na'></i>
            </Link>
          </span>
        ) : (
          <Link
            to='#'
            className='action'
            disabled={editingKey !== ''}
            onClick={() => edit(record)}
          >
            <i className='ti-pencil'></i>
          </Link>
        )
      }
    }
  ]
  const mergedColumns = columns.map((col) => {
    if (!col.editable) {
      return col
    }

    return {
      ...col,
      onCell: (record) => ({
        record,
        inputType: isTime(record.value)
          ? 'time'
          : checkNumber(record.value)
          ? 'number'
          : 'text',
        dataIndex: col.dataIndex,
        title: col.title,
        editing: isEditing(record)
      })
    }
  })
  const checkNumber = (value) => isNaN(Number(value)) === false
  const isTime = (value) => {
    if (value?.toString()?.includes(':')) {
      return checkNumber(value.replace(':', '1'))
    }
  }
  const openHideModal = () => {
    setVisible(false)
  }
  const onConfirmation = async () => {
    try {
      const row = await form.validateFields()
      // console.log(row, 'onConfirmation row')
      const res = await updateSettingService(row, onSaveKey)
      // console.log('onConfirmation', res)
      if (res.success) {
        const newData = [...data]
        const index = newData.findIndex((item) => onSaveKey === item.id)
        if (index > -1) {
          const item = newData[index]
          newData.splice(index, 1, { ...item, ...row })
          setData(newData)
          setEditingKey('')
          setVisible(false)
          modalNotification({
            type: 'success',
            message: 'Success',
            description: res.message || enValidationMsg.statusUpdate
          })
        } else {
          newData.push(row)
          setData(newData)
          setEditingKey('')
          setVisible(false)
          modalNotification({
            type: 'success',
            message: 'Success',
            description: res.message || enValidationMsg.statusUpdate
          })
        }
      }
    } catch (errInfo) {
      // console.log('Validate Failed:', errInfo)
      setVisible(false)
      modalNotification({
        type: 'error',
        message: 'Error',
        description: errInfo.message || enValidationMsg.statusUpdate
      })
    }
  }
  const tableLoading = {
    spinning: isLoading,
    indicator: <LoadingSpinner />
  }
  return (
    <>
      <main className='maincontent setting-page'>
        <section className='page_header'>
          <div className='page_header_overlay'>
            <h2>Settings</h2>
          </div>
        </section>
        <section className='page_content'>
          <div className='container-fluid'>
            <div className='card' id='card_height'>
              <div className='card-header clearfix'>
                <h3>Settings</h3>
              </div>
              <div className='card-block'>
                <div className='table-responsive'>
                  <Form form={form} component={false}>
                    <Table
                      components={{
                        body: {
                          cell: EditableCell
                        }
                      }}
                      bordered
                      dataSource={data}
                      columns={mergedColumns}
                      rowClassName='editable-row'
                      loading={tableLoading}
                    />
                  </Form>
                </div>
              </div>
              <ConfirmationAndInfo
                show={visible}
                onHide={() => openHideModal()}
                title={'Confirmation Box'}
                message={`Are you sure you want to save ?`}
                textOnConfirmBtn={'Confirm'}
                textOnCancelBtn={'Cancel'}
                showLoading={false}
                onConfirmation={() => onConfirmation()}
              />
            </div>
          </div>
        </section>
      </main>
    </>
  )
}
export default GlobalSettings
