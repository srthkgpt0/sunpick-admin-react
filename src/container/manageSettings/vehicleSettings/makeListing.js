import React, { useEffect, useState } from 'react'
import { useTranslation } from 'react-i18next'
import { Link } from 'react-router-dom'
import RemoteDataTable from '../../../components/dataTable'
import {
  deleteMakeService,
  getMakeListingData,
  updateMakeStatusService
} from '../../../services/settings'
import { statusFormatter } from '../../../utilities/common'
import { Menu, Dropdown } from 'antd'
// import AddEditCategoryModal from '../../../components/modals/settings/addEditCategoryModal'
import ConfirmationAndInfo from '../../../components/modals/confirmationAndInfo'
// import { updateRiderStatusService } from '../../../services/riders'
import modalNotification from '../../../utilities/notifications'
// import textMessages from '../../../../utilities/textMessages'
import enValidationMsg from '../../../utilities/lang/validation-en'

import AddEditMakeModal from '../../../components/modals/settings/addEditMakeModal'

function MakeListing() {
  const { t } = useTranslation()
  const [data, setData] = useState('')
  const [sizePerPage, setSizePerPage] = useState(10)
  const [page, setPage] = useState(1)
  const [totalSize, setTotalSize] = useState(0)
  const [isLoading, setIsLoading] = useState(true)
  // const [isFirstTimeFetching, setIsFirstTimeFetching] = useState(true)
  const [statusChangeIntermediate, setStatusChangeIntermediate] = useState({})
  const [editVisible, setEditVisible] = useState(false)
  const [visible, setVisible] = useState(false)
  const [newStatus, setNewStatus] = useState('')
  const [make, setMake] = useState({})

  // const [filterData, setFilterData] = useState({})

  useEffect(() => {
    fetchMakeListingData()
  }, []) // eslint-disable-line react-hooks/exhaustive-deps
  useEffect(() => {
    fetchMakeListingData()
  }, [page]) // eslint-disable-line react-hooks/exhaustive-deps

  const fetchMakeListingData = async (
    queryParams = {
      offset: (page - 1) * sizePerPage,
      limit: sizePerPage
    }
  ) => {
    try {
      queryParams = {
        ...queryParams
      }
      const res = await getMakeListingData(queryParams)
      // console.log(res, 'res')
      setData(res.data.rows)
      setTotalSize(res.data.rows.length > 0 ? res.data.total : 0)
      // setIsFirstTimeFetching(false)
      setIsLoading(false)
    } catch (error) {
      // console.log(error)
    }
  }
  const onConfirmation = async () => {
    try {
      let res
      if (newStatus === 'Delete') {
        res = await deleteMakeService(make.id)
        if (res.success) {
          modalNotification({
            type: 'success',
            message: 'Success',
            description: res.message || enValidationMsg.statusUpdate
          })

          setIsLoading(true)
          setData([])
          fetchMakeListingData()
          setVisible(false)
        }
      } else {
        if (visible) {
          let status = ''
          if (statusChangeIntermediate.val) {
            status = 'active'
          } else {
            status = 'inactive'
          }
          console.log(status, 'status')
          const res = await updateMakeStatusService(
            statusChangeIntermediate.row.id,
            status
          )

          if (res.success) {
            openHideModal()
            const dataTemp = data
            const indexData = dataTemp.findIndex(
              (d) => d.id === statusChangeIntermediate.row.id
            )
            if (indexData > -1) {
              dataTemp[indexData].status = status
            }
            statusChangeIntermediate.resHandleChange(status)
            setStatusChangeIntermediate({})
            //   console.log(dataTemp, 'dataTemp HELOOOOO')
            // setData([])
            setIsLoading(true)
            // fetchMakeListingData()
            setData(dataTemp)
            modalNotification({
              type: 'success',
              message: 'Success',
              description: res.message || enValidationMsg.statusUpdate
            })
          }
        }
      }
    } catch (error) {}
  }
  const handleTableChange = (
    type,
    { page, sizePerPage, filters, sortField, sortOrder, cellEdit }
  ) => {
    setPage(page)
    setSizePerPage(sizePerPage)
    setIsLoading(true)
    setData([])
    setTotalSize(0)
  }
  const handleAction = (action, row = {}) => {
    if (action === 'edit' || action === 'add') {
      row ? setMake(row) : setMake({})
      setEditVisible(true)
    }
    if (action === 'delete') {
      row ? setMake(row) : setMake({})
      setNewStatus('Delete')
      setVisible(true)
    }
  }
  const openHideModal = () => {
    if (newStatus !== 'Delete') {
      let status = ''
      if (statusChangeIntermediate.val) {
        status = 'inactive'
      } else {
        status = 'active'
      }
      statusChangeIntermediate.resHandleChange(status)
    }
    setVisible(false)
  }
  const actionFormatter = (cell, row) => {
    const menu = (
      <Menu>
        <div aria-labelledby='dropdownMenuButton'>
          <Menu>
            <Menu.Item key='0'>
              <Link
                className='dropdown-item'
                onClick={() => {
                  handleAction('edit', row)
                }}
                // to={{ pathname: '/add-driver', search: `?id=${row.id}` }}
              >
                {t('common.edit')}
              </Link>
            </Menu.Item>
            <Menu.Item key='1'>
              <Link
                className='dropdown-item'
                onClick={() => handleAction('delete', row)}
                // to={{ pathname: '/driver-detail', search: `?id=${row.id}` }}
              >
                {t('common.delete')}
              </Link>
            </Menu.Item>
          </Menu>
        </div>
      </Menu>
    )
    return (
      <Dropdown overlay={menu} trigger={['click']}>
        <div className='dropdown mx-auto'>
          <Link
            to='#'
            onClick={(e) => e.preventDefault()}
            className='dropdown-toggle'
            id={`dropdownMenuButton_${row.id}`}
            data-toggle='dropdown'
            aria-haspopup='true'
            aria-expanded='false'
          >
            <span className='icon-more_vert'></span>
          </Link>
        </div>
      </Dropdown>
    )
  }
  const [columns] = useState([
    {
      dataField: 'id',
      text: t('rides.id'),
      sort: true,
      hidden: true
    },
    {
      dataField: 'name',
      text: t('settings.make')
    },
    {
      dataField: 'status',
      text: t('rides.rideStatus'),
      formatter: (cell, row) => statusFormatter(cell, row, onOpenStatus)
    },
    {
      dataField: 'isDummyAction',
      text: t('common.action'),
      formatter: actionFormatter
    }
  ])
  const hideRiderAddEdit = () => {
    fetchMakeListingData()
    setEditVisible(false)
    setMake({})
  }
  const onOpenStatus = async (val, row, resHandleChange) => {
    // console.log(row, 'row', val, 'val')
    try {
      if (row.status === 'active') {
        setNewStatus('inactive')
      } else if (row.status === 'inactive') {
        setNewStatus('active')
      }
      setVisible(true)
      setStatusChangeIntermediate({
        val: val,
        row: row,
        resHandleChange: resHandleChange
      })
    } catch {}
  }
  return (
    <>
      <div className='heading-row clearfix'>
        <h2>Vehicle Make</h2>
        <div className='right-side'>
          <Link
            className='btn btn-warning ripple-effect'
            onClick={() => handleAction('add')}
          >
            ADD
          </Link>
        </div>
      </div>
      <div className=''>
        <RemoteDataTable
          columns={columns}
          data={data}
          totalSize={totalSize}
          page={page}
          sizePerPage={sizePerPage}
          loading={isLoading}
          onTableChange={handleTableChange}
        />
      </div>
      <AddEditMakeModal
        show={editVisible}
        data={make}
        onHide={() => hideRiderAddEdit()}
      />
      <ConfirmationAndInfo
        show={visible}
        onHide={() => openHideModal()}
        title={'Confirmation Box'}
        message={`Are you sure you want to ${newStatus} ?`}
        textOnConfirmBtn={'Confirm'}
        textOnCancelBtn={'Cancel'}
        showLoading={false}
        onConfirmation={() => onConfirmation()}
      />
    </>
  )
}

export default MakeListing
