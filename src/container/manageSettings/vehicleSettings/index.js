import React, { useState } from 'react'
import { Link } from 'react-router-dom'
import CategoryListing from './categoryListing'
import MakeListing from './makeListing'
import ModelListing from './modelListing'

function VehicleSettings() {
  const [tab, setTab] = useState('category')
  return (
    <div>
      <main className='maincontent setting-page'>
        <section className='page_header'>
          <div className='page_header_overlay'>
            <h2>Vehicle Settings</h2>
          </div>
        </section>
        <section className='page_content'>
          <div className='container-fluid'>
            <div className='card' id='card_height'>
              <div className='card-header clearfix'>
                <h3>Vehicle Settings</h3>
              </div>
              <div className='card-block'>
                <ul className='nav nav-tabs' id='myTab' role='tablist'>
                  <li className='nav-item'>
                    <Link
                      to='#'
                      className={`nav-link ${
                        tab === 'category' ? 'active' : ''
                      }`}
                      data-toggle='tab'
                      href='#category'
                      role='tab'
                      aria-controls='home'
                      aria-selected='true'
                      onClick={() => setTab('category')}
                    >
                      Category
                    </Link>
                  </li>
                  <li className='nav-item'>
                    <Link
                      to='#'
                      className={`nav-link ${tab === 'make' ? 'active' : ''}`}
                      data-toggle='tab'
                      href='#make'
                      role='tab'
                      aria-controls='profile'
                      aria-selected='false'
                      onClick={() => setTab('make')}
                    >
                      Make
                    </Link>
                  </li>
                  <li className='nav-item'>
                    <Link
                      to='#'
                      className={`nav-link ${tab === 'model' ? 'active' : ''}`}
                      data-toggle='tab'
                      href='#model'
                      role='tab'
                      aria-controls='contact'
                      aria-selected='false'
                      onClick={() => setTab('model')}
                    >
                      Model
                    </Link>
                  </li>
                </ul>

                <div className='tab-content'>
                  {tab === 'category' ? (
                    <CategoryListing />
                  ) : tab === 'make' ? (
                    <MakeListing />
                  ) : (
                    <ModelListing />
                  )}
                </div>
              </div>
            </div>
          </div>
        </section>
      </main>
    </div>
  )
}

export default VehicleSettings
