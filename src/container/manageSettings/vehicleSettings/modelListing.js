import React, { useEffect, useState } from 'react'
import { useTranslation } from 'react-i18next'
import { Link } from 'react-router-dom'
import RemoteDataTable from '../../../components/dataTable'
import {
  getCategoryDropdown,
  getMakeDropdown,
  getModelListingData,
  updateModelStatusService,
  deleteModelService
} from '../../../services/settings'
import { filterDataObj, statusFormatter } from '../../../utilities/common'
import { Menu, Dropdown } from 'antd'
// import AddEditCategoryModal from '../../../components/modals/settings/addEditCategoryModal'
import ConfirmationAndInfo from '../../../components/modals/confirmationAndInfo'
// import { updateRiderStatusService } from '../../../services/riders'
import modalNotification from '../../../utilities/notifications'
import enValidationMsg from '../../../utilities/lang/validation-en'
import ManageModelFilterForm from '../../../components/forms/settings/manageModelFilterForm'
import AddEditModelModal from '../../../components/modals/settings/addEditModelModal'

function ModelListing() {
  const { t } = useTranslation()
  const [data, setData] = useState('')
  const [sizePerPage, setSizePerPage] = useState(10)
  const [page, setPage] = useState(1)
  const [totalSize, setTotalSize] = useState(0)
  const [isLoading, setIsLoading] = useState(true)
  // const [isFirstTimeFetching, setIsFirstTimeFetching] = useState(true)
  const [statusChangeIntermediate, setStatusChangeIntermediate] = useState({})
  const [editVisible, setEditVisible] = useState(false)
  const [isFilterOpen, setIsFilterOpen] = useState(true)

  const [visible, setVisible] = useState(false)
  const [newStatus, setNewStatus] = useState('')
  // const [isFinish, setIsFinish] = useState(false)
  const [filterData, setFilterData] = useState({})
  const [categoryDropdown, setCategoryDropdown] = useState(null)
  const [makeDropdown, setMakeDropdown] = useState(null)
  const [vehicle, setVehicle] = useState({})
  const formRef = React.createRef()
  // const history = useHistory()
  // const location = useLocation()

  useEffect(() => {
    fetchModelListingData()
    fetchCategoryDropdown()
    fetchMakeDropdown()
  }, []) // eslint-disable-line react-hooks/exhaustive-deps
  useEffect(() => {
    fetchModelListingData()
  }, [page]) // eslint-disable-line react-hooks/exhaustive-deps
  useEffect(() => {
    fetchModelListingData()
  }, [filterData]) // eslint-disable-line react-hooks/exhaustive-deps

  const fetchCategoryDropdown = async () => {
    const res = await getCategoryDropdown()
    setCategoryDropdown(res.data.rows)
  }
  const fetchMakeDropdown = async () => {
    try {
      const res = await getMakeDropdown()
      setMakeDropdown(res.data.rows)
    } catch (error) {}
  }
  const fetchModelListingData = async (
    queryParams = {
      offset: (page - 1) * sizePerPage,
      limit: sizePerPage
    }
  ) => {
    try {
      queryParams = {
        ...queryParams,
        ...filterData
      }
      const res = await getModelListingData(queryParams)
      // console.log(res, 'res')
      setData(res.data.rows)
      setTotalSize(res.data.rows.length > 0 ? res.data.total : 0)
      // setIsFirstTimeFetching(false)
      setIsLoading(false)
    } catch (error) {
      // console.log(error)
    }
  }
  const onConfirmation = async () => {
    try {
      let res
      if (newStatus === 'Delete') {
        res = await deleteModelService(vehicle.id)
        if (res.success) {
          modalNotification({
            type: 'success',
            message: 'Success',
            description: res.message || enValidationMsg.statusUpdate
          })

          setIsLoading(true)
          setData([])
          fetchModelListingData()
          setVisible(false)
        }
      } else {
        if (visible) {
          let status = ''
          if (statusChangeIntermediate.val) {
            status = 'active'
          } else {
            status = 'inactive'
          }
          // console.log(status, 'status')
          res = await updateModelStatusService(
            statusChangeIntermediate.row.id,
            status
          )
          if (res.success) {
            openHideModal()
            const dataTemp = data
            const indexData = dataTemp.findIndex(
              (d) => d.id === statusChangeIntermediate.row.id
            )
            if (indexData > -1) {
              dataTemp[indexData].status = status
            }
            statusChangeIntermediate.resHandleChange(status)
            setStatusChangeIntermediate({})
            modalNotification({
              type: 'success',
              message: 'Success',
              description: res.message || enValidationMsg.statusUpdate
            })
            setData(dataTemp)
            // setData([])
            setIsLoading(true)
            // fetchModelListingData()
          }
        }
      }
    } catch (error) {}
  }
  const handleTableChange = (
    type,
    { page, sizePerPage, filters, sortField, sortOrder, cellEdit }
  ) => {
    setPage(page)
    setSizePerPage(sizePerPage)
    setIsLoading(true)
    setData([])
    setTotalSize(0)
  }
  const onFinish = (values) => {
    // console.log(values, 'values')
    const { filterData } = filterDataObj(values)
    setPage(1)
    setSizePerPage(sizePerPage)
    setIsLoading(true)
    setData([])
    setTotalSize(0)
    // setIsFinish(true)
    setFilterData(filterData)
  }
  const onFinishFailed = () => {
    // console.log('values')
  }
  const onReset = () => {
    setPage(1)
    setSizePerPage(sizePerPage)
    setIsLoading(true)
    setData([])
    setTotalSize(0)
    setFilterData({})
    formRef.current.resetFields()
  }
  const handleAction = (action, row = {}) => {
    if (action === 'edit' || action === 'add') {
      row ? setVehicle(row) : setVehicle({})
      setEditVisible(true)
    }
    if (action === 'delete') {
      row ? setVehicle(row) : setVehicle({})
      setNewStatus('Delete')
      setVisible(true)
    }
  }
  const openHideModal = () => {
    if (newStatus !== 'Delete') {
      let status = ''
      if (statusChangeIntermediate.val) {
        status = 'inactive'
      } else {
        status = 'active'
      }
      statusChangeIntermediate.resHandleChange(status)
    }
    setVisible(false)
  }
  const actionFormatter = (cell, row) => {
    const menu = (
      <Menu>
        <div aria-labelledby='dropdownMenuButton'>
          <Menu>
            <Menu.Item key='0'>
              <Link
                className='dropdown-item'
                onClick={() => {
                  handleAction('edit', row)
                }}
                // to={{ pathname: '/add-driver', search: `?id=${row.id}` }}
              >
                {t('common.edit')}
              </Link>
            </Menu.Item>
            <Menu.Item key='1'>
              <Link
                className='dropdown-item'
                onClick={() => handleAction('delete', row)}
                // to={{ pathname: '/driver-detail', search: `?id=${row.id}` }}
              >
                {t('common.delete')}
              </Link>
            </Menu.Item>
          </Menu>
        </div>
      </Menu>
    )
    return (
      <Dropdown overlay={menu} trigger={['click']}>
        <div className='dropdown mx-auto'>
          <Link
            to='#'
            onClick={(e) => e.preventDefault()}
            className='dropdown-toggle'
            id={`dropdownMenuButton_${row.id}`}
            data-toggle='dropdown'
            aria-haspopup='true'
            aria-expanded='false'
          >
            <span className='icon-more_vert'></span>
          </Link>
        </div>
      </Dropdown>
    )
  }
  const [columns] = useState([
    {
      dataField: 'id',
      text: t('rides.id'),
      sort: true,
      hidden: true
    },
    {
      dataField: 'category.name',
      text: t('rides.category')
    },
    {
      dataField: 'brand.name',
      text: t('settings.make')
      //   formatter: rideTypeFormatter
    },
    {
      dataField: 'name',
      text: t('settings.model')
    },
    {
      dataField: 'status',
      text: t('rides.rideStatus'),
      formatter: (cell, row) => statusFormatter(cell, row, onOpenStatus)
    },
    {
      dataField: 'isDummyAction',
      text: t('common.action'),
      formatter: actionFormatter
    }
  ])
  const hideModelAddEdit = () => {
    fetchModelListingData()
    setEditVisible(false)
    setVehicle({})
  }
  const onOpenStatus = async (val, row, resHandleChange) => {
    try {
      if (row.status === 'active') {
        setNewStatus('inactive')
      } else if (row.status === 'inactive') {
        setNewStatus('active')
      }
      setVisible(true)
      setStatusChangeIntermediate({
        val: val,
        row: row,
        resHandleChange: resHandleChange
      })
    } catch {}
  }
  return (
    <>
      <div
        className='tab-pane fade show active'
        id='model'
        role='tabpanel'
        aria-labelledby='contact-tab'
      >
        <div className='heading-row clearfix'>
          <h2>Vehicle Model and Category</h2>
          <div className='right-side'>
            <Link
              id='btnSearch'
              onClick={() => {
                setIsFilterOpen((state) => !state)
              }}
              className='btn btn-warning ripple-effect'
            >
              <i className='fa fa-search' aria-hidden='true'></i>
            </Link>
            <Link
              className='btn btn-warning ripple-effect ml-3'
              onClick={() => handleAction('add')}
            >
              ADD
            </Link>
          </div>
        </div>
        {isFilterOpen ? (
          <ManageModelFilterForm
            onFinish={onFinish}
            onFinishFailed={onFinishFailed}
            onReset={onReset}
            formRef={formRef}
            categoryDropdown={categoryDropdown}
            makeDropdown={makeDropdown}
          />
        ) : (
          ''
        )}
      </div>
      <RemoteDataTable
        columns={columns}
        data={data}
        totalSize={totalSize}
        page={page}
        sizePerPage={sizePerPage}
        loading={isLoading}
        onTableChange={handleTableChange}
      />
      <AddEditModelModal
        show={editVisible}
        data={vehicle}
        onHide={hideModelAddEdit}
        categoryDropdown={categoryDropdown}
        makeDropdown={makeDropdown}
      />
      <ConfirmationAndInfo
        show={visible}
        onHide={() => openHideModal()}
        title={'Confirmation Box'}
        message={`Are you sure you want to ${newStatus} ?`}
        textOnConfirmBtn={'Confirm'}
        textOnCancelBtn={'Cancel'}
        showLoading={false}
        onConfirmation={() => onConfirmation()}
      />
    </>
  )
}

export default ModelListing
