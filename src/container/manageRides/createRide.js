import { Form, message } from 'antd'
import React, { useEffect, useState } from 'react'
import logger from '../../utilities/logger'
import { connect } from 'react-redux'
import { Link, useHistory } from 'react-router-dom'
import AddRideForm from '../../components/forms/rides/createRideForm'
import PropTypes from 'prop-types'
import { useTranslation } from 'react-i18next'
import {
  bookRideService,
  createFareCalculationService,
  searchCustomerService
} from '../../services/rides'
import moment from 'moment'
import modalNotification from '../../utilities/notifications'
const AddRide = (props) => {
  const [form] = Form.useForm()
  const { t } = useTranslation()
  let history = useHistory()
  // const [isFinish, setIsFinish] = useState(false)
  const [passenger, setPassenger] = useState(null)
  // const [filterData, setFilterData] = useState({})
  const [phoneNumber, setPhoneNumber] = useState('')
  const [countryCode, setCountryCode] = useState('')
  const [reqObject, setRequestObject] = useState({})
  const [select, setSelect] = useState(false)
  const [calculationRequest, setCalculationRequest] = useState({})
  const [fare, setFare] = useState('')
  const [selectRow, setSelectRow] = useState('')
  const [isFare, setIsFare] = useState(false)
  const [pickUpChange, setPickUpChange] = useState('')
  const [dropOffChange, setDropOffChange] = useState('')
  const [callSearchApi, setCallSearchApi] = useState(false);
  const [selectRadio, setSelectRadio] = useState(false);
  let formatDate = 'YYYY-MM-DD HH:mm:00'
  let instantDateFormat = moment().format(formatDate)

  const onPickUpLocationSelect = (values) => {
    const mapData = {}
    mapData['pick_up_latitude'] = values.geometry.location.lat()
    mapData['pick_up_longitude'] = values.geometry.location.lng()
    mapData['pick_up_address'] = values.formatted_address
    mapData['pick_up_postcode'] = ' '
    setRequestObject({ ...reqObject, ...mapData })
  }

  const onDropOffLocationSelect = (values) => {
    const mapData = {}
    mapData['drop_off_latitude'] = values.geometry.location.lat()
    mapData['drop_off_longitude'] = values.geometry.location.lng()
    mapData['drop_off_address'] = values.formatted_address
    mapData['drop_off_postcode'] = ' '
    setRequestObject({ ...reqObject, ...mapData })
  }
  const countryCodeOption = [
    {
      id: 1,
      label: '+91',
      value: '+91'
    },
    {
      id: 2,
      label: '+251',
      value: '+251'
    }
  ]

  const passengerIncrement = () => {
    if (!pickUpChange) {
      message.error('Please Select the Pick Up Address')
    } else if (!dropOffChange) {
      message.error('Please Select the Drop Off Address')
    } else {
      setPassenger(passenger + 1)
    }
  }

  const passengerDecrement = () => {
    if (!pickUpChange) {
      message.error('Please Select the Pick Up Address')
    } else if (!dropOffChange) {
      message.error('Please Select the Drop Off Address')
    } else {
      if (passenger !== 0) {
        setPassenger(passenger - 1)
      }
    }
  }
  useEffect(() => {
    if (callSearchApi) {
      searchPoneNumber()
    }
  }, [phoneNumber, countryCode]) // eslint-disable-line react-hooks/exhaustive-deps

  const searchPoneNumber = async () => {
    try {
      const bodyData = {
        phone_number: phoneNumber,
        phone_number_country_code: countryCode
      }
      const res = await searchCustomerService({ bodyData })
      if (res && res.succes) {
        let customerObj = {}
        let { data } = res
        customerObj['customer_id'] = data.id
        customerObj['first_name'] = data.first_name
        customerObj['last_name'] = data.last_name
        setRequestObject(customerObj)
        form.setFieldsValue({
          full_name: `${data.first_name} ${data.last_name}`
        })
      } else {
        setRequestObject({ ...reqObject, customer_id: 0 })
      }
    } catch (error) {
      logger({ error: error })
    }
  }

  const onChange = async (e) => {
    setPhoneNumber(e.target.value)
  }
  const onCountryCodeSelect = (e) => {
    setCountryCode(e)
    setCallSearchApi(true);
  }

  const onChangeCountryCode = (value) => {
    setCountryCode(value);
    setCallSearchApi(true);
  }

  useEffect(() => {
    if (isFare) {
      fareCalcuation()
      setSelect(true)
    }
  }, [selectRow]) // eslint-disable-line react-hooks/exhaustive-deps

  const fareCalcuation = async () => {
    try {
      let rideObject = {}
      const res = await createFareCalculationService({
        bodyData: calculationRequest
      })
      if (res && res.success) {
        let { data } = res
        rideObject['ride_amount_without_fee'] = data.fare_without_fee
        rideObject['ride_amount'] = data.fare
        rideObject['ride_km'] = data.distance_km
        setRequestObject({ ...reqObject, ...data, ...rideObject })
        setFare(data.fare)
      }
    } catch (error) {
      logger({ error: error })
    }
  }
  const handleSelectOption = (rowKey, row) => {
    let dam = {}
    row.forEach((ele) => {
      dam['icon'] = ele.icon;
      dam['category'] = ele.category;
      dam['categoryId'] = ele.categoryId;
    });
    // row.map((ele) => {
    //   dam['icon'] = ele.icon
    //   dam['category'] = ele.category
    //   dam['categoryId'] = ele.categoryId
    // })
    const { icon, category, categoryId } = dam
    let bookRideObject = {}
    bookRideObject['car_category_icon'] = icon
    bookRideObject['car_category'] = category
    bookRideObject['category_id'] = categoryId
    setRequestObject({ ...reqObject, ...bookRideObject })
    let calculationObject = {
      category_id: categoryId,
      booking_date_time: instantDateFormat,
      destination_latitude: reqObject.drop_off_latitude,
      destination_longitude: reqObject.drop_off_longitude,
      source_latitude: reqObject.pick_up_latitude,
      source_longitude: reqObject.pick_up_longitude,
      car_category_icon: icon,
      car_category: category
    }
    setRequestObject({
      ...reqObject,
      booking_date_time: calculationObject.booking_date_time
    })
    setCalculationRequest({ ...calculationRequest, ...calculationObject })
    setSelectRow(rowKey)
    setIsFare(true)
    setSelectRadio(true);
  }
  const onFinish = async (values) => {
    if (!selectRadio) {
      return message.error('please select the category');
    }
    else {
      const first_name = values.full_name.split(' ')[0]
      const last_name = values.full_name.split(' ')[1] || ' ';
      let timeZone = Intl.DateTimeFormat().resolvedOptions().timeZone;
      try {
        let formObject = {}
        let booking_date_time = null
        let { booking_type } = values
        if (booking_type === 'now') {
          booking_date_time = moment().format('YYYY-MM-DD H:mm:00')
        } else {
          booking_date_time = values.booking_date_time
        }
        booking_date_time = moment.tz(booking_date_time, timeZone).format();
        formObject = { ...reqObject, ...calculationRequest }
        formObject['booking_type'] = values.booking_type
        formObject['booking_date_time'] = booking_date_time
        formObject['first_name'] = first_name
        formObject['last_name'] = last_name
        formObject['phone_number'] = values.phone_number
        formObject['phone_number_country_code'] = values.phone_country_code
        formObject['payment_type'] = 'cash'
        if (values.phone_country_code === '+91') {
          formObject['phone_number_country'] = 'IN'
        } else {
          formObject['phone_number_country'] = 'ET'
        }
        const res = await bookRideService({ bodyData: formObject })
        if (res && res.success) {
          modalNotification({
            type: 'success',
            message: 'Success',
            description: res.message
          })
          history.push('/rides')
        }
      } catch (error) {
        logger({ 'error:': error })
      }
    }
  }
  const handlePickUpOnChange = (values) => {
    setPickUpChange(values.nativeEvent.data)
  }
  const handleDropOffOnChange = (values) => {
    setDropOffChange(values.nativeEvent.data)
  }
  const onFinishFailed = (errorInfo) => {
    logger({ 'Failed:': errorInfo })
  }

  const onFromDateChange = (value, dateString) => {
    form.setFieldsValue({
      booking_date_time: dateString
    })
  }
  const onToDateChange = (value, dateString) => {
    form.setFieldsValue({
      end_date: dateString
    })
  }

  return (
    <main className='maincontent form_box'>
      <section className='page_header'>
        <div className='page_header_overlay'>
          <h2>{`${t('rides.addRide')}`}</h2>
          <Link className='back-btn' to='/rides'>
            <i className='ti-hand-point-left' aria-hidden='true'></i>BACK{' '}
          </Link>
        </div>
      </section>
      <section className='page_content'>
        <div className='container-fluid'>
          <div className='card' id='card_height'>
            <div className='card-header clearfix'>
              <h3>{`${t('rides.addRide')}`}</h3>
            </div>
            <div className='card-block'>
              <div>
                <div className='row'>
                  <div className='col-sm-12'>
                    <AddRideForm
                      form={form}
                      onFinish={onFinish}
                      onFinishFailed={onFinishFailed}
                      onFromDateChange={onFromDateChange}
                      onToDateChange={onToDateChange}
                      onChange={onChange}
                      onPickUpLocationSelect={onPickUpLocationSelect}
                      onDropOffLocationSelect={onDropOffLocationSelect}
                      countryOption={countryCodeOption}
                      onCountryCodeSelect={onCountryCodeSelect}
                      passengerIncrement={passengerIncrement}
                      passengerDecrement={passengerDecrement}
                      passenger={passenger}
                      // isFinish={isFinish}
                      //   filterData={filterData}
                      handleSelectOption={handleSelectOption}
                      select={select}
                      fare={fare}
                      handlePickUpOnChange={handlePickUpOnChange}
                      handleDropOffOnChange={handleDropOffOnChange}
                      onChangeCountryCode={onChangeCountryCode}
                    />
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
    </main>
  )
}

const mapStateToProps = (state) => {
  return {
    userData: state.auth.userData,
    selectedVendorId: state.common.selectedVendor
  }
}

const mapDispatchToProps = () => {
  return {}
}
AddRide.propTypes = {
  userData: PropTypes.object.isRequired
}
export default connect(mapStateToProps, mapDispatchToProps)(AddRide)
