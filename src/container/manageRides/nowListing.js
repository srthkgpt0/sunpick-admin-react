import React, { useEffect, useState } from 'react'
import { Link, useHistory, useLocation } from 'react-router-dom'
import RemoteDataTable from '../../components/dataTable'
import { useTranslation } from 'react-i18next'
import {
  getPageSizeFromURL,
  addPageSizeInURL,
  amountFormatter,
  tollFormatter,
  customerNameFormatter,
  driverNameFormatter,
  rideStatusFormatter,
  paymentStatusFormatter,
  rideTypeFormatter,
  twentyFourHourFormatter
} from '../../utilities/common'
import { getRideListService } from '../../services/rides'

function NowListing({ isFinish, filterData }) {
  const { t } = useTranslation()
  const [data, setData] = useState('')
  const [sizePerPage, setSizePerPage] = useState(10)
  const [page, setPage] = useState(1)
  const [totalSize, setTotalSize] = useState(0)
  const [isLoading, setIsLoading] = useState(true)
  const [isFirstTimeFetching, setIsFirstTimeFetching] = useState(true)
  // const [filterData, setFilterData] = useState({})
  const history = useHistory()
  const location = useLocation()
  useEffect(() => {
    fetchNowRidesDetails()
  }, []) // eslint-disable-line react-hooks/exhaustive-deps

  useEffect(() => {
    const queryParams = {
      offset: (page - 1) * sizePerPage,
      limit: sizePerPage
    }
    if (!isFirstTimeFetching) {
      addPageSizeInURL(page, sizePerPage, history)
    }
    fetchOnHandleTableChange(queryParams)
  }, [page]) // eslint-disable-line react-hooks/exhaustive-deps

  useEffect(() => {
    formResponse()
  }, [isFinish]) // eslint-disable-line react-hooks/exhaustive-deps
  useEffect(() => {
    fetchNowRidesDetails()
  }, [filterData]) // eslint-disable-line react-hooks/exhaustive-deps

  const formResponse = () => {
    setPage(1)
    setSizePerPage(sizePerPage)
    setIsLoading(true)
    setData([])
    setTotalSize(0)
    // setFilterData(filterData)
  }
  // const handleStatus = () => {}
  const actionFormatter = (cell, row) => {
    return (
      <Link to={`/rides/detail/${row.id}?tab_type=now`} className='action'>
        <span className='ti-eye'></span>
      </Link>
    )
  }
  const [columns] = useState([
    {
      dataField: 'id',
      text: t('rides.id'),
      sort: true
    },
    {
      dataField: 'ride_number',
      text: t('rides.number')
    },
    {
      dataField: 'ride_type',
      text: t('rides.type'),
      formatter: rideTypeFormatter
    },
    {
      dataField: 'car_category',
      text: t('rides.category')
    },
    {
      dataField: 'payment_type',
      text: t('rides.paymentType')
    },
    {
      dataField: 'booking_date',
      text: t('rides.bookingDate'),
      formatter: twentyFourHourFormatter
    },
    {
      dataField: 'customer_id',
      text: t('riders.id')
    },
    {
      dataField: 'customer.first_name',
      text: t('rides.rider'),
      formatter: customerNameFormatter
    },
    {
      dataField: 'driver_id',
      text: t('reports.driverId')
    },
    {
      dataField: '',
      text: t('rides.driver'),
      formatter: driverNameFormatter
    },
    {
      dataField: 'ride_amount',
      text: t('rides.rideFare'),
      formatter: amountFormatter
    },
    {
      dataField: 'toll_total',
      text: t('rides.tollTaxes'),
      formatter: tollFormatter
    },
    {
      dataField: 'cancel_reason',
      text: t('rides.cancellationReason')
    },
    {
      dataField: 'status',
      text: t('rides.rideStatus'),
      formatter: rideStatusFormatter
    },
    {
      dataField: 'payment.status',
      text: t('rides.paymentStatus'),
      formatter: paymentStatusFormatter
    },
    {
      dataField: 'payment.charge_id',
      text: t('rides.paymentID')
    },
    {
      dataField: 'isDummyAction',
      text: t('common.action'),
      formatter: actionFormatter
    }
  ])
  const fetchOnHandleTableChange = (queryParams) => {
    if (isFirstTimeFetching) {
      if (location) {
        const query = location.search
        const res = getPageSizeFromURL(query, location)
        if (res) {
          // reFetchOnUrlBasis(query)
        } else {
          fetchNowRidesDetails()
        }
      }
    } else {
      fetchNowRidesDetails()
    }
  }
  const handleTableChange = (
    type,
    { page, sizePerPage, filters, sortField, sortOrder, cellEdit }
  ) => {
    setPage(page)
    setSizePerPage(sizePerPage)
    setIsLoading(true)
    setData([])
    setTotalSize(0)
  }
  const fetchNowRidesDetails = async (
    queryParams = {
      offset: (page - 1) * sizePerPage,
      limit: sizePerPage,
      is_pre_booking: false
    }
  ) => {
    try {
      queryParams = {
        ...queryParams,
        ...filterData
      }

      const res = await getRideListService({ queryParams })
      // console.log(res, 'res')
      setData(res.data.rows)
      setTotalSize(res.data.rows.length > 0 ? res.data.total : 0)
      setIsFirstTimeFetching(false)
      setIsLoading(false)
    } catch {}
  }
  return (
    <div>
      <RemoteDataTable
        columns={columns}
        data={data}
        totalSize={totalSize}
        page={page}
        sizePerPage={sizePerPage}
        loading={isLoading}
        onTableChange={handleTableChange}
        // defaultSorted={defaultSorted}
      />
    </div>
  )
}

export default NowListing
