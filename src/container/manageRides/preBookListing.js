import React, { useEffect, useState } from 'react'
import RemoteDataTable from '../../components/dataTable'
import { Link, useHistory, useLocation } from 'react-router-dom'
import AssignDriverModal from '../../components/modals/rides/assignDriverModal'
import { useTranslation } from 'react-i18next'
import {
  getPageSizeFromURL,
  addPageSizeInURL,
  amountFormatter,
  tollFormatter,
  customerNameFormatter,
  driverNameFormatter,
  rideStatusFormatter,
  paymentStatusFormatter,
  rideTypeFormatter,
  twentyFourHourFormatter
} from '../../utilities/common'
import {
  getAvailableDrivers,
  getRideListService,
  getSingleRideDetailService
} from '../../services/rides'
import { selectUserData } from '../../redux/auth/authSlice'
import { useSelector } from 'react-redux'
import { Dropdown, Menu, message } from 'antd'

function PreBookListing({ isFinish, filterData }) {
  const { t } = useTranslation()
  const [data, setData] = useState('')
  const [sizePerPage, setSizePerPage] = useState(10)
  const [page, setPage] = useState(1)
  const [totalSize, setTotalSize] = useState(0)
  const [isLoading, setIsLoading] = useState(true)
  const [isFirstTimeFetching, setIsFirstTimeFetching] = useState(true)
  const [driverAssignData, setDriverAssignData] = useState([])
  const [availableDrivers, setAvailableDrivers] = useState([])
  const [ride, setRide] = useState([])
  const [loadingDriverList, setLoadingDriverList] = useState(true)

  const [visible, setVisible] = useState(false)

  const history = useHistory()
  const location = useLocation()
  const { userType } = useSelector(selectUserData)

  useEffect(() => {
    fetchPreBookRidesDetails()
  }, []) // eslint-disable-line react-hooks/exhaustive-deps
  useEffect(() => {
    console.log(loadingDriverList, 'loadingDriverList from prebookig')
  }, [loadingDriverList])
  useEffect(() => {
    const queryParams = {
      offset: (page - 1) * sizePerPage,
      limit: sizePerPage
    }
    if (!isFirstTimeFetching) {
      addPageSizeInURL(page, sizePerPage, history)
    }
    fetchOnHandleTableChange(queryParams)
  }, [page]) // eslint-disable-line react-hooks/exhaustive-deps

  useEffect(() => {
    formResponse()
  }, [isFinish]) // eslint-disable-line react-hooks/exhaustive-deps
  useEffect(() => {
    fetchPreBookRidesDetails()
  }, [filterData]) // eslint-disable-line react-hooks/exhaustive-deps

  const formResponse = () => {
    setPage(1)
    setSizePerPage(sizePerPage)
    setIsLoading(true)
    setData([])
    setTotalSize(0)
    // setFilterData(filterData)
  }
  // const handleStatus = () => {}
  const fetchAssignRideDetails = async (id) => {
    try {
      const res = await getSingleRideDetailService(id)
      setDriverAssignData(res.data)
      if (res.success) {
        setVisible(true)
      }
    } catch (error) {
      message.error(error.message)
    }
  }
  const fetchAvailableDrivers = async (row) => {
    try {
      const queryParams = {
        category_id: row.category_id,
        is_pre_booking: true,
        ride_id: row.id
      }
      const res = await getAvailableDrivers(queryParams)
      if (res.success) {
        setAvailableDrivers(res.data)
        setLoadingDriverList(false)
        console.log('res was success')
      }
    } catch (error) {
      setLoadingDriverList(false)
    }
  }
  const handleRefresh = (row) => {
    console.log('handleRefresh')
    setLoadingDriverList(true)
    fetchAvailableDrivers(row)
  }
  const handleAssignDriverModal = (row) => {
    setRide(row)
    fetchAssignRideDetails(row.id)
    fetchAvailableDrivers(row)
  }

  const hideDriverModal = () => {
    setVisible(false)
  }

  const actionFormatter = (cell, row) => {
    const menu = (
      <Menu>
        <div aria-labelledby='dropdownMenuButton'>
          <Menu>
            <Menu.Item key='1'>
              <Link
                className='dropdown-item'
                to={`/rides/detail/${row.id}?tab_type=pre_book`}
              >
                {t('common.view')}
              </Link>
            </Menu.Item>
            {row.status === 'requested' && (
              <Menu.Item key='0'>
                <Link
                  className='dropdown-item'
                  onClick={() => handleAssignDriverModal(row)}
                  to='#'
                >
                  {t('rides.assignDriver')}
                </Link>
              </Menu.Item>
            )}
          </Menu>
        </div>
      </Menu>
    )
    return userType === 'executive' ? (
      <Dropdown overlay={menu} trigger={['click']}>
        <div className='dropdown mx-auto'>
          <Link
            to='#'
            onClick={(e) => e.preventDefault()}
            className='dropdown-toggle'
            id={`dropdownMenuButton_${row.id}`}
            data-toggle='dropdown'
            aria-haspopup='true'
            aria-expanded='false'
          >
            <span className='icon-more_vert'></span>
          </Link>
        </div>
      </Dropdown>
    ) : (
      <Link to={`/rides/detail/${row.id}?tab_type=pre_book`} className='action'>
        <span className='ti-eye'></span>
      </Link>
    )
  }
  const [columns] = useState([
    {
      dataField: 'id',
      text: t('rides.id'),
      sort: true
    },
    {
      dataField: 'ride_number',
      text: t('rides.number')
    },
    {
      dataField: 'ride_type',
      text: t('rides.type'),
      formatter: rideTypeFormatter
    },
    {
      dataField: 'car_category',
      text: t('rides.category')
    },
    {
      dataField: 'payment_type',
      text: t('rides.paymentType')
    },
    {
      dataField: 'booking_date',
      text: t('rides.bookingDate'),
      formatter: twentyFourHourFormatter
    },
    {
      dataField: 'customer_id',
      text: t('riders.id')
    },
    {
      dataField: 'customer.first_name',
      text: t('rides.rider'),
      formatter: customerNameFormatter
    },
    {
      dataField: 'driver_id',
      text: t('drivers.id')
    },
    {
      dataField: '',
      text: t('rides.driver'),
      formatter: driverNameFormatter
    },
    {
      dataField: 'ride_amount',
      text: t('rides.rideFare'),
      formatter: amountFormatter
    },
    {
      dataField: 'toll_total',
      text: t('rides.tollTaxes'),
      formatter: tollFormatter
    },
    {
      dataField: 'cancel_reason',
      text: t('rides.cancellationReason')
    },
    {
      dataField: 'status',
      text: t('rides.rideStatus'),
      formatter: rideStatusFormatter
    },
    {
      dataField: 'payment.status',
      text: t('rides.paymentStatus'),
      formatter: paymentStatusFormatter
    },
    {
      dataField: 'payment.charge_id',
      text: t('rides.paymentID')
    },
    {
      dataField: 'isDummyAction',
      text: t('common.action'),
      formatter: actionFormatter
    }
  ])
  const fetchOnHandleTableChange = (queryParams) => {
    if (isFirstTimeFetching) {
      if (location) {
        const query = location.search
        const res = getPageSizeFromURL(query, location)
        if (res) {
          // reFetchOnUrlBasis(query)
        } else {
          fetchPreBookRidesDetails()
        }
      }
    } else {
      fetchPreBookRidesDetails()
    }
  }
  const handleTableChange = (
    type,
    { page, sizePerPage, filters, sortField, sortOrder, cellEdit }
  ) => {
    setPage(page)
    setSizePerPage(sizePerPage)
    setIsLoading(true)
    setData([])
    setTotalSize(0)
  }
  const fetchPreBookRidesDetails = async (
    queryParams = {
      offset: (page - 1) * sizePerPage,
      limit: sizePerPage,
      is_pre_booking: true
    }
  ) => {
    try {
      queryParams = {
        ...queryParams,
        ...filterData
      }

      const res = await getRideListService({ queryParams })
      setData(res.data.rows)
      setTotalSize(res.data.rows.length > 0 ? res.data.total : 0)
      setIsFirstTimeFetching(false)
      setIsLoading(false)
    } catch {}
  }

  return (
    <div>
      <RemoteDataTable
        columns={columns}
        data={data}
        totalSize={totalSize}
        page={page}
        sizePerPage={sizePerPage}
        loading={isLoading}
        onTableChange={handleTableChange}
        // defaultSorted={defaultSorted}
      />
      {visible && (
        <AssignDriverModal
          show={visible}
          data={driverAssignData}
          availableDrivers={availableDrivers}
          onHide={() => hideDriverModal()}
          handleRefresh={handleRefresh}
          ride={ride}
          loadingDriverList={loadingDriverList}
        />
      )}
    </div>
  )
}

export default PreBookListing
