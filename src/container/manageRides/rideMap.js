import React, { Component, useState, useEffect } from 'react'
import { compose } from 'recompose'
// import { currentTimeStamp } from '../../utilities/common'
import {
  withScriptjs,
  withGoogleMap,
  GoogleMap,
  Marker,
  InfoWindow,
  Polyline,
  DirectionsRenderer
} from 'react-google-maps'
import config from '../../config'
const MapWithAPolyLine = compose(
  withScriptjs,
  withGoogleMap
)((props) => {
  const data = props.rideData
  const [isStartPoint, setIsStartPoint] = useState(false)
  const [isEndPoint, setIsEndPoint] = useState(false)
  const [direction, setDirection] = useState(null)
  const onStartMarkerClick = (evt) => {
    setIsStartPoint(!isStartPoint)
  }
  useEffect(() => {
    // props.updateZoom(7)

    if (data.way_point.length === 0) {
      console.log('data.way_point.length', data.way_point.length)
      if (props.rideData && Object.keys(props.rideData).length !== 0) {
        const directionsService = new window.google.maps.DirectionsService()

        const origin = {
          lat: props.rideData.pick_up_latitude,
          lng: props.rideData.pick_up_longitude
        }
        const destination = {
          lat: props.rideData.drop_off_latitude,
          lng: props.rideData.drop_off_longitude
        }

        directionsService.route(
          {
            origin: origin,
            destination: destination,
            travelMode: window.google.maps.TravelMode.DRIVING
          },
          (result, status) => {
            if (status === window.google.maps.DirectionsStatus.OK) {
              setDirection(result)
              // props.updateZoom(7)
            } else {
              console.error(`error fetching directions ${result}`)
            }
          }
        )
      } else {
        setDirection(null)
      }
    }
  }, [props.rideData]) // eslint-disable-line react-hooks/exhaustive-deps

  const onEndMarkerClick = (evt) => {
    setIsEndPoint(!isEndPoint)
  }
  var langList = []
  langList.push(
    new window.google.maps.LatLng(data.pick_up_latitude, data.pick_up_longitude)
  )

  langList.push(
    new window.google.maps.LatLng(data.end_latitude, data.end_longitude)
  )
  var bounds = new window.google.maps.LatLngBounds()
  langList.forEach(function (n) {
    bounds.extend(n)
  })

  console.log('direction', data)
  //  window.google.maps.Map.setCenter(bounds.getCenter())
  return (
    <GoogleMap
      ref={(map) => map && map.fitBounds(bounds)}
      defaultCenter={bounds.getCenter()}
      defaultZoom={7}
    >
      {data.way_point.length !== 0 ? (
        <>
          <Polyline
            path={data.way_point}
            options={{
              geodesic: true,
              strokeColor: '#73B6F3',
              strokeOpacity: 1.0,
              strokeWeight: 5.5
            }}
          />
          <Marker
            position={{
              lat: data.pick_up_latitude,
              lng: data.pick_up_longitude
            }}
            onClick={onStartMarkerClick}
            label={{ text: 'A', color: 'white' }}
          >
            {isStartPoint && (
              <InfoWindow onClick={onStartMarkerClick}>
                <div>
                  <div>
                    Address: <b> {data.pick_up_address} </b>
                  </div>
                </div>
              </InfoWindow>
            )}
          </Marker>
          <Marker
            position={{ lat: data.end_latitude, lng: data.end_longitude }}
            onClick={onEndMarkerClick}
            label={{ text: 'B', color: 'white' }}
          >
            {isEndPoint && (
              <InfoWindow onClick={onEndMarkerClick}>
                <div>
                  Address:
                  <b>
                    {data.end_address
                      ? data.end_address
                      : data.drop_off_address}
                  </b>
                </div>
              </InfoWindow>
            )}
          </Marker>
        </>
      ) : (
        <DirectionsRenderer directions={direction} />
      )}
    </GoogleMap>
  )
})

export default class RideMap extends Component {
  render() {
    return (
      <>
        {
          <MapWithAPolyLine
            googleMapURL={`https://maps.googleapis.com/maps/api/js?key=${config.GOOGLE_API_KEY}&v=3.exp&libraries=geometry,drawing,places`}
            loadingElement={<div style={{ height: `100%` }} />}
            containerElement={<div style={{ height: `400px` }} />}
            mapElement={<div style={{ height: `500px` }} />}
            rideData={this.props.rideData}
          />
        }
      </>
    )
  }
}
