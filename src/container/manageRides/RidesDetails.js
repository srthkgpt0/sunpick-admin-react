import React, { useEffect, useState } from 'react'
import {
  showTimeOnlyInBrowser,
  titleCase,
  showDateOnlyInBrowser,
  currency_symbol,
  rideStatusFormatter
} from '../../utilities/common'
import { getRideStatusClass } from '../../services/common/index'
import { GlobalLoader } from '../../components/common'
import { Link } from 'react-router-dom'
import { getSingleRideDetailService } from '../../services/rides'
import RideMap from './rideMap'

function RidesDetails(props) {
  const [data, setData] = useState([])
  const [isLoading, setIsLoading] = useState(true)
  const [showRatePerKm, setShowRatePerKm] = useState(true)
  const [showMinFare, setShowMinFare] = useState(true)

  const rideId = props.match.params.id
  useEffect(() => {
    fetchRideDetails(rideId)
  }, []) // eslint-disable-line react-hooks/exhaustive-deps
  // useEffect(() => {
  //   if (data.ride_km <= data.minimum_fare_km) {
  //     setData((state) => (state['base_fare'] = state['minimum_fare']))
  //     // data.base_fare = data.minimum_fare
  //     setShowRatePerKm(false)
  //     // this.show_rate_per_km = false
  //   } else {
  //     setShowMinFare(false)
  //     // this.show_min_fare = false
  //   }
  // }, [data])
  const fetchRideDetails = async (id) => {
    try {
      const res = await getSingleRideDetailService(id)
      // console.log(res)
      setData(res.data)
      if (data.ride_km <= data.minimum_fare_km) {
        setData((state) => (state['base_fare'] = state['minimum_fare']))
        // data.base_fare = data.minimum_fare
        setShowRatePerKm(false)
        // this.show_rate_per_km = false
      } else {
        setShowMinFare(false)
        // this.show_min_fare = false
      }
      setIsLoading(false)
    } catch {}
  }
  return isLoading ? (
    <GlobalLoader />
  ) : (
    <main className='maincontent view-p'>
      <section className='page_header'>
        <div className='page_header_overlay'>
          <h2>Ride DETAIL</h2>
          <Link
            to='#'
            className='back-btn'
            onClick={() => props.history.goBack()}
          >
            <i className='ti-hand-point-left' aria-hidden='true'></i> BACK
          </Link>
        </div>
      </section>
      <section className='page_content'>
        <div className='top-info'>
          <div className='container-fluid'>
            <div className='row'>
              <div className='col-sm-6  col-xl-4'>
                <div className='box user-detail custom-height mb-15'>
                  <h2 className='view-heading'>RIDE DETAIL :</h2>

                  <div className='row'>
                    <div className='col-6 custom-col'>
                      <div className='info-set'>
                        <p>Ride Number</p>
                        <span>{data?.ride_number || '-'}</span>
                      </div>
                      <div className='info-set'>
                        <p>PAYMENT STATUS</p>

                        <span
                          className={getRideStatusClass(data?.payment?.status)}
                        >
                          {titleCase(data?.payment?.status) || '-'}
                        </span>
                      </div>
                      <div className='info-set'>
                        <p>Rider</p>
                        <span>
                          {`${
                            data?.customer?.first_name
                              ? data?.customer?.first_name
                              : '-'
                          } ${
                            data?.customer?.last_name
                              ? data?.customer?.last_name
                              : '-'
                          }`}
                        </span>
                      </div>
                    </div>

                    <div className='col-6 custom-col'>
                      <div className='info-set'>
                        <p>Ride Status</p>
                        <span>{rideStatusFormatter('', data)}</span>
                      </div>
                      <div className='info-set'>
                        <p>PAYMENT MODE</p>
                        <span>
                          {titleCase(data?.payment?.payment_method) || '-'}
                        </span>
                      </div>
                      <div className='info-set'>
                        <p>Driver</p>
                        <span>
                          {`${
                            data?.driver?.first_name
                              ? data?.driver?.first_name
                              : '-'
                          }
                          ${
                            data?.driver?.last_name
                              ? data?.driver?.last_name
                              : '-'
                          }`}
                        </span>
                      </div>
                    </div>
                  </div>
                  <div className='row'>
                    <div className='col-md-12'>
                      <div className='info-set'>
                        <p>Payment ID</p>
                        <span>{data?.payment?.charge_id || '-'}</span>
                      </div>
                    </div>
                    <div className='col-md-12'>
                      <div className='info-set'>
                        <p>Cancellation Reason</p>
                        <span>{data?.cancel_reason || '-'}</span>
                      </div>
                    </div>
                  </div>
                </div>
              </div>

              <div className='col-sm-6 col-xl-4'>
                <div className='box user-detail custom-height mb-15'>
                  <h2 className='view-heading'>BOOKING DETAIL :</h2>
                  <div className='row'>
                    <div className='col-6 custom-col'>
                      <div className='info-set'>
                        <p>Booking Date & Time</p>
                        <ul className='time-detail'>
                          <li className='date'>
                            {showDateOnlyInBrowser(data?.booking_date)}
                          </li>
                          <li className='time'>
                            {showTimeOnlyInBrowser(data?.booking_date)}
                          </li>
                        </ul>
                      </div>
                    </div>
                    <div className='col-6 custom-col'>
                      <div className='info-set'>
                        <p>Ride Date & Time</p>
                        <ul className='time-detail'>
                          <li className='date'>
                            {showDateOnlyInBrowser(data?.start_date)}
                          </li>
                          <li className='time'>
                            {showTimeOnlyInBrowser(data?.start_date)}
                          </li>
                        </ul>
                      </div>
                    </div>
                  </div>

                  <div className='info-set'>
                    <p>Route From To</p>
                    <ul className='ride-detail'>
                      <li className='from'>
                        {data?.pick_up_address ? data?.pick_up_address : '-'}
                      </li>
                      <li className='to'>
                        {data?.end_address
                          ? data?.end_address
                          : data?.drop_off_address
                          ? data?.drop_off_address
                          : '-'}
                      </li>
                    </ul>
                  </div>
                </div>
              </div>

              <div className='col-sm-12 col-xl-4'>
                <div className='box fare-detail custom-height mb-15'>
                  <h2 className='view-heading'>FARE BREAKDOWN :</h2>
                  <div className='fare-row'>
                    <div className='row'>
                      <div className='col-7 col-md-8'>
                        <div className='labeling'>
                          Base Fare
                          <span>:</span>
                        </div>
                      </div>
                      <div className='col-5 col-md-4'>
                        <div className='value'>
                          (+)
                          {`${currency_symbol}
                          ${data?.base_fare ? data?.base_fare : '0'}`}
                        </div>
                      </div>
                    </div>
                  </div>
                  {showMinFare && (
                    <div className='fare-row'>
                      <div className='row'>
                        <div className='col-7 col-md-8'>
                          <div className='labeling'>
                            KM applicable for minimum fare
                            <span>:</span>
                          </div>
                        </div>
                        <div className='col-5 col-md-4'>
                          <div className='value'>
                            {data?.minimum_fare_km || '0'} KM
                          </div>
                        </div>
                      </div>
                    </div>
                  )}

                  <div className='fare-row'>
                    <div className='row'>
                      <div className='col-7 col-md-8'>
                        <div className='labeling'>
                          Distance Travelled
                          <span>:</span>
                        </div>
                      </div>
                      <div className='col-5 col-md-4'>
                        <div className='value'>
                          {data?.ride_km ? data?.ride_km : '0'} KM
                        </div>
                      </div>
                    </div>
                  </div>
                  {showRatePerKm && (
                    <div className='fare-row'>
                      <div className='row'>
                        <div className='col-7 col-md-8'>
                          <div className='labeling'>
                            Time Taken(in Mins)
                            <span>:</span>
                          </div>
                        </div>
                        {data && data.actual_time ? (
                          <div className='col-5 col-md-4'>
                            <div className='value'>
                              {data?.actual_time ? data?.actual_time : '0'}
                            </div>
                          </div>
                        ) : (
                          <div className='col-5 col-md-4'>
                            <div className='value'>
                              {data?.estimate_time ? data?.estimate_time : '0'}
                            </div>
                          </div>
                        )}
                      </div>
                    </div>
                  )}
                  {/* <div className='fare-row'>
                    <div className='row'>
                      <div className='col-7 col-md-8'>
                        <div className='labeling'>
                          <span>:</span>
                        </div>
                      </div>
                      <div className='col-5 col-md-4'>
                        <div className='value'>
                          {data?.estimate_time ? data?.estimate_time : '0'}
                        </div>
                      </div>
                    </div>
                  </div> */}
                  {showRatePerKm && (
                    <div className='fare-row'>
                      <div className='row'>
                        <div className='col-7 col-md-8'>
                          <div className='labeling'>
                            Rate/KM (
                            {data?.car_category ? data?.car_category : '-'})
                            <span>:</span>
                          </div>
                        </div>
                        {data && data.ride_km && data.ride_km <= 25 ? (
                          <div className='col-5 col-md-4'>
                            <div className='value'>
                              (+) {data?.ride_km ? data?.ride_km : '0'} x
                              {` ${currency_symbol}
                          ${data?.per_km_fare ? data?.per_km_fare : '0'}`}
                            </div>
                          </div>
                        ) : (
                          ''
                        )}
                        {data &&
                        data.ride_km &&
                        data.ride_km > 25 &&
                        data.ride_km <= 50 ? (
                          <div className='col-5 col-md-4'>
                            <div className='value'>
                              (+) {data?.ride_km ? data?.ride_km : '0'} x
                              {` ${currency_symbol}
                          ${
                            data?.per_km_fare_slab1
                              ? data?.per_km_fare_slab1
                              : '0'
                          }`}
                            </div>
                          </div>
                        ) : (
                          ''
                        )}
                        {data &&
                        data.ride_km &&
                        data.ride_km > 50 &&
                        data.ride_km <= 100 ? (
                          <div className='col-5 col-md-4'>
                            <div className='value'>
                              (+) {data?.ride_km ? data?.ride_km : '0'} x
                              {` ${currency_symbol}
                          ${
                            data?.per_km_fare_slab2
                              ? data?.per_km_fare_slab2
                              : '0'
                          }`}
                            </div>
                          </div>
                        ) : (
                          ''
                        )}
                        {data &&
                        data.ride_km &&
                        data.ride_km > 100 &&
                        data.ride_km <= 250 ? (
                          <div className='col-5 col-md-4'>
                            <div className='value'>
                              (+) {data?.ride_km ? data?.ride_km : '0'} x
                              {` ${currency_symbol}
                          ${
                            data?.per_km_fare_slab3
                              ? data?.per_km_fare_slab3
                              : '0'
                          }`}
                            </div>
                          </div>
                        ) : (
                          ''
                        )}
                        {data &&
                        data.ride_km &&
                        data.ride_km > 250 &&
                        data.ride_km <= 500 ? (
                          <div className='col-5 col-md-4'>
                            <div className='value'>
                              (+) {data?.ride_km ? data?.ride_km : '0'} x
                              {` ${currency_symbol}
                          ${
                            data?.per_km_fare_slab4
                              ? data?.per_km_fare_slab4
                              : '0'
                          }`}
                            </div>
                          </div>
                        ) : (
                          ''
                        )}
                        {data && data.ride_km && data.ride_km > 500 ? (
                          <div className='col-5 col-md-4'>
                            <div className='value'>
                              (+) {data?.ride_km ? data?.ride_km : '0'} x
                              {` ${currency_symbol}
                          ${
                            data?.per_km_fare_slab5
                              ? data?.per_km_fare_slab5
                              : '0'
                          }`}
                            </div>
                          </div>
                        ) : (
                          ''
                        )}
                      </div>
                    </div>
                  )}
                  {showRatePerKm && (
                    <div className='fare-row'>
                      <div className='row'>
                        <div className='col-7 col-md-8'>
                          <div className='labeling'>
                            Time Factor for Travel(Rs. / Min)
                            <span>:</span>
                          </div>
                        </div>
                        {data && data?.actual_time ? (
                          <div className='col-5 col-md-4'>
                            <div className='value'>
                              {`(+) ${
                                data?.actual_time ? data?.actual_time : '0'
                              } x ${currency_symbol} ${
                                data?.per_min_fare ? data?.per_min_fare : '0'
                              }`}
                            </div>
                          </div>
                        ) : (
                          <div className='col-5 col-md-4'>
                            <div className='value'>
                              (+)
                              {data?.estimate_time ? data?.estimate_time : '0'}
                              {` ${currency_symbol} ${
                                data?.per_min_fare ? data?.per_min_fare : '0'
                              }`}
                            </div>
                          </div>
                        )}
                      </div>
                    </div>
                  )}
                  <div className='fare-row'>
                    <div className='row'>
                      <div className='col-7 col-md-8'>
                        <div className='labeling'>
                          Toll Taxes
                          <span>:</span>
                        </div>
                      </div>
                      <div className='col-5 col-md-4'>
                        <div className='value'>
                          (+)
                          {` ${currency_symbol}
                          ${data?.toll_total ? data?.toll_total : '0'}`}
                        </div>
                      </div>
                    </div>
                  </div>

                  <div className='fare-row'>
                    <div className='row'>
                      <div className='col-7 col-md-8'>
                        <div className='labeling'>
                          Discount (Promo)
                          <span>:</span>
                        </div>
                      </div>
                      <div className='col-5 col-md-4'>
                        <div className='value'>
                          (-)
                          {` ${currency_symbol}
                          ${
                            data?.promo_discount_value
                              ? data?.promo_discount_value
                              : '0'
                          }`}
                        </div>
                      </div>
                    </div>
                  </div>

                  <div className='fare-row'>
                    <div className='row'>
                      <div className='col-7 col-md-8'>
                        <div className='labeling'>
                          Tax
                          <span>:</span>
                        </div>
                      </div>
                      <div className='col-5 col-md-4'>
                        <div className='value'>
                          (+)
                          {` ${currency_symbol}
                          ${data?.tax_value ? data?.tax_value : '0'}`}
                        </div>
                      </div>
                    </div>
                  </div>

                  <div className='fare-row total'>
                    <div className='row'>
                      <div className='col-7 col-md-8'>
                        <div className='labeling'>
                          Total Fare
                          <span>:</span>
                        </div>
                      </div>
                      <div className='col-5 col-md-4'>
                        <div className='value'>
                          {` ${currency_symbol}
                          ${data?.ride_amount ? data?.ride_amount : '0'}`}
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div className='row'>
              <div className='col-sm-12 '>
                <RideMap rideData={data}></RideMap>
              </div>
            </div>
          </div>
        </div>
      </section>
    </main>
  )
}

export default RidesDetails
