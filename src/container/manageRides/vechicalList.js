import { Table } from 'antd'

import React, { useEffect, useState } from 'react'
import { useTranslation } from 'react-i18next'
import { getVechicalListService } from '../../services/rides'
import logger from '../../utilities/logger';

export default function VechicalListing({
  // isFinish,
  passenger,
  handleSelectOption,
}) {
  const { t } = useTranslation();
  const [data, setData] = useState('')
  const [isLoading, setIsLoading] = useState(true)

  useEffect(() => {
    fetchVechicalDetail()
  }, [passenger]) // eslint-disable-line react-hooks/exhaustive-deps

  useEffect(() => {
    formResponse()
  }, []) // eslint-disable-line react-hooks/exhaustive-deps

  const formResponse = () => {
    setIsLoading(true)
    setData([])
  }

  const columns = [
    {
      dataIndex: 'category',
      title: t('common.category')
    },
    {
      dataIndex: 'name',
      title: t('common.model')
    },
    {
      dataIndex: 'brand',
      title: t('common.brand')
    },
    {
      dataIndex: 'capacity',
      title: t('common.capacity')
    }
  ]

  const fetchVechicalDetail = async (
    queryParams = {
      passenger_capacity: passenger
    }
  ) => {
    try {
      setIsLoading(true)

      const res = await getVechicalListService({ queryParams })
      let { data } = res;
      let tempArray = []
      for (let i = 0; i < data.length; i++) {
        let ele = data[i]
        tempArray.push({
          key: i,
          categoryId: ele.category_id,
          icon: ele.category.icon,
          category: ele.category.name,
          name: ele.name,
          brand: ele.brand.name,
          capacity: ele.category.capacity
        })
      }
      setData(tempArray)

      // setIsFirstTimeFetching(false)
      setIsLoading(false)
    } catch (error) {
      logger({ 'error': error });
    }
  }

  const rowSelection = {
    onChange: (selectedRowKeys, selectedRows) => {
      handleSelectOption(selectedRowKeys, selectedRows)
    }
  }

  return (
    <div>
      <Table
        rowSelection={{
          type: 'radio',
          ...rowSelection
        }}
        columns={columns}
        dataSource={data}
        loading={isLoading}
        pagination={false}
        size={"large"}
      />
    </div>
  )
}
