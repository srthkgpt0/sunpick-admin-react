import React, { useState } from 'react'
import { Link } from 'react-router-dom'
import { useTranslation } from 'react-i18next'
import ManageRidesForm from '../../components/forms/rides/manageRidesFilterForm'
import NowListing from './nowListing'
import PreBookListing from './preBookListing'
import { filterDataObj } from '../../utilities/common'
import ApiEndPoints from '../../utilities/apiEndPoints'
import ExportCsvPdfComponent from '../../utilities/export-csv-pdf'
import { useSelector } from 'react-redux'
import { selectUserData } from '../../redux/auth/authSlice'

function ManageRides() {
  const { t } = useTranslation()
  const [showFilter, setShowFilter] = useState(true)
  const [tabActive, setTabActive] = useState(true)
  const [isFinish, setIsFinish] = useState(false)
  const [filterData, setFilterData] = useState({})
  const [fromDate, setFromDate] = useState('')
  const [toDate, setToDate] = useState('')
  // const userType = useSelector(state => state.auth.userData.userType);

  const { userType } = useSelector(selectUserData)

  const formRef = React.createRef()

  const onTabClick = (clickedTab) => {
    if (clickedTab === 'now') {
      setTabActive(true)
    }
    if (clickedTab === 'prebook') {
      setTabActive(false)
    }
  }

  const onFromDateChange = (value, dateString) => {
    setFromDate(dateString)
  }

  const onToDateChange = (value, dateString) => {
    setToDate(dateString)
  }

  const onFinish = (values) => {
    const { filterData } = filterDataObj(values)
    filterData.from_date = fromDate
    filterData.to_date = toDate
    setIsFinish(true)
    setFilterData(filterData)
  }
  const onFinishFailed = () => {
    // console.log('values')
  }
  const onReset = () => {
    setIsFinish(false)
    setFilterData({})

    formRef.current.resetFields()
  }
  return (
    <main className='maincontent setting-page'>
      <section className='page_header'>
        <div className='page_header_overlay'>
          <h2>{t('rides.manageTitle')}</h2>
        </div>
      </section>
      <section className='page_content'>
        <div className='container-fluid'>
          <div className='row'>
            <div className='col-md-12'>
              <div className='card' id='card_height'>
                <div className='card-header clearfix'>
                  <h3>{t('rides.manageTitle')}</h3>
                  <div className='action'>
                    <div className='d-inline-block addbtndiv'>
                      <Link
                        to='#'
                        className='btn btn-warning'
                        id='btnSearch'
                        onClick={() => setShowFilter((state) => !state)}
                      >
                        <i className='fa fa-search'></i>
                      </Link>
                      {userType === 'executive' ? (
                        <Link
                          className='btn btn-warning'
                          to={{ pathname: '/add-rides' }}
                        >
                          {t('rides.createNewRide')}
                        </Link>
                      ) : null}
                    </div>
                  </div>
                </div>
                {showFilter && (
                  <div className='filter_section' id='driverSearch'>
                    <div className='container-fluid'>
                      <ManageRidesForm
                        onFinish={onFinish}
                        onFinishFailed={onFinishFailed}
                        onReset={onReset}
                        formRef={formRef}
                        onFromDateChange={onFromDateChange}
                        onToDateChange={onToDateChange}
                      />
                    </div>
                  </div>
                )}

                <div className='card-block'>
                  <ul className='nav nav-tabs mb-20' id='myTab' role='tablist'>
                    <li className='nav-item'>
                      <Link
                        className={`nav-link ${tabActive ? 'active' : ''}`}
                        role='tab'
                        aria-controls='home'
                        to='#'
                        aria-selected='true'
                        onClick={() => {
                          onTabClick('now')
                        }}
                      >
                        NOW
                      </Link>
                    </li>
                    <li className='nav-item'>
                      <Link
                        className={`nav-link ${tabActive ? '' : 'active'}`}
                        data-toggle='tab'
                        to='#'
                        aria-controls='profile'
                        aria-selected='false'
                        onClick={() => {
                          onTabClick('prebook')
                        }}
                      >
                        PRE-BOOK
                      </Link>
                    </li>
                  </ul>
                  <div className='tab-content'>
                    {userType === 'admin' && (
                      <ExportCsvPdfComponent
                        filterData={filterData}
                        path={ApiEndPoints.RIDES_LIST_FILE_DOWNLOAD}
                      />
                    )}

                    {tabActive && (
                      <NowListing isFinish={isFinish} filterData={filterData} />
                    )}
                    {!tabActive && (
                      <PreBookListing
                        isFinish={isFinish}
                        filterData={filterData}
                      />
                    )}
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
    </main>
  )
}

export default ManageRides
