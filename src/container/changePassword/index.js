import React from 'react';
import PropTypes from 'prop-types';
import { Form, Input, Button } from 'antd';
import { connect } from 'react-redux';
import validation from '../../utilities/validation';
import {changePassService} from '../../services/auth';
import logger from "../../utilities/logger";
import modalNotification from '../../utilities/notifications';
// import { Link } from 'react-router-dom';
function changePassWord(props) {

  const onFinish = async (values) => {
    // console.log(values);
    try {
      let res = await changePassService(values);
      if (res && res.success) {
        formRef.current.resetFields();
        modalNotification({
          type: 'success',
          message: 'Success',
          description: res.message
        })
      }
   
    } catch (error) {
      // console.log("error",error)
      logger(error)
    }
  };

  const onFinishFailed = (errorInfo) => {
    // console.log('Failed:', errorInfo);
  };
 const formRef = React.createRef();
  return (
    <main className="maincontent settings_page">
      <section className="page_header">
        <div className="page_header_overlay">
          <h2>CHANGE PASSWORD</h2>
        </div>
      </section>
      <section className="page_content">
        <div className="container-fluid">
          <div className="row">
            <div className="col-md-12">
              <div className="card" id="card_height">
                <div className="card-header clearfix">
                  <h3>CHANGE PASSWORD</h3>
                </div>
                <div className="card-block">
                  <div className="changepassword">
                    <Form 
                    name="basic"
                    onFinish={onFinish}
                    onFinishFailed={onFinishFailed}
                    ref={formRef} >
                      <div className="row">
                        <div className="col-sm-6">
                          <div className="form-group">
                            <label >Current Password
                            <span className="errorMessage">*</span>
                            </label>
                            <Form.Item
                              name="current_password"
                              // rules={[{ required: true, message: 'Please Enter Password!' },{ min: 5, message: 'Password should have minimum 5 characters.' },{ max: 12, message: 'Password should have maximum 12 characters.' }]}
                              rules={validation.changePassword.currentPassword}
                              >
                              <Input type="password" placeholder="Current Password" id="currentPassword" />
                            </Form.Item>
                          </div>
                        </div>
                      </div>
                      <div className="row">
                        <div className="col-sm-6">
                          <div className="form-group">
                            <label >New Password
                            <span className="errorMessage">*</span>
                            </label>
                            <Form.Item
                              name="new_password"
                              rules={validation.changePassword.newPassword}>
                              <Input type="password" placeholder="New Password" />
                            </Form.Item>
                          </div>
                        </div>
                      </div>
                      <div className="row">
                        <div className="col-sm-6">
                          <div className="form-group">
                            <label >Confirm Password
                            <span className="errorMessage">*</span>
                            </label>
                            <Form.Item
                              name="confirm_password"
                              rules={validation.changePassword.confirmNewPassword}>
                              <Input type="password" placeholder="Confrim Password" />
                            </Form.Item>

                          </div>
                        </div>
                      </div>
                      <Form.Item name="button">
                        <Button className="btn btn-warning btn-pad" htmlType="submit">
                        SAVE
                         </Button>
                      </Form.Item>
                    </Form>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
    </main>
  )
}

const mapStateToProps = (state) => {
  return {
    userData: state.auth.userData,
    selectedVendorId: state.common.selectedVendor
  }
}

const mapDispatchToProps = () => {
  return {

  }
}
changePassWord.propTypes = {
  userData: PropTypes.object.isRequired
}
export default connect(mapStateToProps, mapDispatchToProps)(changePassWord)