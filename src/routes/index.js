import React, { PureComponent, Suspense, lazy } from 'react'
import { connect } from 'react-redux'
import { Switch, Router } from 'react-router-dom'
import RouteWithLayout from './routeWithLayout'
import browserHistory from '../utilities/browserHistory'
import { WithAuth } from '../utilities/withAuth'
import MainLayout from '../layouts/main'
import LoginLayout from '../layouts/login'
import FullPageLoader from '../components/loadingView/fullPageLoader'
import LoadingView from '../components/loadingView'
const NotFound = lazy(() => import('../container/notFound'))

const ManageRides = lazy(() => import('../container/manageRides/index'))
const ManageRiders = lazy(() => import('../container/manageRiders'))
const ManageExecutives = lazy(() => import('../container/manageExecutives'))
const Tracking = lazy(() => import('../container/tracking'))
const ManagePromoCode = lazy(() => import('../container/managePromoCode'))
const RidesDetails = lazy(() => import('../container/manageRides/RidesDetails'))
const RiderDetail = lazy(() => import('../container/manageRiders/riderDetail'))
const VehicleSettings = lazy(() =>
  import('../container/manageSettings/vehicleSettings')
)
const FareSettings = lazy(() =>
  import('../container/manageSettings/FareSettings')
)
const PreDefinedSettings = lazy(() =>
  import('../container/manageSettings/PreDefinedMessages')
)
const GlobalSettings = lazy(() =>
  import('../container/manageSettings/GlobalSettings')
)
const Dashboard = lazy(() => import('../container/dashboard'))
const ManageDriver = lazy(() => import('../container/manageDriver'))
const AddEditDriver = lazy(() =>
  import('../container/manageDriver/addEditDriver')
)
const DriverDetail = lazy(() =>
  import('../container/manageDriver/driverDetail')
)
const PendingDriverDetail = lazy(() =>
  import('../container/manageDriver/pendingDriverDetail')
)
const changePassWord = lazy(() => import('../container/changePassword'))
const ScrollToTop = lazy(() => import('../components/scrollToTop'))
const NetworkDetector = lazy(() => import('../components/networkDetector'))
const Login = lazy(() => import('../container/login'))
const forgetPassword = lazy(() => import('../container/forgetPassword'))
const AddEditPromoCode = lazy(() =>
  import('../container/managePromoCode/addEditPromoCode')
)
const PromoCodeDetail = lazy(() =>
  import('../container/managePromoCode/promoCodeDetail')
)
const ReportTrip = lazy(() => import('../container/report/tripData'))
const ReportCompleteRide = lazy(() =>
  import('../container/report/completeRide')
)
const ReportRejectedRide = lazy(() =>
  import('../container/report/rejectedRide')
)
const ReportDriverParticular = lazy(() =>
  import('../container/report/driverParticular')
)

const CreateRide = lazy(() => import('../container/manageRides/createRide'))

const Message = lazy(() => import('../container/messages'))

class Routes extends PureComponent {
  render() {
    const { isLoggedIn } = this.props
    return (
      <Router history={browserHistory}>
        <Suspense
          fallback={
            isLoggedIn ? (
              <MainLayout>
                <LoadingView />
              </MainLayout>
            ) : (
              <FullPageLoader />
            )
          }
        >
          <NetworkDetector />
          <ScrollToTop />
          <Switch>
            <RouteWithLayout
              component={Login}
              exact
              layout={WithAuth(LoginLayout)}
              path='/'
            />
            <RouteWithLayout
              component={forgetPassword}
              layout={LoginLayout}
              path='/forgot-password'
            />
            <RouteWithLayout
              component={RiderDetail}
              exact
              layout={WithAuth(MainLayout)}
              path='/rider/detail/:id'
            />
            <RouteWithLayout
              component={VehicleSettings}
              exact
              layout={WithAuth(MainLayout)}
              path='/settings/vehicle-settings'
            />
            <RouteWithLayout
              component={FareSettings}
              exact
              layout={WithAuth(MainLayout)}
              path='/settings/fare-settings'
            />
            <RouteWithLayout
              component={PreDefinedSettings}
              exact
              layout={WithAuth(MainLayout)}
              path='/settings/pre-defined-messages'
            />
            <RouteWithLayout
              component={GlobalSettings}
              exact
              layout={WithAuth(MainLayout)}
              path='/settings/global-settings'
            />

            <RouteWithLayout
              component={Dashboard}
              exact
              layout={WithAuth(MainLayout)}
              path='/dashboard'
            />
            <RouteWithLayout
              component={RidesDetails}
              exact
              layout={WithAuth(MainLayout)}
              path='/rides/detail/:id'
            />
            <RouteWithLayout
              component={ManageDriver}
              exact
              layout={WithAuth(MainLayout)}
              path='/drivers'
            />
            <RouteWithLayout
              component={CreateRide}
              exact
              layout={WithAuth(MainLayout)}
              path='/add-rides'
            />
            <RouteWithLayout
              component={AddEditDriver}
              exact
              layout={WithAuth(MainLayout)}
              path='/add-driver'
            />
            <RouteWithLayout
              component={ManageExecutives}
              exact
              layout={WithAuth(MainLayout)}
              path='/executives'
            />
            <RouteWithLayout
              component={ManageRides}
              exact
              layout={WithAuth(MainLayout)}
              path='/rides'
            />
            <RouteWithLayout
              component={Tracking}
              exact
              layout={WithAuth(MainLayout)}
              path='/tracking'
            />
            <RouteWithLayout
              component={ManagePromoCode}
              exact
              layout={WithAuth(MainLayout)}
              path='/promo-code'
            />
            <RouteWithLayout
              component={DriverDetail}
              exact
              layout={WithAuth(MainLayout)}
              path='/driver-detail'
            />
            <RouteWithLayout
              component={PendingDriverDetail}
              layout={WithAuth(MainLayout)}
              path='/pending-driver-detail'
              exact
            />
            <RouteWithLayout
              component={changePassWord}
              layout={WithAuth(MainLayout)}
              path='/change-password'
              exact
            />
            <RouteWithLayout
              component={changePassWord}
              exact
              layout={WithAuth(MainLayout)}
              path='/trip-data'
            />
            <RouteWithLayout
              component={ManageRiders}
              layout={WithAuth(MainLayout)}
              path='/riders'
              exact
            />
            <RouteWithLayout
              component={PromoCodeDetail}
              layout={WithAuth(MainLayout)}
              path='/promo-code/detail/:id'
              exact
            />
            <RouteWithLayout
              component={ReportTrip}
              layout={WithAuth(MainLayout)}
              path='/trip/data'
              exact
            />
            <RouteWithLayout
              component={ReportCompleteRide}
              layout={WithAuth(MainLayout)}
              path='/completed/ride'
              exact
            />
            <RouteWithLayout
              component={ReportRejectedRide}
              layout={WithAuth(MainLayout)}
              path='/rejected/ride'
              exact
            />
            <RouteWithLayout
              component={ReportDriverParticular}
              layout={WithAuth(MainLayout)}
              path='/particular/ride'
              exact
            />
            <RouteWithLayout
              component={AddEditPromoCode}
              layout={WithAuth(MainLayout)}
              path='/add-promo-code'
              exact
            />
            <RouteWithLayout
              component={Message}
              layout={WithAuth(MainLayout)}
              path='/mass-notification'
              exact
            />
            <RouteWithLayout
              component={NotFound}
              layout={WithAuth(MainLayout)}
              path='*'
              exact
            />
          </Switch>
        </Suspense>
      </Router>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    isLoggedIn: state.auth.isLoggedIn
  }
}

const mapDispatchToProps = (dispatch) => {
  return {}
}

export default connect(mapStateToProps, mapDispatchToProps)(Routes)
