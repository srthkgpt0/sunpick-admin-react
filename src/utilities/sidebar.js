export const routesJSON = (t = (arg) => arg, permissions = []) => [
  {
    endPoint: '/dashboard',
    authRequire: true,
    addInSideBar: true,
    active: ['/dashboard'],
    title: t('dashboard.title'),
    icon: 'ti-pie-chart icon',
    userTypes: ['admin', 'executive'],
    moduleKey: 'dashboard'
  },
  {
    endPoint: '/executives',
    authRequire: true,
    addInSideBar: true,
    active: ['/executives'],
    title: t('executives.title'),
    icon: 'fa fa-user icon',
    userTypes: ['admin'],
    moduleKey: 'executives'
  },

  {
    endPoint: '/drivers',
    authRequire: true,
    addInSideBar: true,
    active: ['/drivers', '/add-driver', '/driver-detail'],
    title: t('drivers.title'),
    icon: 'ti-car icon',
    userTypes: checkPermission(permissions, 'drivers'),
    moduleKey: 'drivers'
  },
  {
    endPoint: '/add-driver',
    authRequire: true,
    addInSideBar: false,
    active: [],
    title: '',
    icon: '',
    userTypes: ['admin'],
    moduleKey: ''
  },
  {
    endPoint: '/add-rides',
    authRequire: true,
    addInSideBar: false,
    active: ['/add-rides'],
    title: t('settings.fareSettings'),
    icon: '',
    userTypes: ['admin', 'executive'],
    moduleKey: ''
  },
  {
    endPoint: '/riders',
    authRequire: true,
    addInSideBar: true,
    active: ['/riders', '/rider/detail/:id'],
    title: t('riders.title'),
    icon: 'ti-user icon',
    userTypes: checkPermission(permissions, 'riders'),
    moduleKey: 'riders'
  },
  {
    endPoint: '/rides',
    authRequire: true,
    addInSideBar: true,
    active: ['/rides', '/rides/detail/:id'],
    title: t('rides.title'),
    icon: 'fa fa-road icon',
    userTypes: checkPermission(permissions, 'rides'),
    moduleKey: 'rides'
  },
  {
    endPoint: '/add-promo-code',
    authRequire: true,
    addInSideBar: false,
    active: ['/add-promo-code'],
    title: '',
    icon: 'fa fa-road icon',
    userTypes: ['admin', 'executive'],
    moduleKey: ''
  },
  {
    endPoint: '/tracking',
    authRequire: true,
    addInSideBar: true,
    active: ['/tracking'],
    title: t('tracking.title'),
    icon: 'fa fa-bullseye icon',
    userTypes: checkPermission(permissions, 'tracking'),
    moduleKey: 'tracking'
  },

  {
    endPoint: '/settings',
    authRequire: true,
    addInSideBar: true,
    active: [
      '/settings',
      '/settings/vehicle-settings',
      '/settings/fare-settings',
      '/settings/pre-defined-messages',
      '/settings/global-settings'
    ],
    title: t('settings.title'),
    icon: 'ti-settings icon',
    userTypes: ['admin'],
    moduleKey: 'settings',
    child: [
      {
        endPoint: '/settings/vehicle-settings',
        authRequire: true,
        addInSideBar: true,
        active: ['/settings/vehicle-settings'],
        title: t('settings.vehicleSettings'),
        icon: '',
        userTypes: ['admin', 'executive'],
        moduleKey: ''
      },
      {
        endPoint: '/settings/fare-settings',
        authRequire: true,
        addInSideBar: true,
        active: ['/settings/fare-settings'],
        title: t('settings.fareSettings'),
        icon: '',
        userTypes: ['admin', 'executive'],
        moduleKey: ''
      },
      {
        endPoint: '/settings/pre-defined-messages',
        authRequire: true,
        addInSideBar: true,
        active: ['/settings/pre-defined-messages'],
        title: t('settings.preDefinedMessages'),
        icon: '',
        userTypes: ['admin', 'executive'],
        moduleKey: ''
      },
      {
        endPoint: '/settings/global-settings',
        authRequire: true,
        addInSideBar: true,
        active: ['/settings/global-settings'],
        title: t('settings.globalSettings'),
        icon: '',
        userTypes: ['admin', 'executive'],
        moduleKey: ''
      }
    ]
  },
  {
    endPoint: '/promo-code',
    authRequire: true,
    addInSideBar: true,
    active: ['/promo-code', '/add-promo-code','/promo-code/detail/:id'],
    title: t('promoCode.title'),
    icon: 'ti-gift icon',
    userTypes: ['admin'],
    moduleKey: 'promo-code'
  },
  {
    endPoint: '/add-promo-code',
    authRequire: true,
    addInSideBar: false,
    active: ['/promo-code', '/add-promo-code'],
    title: t('promoCode.title'),
    icon: 'ti-gift icon',
    userTypes: ['admin'],
    moduleKey: 'promo-code'
  },
  {
    endPoint: '/promo-code/detail/:id',
    authRequire: true,
    addInSideBar: false,
    active: ['/promo-code', '/add-promo-code','/promo-code/detail/:id'],
    title: t('promoCode.title'),
    icon: 'ti-gift icon',
    userTypes: ['admin'],
    moduleKey: 'promo-code'
  },
  {
    endPoint: '/rider/detail/:id',
    authRequire: true,
    addInSideBar: false,
    active: ['/riders'],
    title: t('promoCode.title'),
    icon: 'ti-gift icon',
    userTypes: checkPermission(permissions, 'riders'),
    moduleKey: 'riders'
  },

  {
    endPoint: '#',
    authRequire: true,
    addInSideBar: true,
    active: [
      '/reports',
      '/trip/data',
      '/completed/ride',
      '/rejected/ride',
      '/particular/ride'
    ],
    title: t('reports.title'),
    icon: 'ti-file icon',
    userTypes: ['admin', 'executive'],
    moduleKey: '',
    child: [
      {
        endPoint: '/trip/data',
        authRequire: true,
        addInSideBar: true,
        active: ['/trip-data'],
        title: t('reports.tripData'),
        icon: '',
        userTypes: ['admin'],
        moduleKey: ''
      },
      {
        endPoint: '/completed/ride',
        authRequire: true,
        addInSideBar: true,
        active: ['/completed/ride'],
        title: t('reports.completedRides'),
        icon: '',
        userTypes: ['admin', 'executive'],
        moduleKey: ''
      },
      {
        endPoint: '/rejected/ride',
        authRequire: true,
        addInSideBar: true,
        active: ['/rejected/ride'],
        title: t('reports.rejectedRides'),
        icon: '',
        userTypes: ['admin'],
        moduleKey: ''
      },
      {
        endPoint: '/particular/ride',
        authRequire: true,
        addInSideBar: true,
        active: ['/particular/ride'],
        title: t('reports.driverParticulars'),
        icon: '',
        userTypes: ['admin'],
        moduleKey: ''
      }
    ]
  },
  {
    endPoint: '/messages',
    authRequire: true,
    addInSideBar: true,
    active: ['/messages', '/mass-notification'],
    title: t('messages.title'),
    icon: 'ti-email icon',
    userTypes: ['admin'],
    moduleKey: 'messages',
    child: [
      {
        endPoint: '/mass-notification',
        authRequire: true,
        addInSideBar: true,
        active: ['/mass-notification'],
        title: t('messages.massNotification'),
        icon: '',
        userTypes: ['admin', 'executive'],
        moduleKey: ''
      }
    ]
  },
  {
    endPoint: '/change-password',
    authRequire: true,
    addInSideBar: false,
    active: ['/change-password'],
    title: t('user.title'),
    icon: 'icon-total-user',
    userTypes: checkPermission(permissions, 'zingler'),
    moduleKey: 'zingler'
  },

  {
    endPoint: '/driver-detail',
    authRequire: true,
    addInSideBar: false,
    active: ['/driver-detail'],
    title: t('role.roleTitle'),
    icon: 'icon-manage-use02',
    userTypes: ['admin'],
    moduleKey: 'role'
  },
  {
    endPoint: '/rides/detail/:id',
    authRequire: true,
    addInSideBar: false,
    active: ['/rides/detail/:id'],
    title: '',
    icon: '',
    userTypes: checkPermission(permissions, 'rides'),
    moduleKey: 'role'
  },
  {
    endPoint: '/pending-driver-detail',
    authRequire: true,
    addInSideBar: false,
    active: ['/pending-driver-detail'],
    title: 'driver detail',
    icon: '',
    userTypes: checkPermission(permissions, 'role'),
    moduleKey: ''
  },

  // {
  //   endPoint: '/riders',
  //   authRequire: true,
  //   addInSideBar: false,
  //   active: ['/riders', '/rider/detail/:id'],
  //   title: 'Manage Riders',
  //   icon: '',
  //   userTypes: ['admin', 'executives'],
  //   moduleKey: 'riders'
  // },
  {
    endPoint: '/',
    authRequire: false,
    addInSideBar: false,
    active: ['/'],
    title: t('category.title'),
    icon: 'icon-manage-category',
    userTypes: checkPermission(permissions, 'category'),
    moduleKey: 'category'
  }
]

const checkPermission = (permissions, moduleKey) => {
  if (permissions) {
    let moduleKeyIndex = permissions.findIndex((item) => {
      return moduleKey === 'send_notification'
        ? item.moduleKey === moduleKey && item.permission === 'edit'
        : item.moduleKey === moduleKey
    })
    if (moduleKeyIndex > -1) {
      return ['admin', 'executive']
    }
  }
  return ['admin']
}
export const redirectPathIfRequireAuthFails = '/'

export const redirectPathIfNotRequireAuthFails = [
  {
    path: '/dashboard',
    userTypes: ['admin', 'executive']
  }
]
