import React, { useEffect } from 'react'
import { connect } from 'react-redux'
import { getFile } from '../services/common'
import logger from '../utilities/logger'
import { Link } from 'react-router-dom'

const ExportCsvPdfComponent = (props) => {
  // const history = props.history
  useEffect(() => {}, []) // eslint-disable-line react-hooks/exhaustive-deps

  const generateQueryString = (type, queryParams) => {
    const queryString = new URLSearchParams(queryParams)
    return queryString.toString()
  }
  const getCSV = async () => {
    try {
      const type = 'csv'
      const queryParams = generateQueryString(type, props.queryParams)
      await getFile(props.path.url + '/' + type, queryParams)
    } catch (error) {
      logger({ 'error:': error })
    }
  }

  return (
    <div className='action-btn'>
      Export via :
      <div className='d-inline-block addbtndiv'>
        <Link
          className='btn btn-warning'
          to='/'
          onClick={(e) => {
            e.preventDefault()
            getCSV()
          }}
        >
          <i className='fa fa-file-excel-o' aria-hidden='true'></i> CSV
        </Link>
      </div>
    </div>
  )
}

const mapStateToProps = (state) => {
  return {
    userData: state.auth.userData,
    selectedVendorId: state.common.selectedVendor
  }
}

const mapDispatchToProps = () => {
  return {}
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ExportCsvPdfComponent)
