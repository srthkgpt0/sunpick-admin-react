// import config from '../../config'

const enValidationMsg = {
  // Name
  NAME_MIN_LENGTH: (minLength) =>
    `Name should have minimum ${minLength} characters`,
  NAME_MAX_LENGTH: (maxLength) => `Name can't exceed ${maxLength} characters`,
  // First Name
  FIRST_NAME_REQUIRED: 'First name required',
  FIRST_NAME_MIN_LENGTH: (minLength) =>
    `First name should have minimum ${minLength} characters`,
  FIRST_NAME_MAX_LENGTH: (maxLength) =>
    `First name can't exceed ${maxLength} characters`,
  FIRST_NAME_ONLY_CHARS: 'First name can only have alphabets',

  // Last Name
  // Last name field
  LAST_NAME_REQUIRED: 'Last name required',
  LAST_NAME_MIN_LENGTH: (minLength) =>
    `Last name should have minimum ${minLength} characters`,
  LAST_NAME_MAX_LENGHT: (maxLength) =>
    `Last name can't exceed ${maxLength} characters`,
  LAST_NAME_ONLY_CHARS: 'Last name can only have alphabets',

  // UPload profile image
  uploadImage: 'Image required',
  //Settings
  CATEGORY_NAME_REQUIRED: 'Category Name Required',
  CAPACITY_REQUIRED: 'Capacity Required',
  USER_TYPE: 'User Type required',
  TEXT_TEMPLATE: 'Text Template required',
  SEQUENCE_NUMBER: 'Sequence Number required',
  MESSAGE_TYPE: 'Message Type Required',
  // cell number
  CELL_NUMBER_REQUIRED: 'Mobile number required',
  CELL_NUMBER_CODE: 'Mobile number Country Code required',
  CELL_NUMBER_MIN_LENGTH: (minLength) =>
    `Mobile number should have minimum ${minLength} numbers`,
  CELL_NUMBER_MAX_LENGTH: (maxLength) =>
    `Mobile number can't exceed ${maxLength} numbers`,
  CELL_NUMBER_PATTERN: 'Mobile number can only have numbers',

  // query params

  DRIVER_ID_MIN_LENGTH: (minLength) =>
    `Driver id should have minimum ${minLength} numbers`,
  DRIVER_ID_MAX_LENGTH: (maxLength) =>
    `Driver id number can't exceed ${maxLength} numbers`,
  DRIVER_ID_NUMBER_PATTERN: 'Driver id can only have numbers',

  RIDE_ID_MIN_LENGTH: (minLength) =>
    `Ride id should have minimum ${minLength} numbers`,
  RIDE_ID_MAX_LENGTH: (maxLength) =>
    `Ride id number can't exceed ${maxLength} numbers`,
  RIDE_ID_NUMBER_PATTERN: 'Ride id can only have numbers',

  // vehicle year
  VEHICLE_YEAR_REQUIRED: 'Vehicle year required',
  VEHICLE_YEAR_PATTERN: 'Vehicle year can only have numbers',

  // Vehicle plate Number
  VEHICLE_PLATE_NUMBER_REQUIRED: ' Vehicle Registration Number required',
  VEHICLE_PLATE_NUMBER_MIN_LENGTH: (minLength) =>
    ` Vehicle Registration Number should have minimum ${minLength} numbers`,
  VEHICLE_PLATE_NUMBER_MAX_LENGTH: (maxLength) =>
    ` Vehicle Registration Number can't exceed ${maxLength} numbers`,
  VEHICLE_PLATE_NUMBER_PATTERN:
    'Vehicle Registration Number can only have numbers',
  VEHICLE_MAKE_REQUIRED: 'Vehicle Make required',

  // EMERGENCY number
  EMERGENCY_NUMBER_REQUIRED: 'Emergency number required',
  EMERGENCY_NUMBER_MIN_LENGTH: (minLength) =>
    `Emergency number should have minimum ${minLength} numbers`,
  EMERGENCY_NUMBER_MAX_LENGTH: (maxLength) =>
    `Emergency number can't exceed ${maxLength} numbers`,
  EMERGENCY_NUMBER_PATTERN: 'Emergency number can only have numbers',

  // DOB
  DOB_REQUIRED: 'Date of birth required',

  // BLOOD GROUP
  BLOOD_REQUIRED: 'Blood group required',

  ADDRESS_REQUIRED: 'Address required',

  STATE_REQUIRED: 'State required',

  CITY_REQUIRED: 'City required',

  POSTAL_CODE_REQUIRED: 'Postal Code required',
  // Emergency Number
  EMERGENCY_REQUIRED: 'Emergency number required',

  // vehicle model
  VEHICLE_MODEL_REQUIRED: 'Vehicle Model required',
  VEHICLE_CATEGORY_REQUIRED: 'Vehicle category required',

  // Profile image
  PROFILE_IMAGE_REQUIRED: 'Profile image required',

  // Insurance image
  INSURANCE_IMAGE_REQUIRED: 'Insurance image required',

  EXECUTIVE_IMAGE_REQUIRED: 'Executive image required',

  // License front image
  LICENSE_FRONT_IMAGE_REQUIRED: 'License front image required',

  // License back image
  LICENSE_BACK_IMAGE_REQUIRED: 'License back image required',

  // nric front image
  NRIC_FRONT_IMAGE_REQUIRED: 'Aadhar front image required',
  OWNER_TIN_REQUIRED: 'Owner TIN image required',

  // nric back image
  NRIC_BACK_IMAGE_REQUIRED: 'Aadhar back image required',

  // rental agreement image
  RENTAL_AGREEMENT_FRONT_IMAGE_REQUIRED:
    'Rental agreement front image required',
  Employment_AGREEMENT_FRONT_IMAGE_REQUIRED:
    'Driver Employment Agreement image required',
  RENTAL_AGREEMENT_BACK_IMAGE_REQUIRED: 'Rental agreement back image required',

  // vocational license image
  VOCATIONAL_LICENSE_IMAGE_REQUIRED: 'PAN card image required',

  // vehicle registration image
  VEHICLE_REGISTRATION_IMAGE_REQUIRED: 'Vehicle registration image required',
  /**
   * Login
   */
  enterEmail: 'Enter an email',
  enterValidEmail: 'Enter a valid email',
  enterPassword: 'Enter a password',
  loginSuccessfully: 'You login successfully',
  loginError: 'Incorrect email or password',
  /**
   * Logout
   */
  logoutSuccessfully: 'You logout successfully',
  /**
   * Reset Password
   */
  passwordSuccess: 'Password is updated successfully',
  passwordLengthMessage: (min, max) =>
    `Use ${min}-${max} characters for your password`,
  passwordNotMatchMessage: 'New Password and Confirm Password does not match',
  reEnterPassword: 'Re-enter the password',
  /**
   * Change Password
   */
  currentPasswordError: 'Current password is wrong',
  passwordChangeSuccess: 'You changed password successfully',
  enterCurrentPassword: 'Enter current password',
  enterNewPassword: 'Enter new password',
  reEnterNewPassword: 'Re-enter the new password',
  /**
  /**
   * Promo Code
   */
  promoTitle: 'Enter Title',
  promoDiscountType: 'Please Select Discount Type',
  promoDiscountPercent: 'Enter Discount Percent',
  maximumDiscount: 'Enter Maximum Discount',
  limitPerUser: 'Enter Limit Per User',
  startDate: 'Select Start Date',
  endDate: 'Select End Date',
  couponCode: 'Enter Coupon Code',
  termCondition: 'Enter Terms And Condition',

  /**
   * create Ride
   */
  travelerName: 'Enter Traveler Name',
  travelerCountryCode: 'Traveler Number Country Code Required',
  travelerMobileNo: 'Traveler Mobile  required',
  pickupLocation: 'Pick Up location required',
  dropLocation: 'Drop Location  required ',
  selectPassenger: 'Passenger should be more than 0',
  availableDriver: 'Please select driver',
  /**
  /**
   * Update Profile
   */
  firstName: 'Enter first Name',
  lastName: 'Enter last Name',
  selectIcon: 'Select profile picture',
  profileSuccessfully: 'Your profile updated successfully',
  /**
   * Reply Message
   */
  enterResponse: 'Enter message',
  status: 'please select the status',
  message: 'please enter the message',

  /**
   * FAQ
   */
  addFAQ: 'New FAQ added successfully',
  updateFAQ: 'FAQ updated successfully',
  deleteFAQ: 'FAQ deleted successfully',
  enterFAQquestion: 'Enter question',
  enterFAQanswer: 'Enter answer',

  selectKycStatus: 'Select KYC Status',
  reason: 'Enter reason',

  /**
   * Upload file
   */
  fileUploadSuccess: (fileName) => `${fileName} upload successfully`,
  fileUploadFail: (fileName) => `${fileName} upload failed`,
  filesAllowed: (files) => `Only this ${files} file type are allowed`,
  fileSizeLimit: (size) => `File should be upto ${size} MB`,
  fileAudioLengthLimit: ({ minLength, maxLength }) =>
    `Audio file should be between ${minLength}-${maxLength} seconds in length`,
  importExcel: 'Excel is Required',
  /**
   * Common
   */
  statusUpdate: 'Status updated successfully',
  enterMinChar: (val) => `Enter minimun ${val} characters`,
  enterMaxChar: (val) => `Maximum ${val} characters allowed`,
  alphaNumericOnly: 'Only alphanumeric character allowed',
  numericOnly: 'Only numeric value allowed',

  orderLengthMessage: (min, max) => `Order must be between ${min}-${max}`,
  /**
   * No Data Message
   */
  notAvailable: 'Not Available',
  noDataFound: 'No record found',
  noDataUserList: 'There is no user registered in the system',
  noDataMerchantList: 'There is no merchant registered in the system',
  noDataTransactionList: 'No transaction found',
  noDataPaymentRequestList: 'No payment request found',
  noDataFaqList: 'Click on the "Add FAQ" button to add FAQ',
  /**
   * Update Settings
   */
  twilioAccountSid: 'Enter twilio account Id',
  twilioAuthToken: 'Enter twilio auth Token',
  twilioFromNumber: 'Enter twilio from Number',
  smtpEmailFromName: 'Enter SMTP email from name',
  smtpEmailFromEmail: 'Enter SMTP email from Email',
  nonKycUserTransactionLimit: 'Enter non-kyc user transaction limit',
  kycUserTransactionLimit: 'Enter kyc user transaction limit',
  transactionLimitDays: 'Enter transaction limit days',
  smtpHostd: 'Enter SMTP Host',
  smtpPort: 'Enter SMTP Port',
  smtpUsername: 'Enter SMTP Username',
  smtpPassword: 'Enter SMTP Password',
  portLength: 'SMPT port should be 3 digits'
}

export default enValidationMsg

// import config from '../../config'

// const enValidationMsg = {
//   /**
//    * Login
//    */
//   enterEmail: 'Enter an email',
//   enterValidEmail: 'Enter a valid email',
//   enterPassword: 'Enter a password',
//   loginSuccessfully: 'You login successfully',
//   loginError: 'Incorrect email or password',
//   /**
//    * Logout
//    */
//   logoutSuccessfully: 'You logout successfully',
//   /**
//    * Reset Password
//    */
//   passwordSuccess: 'Password is updated successfully',
//   passwordLengthMessage: (min, max) => `Use ${min}-${max} characters for your password`,
//   passwordNotMatchMessage: 'New Password and Confirm Password does not match',
//   reEnterPassword: 'Re-enter the password',
//   /**
//    * Change Password
//    */
//   currentPasswordError: 'Current password is wrong',
//   passwordChangeSuccess: 'You changed password successfully',
//   enterCurrentPassword: 'Enter current password',
//   enterNewPassword: 'Enter new password',
//   reEnterNewPassword: 'Re-enter the new password',
//   /**
//    * Approver
//    */
//   enterApproverName: 'Enter approver name',
//   addApproverMessage: 'Approver added successfully',
//   updateApproverMessage: 'Approver updated successfully',
//   selectUserRole: 'Select user type',
//   selectRole: 'Select role',
//   /**
//    * Category
//    */
//   enterCategoryName: 'Enter category name',
//   addCategoryMessage: 'Category added successfully',
//   updateCategoryMessage: 'Category updated successfully',
//   selectCategoryIcon: 'Select catgeory icon',
//   /**
//    * Product
//    */
//   enterProductTitle: 'Enter product title',
//   enterProductDescription: 'Enter product description',
//   enterProductCatId: 'Select Product Category',
//   enterProductConId: 'Select Product Condition',
//   enterProductAvailability: 'Enter Product Availability',
//   enterProductBrand: 'Enter product brand',
//   enterProductPrice: 'Enter product price',
//   enterProductCity: 'Enter city',
//   enterProductState: 'Enter state',
//   enterProductCountry: 'Enter country',
//   addProductMessage: 'Product added successfully',
//   updateProductMessage: 'Product updated successfully',
//   selectProductIcon: 'Select product icon',
//   availabilityNumberOnly: "Availability can only have numbers",
//   /**
//    * Collection
//    */
//   enterCollectionName: 'Enter collection name',
//   addCollectionMessage: 'Collection added successfully',
//   updateCollectionMessage: 'Collection updated successfully',
//   enterCollectionCurrentOrder: 'Enter collection current order',
//   enterCollectionNewOrder: 'Enter collection new order',
//   updateOrderCollectionMessage: 'Collection order updated successfully',
//   selectCollection: 'Select Collection',
//   collectionAssigned: 'Collection assigned to audio successfully',
//   /**
//    * Artist
//    */
//   enterArtistName: 'Enter artist name',
//   selectArtistIcon: 'Select artist icon',
//   addArtistMessage: 'Artist added successfully',
//   updateArtistMessage: 'Artist updated successfully',
//   /**
//    * Tag
//    */
//   enterTagName: 'Enter tag title',
//   validTagMessage: '# at starting and spaces in words not allowed',
//   addTagMessage: 'New tag added successfully',
//   updateTagMessage: 'Tag updated successfully',
//   /**
//    * Points
//    */
//   updatePointsMessage: 'Points updated successfully',
//   enterPoint: 'Enter points',
//   pointValueMessage: 'Only numbers are allowed',
//   /**
//    * Settings
//    */
//   updateSettingsMessage: 'Settings updated successfully',
//   enterSetting: 'Enter Setting value',
//   /**
//    * CMS
//    */
//   updateCMSMessage: 'CMS updated successfully',
//   enterCMSTitle: 'Enter CMS title',
//   enterCMSContent: 'Enter CMS content',
//   /**
//    * FAQ
//    */
//   addFAQ: 'New FAQ added successfully',
//   updateFAQ: 'FAQ updated successfully',
//   deleteFAQ: 'FAQ deleted successfully',
//   enterFAQquestion: 'Enter question',
//   enterFAQanswer: 'Enter answer',
//   /**
//    * ROLE
//    */
//   addRole: 'New Role added successfully',
//   updateRole: 'Role updated successfully',
//   enterRole: 'Enter Role',
//   /**
//    * Album
//    */
//   addAlbumMessage: 'Album added successfully',
//   updateAlbumMessage: 'Album updated successfully',
//   enterAlbumName: 'Enter name',
//   selectAlbumIcon: 'Select album icon',
//   selectAlbumCategory: 'Select category',
//   /**
//    * Track
//    */
//   addTrackMessage: 'Audio added successfully',
//   updateTrackMessage: 'Audio updated successfully',
//   enterTrackLength: 'Enter Audio length',
//   enterTrackName: 'Enter name',
//   selectTrackIcon: 'Select Audio file',
//   selectTrackArtist: 'Select artist',
//   /**
//    * Contest
//    */
//   addContestMessage: 'Contest added successfully',
//   updateContestMessage: 'Contest updated successfully',
//   enterContestName: 'Enter contest name',
//   enterContestRules: 'Enter contest Rules',
//   enterContestWinnerPrizeTitle: 'Enter prize name',
//   enterContestWinnerPrizeCorrectTitle: 'Enter Correct prize name',
//   selectContestPrizeImage: 'Select Contest prize image',
//   selectContestContestImage: 'Select Contest image',
//   enterCreateAt: 'Select Contest start date and end date',
//   entervideoIntervalLimit: 'Enter valid video interval limit',
//   enterTagId: 'Select hastag name',
//   /**
//    * Preference
//    */
//   selectLanguage: 'Select Language',
//   selectCategory: 'Select Category',
//   /**
//    * Interval
//    */
//   enterIntervalTime: 'Enter interval time in seconds',
//   timeIntervalError: 'Time must be greater than 0',
//   enterIntervalTimeFollowers: 'Enter followers in numeric value',
//   followerIntervalError: 'Follower value should in the numeric',
//   timeIntervalMustInError: `Time must be in the multiple of ${config.VIDEO_UPLOAD_LENGTH_MINIMUM}`,
//   timeIntervalMaxError: (time) => `Max time ${time} sec is allowed`,
//   addIntervalMessage: 'Interval added successfully',
//   updateIntervalMessage: 'Interval updated successfully',
//   deleteInterval: 'Interval deleted successfully',
//   helpingIntervalMessage: `NOTE: Enter interval in multiple of ${config.VIDEO_UPLOAD_LENGTH_MINIMUM}`,
//   /**
//    * Reply Message
//    */
//   enterResponse: 'Enter message',
//   /**
//    * Notification
//    */
//   enterNotificationTitle: 'Enter title',
//   enterNotificationMessage: 'Enter message',
//   sentNotificationSuccessfully: 'Notification sent successfully',
//   /**
//    * Upload file
//    */
//   fileUploadSuccess: (fileName) => `${fileName} upload successfully`,
//   fileUploadFail: (fileName) => `${fileName} upload failed`,
//   filesAllowed: (files) => `Only this ${files} file type are allowed`,
//   fileSizeLimit: (size) => `File should be upto ${size} MB`,
//   fileAudioLengthLimit: ({ minLength, maxLength }) => `Audio file should be between ${minLength}-${maxLength} seconds in length`,
//   fileCsvLength: `Empty Csv file not allow`,
//   /**
//    * Common
//    */
//   statusUpdate: 'Status updated successfully',
//   enterMinChar: (val) => `Enter minimun ${val} characters`,
//   enterMaxChar: (val) => `Maximum ${val} characters allowed`,
//   enterMinNumber: (val) => `Minimun ${val} numbers allowed`,
//   enterMaxNumber: (val) => `Maximum ${val} numbers allowed`,
//   alphaNumericOnly: 'Only alphanumeric character allowed',
//   orderLengthMessage: (min, max) => `Order must be between ${min}-${max}`,
//   /**
//    * No Data Message
//    */
//   notAvailable: 'Not Available',
//   noDataFound: 'No record found',
//   noDataUserList: 'There is no user registered in the system',
//   noDataApproverList: 'Click on the "Add Approver" button to add an approver',
//   noDataCategoryList: 'Click on the "Add Category" button to add a category',
//   noDataCollectionList: 'Click on the "Add Collection" button to add a collection',
//   noDataArtistList: 'Click on the "Add Artist" button to add an artist',
//   noDataAlbumList: 'Click on the "Add Album" button to add an album',
//   noDataTrackList: 'Click on the "Add Audio" button to add an audio',
//   noDataTagList: 'Click on the "Add Hashtag" button to add a hashtag',
//   noDataIntervalList: 'Click on the "Add Interval" button to add an interval',
//   noDataFaqList: 'Click on the "Add FAQ" button to add FAQ',
//   noDataContestList: 'Click on the "Add Contest" button to add an contest',
//   noDataRoleList: 'Click on the "Add Role" button to add an role',
// }
