

export const notification_msg = {
  // Start : driver Section//
  DRIVER_ADD_SUCCESS: "Driver details added successfully",
  RIDE_ADD_SUCCESS: "Ride added successfully",
  DRIVER_UPDATE_SUCCESS: "Driver details updated successfully",
  // End : driver Section//

  // Start : rider section //
  RIDER_UPDATE_SUCCESS: "Rider details updated successfully",
  // End : rider Section

  //Start : promo code section
  FIRST_TIME_RIDE_ADD_SUCCESS: "First time ride promo code added successfully",
  FIRST_TIME_RIDE_UPDATE_SUCCESS: "First time ride promo code updated successfully",
  HAPPY_HOUR_DISCOUNT_ADD_SUCCESS: "Happy hour discount promo code added successfully",
  HAPPY_HOUR_DISCOUNT_UPDATE_SUCCESS: "Happy hour discount promo code updated successfully",
  HAPPY_DAY_DISCOUNT_ADD_SUCCESS: "Happy day discount promo code added successfully",
  HAPPY_DAY_DISCOUNT_UPDATE_SUCCESS: "Happy day discount promo code updated successfully",
  // End: promo code section

  // Start: setting section
  CATEGORY_ADD_SUCCESS: "Category added successfully",
  CATEGORY_UPDATE_SUCCESS: "Category updated successfully",

  MODEL_ADD_SUCCESS: "Model added successfully",
  MODEL_UPDATE_SUCCESS: "Model updated successfully",

  MAKE_ADD_SUCCESS: "Make added successfully",
  MAKE_UPDATE_SUCCESS: "Make updated successfully",

  SETTING_UPDATE_SUCCESS: "settings updated successfully",
  SETTING_UPDATE_ALERT: "Are you sure want to update setting ?",

  FARE_SETTING_UPDATE_SUCCESS: "Fare settings updated successfully",
  FARE_SETTING_UPDATE_ALERT: "Are you sure want to update setting ?",

  //End: setting section

  // Start: change password
  PASSWORD_UPDATE_SUCCESS: "Password changed successfully",
  //EndL change password

  TEMPLATE_UPDATE_SUCCESS: "Email template updated successfully",

  SERVER_NOT_RESPONDING: "Server not responding",

  FILL_ALL_DETAILS: "Please fill all the necessary details",
  UNAUTHORIZED_ACCESS: "Unauthorized Access",

  MASS_EMAIL_SENT_SUCCUSS: "Email sent successfully",
  MASS_EMAIL_NOT_SENT_SUCCUSS: "Email not sent successfully",

  MASS_NOTIFICATION_SENT_SUCCUSS: "Notification sent successfully",

  // Template

  TEXT_TEMPLATE_ADD_SUCCESS: "Text Template added successfully",
  TEXT_TEMPLATE_UPDATE_SUCCESS: "Text Template updated successfully"
};