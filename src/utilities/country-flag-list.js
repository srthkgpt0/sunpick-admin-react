export const default_const_flag = {
  CountryflagData: [
    {
      key: "ET",
      currency: "ETB",
      callingCode: "+251",
      flag:
        "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACgAAAAeCAMAAABpA6zvAAAABGdBTUEAALGPC/xhBQAAAAFzUkdCAK7OHOkAAAAgY0hSTQAAeiYAAICEAAD6AAAAgOgAAHUwAADqYAAAOpgAABdwnLpRPAAAAqxQTFRFKYwIKIwLKYwJKIwKLI0ILY0IK4wKLI4AK44AK40CGogGF4cIH4oDKo0GHIFFD3eDBnCiB3GfD3eAHoM+K40DHYkEGIcIiqYTh6UVk6oOcJ0hCnOWBmjVAGbYLHizInW6AGfWAWfWC3SQeaEakakQiKYV/8oj/sok/8sg9ccpMXu0F3K+b5N9AGHWZJCCdJZ3AF/aV4mOJHe3PoCr/soj/8si/MUh+8Uh+MQj/8sWaI+CAF/ZAGTRWImPPH+iip1rjJ1rInW1bZJ/AGnHAF7bfJd0/8sV/8Yh/sYi/8gd6L4vEW++L3ynNn6jL3uofJh0k6FknqZae5h1NX6kGHS2LHuqHXO39cMn/8cf/MUj/8sYu61PAGbQToeTvK5MnqNgsKxRdJV6jJ1slqFloaVexrJGRoSYAGfOzrRC/8oa/cUj+8Uj/8wWqaZbAGXQAGDaH3O4laFlmqNhCmjMFGnKu69Kh5xtF2+/AGLXAGjMvK1O/8sXuaxQAGrIZpCEZY+FRoOavrJFcZN9g5lypKhXTIaVX42JOX+hAGnK/8Uh/sUi/8ceLnuoZo+Fl6Jjp6ZasqtToKReUYaUGXS2SoWWJHWy9cIm/8Yf/sUh/8ch/MYk/84Wb5OBAGDbA2rEoKZad5Z5P4GfKHivmqJijp5qAGXMAF/dg5ty/84V/8Eh/sEh/8If8b4lLHmtDHPGU4ePAF3bWYyHM32jAGDUVYmOB3LKPH6g+sEg/8Ig8TQZ7zMb+DUU4DQkP1WYAG3WAG3XPIakIXy3AG7WAGvXTFGP6TMe9jUW7zMa7x0Y7x0X7Bwa9BwU9x8SqzdKW0+ENVifOFidYE6AszVE+h4Q8hwV7BwZ7yIY7SIZ7iEZ/B0O/hwM+B8S+B4R/xwM+x0P7iIZ7yEY7CIa6yIb////ZbFmDQAAAAFiS0dE47EGrooAAAAJcEhZcwAAAEgAAABIAEbJaz4AAAFqSURBVDjLY2AYBaQARkYmJkZmLBIsyICVjYGdg5OTg4OJjZUFFTBwIQNuHl4+fgFBIWERUTEuVMAgjgQkJKWkZWTl5BUUlZRVVMVRAIMaAqhraGpp6+jq6RsYGhmbmJqoIQMGMzgwt7C0sraxtbN3cHRydnF1szA3QwIM7nDg4enl7ePr5x8QGBQcEhoWHuHhjgSQFEZGRcfExsUnJCYlp6SmpWdkZuFQmJ2Tm5dfUFhUXFJaVl5RWRUVicvE6prauvqGxqbmlta29g50EzvhoKvbK6wnvbevf8LESZOnTJ02fUYnEmCYiQCzZs+ZO2/+goWLFi9Zumz5ilkzkQHDSgRYtXrN2nXrN2zctHnL1m3bd6xaiQwYdiKBXbv37N23/8DBQ4ePHD12fCcKYDiBBE6eOn3m7LnzFy5eunzl6skTKIDhGgq4fuPmrdt37t67fv8aGmB4gAYePrp+/dHDBxiA4QGRYFgpBAAoZOePTU0n9wAAACV0RVh0ZGF0ZTpjcmVhdGUAMjAxMy0xMC0wN1QxMzoxNToxMyswMjowMIu2eggAAAAldEVYdGRhdGU6bW9kaWZ5ADIwMTMtMTAtMDdUMTM6MTU6MTMrMDI6MDD668K0AAAAAElFTkSuQmCC",
      name: {
        common: "Ethiopia",
        cym: "Ethiopia",
        deu: "Äthiopien",
        fra: "Éthiopie",
        hrv: "Etiopija",
        ita: "Etiopia",
        jpn: "エチオピア",
        nld: "Ethiopië",
        por: "Etiópia",
        rus: "Эфиопия",
        spa: "Etiopía",
        svk: "Etiópia",
        fin: "Etiopia",
        zho: "埃塞俄比亚",
        isr: "אתיופיה"
      },
      text: "Ethiopia"
    },
    {
      key: "IN",
      currency: "INR",
      callingCode: "+91",
      flag:
        "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACgAAAAeCAMAAABpA6zvAAAABGdBTUEAALGPC/xhBQAAAAFzUkdCAK7OHOkAAAAgY0hSTQAAeiYAAICEAAD6AAAAgOgAAHUwAADqYAAAOpgAABdwnLpRPAAAAqZQTFRF53MA5HIC43ID53MB9XcA+3oA9ngA53QB83cA6XMCpV8oX0hPR0RkSUVlXkdPpF8o53EA428B9ncArmIkL0OAG02vZo3TfJ3agqLcZYzTJFSzLkJ/6X0R6X0S5XsS+IQOl2E+BDqjgJ7X5eny5enz5uv05er05OjyjancCD6llmE+/fXu+/Ps///zvMPUCDefqbre8fP32ODwyNPpwMzmwc3mxNDo1N3u8fL3v8znEj+kucHS/////v//NVuwf5bK9/n7vcrkma3WgZnMf5fLlKnUusjjzdfr+Pn7iZ/OOmCz/f7///7+/Pz9tcPgGUWh4efz2eDwvMrkf5nMXH2+aojDa4jEW3y9gJnMuMbi6u/3Jk+ms8Hf+/z9aofDXn2+9ff70Nrsm6/XXHy+jKPRxNDnxdHol6vVWXq8kqfTz9ns9vj7co3Ga4fD/P3+UHK5dZDI8fT5w8/ngpvNaIbDxM/nvMnku8jkcIzGfZfLws7m8/b6iaDQU3W6dI/H8vT6aIbCu8nkucfjydTpepXK9Pb6UnS67vL4ztfrmKzWWnu9kKbTmK3WW3u9kqjTy9Xq8PP5cY3GbIjD/v/+GEWh4ujz1t7ugZrNbInEfJbL7fH4JlCns8LgOVuxfpbKzNbqlqrUfpbLe5TKytTqj6XRPWCz/v7/8fju7vXt/v/zssXUCTigs8Hi8fT309zuxtHo0Nns7vL2wMznFECkr8LT///0QJkUQJoUPpcVSKIRKHVADDqki6DY4+ny4ujy4Oby4ejyl6ndEj+mJ3Q/LpAAL5AALI4BNZgAJHomD0iAJk6xd47Uip7bjqHceI7VLlW0LY4BM5IEM5MEMZEFNZcAMpMDIXUqDFZQDE9lDlBmDFVQMpIDNpgAOJsANpkAOZsAMZEGMpEFwJ5XlQAAAAFiS0dEPKdqYc8AAAAJcEhZcwAAAEgAAABIAEbJaz4AAAGtSURBVDjLY2AY1oCRiRGICCtjZmFlY2NlYcZQyo4CGDk4ubh5ePn4OTkYUWUYBJCBoJCwiKiYuISklLSwkCCKFIMMAsjKySsoKimrqKqpq2toasnLySJJMmgjgI6unr6BoZGxiamZuYWllbWuDpIkgw0c2NrY2tk7mDk6Obu4url7eHp5A4XggMEHDnxt/PwDAoOCQ0LDwiMi3aOiY2x8EbJIJsbaxMUnJCYlp6SmpWdkZmXn5AKFECYimHk2+QWFRcUlpWXlxhWVVdU1tUAhHArr6k0jGooam5orWkxaa9pwKIy1yY1v7+js6k4t6unt658wcRKK1ZPhINbGb8rUaeXTw0NndM1sMp81e45NLEIWJXhs5s5zmF+2YOGivsjFDkuWLkMJnuUIsGLlqtVr1q5bXxZUumHjps1btq5AkmTYhgDbd+zctXvP3n1T9x/Ye/DQ4Z07tiNJMhxBAkePHT9x8tTpM2fPnT954viFo8hyDBeRwKVLl69cvXb9xs1b125fuXzpErIcw21UcPn2nbv37t2/A2SgAnSFQKUPHj58cBlDGFMhDjAUFAIALMfjyKVz+egAAAAldEVYdGRhdGU6Y3JlYXRlADIwMTMtMTAtMDdUMTM6MTQ6MzQrMDI6MDDj9ijFAAAAJXRFWHRkYXRlOm1vZGlmeQAyMDEzLTEwLTA3VDEzOjE0OjM0KzAyOjAwkquQeQAAAABJRU5ErkJggg==",
      name: {
        common: "India",
        deu: "Indien",
        fra: "Inde",
        hrv: "Indija",
        ita: "India",
        jpn: "インド",
        nld: "India",
        por: "Índia",
        rus: "Индия",
        spa: "India",
        svk: "India",
        fin: "Intia",
        zho: "印度",
        isr: "הודו"
      },
      text: "India"
    },
  ]
};
