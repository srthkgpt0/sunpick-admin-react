export const permissionKeys = [
  {
    moduleKey: 'dashboard',
    moduleName: 'Dashboard'
  },
  {
    moduleKey: 'rides',
    moduleName: 'Rides'
  },
  {
    moduleKey: 'drivers',
    moduleName: 'Drivers'
  },
  {
    moduleKey: 'riders',
    moduleName: 'Riders'
  },
  {
    moduleKey: 'tracking',
    moduleName: 'Tracking'
  },
  {
    moduleKey: 'reports',
    moduleName: 'Reports'
  }
]
