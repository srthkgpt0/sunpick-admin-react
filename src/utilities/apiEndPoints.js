import config from '../config'

const ApiEndPoints = {
  /**
   * Master API
   */
  masterVendorList: {
    url: '/MAPGateway/MasterData/VendorList',
    method: 'GET'
  },
  login: {
    url: '/auth/login',
    method: 'POST'
    // baseURL:'http://localhost:5003/api'
  },
  changePassword: {
    url: '/auth/change-password',
    method: 'POST'
    // baseURL:'http://localhost:5003/api'
  },
  logout: {
    url: '/auth/logout',
    method: 'POST'
  },
  forgetPassword: {
    url: '/auth/forgot-password',
    method: 'POST'
  },

  //SETTINGS API

  getCategoryListingData: {
    url: '/admin/category',
    method: 'GET'
  },
  getGlobalSettingsListingData: {
    url: '/setting',
    method: 'GET'
  },
  getModelListingData: {
    url: '/admin/model',
    method: 'GET'
  },

  getMakeListingData: {
    url: '/admin/brand',
    method: 'GET'
  },
  getFareListingData: {
    url: '/fare',
    method: 'GET'
  },
  getPreDefinedListingData: {
    url: 'admin/chat/predefined-message',
    method: 'GET'
  },

  getCategoryDropdownData: {
    url: '/admin/category',
    method: 'GET'
  },
  getMakeDropdownData: {
    url: '/admin/brand',
    method: 'GET'
  },

  getRelatedCategories: {
    url: '/admin/related-category',
    method: 'GET'
  },
  updateCategory: (id) => ({
    url: id ? `/admin/category/${id}` : '/admin/category',
    method: 'POST'
  }),
  updateGlobalSetting: (id) => ({
    url: `/setting/${id}`,
    method: 'PUT'
  }),
  updateMessage: (id) => ({
    url: id
      ? `/admin/chat/predefined-message/${id}`
      : '/admin/chat/predefined-message',
    method: id ? 'PUT' : 'POST'
  }),
  updateModel: (id) => ({
    url: id ? `/admin/model/${id}` : '/admin/model',
    method: id ? 'PUT' : 'POST'
  }),

  updateCategoryStatus: (id) => ({
    url: `/admin/category/${id}/change-status`,
    method: 'PUT'
  }),
  updateFareStatus: (id) => ({
    url: `/fare/${id}/change-status`,
    method: 'PUT'
  }),
  updateModelStatus: (id) => ({
    url: `/admin/model/${id}/change-status`,
    method: 'PUT'
  }),

  updateMakeStatus: (id) => ({
    url: `/admin/brand/${id}/change-status`,
    method: 'PUT'
  }),
  updateMake: (id) => ({
    url: id ? `/admin/brand/${id}` : '/admin/brand',
    method: id ? 'PUT' : 'POST'
  }),
  updateFareSetting: (id) => ({
    url: `/fare/${id}`,
    method: 'PUT'
  }),
  deleteModel: (id) => ({
    url: `/admin/model/${id}`,
    method: 'DELETE'
  }),
  deleteMessage: (id) => ({
    url: `/admin/chat/predefined-message/${id}`,
    method: 'DELETE'
  }),

  deleteCategory: (id) => ({
    url: `/admin/category/${id}`,
    method: 'DELETE'
  }),
  deleteMake: (id) => ({
    url: `/admin/brand/${id}`,
    method: 'DELETE'
  }),
  //SETTINGS API END
  /**
   * Dashboard API
   */
  dashboardList: {
    url: '/admin/dashboard',
    method: 'GET'
  },
  dashboardUserGraph: {
    url: '/admin/dashboard/user-graph',
    method: 'GET'
  },
  driverDashList: {
    url: '/admin/dashboard/active-driver',
    method: 'GET'
  },
  driverList: {
    url: '/admin/driver',
    method: 'GET'
  },
  categoryList: {
    url: '/admin/category',
    method: 'GET'
  },
  makeList: {
    url: '/admin/brand',
    method: 'GET'
  },
  modelList: {
    url: '/admin/model',
    method: 'GET'
  },
  stateList: {
    url: '/state',
    method: 'GET'
  },
  //DASHBOARD API ENDS

  // Executive API
  getExecutiveList: {
    url: '/admin/agent',
    method: 'GET'
  },
  EXECUTIVE_LIST_FILE_DOWNLOAD: {
    url: `${config.API_BASE_URL}/admin/agent/download-list`
  },
  executiveChangePass: (id) => ({
    url: `/admin/agent/${id}/change-password`,
    method: 'POST'
  }),
  addEditExecutive: (id = null) => ({
    url: id ? `/admin/agent/${id}/edit` : `/admin/agent`,
    multipart: `multipart/form-data`,
    method: 'POST'
  }),
  updateExcutiveStatus: (id) => ({
    url: `/admin/agent/${id}/change-status`,
    method: 'PUT'
  }),
  deleteExcutiveStatus: (id) => ({
    url: `/admin/agent/${id}`,
    method: 'delete'
  }),
  getFareCalculation: {
    url: `/ride/calculate`,
    method: 'POST'
  },
  // End Executive
  //RIDERS API

  getRidersDetails: {
    url: '/admin/customer',
    method: 'GET'
  },
  getRiderDetails: (id) => ({
    url: `/admin/customer/${id}`,
    method: 'GET'
  }),
  getRiderListingDetails: (id) => ({
    url: `/admin/customer/${id}/ride`,
    method: 'GET'
  }),
  updateRider: (id) => ({
    url: `/admin/customer/${id}/edit`,
    multipart: `multipart/form-data`,
    method: 'POST'
  }),
  updateRiderStatus: (id) => ({
    url: `/admin/customer/${id}/change-status`,
    method: 'PUT'
  }),
  RIDERS_LIST_FILE_DOWNLOAD: {
    url: `${config.API_BASE_URL}/admin/customer/download-list`
  },
  //RIDERS API END

  //Rides API
  getRidesList: {
    url: '/admin/payment/ride',
    method: 'GET'
  },

  getRideDetails: (id) => ({
    url: `/admin/payment/ride/${id}`,
    method: 'GET'
  }),
  assignDriver: (id) => ({
    url: `/agent/ride/${id}/assign-driver`,
    method: 'POST'
  }),
  getAvailableDrivers: {
    url: '/agent/available-driver',
    method: 'GET'
  },
  getVechicalList: {
    url: '/agent/vehicle-list',
    method: 'GET'
  },
  searchCustomer: {
    url: '/agent/search-customer',
    method: 'POST'
  },
  bookingRide: {
    url: '/agent/book-ride',
    method: 'POST'
  },
  RIDES_LIST_FILE_DOWNLOAD: {
    url: `${config.API_BASE_URL}/admin/payment/ride/download`
  },

  //RIDES API ENDS
  //Tracking API
  getTrackingList: {
    url: '/ride',
    method: 'GET'
  },
  getTrackingDetails: (id) => ({
    url: `/ride/${id}`,
    method: 'GET'
  }),
  getTrakerDetails: (id) => ({
    url: `/admin/ride/${id}/track`,
    method: 'GET'
  }),

  //TRACKING API ENDS

  /**
   * Driver Api
   */
  getActiveDriverList: {
    url: '/admin/driver',
    method: 'GET'
  },
  DRIVER_LIST_FILE_DOWNLOAD: {
    url: `${config.API_BASE_URL}/admin/driver/download-list`
  },

  updateStatus: (id) => ({
    url: `/admin/driver/${id}/change-status`,
    method: 'PUT'
  }),
  deleteDriver: (id) => ({
    url: `/admin/driver/${id}`,
    method: 'DELETE'
  }),
  addUpdateDriver: (id = null) => ({
    url: id ? `/admin/driver/${id}/edit` : `/admin/driver`,
    method: 'POST'
  }),
  ADD_DOCUMENT: (type) => ({
    url: `${config.API_BASE_URL}/admin/driver/document?type=${type}`,
    method: 'POST'
  }),
  GET_DRIVER_DETAIL: (id) => ({
    url: `/admin/driver/${id}`,
    method: 'GET'
  }),
  DRIVER_RIDES_EARNING_LIST: (id) => ({
    url: `/admin/driver/${id}/ride`,
    method: 'GET'
  }),
  DRIVER_DOCUMENT_LIST: (id) => ({
    url: `/admin/driver/${id}/document`,
    method: 'GET'
  }),
  DELETE_DRIVER: (id) => ({
    url: `/admin/driver/${id}`,
    method: 'POST'
  }),
  importDriver: (type) => ({
    url: `/document/import-driver?type=${type}`,
    method: 'POST'
  }),

  /*
  promo code
  */
  getPromoCodeList: {
    url: '/admin/promocode',
    method: 'GET'
  },
  changePromoStatus: (id) => ({
    url: `/admin/promocode/${id}/change-status`,
    method: 'PUT'
  }),

  GET_PROMOCODE_DETAIL: (id) => ({
    url: `/admin/promocode/${id}`,
    method: 'GET'
  }),

  addEditPromoCode: (id = null) => ({
    url: id ? `/admin/promocode/${id}` : `/admin/promocode`,
    method: 'POST'
  }),

  deletePromoCode: (id) => ({
    url: `/admin/promocode/${id}`,
    method: 'DELETE'
  }),
  //PROMOCODE API ENDS
  /**
   * Report Data
   */
  getTripData: {
    url: '/admin/report/trip-detail',
    method: 'GET'
  },
  getCompeltedRide: {
    url: '/admin/report/completed-ride',
    method: 'GET'
  },
  getRejectedRides: {
    url: '/admin/report/soft-rejected-ride',
    method: 'GET'
  },
  getDriverParticular: {
    url: '/admin/report/driver-particular',
    method: 'GET'
  },
  TRIP_DATA_LIST_FILE_DOWNLOAD: {
    url: `${config.API_BASE_URL}/admin/report/trip-detail/download`
  },
  COMPLETED_RIDES_LIST_FILE_DOWNLOAD: {
    url: `${config.API_BASE_URL}/admin/report/completed-ride/download`
  },
  REJECTED_RIDES_LIST_FILE_DOWNLOAD: {
    url: `${config.API_BASE_URL}/admin/report/soft-rejected-ride/download`
  },
  DRIVER_PARTICULARS_LIST_FILE_DOWNLOAD: {
    url: `${config.API_BASE_URL}/admin/report/driver-particular/download`
  },
  //REPORT DATA API ENDS
  /**
   * Message
   */
  sendDriverMessage: {
    url: '/admin/driver-message',
    method: 'POST'
  },
  sendCustomerMessage: {
    url: '/admin/customer-message',
    method: 'POST'
  }
  //MESSAGE API ENDS
}

export default ApiEndPoints
