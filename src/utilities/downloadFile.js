import XLSX from 'xlsx';
import FileDownload from 'js-file-download'
import { fileContentTransactionService } from '../services/transaction'
import modalNotification from './notifications';
import logger from './logger';

const fileInfoToDownload = (filename, fileData) => {
  let a = document.createElement('a');
  a.href = URL.createObjectURL(fileData);
  a.download = filename;
  document.body.appendChild(a);
  a.click();
  document.body.removeChild(a);
}

const saveAsExcelFile = (buffer, fileName) => {
  // const EXCEL_TYPE = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8';
  // const EXCEL_EXTENSION = '.xlsx';

  const data = new Blob([buffer], {
    type: 'application/vnd.ms-excel'
  });

  FileDownload(data, fileName);
}

export const downloadFiles = async (item, isCustomer = false) => {
  let detailFile = item.fileName;
  let headerFile = item.outputFileName;
  let transactionId = item.transactionID;
  // let fileType = item.fileTypeValue;
  let fileExt = 'text/csv';
  // let headerblob = ''
  let headerfile = ''

  try {
    let result = isCustomer === true ? await fileContentTransactionService(transactionId, item.customerID) : await fileContentTransactionService({ transactionId });

    let fileContent = result.fileContent;

    // let headerContent = '';
    let headertext = '';

    let text = atob(fileContent.detailContent);
    let blob = new Blob([fileContent.detailContent]);
    let file = new File([text], detailFile, { type: fileExt, lastModified: Date.now() });

    let Obj = [];
    Obj = text.split('\n');

    if (fileContent.typeOfFile.toLowerCase().trim() === 'xlsx') {
      let tempObj = []
      let sheetHeader = []

      for (let index = 0; index < Obj.length; index++) {
        const element = Obj[index];
        if (element) {
          const localObj = {}
          const splitData = element.split(',')
          if (index === 0) {
            sheetHeader = [...splitData]
          } else {
            for (let indexJ = 0; indexJ < splitData.length; indexJ++) {
              const element = splitData[indexJ];
              localObj[sheetHeader[indexJ]] = element
            }
            tempObj.push(localObj)
          }
        }
      }

      const worksheet = XLSX.utils.json_to_sheet(tempObj);
      const workbook = { Sheets: { 'Sheet1': worksheet }, SheetNames: ['Sheet1'] };
      const excelBuffer = XLSX.write(workbook, { bookType: 'xlsx', type: 'array' });

      saveAsExcelFile(excelBuffer, detailFile);
    } else {
      if (fileContent.headerContent) {
        // headerContent = result.headerContent;
        headertext = atob(fileContent.headerContent);
        // headerblob = new Blob([fileContent.headerContent], { type: fileExt });
        headerfile = new File([headertext], detailFile, { type: fileExt, lastModified: Date.now() });
      }

      if (window.navigator && window.navigator.msSaveOrOpenBlob) {
        window.navigator.msSaveOrOpenBlob(blob, detailFile);
      } else {
        fileInfoToDownload(detailFile, file);
        if (fileContent.headerContent) {
          fileInfoToDownload(headerFile, headerfile);
        }
      }
    }
  } catch (error) {
    logger(error)
    modalNotification({
      type: 'error',
      message: 'Failed',
      description: 'Server error while downloading file'
    })
  }
}
