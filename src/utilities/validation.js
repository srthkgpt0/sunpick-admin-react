// import config from '../config';
// import enValidationMsg from './enValidationMsg'
import enValidationMsg from './lang/validation-en'

const EMAIL = [
  {
    required: true,
    whitespace: true,
    message: enValidationMsg.enterEmail
  },
  {
    type: 'email',
    message: enValidationMsg.enterValidEmail
  }
]
const RIDER_EMAIL = [
  {
    required: false,
    whitespace: true
  },
  {
    type: 'email',
    message: enValidationMsg.enterValidEmail
  }
]

const PASSWORD = [
  {
    required: true,
    whitespace: true,
    message: enValidationMsg.enterPassword
  }
]

const PASSWORD_LENGTH = [
  {
    min: 6,
    message: enValidationMsg.passwordLengthMessage(6, 12)
  },
  {
    max: 12,
    message: enValidationMsg.passwordLengthMessage(6, 12)
  }
]

const ALPHA_NUMERIC = [
  {
    pattern: /^[a-zA-Z0-9\-\s]+$/,
    message: enValidationMsg.alphaNumericOnly
  }
]
const ONLY_NUMBER_PATTERN = (key) => [
  {
    pattern: /^[0-9]*$/,
    message: key
  }
]

const CHAR_LENGTH = (min, max) => [
  {
    min: min,
    message: enValidationMsg.enterMinChar(min)
  },
  {
    max: max,
    message: enValidationMsg.enterMaxChar(max)
  }
]
const FIRST_NAME_LENGTH = (min, max) => [
  {
    min: min,
    message: enValidationMsg.FIRST_NAME_MIN_LENGTH(min)
  },
  {
    max: max,
    message: enValidationMsg.FIRST_NAME_MAX_LENGTH(max)
  }
]
const NAME_LENGTH = (min, max) => [
  {
    min: min,
    message: enValidationMsg.NAME_MIN_LENGTH(min)
  },
  {
    max: max,
    message: enValidationMsg.NAME_MAX_LENGTH(max)
  }
]
const LAST_NAME_LENGTH = (min, max) => [
  {
    min: min,
    message: enValidationMsg.LAST_NAME_MIN_LENGTH(min)
  },
  {
    max: max,
    message: enValidationMsg.LAST_NAME_MAX_LENGHT(max)
  }
]
const NUMBER_LENGTH = (min, max, minkey, maxkey) => [
  {
    min: min,
    message: minkey(min)
  },
  {
    max: max,
    message: maxkey(max)
  }
]

const validation = {
  changePassword: {
    currentPassword: [
      {
        required: true,
        whitespace: true,
        message: enValidationMsg.enterCurrentPassword
      },
      ...CHAR_LENGTH(8, 12)
    ],
    password: [
      {
        required: true,
        whitespace: true,
        message: enValidationMsg.enterPassword
      },
      ...CHAR_LENGTH(8, 12)
    ],
    newPassword: [
      {
        required: true,
        whitespace: true,
        message: enValidationMsg.enterNewPassword
      },
      ...CHAR_LENGTH(8, 12)
    ],
    confirmNewPassword: [
      {
        required: true,
        whitespace: true,
        message: enValidationMsg.reEnterNewPassword
      },
      ({ getFieldValue }) => ({
        validator(rule, value) {
          if (!value || getFieldValue('new_password') === value) {
            return Promise.resolve()
          }
          return Promise.reject(enValidationMsg.passwordNotMatchMessage)
        }
      })
    ],
    confirmPassword: [
      {
        required: true,
        whitespace: true,
        message: enValidationMsg.reEnterNewPassword
      },
      ({ getFieldValue }) => ({
        validator(rule, value) {
          if (!value || getFieldValue('password') === value) {
            return Promise.resolve()
          }
          return Promise.reject(enValidationMsg.passwordNotMatchMessage)
        }
      })
    ]
  },
  promoCode: {
    title: [
      {
        required: true,
        message: enValidationMsg.promoTitle
      }
    ],
    discountType: [
      {
        required: true,
        message: enValidationMsg.promoDiscountType
      }
    ],
    discountPercentage: [
      {
        required: true,
        message: enValidationMsg.promoDiscountPercent
      }
    ],
    maximumDiscount: [
      {
        required: true,
        message: enValidationMsg.maximumDiscount
      }
    ],
    limitPerUser: [
      {
        required: true,
        message: enValidationMsg.limitPerUser
      }
    ],
    startDate: [
      {
        required: true,
        message: enValidationMsg.startDate
      }
    ],
    endDate: [
      {
        required: true,
        message: enValidationMsg.endDate
      }
    ],
    termCondition: [
      {
        required: true,
        message: enValidationMsg.termCondition
      }
    ],
    couponCode: [
      {
        required: true,
        message: enValidationMsg.couponCode
      }
    ]
  },
  addCategory: {
    categoryName: [
      {
        required: true,
        message: enValidationMsg.CATEGORY_NAME_REQUIRED
      }
    ],
    capacity: [
      {
        required: true,
        message: enValidationMsg.CAPACITY_REQUIRED
      }
    ]
  },
  addMessage: {
    userType: [
      {
        required: true,
        message: enValidationMsg.USER_TYPE
      }
    ],
    textTemplate: [
      {
        required: true,
        whitespace: true,
        message: enValidationMsg.TEXT_TEMPLATE
      }
    ],
    sequenceNumber: [
      {
        required: true,
        message: enValidationMsg.SEQUENCE_NUMBER
      }
    ],
    messageType: [
      {
        required: true,
        message: enValidationMsg.MESSAGE_TYPE
      }
    ]
  },
  addMake: {
    make: [
      {
        required: true,
        message: enValidationMsg.VEHICLE_MAKE_REQUIRED
      }
    ]
  },
  addModel: {
    model: [
      {
        required: true,
        whitespace: true,
        message: enValidationMsg.VEHICLE_MODEL_REQUIRED
      }
    ]
  },
  addDriver: {
    email: EMAIL,
    first_name: [
      {
        required: true,
        whitespace: true,
        message: enValidationMsg.FIRST_NAME_REQUIRED
      },
      ...FIRST_NAME_LENGTH(3, 12),
      ...ALPHA_NUMERIC
    ],
    last_name: [
      {
        required: true,
        whitespace: true,
        message: enValidationMsg.LAST_NAME_REQUIRED
      },
      ...LAST_NAME_LENGTH(3, 12),
      ...ALPHA_NUMERIC
    ],
    uploadImage: [
      {
        required: true,
        // whitespace: true,
        message: enValidationMsg.uploadImage
      }
    ],
    mobile_number: [
      {
        required: true,
        whitespace: true,
        message: enValidationMsg.CELL_NUMBER_REQUIRED
      },
      ...NUMBER_LENGTH(
        6,
        10,
        enValidationMsg.CELL_NUMBER_MIN_LENGTH,
        enValidationMsg.CELL_NUMBER_MAX_LENGTH
      ),
      ...ONLY_NUMBER_PATTERN(enValidationMsg.CELL_NUMBER_PATTERN)
    ],
    phone_number_country_code: [
      {
        required: true,
        message: enValidationMsg.CELL_NUMBER_CODE
      }
    ],
    car_registration_number: [
      {
        required: true,
        whitespace: true,
        message: enValidationMsg.VEHICLE_PLATE_NUMBER_REQUIRED
      },
      ...NUMBER_LENGTH(
        6,
        10,
        enValidationMsg.VEHICLE_PLATE_NUMBER_MIN_LENGTH,
        enValidationMsg.VEHICLE_PLATE_NUMBER_MAX_LENGTH
      ),
      ...ONLY_NUMBER_PATTERN(enValidationMsg.VEHICLE_PLATE_NUMBER_PATTERN)
    ],
    emergency_number: [
      {
        required: true,
        whitespace: true,
        message: enValidationMsg.EMERGENCY_NUMBER_REQUIRED
      },
      ...NUMBER_LENGTH(
        6,
        10,
        enValidationMsg.EMERGENCY_NUMBER_MIN_LENGTH,
        enValidationMsg.VEHICLE_PLATE_NUMBER_MAX_LENGTH
      ),
      ...ONLY_NUMBER_PATTERN(enValidationMsg.EMERGENCY_NUMBER_PATTERN)
    ],
    vehicle_year: [
      {
        required: true,
        message: enValidationMsg.VEHICLE_YEAR_REQUIRED
      },
      ...ONLY_NUMBER_PATTERN(enValidationMsg.VEHICLE_YEAR_PATTERN)
    ],
    vehicle_category: [
      {
        required: true,
        whitespace: true,
        message: enValidationMsg.VEHICLE_CATEGORY_REQUIRED
      }
    ],
    vehicle_model: [
      {
        required: true,
        message: enValidationMsg.VEHICLE_MODEL_REQUIRED
      }
    ],
    vehicle_make: [
      {
        required: true,
        message: enValidationMsg.VEHICLE_MAKE_REQUIRED
      }
    ],
    date_of_birth: [
      {
        required: false,
        message: enValidationMsg.DOB_REQUIRED
      }
    ],
    emergency_contact_name: [
      {
        required: true,
        whitespace: true,
        message: enValidationMsg.EMERGENCY_REQUIRED
      }
    ],
    state: [
      {
        required: true,
        message: enValidationMsg.STATE_REQUIRED
      }
    ],
    city: [
      {
        required: true,
        message: enValidationMsg.CITY_REQUIRED
      }
    ],
    address: [
      {
        required: true,
        whitespace: true,
        message: enValidationMsg.ADDRESS_REQUIRED
      }
    ],
    profile_image: [
      {
        required: true,
        message: enValidationMsg.PROFILE_IMAGE_REQUIRED
      }
    ],
    insurance_image: [
      {
        required: true,
        message: enValidationMsg.INSURANCE_IMAGE_REQUIRED
      }
    ],
    license_front_image: [
      {
        required: true,
        message: enValidationMsg.LICENSE_FRONT_IMAGE_REQUIRED
      }
    ],
    license_back_image: [
      {
        required: true,
        message: enValidationMsg.LICENSE_BACK_IMAGE_REQUIRED
      }
    ],
    owner_tin: [
      {
        required: true,
        message: enValidationMsg.OWNER_TIN_REQUIRED
      }
    ],
    vehicle_registration_image: [
      {
        required: true,
        message: enValidationMsg.VEHICLE_REGISTRATION_IMAGE_REQUIRED
      }
    ],
    employment_agreement: [
      {
        required: true,
        message: enValidationMsg.Employment_AGREEMENT_FRONT_IMAGE_REQUIRED
      }
    ],
    postal_code: [
      {
        required: false,
        whitespace: true
      },
      ...ALPHA_NUMERIC
    ],
    importExcel: [
      {
        required: true,
        message: enValidationMsg.importExcel
      }
    ]
  },
  message: {
    status: [
      {
        required: true,
        message: enValidationMsg.status
      }
    ],
    messages: [
      {
        required: true,
        whitespace: true,
        message: enValidationMsg.message
      }
    ]
  },

  manageRiders: {
    email: RIDER_EMAIL,
    name: [
      {
        required: false,
        whitespace: true
      },
      ...NAME_LENGTH(3, 12),
      ...ALPHA_NUMERIC
    ],
    mobile_number: [
      {
        required: false,
        whitespace: true
      },
      ...NUMBER_LENGTH(
        6,
        10,
        enValidationMsg.CELL_NUMBER_MIN_LENGTH,
        enValidationMsg.CELL_NUMBER_MAX_LENGTH
      ),
      ...ONLY_NUMBER_PATTERN(enValidationMsg.CELL_NUMBER_PATTERN)
    ]
  },
  login: {
    email: EMAIL,
    password: [...PASSWORD, ...CHAR_LENGTH(8, 12)]
  },
  forgotPassword: {
    email: EMAIL
  },
  ride: {
    travelerName: [
      {
        required: true,
        message: enValidationMsg.travelerName
      }
    ],
    availableDriver: [
      {
        required: true,
        message: enValidationMsg.availableDriver
      }
    ],
    pickUpLocation: [
      {
        required: true,
        message: enValidationMsg.pickupLocation
      }
    ],
    dropOffLocation: [
      {
        required: true,
        message: enValidationMsg.dropLocation
      }
    ]
  },
  Password: {
    password: [...PASSWORD, ...PASSWORD_LENGTH],
    confirm: [
      {
        required: true,
        whitespace: true,
        message: enValidationMsg.reEnterPassword
      },
      ({ getFieldValue }) => ({
        validator(rule, value) {
          if (!value || getFieldValue('password') === value) {
            return Promise.resolve()
          }

          return Promise.reject(enValidationMsg.passwordNotMatchMessage)
        }
      })
    ]
  },
  queryParams: {
    driver_id: [
      {
        required: false,
        whitespace: true
      },
      ...NUMBER_LENGTH(
        0,
        10,
        enValidationMsg.DRIVER_ID_MIN_LENGTH,
        enValidationMsg.DRIVER_ID_MAX_LENGTH
      ),
      ...ONLY_NUMBER_PATTERN(enValidationMsg.DRIVER_ID_NUMBER_PATTERN)
    ],
    ride_id: [
      {
        required: false,
        whitespace: true
      },
      ...NUMBER_LENGTH(
        0,
        10,
        enValidationMsg.RIDE_ID_MIN_LENGTH,
        enValidationMsg.RIDE_ID_MAX_LENGTH
      ),
      ...ONLY_NUMBER_PATTERN(enValidationMsg.RIDE_ID_NUMBER_PATTERN)
    ]
  }
}

const inputParser = {
  number: (value) => {
    value = value.replace(/[^0-9]/g, '')
    const regexNum = /^[0-9]*$/
    if (regexNum.test(value)) {
      return value
    }
  }
}

export default validation
export { inputParser }
