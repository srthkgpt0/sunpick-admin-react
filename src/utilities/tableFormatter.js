export const serialNumberFormatter = (rowIndex, currentPage, dataPerPage) => {
  return ((rowIndex + (dataPerPage * (currentPage - 1))) + 1) || rowIndex
}