import React, { Component } from 'react'
import { compose } from 'recompose'
import { currentTimeStamp } from '../../utilities/common'
import {
  withScriptjs,
  withGoogleMap,
  GoogleMap,
  Marker,
  InfoWindow
} from 'react-google-maps'
import config from '../../config'
const MapWithAMarker = compose(
  withScriptjs,
  withGoogleMap
)((props) => {
  return (
    <GoogleMap defaultZoom={7} defaultCenter={{ lat: 9.145, lng: 40.489673 }}>
      {props.markers.map((marker) => {
        const onClick = props.onClick.bind(this, marker)
        if (
          marker &&
          marker.status === 'active' &&
          marker.latitude &&
          marker.longitude &&
          marker.is_shift_started === true
        ) {
          return (
            <div>
              {
                <Marker
                  key={`${marker.id}${currentTimeStamp()}`}
                  icon={props.icon.available.icon}
                  onClick={onClick}
                  position={{ lat: marker.latitude, lng: marker.longitude }}
                >
                  {props.selectedMarker === marker && (
                    <InfoWindow>
                      <div>
                        <div>
                          Name:
                          <b>
                            {marker.first_name ? marker.first_name : ''}
                            {marker.last_name ? marker.last_name : ''}
                          </b>
                        </div>
                        <div>
                          Phone:
                          <b>
                            {marker.phone_number ? marker.phone_number : ''}
                          </b>
                        </div>
                        <div>
                          Vehicle Category:
                          <b>
                            {marker.category && marker.category.name
                              ? marker.category.name
                              : marker.category}
                          </b>
                        </div>
                      </div>
                    </InfoWindow>
                  )}
                </Marker>
              }
            </div>
          )
        }
        return null
      })}
    </GoogleMap>
  )
})

export default class MapMarker extends Component {
  constructor(props) {
    super(props)
    this.state = {
      allData: this.props.driverData,
      selectedMarker: false,
      icons: {
        available: {
          icon: `${'assets/images/'}${'available-cab.png'}`
        },
        booked: {
          icon: `${`assets/images/`}${'booked-cab.png'}`
        }
      }
    }
  }
  componentDidUpdate(newProps) {
    if (newProps.driverData !== this.props.driverData)
      this.setState({
        allData: newProps.driverData
      })
  }

  handleClick = (marker, event) => {
    this.setState({ selectedMarker: marker })
  }

  render() {
    return (
      <>
        {this.state.allData && (
          <MapWithAMarker
            selectedMarker={this.state.selectedMarker}
            markers={this.state.allData}
            onClick={this.handleClick}
            // googleMapURL="https://maps.googleapis.com/maps/api/js?key=AIzaSyDIdNupi7p6HVYzNkeQerwVSvrSQJ-hDl0"
            googleMapURL={`https://maps.googleapis.com/maps/api/js?key=${config.GOOGLE_API_KEY}&v=3.exp&libraries=geometry,drawing,places`}
            loadingElement={<div style={{ height: `100%` }} />}
            containerElement={<div style={{ height: `400px` }} />}
            mapElement={<div style={{ height: `100%` }} />}
            icon={this.state.icons}
          />
        )}
      </>
    )
  }
}
