import React, { useEffect, useState } from 'react'
import { currency_symbol } from '../../utilities/common'

export const FarePopover = (props) => {
  const [data, setData] = useState(props.data)
  const [showRatePerKm, setShowRatePerKm] = useState(true)
  const [showMinFare, setShowMinFare] = useState(true)

  useEffect(() => {
    //  console.log("data", data)
    if (data.ride_km <= data.minimum_fare_km) {
      setData((state) => (state['base_fare'] = state['minimum_fare']))
      setShowRatePerKm(false)
    } else {
      setShowMinFare(false)
    }
  }, []) // eslint-disable-line react-hooks/exhaustive-deps

  return (
    <div id='tip-first' class='tip-content hidden'>
      <div class='drop-menu'>
        <div class='drop-item head text-left'>Fare Breakdown :</div>
        <div class='drop-item clearfix'>
          <span class='float-left'>Base Fare :</span>
          <span class='float-right'>
            (+) {currency_symbol}
            {data?.base_fare ? data?.base_fare : '0'}
          </span>
        </div>
        {showMinFare && (
          <div class='drop-item clearfix'>
            <span class='float-left'>KM applicable for minimum fare :</span>
            <span class='float-right'> {data?.minimum_fare_km || '0'} KM</span>
          </div>
        )}
        <div class='drop-item clearfix'>
          <span class='float-left'>Distance Travelled :</span>
          <span class='float-right'>
            {' '}
            {data?.ride_km ? data?.ride_km : '0'} KM{' '}
          </span>
        </div>
        {showRatePerKm && (
          <div class='drop-item clearfix'>
            <span class='float-left'>Time Taken(in Mins) :</span>
            {data && data?.actual_time ? (
              <span class='float-right'>{data?.actual_time} </span>
            ) : (
              <span class='float-right'>{data?.estimate_time || '0'} </span>
            )}
          </div>
        )}
        {showRatePerKm && (
          <div class='drop-item clearfix'>
            <span class='float-left'>
              Rate/KM {data?.car_category ? data?.car_category : '-'} :
            </span>
            {data.ride_km && data.ride_km <= 25 && (
              <span class='float-right'>
                {' '}
                (+) {data?.ride_km ? data?.ride_km : '0'} x {currency_symbol}{' '}
                {data?.per_km_fare ? data?.per_km_fare : '0'}
              </span>
            )}
            {data.ride_km && data.ride_km > 25 && data.ride_km <= 50 && (
              <span class='float-right'>
                {' '}
                (+) {data?.ride_km ? data?.ride_km : '0'} x {currency_symbol}{' '}
                {data?.per_km_fare_slab1 ? data?.per_km_fare_slab1 : '0'}
              </span>
            )}
            {data.ride_km && data.ride_km > 50 && data.ride_km <= 100 && (
              <span class='float-right'>
                {' '}
                (+) {data?.ride_km ? data?.ride_km : '0'} x {currency_symbol}{' '}
                {data?.per_km_fare_slab2 ? data?.per_km_fare_slab2 : '0'}
              </span>
            )}
            {data.ride_km && data.ride_km > 100 && data.ride_km <= 250 && (
              <span class='float-right'>
                {' '}
                (+) {data?.ride_km ? data?.ride_km : '0'} x {currency_symbol}{' '}
                {data?.per_km_fare_slab3 ? data?.per_km_fare_slab3 : '0'}
              </span>
            )}
            {data.ride_km && data.ride_km > 250 && data.ride_km <= 500 && (
              <span class='float-right'>
                {' '}
                (+) {data?.ride_km ? data?.ride_km : '0'} x {currency_symbol}{' '}
                {data?.per_km_fare_slab4 ? data?.per_km_fare_slab4 : '0'}
              </span>
            )}
            {data.ride_km && data.ride_km > 500 && (
              <span class='float-right'>
                {' '}
                (+) {data?.ride_km ? data?.ride_km : '0'} x {currency_symbol}
                {data?.per_km_fare_slab5 ? data?.per_km_fare_slab5 : '0'}
              </span>
            )}
          </div>
        )}
        {showRatePerKm && (
          <div class='drop-item clearfix'>
            <span class='float-left'>Time Factor for Travel(Rs. / Min) :</span>
            {data && data?.actual_time ? (
              <span class='float-right'>
                (+) {data?.actual_time ? data?.actual_time : '0'} x{' '}
                {currency_symbol}{' '}
                {data?.per_min_fare ? data?.per_min_fare : '0'}{' '}
              </span>
            ) : (
              <span class='float-right'>
                (+) {data?.estimate_time ? data?.estimate_time : '0'} x{' '}
                {currency_symbol}{' '}
                {data?.per_min_fare ? data?.per_min_fare : '0'}{' '}
              </span>
            )}
          </div>
        )}

        <div class='drop-item clearfix'>
          <span class='float-left'>Toll Taxes :</span>
          <span class='float-right'>
            (+) {currency_symbol} {data?.toll_total || '0'}
          </span>
        </div>
        <div class='drop-item clearfix'>
          <span class='float-left'>Discount (Promo) :</span>
          <span class='float-right'>
            {' '}
            (-) {currency_symbol} {data?.promo_discount_value || '0'}
          </span>
        </div>
        <div class='drop-item clearfix'>
          <span class='float-left'>Tax :</span>
          <span class='float-right'>
            {' '}
            (+) {currency_symbol} {data?.tax_value || '0'}
          </span>
        </div>
        <div class='drop-item total clearfix'>
          <span class='float-left'>Total Fare :</span>
          <span class='float-right'>
            {currency_symbol} {data?.ride_amount || '0'}
          </span>
        </div>
      </div>
    </div>
  )
}
