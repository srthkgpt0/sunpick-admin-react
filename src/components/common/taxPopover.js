import React from 'react'
import { currency_symbol } from '../../utilities/common'

export const TaxPopover = (props) => {
    const { toll } = props
    // console.log("TOLL",toll)
    return (
        <div class="tip-content hidden">
            <div class="drop-menu">
                <div class="drop-item head text-left">Toll Taxes :</div>
                {toll.tolls && toll.tolls.map(toll => {
                    return <div>
                            <div class="drop-item clearfix">
                                <span class="place-name float-left">{toll.name} :</span>
                                <span class="float-right">(+) {currency_symbol} {toll.amount}</span>
                            </div>
                            </div>
                })}
                <div class="drop-item total clearfix">
                <span class="float-left">Total Toll Paid :</span>
                <span class="float-right">{currency_symbol} {toll.toll_total? toll.toll_total:'0'}</span>
                </div>
            </div>
        </div>
    )
}