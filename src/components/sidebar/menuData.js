import React from 'react'
import { Link } from 'react-router-dom'
import { withRouter } from 'react-router'
import { Menu } from 'antd'
import { currentTimeStamp } from '../../utilities/common'
import { CopyOutlined } from '@ant-design/icons'

function MenuData({ data, userType, path }) {
  const { SubMenu } = Menu
  return (
    <Menu style={{ width: 256 }} mode='inline'>
      <SubMenu key='sub1' icon={<CopyOutlined />} title={data.title}>
        {data.child.map((childData, childId) => {
          if (
            childData.userTypes.includes(userType) &&
            childData.addInSideBar
          ) {
            return (
              <Menu.Item key={childId}>
                <ul>
                  <li
                    key={`${currentTimeStamp()}_${childId}`}
                    className={childData.active.includes(path) ? 'active' : ''}
                  >
                    <Link className='nav-link' to={childData.endPoint}>
                      <span className='nav__title'>{childData.title}</span>
                    </Link>
                  </li>
                </ul>
              </Menu.Item>
            )
          } else {
            return null
          }
        })}
      </SubMenu>
    </Menu>
  )
}

export default withRouter(MenuData)
