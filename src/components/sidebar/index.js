import React from 'react'
import { useTranslation } from 'react-i18next'
import { Link } from 'react-router-dom'
import { withRouter } from 'react-router'
import { useSelector } from 'react-redux'
import { routesJSON } from '../../utilities/sidebar'
// import { selectUserData } from '../../redux/auth/authSlice'
import { permissionKeys } from '../../utilities/permission'
import { currentTimeStamp, tagClassToggle } from '../../utilities/common'
// import { Menu, Dropdown } from 'antd'
import MenuData from './menuData'
function SideBar({ match }) {
  const { t } = useTranslation()
  // const dispatch = useDispatch()
  const userType = useSelector((state) => state.auth.userData.userType)
  // const userData = useSelector(selectUserData)
  const path = match.path
  // const pathURL = match.url
  //   const [show, setShow] = useState(true)
  const filterOpen = (event) => {
    document
      .getElementsByTagName('body')[0]
      .classList.add('page-sidebar-closed')
    event.preventDefault()
  }
  const toggleDrawerMenu = () => {
    // console.log('toggleDrawerMenu called ')
    const element = document.getElementsByTagName('body')[0]
    if (element.classList.contains('open-menu')) {
      tagClassToggle('body', 'open-menu')
    }
  }
  // const menu = (data) => {
  //   return (
  //     <Menu>
  //       <div aria-labelledby='dropdownMenuButton'>
  //         <Menu>
  //           <Menu.Item key='0'>
  //             <ul>
  //               {data.child.map((childData, childId) => {
  //                 if (
  //                   childData.userTypes.includes(userType) &&
  //                   childData.addInSideBar
  //                 ) {
  //                   return (
  //                     <li
  //                       key={`${currentTimeStamp()}_${childId}`}
  //                       className={
  //                         childData.active.includes(path) ? 'active' : ''
  //                       }
  //                     >
  //                       <Link
  //                         className='nav-link'
  //                         to={childData.endPoint}
  //                         onClick={(e) => {
  //                           toggleDrawerMenu()
  //                         }}
  //                       >
  //                         <span className='nav__title'>{childData.title}</span>
  //                       </Link>
  //                     </li>
  //                   )
  //                 }
  //               })}
  //             </ul>
  //           </Menu.Item>
  //         </Menu>
  //       </div>
  //     </Menu>
  //   )
  // }

  return (
    <div className='side_menu' data-mcs-theme='dark' id='sidemenu'>
      <div className='navbar_header'>
        <Link
          className='close-icon d-sm-none'
          id='closeToggle'
          to='/'
          onClick={(e) => filterOpen(e)}
        >
          <i className='ti-close'></i>
        </Link>
        <Link to='/dashboard' className='logo'>
          <img src='assets/images/logo.png' className='img-fluid' alt='logo' />
        </Link>
      </div>
      <ul className='list-unstyled menu' data-mcs-theme='light'>
        {routesJSON(t, permissionKeys).map((data, index) => {
          if (data.userTypes.includes(userType) && data.addInSideBar) {
            if (data.child && data.child.length > 0) {
              return (
                <li
                  key={`${currentTimeStamp()}_${index}`}
                  className='sidebar-drop'
                >
                  {/* <Dropdown overlay={<MenuData data={data} userType={userType} path={path}/> } trigger={['click']}>
                    <Link
                      type='button'
                      id='dropdownMenuButton'
                      onClick={(e) => e.preventDefault()}
                      aria-haspopup='true'
                      aria-expanded='false'
                    >
                      <i className="ti-file icon" aria-hidden="true"></i> {data.title}
                      <i className="fa fa-angle-right"></i>
                    </Link>
                  </Dropdown> */}
                  <MenuData
                    key={`${currentTimeStamp()}_${index}`}
                    data={data}
                    userType={userType}
                    path={path}
                  />
                </li>
              )
            } else {
              if (userType === 'admin') {
                // console.log('data.active', data.active)
                return (
                  <li
                    key={`${currentTimeStamp()}_${index}`}
                    className={data.active.includes(path) ? 'active' : ''}
                  >
                    <Link
                      className='nav-link nav__link ripple-effect'
                      to={data.endPoint}
                      onClick={(e) => {
                        toggleDrawerMenu()
                      }}
                    >
                      <span className='nav__icon'>
                        <i className={data.icon}></i>
                      </span>
                      <span className='nav__title'>{data.title}</span>
                    </Link>
                  </li>
                )
              } else {
                if (permissionKeys) {
                  let moduleKeyIndex = permissionKeys.findIndex((item) => {
                    return item.moduleKey === data.moduleKey
                  })
                  if (moduleKeyIndex > -1) {
                    return (
                      <li
                        key={`${currentTimeStamp()}_${index}`}
                        className={data.active.includes(path) ? 'active' : ''}
                      >
                        <Link
                          className='nav-link nav__link ripple-effect'
                          to={data.endPoint}
                          onClick={(e) => {
                            toggleDrawerMenu()
                          }}
                        >
                          <span className='nav__icon'>
                            <i className={data.icon}></i>
                          </span>
                          <span className='nav__title'>{data.title}</span>
                        </Link>
                      </li>
                    )
                  } else {
                    return null
                  }
                } else {
                  return null
                }
              }
            }
          } else {
            return null
          }
        })}
      </ul>
    </div>
  )
}

export default withRouter(SideBar)
