import React, { useState ,useEffect } from 'react'
import {
   DEFAULT_CURRENCY_SYMBOL,
} from '../../utilities/common';


const ToolTipComponent = (props) => {
    const [show_rate_per_km, setRatePerKm] = useState(true)
    const [show_min_fare, setMinFare] = useState(true);
    const ride = props.toolTipData;

  useEffect(() => {
   
    if(ride.ride_km <= ride.minimum_fare_km){
        ride.base_fare = ride.minimum_fare;
        setRatePerKm(false)
      }else{
        setMinFare(false)
      }
  }, []) // eslint-disable-line react-hooks/exhaustive-deps

  return ( <div id="tip-first" className="tip-content hidden">
  <div className="drop-menu">
     <div className="drop-item head text-left">Fare Breakdown :</div>
     <div className="drop-item clearfix">
        <span className="float-left">Base Fare :</span>
        <span className="float-right">(+) {DEFAULT_CURRENCY_SYMBOL}{ride?.base_fare || '0'}</span>
     </div>

     <div className="drop-item clearfix">
        {

           show_min_fare && <span className="float-left">KM applicable for minimum fare :</span>
           // <span className="float-right"> {ride?.minimum_fare_km || '0'} KM</span>
        }

     </div>

     <div className="drop-item clearfix">
        <span className="float-left">Distance Travelled :</span>
        <span className="float-right">{ride?.ride_km || '0'} KM</span>
     </div>
     <div className="drop-item clearfix" >
        {
           show_rate_per_km && <span className="float-left">Time Taken(in Mins) :</span>
        }
        {
           ride && ride?.actual_time && <span className="float-right">{ride?.actual_time || '0'} </span>
        }
        {
           !(ride && ride?.actual_time) && <span className="float-right" >{ride?.estimate_time || '0'} </span>
        }
     </div>
     <div className="drop-item clearfix" >
        {
           show_rate_per_km && <span className="float-left">Rate/KM ({ride?.car_category ? ride?.car_category : '-'}) :</span>
        }
        {
           ride.ride_km && ride.ride_km <= 25 && <span className="float-right" >(+) {ride?.ride_km ? ride?.ride_km : '0'}
         x
         {DEFAULT_CURRENCY_SYMBOL}{ride?.per_km_fare ? ride?.per_km_fare : '0'} </span>
        }
        {
           ride.ride_km && ride.ride_km > 25 && ride.ride_km <= 50 && <span className="float-right" >(+)
         {ride?.ride_km ? ride?.ride_km : '0'} x
         {DEFAULT_CURRENCY_SYMBOL}{ride?.per_km_fare_slab1 ? ride?.per_km_fare_slab1 : '0'}  </span>
        }
        {
           ride.ride_km && ride.ride_km > 50 && ride.ride_km <= 100 && <span className="float-right" > {ride?.ride_km ? ride?.ride_km : '0'} x
         {DEFAULT_CURRENCY_SYMBOL}{ride?.per_km_fare_slab2 ? ride?.per_km_fare_slab2 : '0'} </span>
        }
        {
           ride.ride_km && ride.ride_km > 100 && ride.ride_km <= 250 && <span className="float-right" > (+)
         {ride?.ride_km ? ride?.ride_km : '0'} x
         {DEFAULT_CURRENCY_SYMBOL}{ride?.per_km_fare_slab3 ? ride?.per_km_fare_slab3 : '0'}</span>
        }
        {
           ride.ride_km && ride.ride_km > 250 && ride.ride_km <= 500 && <span className="float-right" > (+)
         {ride?.ride_km ? ride?.ride_km : '0'} x
         {DEFAULT_CURRENCY_SYMBOL}{ride?.per_km_fare_slab4 ? ride?.per_km_fare_slab4 : '0'}</span>
        }
        {
           ride.ride_km && ride.ride_km > 500 && <span className="float-right" > (+)
         {ride?.ride_km ? ride?.ride_km : '0'} x
         {DEFAULT_CURRENCY_SYMBOL}{ride?.per_km_fare_slab5 ? ride?.per_km_fare_slab5 : '0'}</span>
        }


     </div>
     <div className="drop-item clearfix" >
        {
           show_rate_per_km && <span className="float-left">Time Factor for Travel(Rs. / Min) :</span> &&


           ride && ride?.actual_time && <span className="float-right" >(+)
                 {ride?.actual_time ? ride?.actual_time : '0'} x
                 {DEFAULT_CURRENCY_SYMBOL}{ride?.per_min_fare ? ride?.per_min_fare : '0'} </span> &&


           !(ride && ride?.actual_time) && <span className="float-right" >(+)
             {ride?.estimate_time ? ride?.estimate_time : '0'} x
             {DEFAULT_CURRENCY_SYMBOL}{ride?.per_min_fare ? ride?.per_min_fare : '0'}</span>

        }


     </div>
     <div className="drop-item clearfix">
        <span className="float-left">Toll Taxes :</span>
        <span className="float-right">(+) {DEFAULT_CURRENCY_SYMBOL}{ride?.toll_total || '0'}</span>
     </div>
     <div className="drop-item clearfix">
        <span className="float-left">Discount (Promo) :</span>
        <span className="float-right"> (-) {DEFAULT_CURRENCY_SYMBOL}{ride?.promo_discount_value || '0'}</span>
     </div>
     <div className="drop-item clearfix">
        <span className="float-left">Tax :</span>
        <span className="float-right"> (+) {DEFAULT_CURRENCY_SYMBOL}{ride?.tax_value || '0'}</span>
     </div>
     <div className="drop-item total clearfix">
        <span className="float-left">Total Fare :</span>
        <span className="float-right">{DEFAULT_CURRENCY_SYMBOL}{ride?.ride_amount || '0'}</span>
     </div>
  </div>
</div>
)
}



// ToolTipComponent.propTypes = {
//   toolTipData: PropTypes.any.isRequired,
// }
export default ToolTipComponent

