import React, { useState, useEffect } from 'react'

import { Rate } from 'antd'

const RatingComponent = (props) => {
  const [ratingValue, setRatingValue] = useState(0)
  useEffect(() => {
    if (props.ratingValue) {
      setRatingValue(props.ratingValue)
    }
  })

  return (
    <span>
      <Rate disabled value={ratingValue} />
    </span>
  )
}

export default RatingComponent
