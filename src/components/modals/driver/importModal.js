import React, { useState, useEffect } from 'react'
import { Modal } from 'react-bootstrap'
import { Button, Form, Input } from 'antd'
import PropTypes from 'prop-types'
import LoadingSpinner from '../../subComponent/loadingSpinner'
// import GlobalLoader from '../../subComponent/globalLoader'
import { useTranslation } from 'react-i18next'
import { importDriverService } from '../../../services/driver'
// import { filterDataObj } from '../../../utilities/common'
import modalNotification from '../../../utilities/notifications'
import logger from '../../../utilities/logger'
import validation from '../../../utilities/validation'
import { Link } from 'react-router-dom'

export default function DriverImportModal(props) {
  const [isSpin, setIsSpin] = useState(false)
  // const [errorMsg, setErrorMsg] = useState('')
  const [isDoc, setIsDoc] = useState()
  const [docData, setDocData] = useState()

  const { t } = useTranslation()
  const [form] = Form.useForm()
  const sampleExcel =
    'https://sunpick-api.codiantdev.com/assets/driver_dump.xlsx'

  useEffect(() => {
    // const isEditFormType = Object.keys(props.data).length > 0
    // console.log("isEditFormType",isEditFormType)
    if (props.show) {
      form.setFieldsValue({
        document: ''
      })
    }
    setIsDoc('')
    // setErrorMsg('')
    setIsSpin(false)
  }, [props.show]) // eslint-disable-line react-hooks/exhaustive-deps

  const onFileUploaded = (documnet) => {
    setDocData(documnet)
  }

  const onFinish = async (values) => {
    setIsSpin(true)
    // setErrorMsg('')

    try {
      // const { filterData } = filterDataObj(values)
      const formData = new FormData()

      if (docData) {
        console.log('DOCData', docData)
        formData.append('document', docData, docData.name)
      }
      console.log('FORMDATA', formData)
      const res = await importDriverService('document', formData)
      if (res && res.success) {
        setIsSpin(false)
        modalNotification({
          type: 'success',
          message: 'Success',
          description: res.message
        })
      }

      props.onHide()
    } catch (error) {
      setIsSpin(false)
      // setErrorMsg(error.message)
      logger({ 'error:': error })
    }
  }

  const onFinishFailed = (errorInfo) => {
    logger({ 'Failed:': errorInfo })
  }

  const onCancel = () => {
    form.resetFields()
    props.onHide()
  }

  const handleChanage = (event) => {
    // debugger
    const file = event.target.files[0]
    onFileUploaded(file)
    setIsDoc(file.name)
    form.setFieldsValue({
      document: file.name
    })
    // let reader = new FileReader()
    // reader.readAsDataURL(file)
    // reader.onloadend = (e) => {
    //   setPrevImage(reader.result)
    // }
  }

  if (!props.show) {
    return <></>
  }

  return (
    <Modal
      show={props.show}
      onHide={props.onHide}
      size='md'
      className='editModal'
      aria-labelledby='contained-modal-title-vcenter'
      centered
    >
      <Modal.Header className='justify-content-left text-center'>
        <Modal.Title id='contained-modal-title-vcenter'>
          <h2 className='modal-title text-capitalize'>
            {t('drivers.importDriver')}
          </h2>
        </Modal.Title>
      </Modal.Header>
      <Modal.Body className=' p-20'>
        <Form
          name='importDriver'
          form={form}
          onFinish={onFinish}
          onFinishFailed={onFinishFailed}
          // initialValues={initialValues}
        >
          <div className='form-group'>
            <label htmlFor=''>Upload Excel File</label>
            <Form.Item name='document' rules={validation.addDriver.importExcel}>
              <Input
                type='file'
                id='UploadImage'
                className='form-control file-control'
                size='40'
                onChange={(event) => {
                  handleChanage(event)
                }}
              />
            </Form.Item>
          </div>
          <div className='form-group'>
            <div className='uploadBox d-flex align-items-start'>
              {isDoc && (
                <div className='uploadBox__doc d-flex align-items-center position-relative'>
                  <img src='assets/images/xls.svg' alt='' />
                  <div className='caption'>
                    <h4>{isDoc}</h4>
                  </div>
                </div>
              )}
            </div>
          </div>

          <div className='submit_btn d-sm-flex justify-content-between'>
            <Link className='btn btn-warning' to={sampleExcel}>
              Sample Excel
            </Link>
            <div className='mt-2 mt-sm-0'>
              <Button
                type='button'
                className='btn btn-warning'
                onClick={onCancel}
              >
                Close
              </Button>
              <Button
                type='button'
                className='btn btn-warning'
                htmlType='submit'
              >
                {isSpin ? <LoadingSpinner /> : 'Import'}
              </Button>
            </div>
          </div>
        </Form>
      </Modal.Body>
    </Modal>
  )
}

DriverImportModal.propTypes = {
  show: PropTypes.bool.isRequired,
  onHide: PropTypes.func.isRequired,
  // title: PropTypes.string.isRequired,
  // message: PropTypes.string.isRequired,
  // textOnCancelBtn: PropTypes.string.isRequired,
  textOnConfirmBtn: PropTypes.string,
  onConfirmation: PropTypes.func,
  showLoading: PropTypes.bool
}
