import React, { useState, useEffect } from 'react'
import { useTranslation } from 'react-i18next'
import { Modal } from 'react-bootstrap'

import modalNotification from '../../../utilities/notifications'
import { Form } from 'antd'
import PropTypes from 'prop-types'

import logger from '../../../utilities/logger'
import { filterDataObj } from '../../../utilities/common'

import AddEditCategoryForm from '../../forms/settings/addEditCategoryForm'
import {
  getRelatedCategories,
  updateCategoryService
} from '../../../services/settings'
export default function AddEditCategoryModal(props) {
  const { t } = useTranslation()
  const [isSpin, setIsSpin] = useState(false)
  const [errorMsg, setErrorMsg] = useState('')
  const [checkBoxValue, setCheckBoxValue] = useState(null)
  const [isEditFormType, setIsEditFormType] = useState(
    Object.keys(props.data).length > 0
  )
  const [isImage, setIsImage] = useState()
  const [relatedCategory, setRelatedCategory] = useState([])
  const [checkedList, setCheckedList] = useState([])
  const [checkedId, setCheckedId] = useState([])

  const [form] = Form.useForm()

  useEffect(() => {
    const isEditFormType = Object.keys(props.data).length > 0
    if (props.show && isEditFormType) {
      fetchRelatedCategories(props.data.id)
      form.setFieldsValue({
        name: props.data.name,
        capacity: props.data.capacity,
        icon: props.data.icon
        // related_categories: checkedId
      })
    } else {
      fetchRelatedCategories()
      form.setFieldsValue({
        name: '',
        capacity: '',
        icon: ''
        // related_categories: ''
      })
    }
    setIsEditFormType(isEditFormType)
    setErrorMsg('')
    setIsSpin(false)
    return () => {}
  }, [props.show, props.data]) // eslint-disable-line react-hooks/exhaustive-deps
  /////////////////////////////////////////////////////
  const fetchRelatedCategories = async (id = null) => {
    try {
      if (props?.data.id) {
        var queryParams = {
          exclude_categories: id
        }
      }
      const res = await getRelatedCategories(queryParams)
      setRelatedCategory(res.data.rows)
    } catch (error) {}
  }
  useEffect(() => {
    const defaultCheckedList = []
    const defaultCheckedId = []
    relatedCategory.map(
      (category) =>
        defaultCheckedList.push({
          label: category.name,
          value: category.id.toString()
        })
      // defaultCheckedId.push(category.id.toString())

      // defaultCheckedList.push(category.name)
    )
    props.data &&
      props.data.related_categories &&
      props.data.related_categories.map((categoryId) =>
        defaultCheckedId.push(categoryId.related_category_id.toString())
      )
    setCheckedList(defaultCheckedList)
    if (isEditFormType && props.data.related_categories.length > 0) {
      setCheckedId(defaultCheckedId)
    }
  }, [relatedCategory, isEditFormType]) // eslint-disable-line react-hooks/exhaustive-deps
  ////////////////////////////////////////////////////

  const onFileUploaded = (photo) => {
    setIsImage(photo)
  }
  const handleDropdown = (value) => {}

  const onFinish = async (values) => {
    setIsSpin(true)
    setErrorMsg('')
    try {
      if (checkBoxValue) {
        values.related_categories = checkBoxValue
      } else {
        values.related_categories = checkedId
      }

      const { filterData } = filterDataObj(values)
      const formData = new FormData()
      formData.append('name', filterData.name)
      formData.append('capacity', filterData.capacity)
      formData.append('related_categories', filterData.related_categories)

      if (filterData.password) {
        formData.append('password', filterData.password)
      }

      if (isImage) {
        formData.append('icon', isImage, isImage.name)
      }

      const res = await updateCategoryService(formData, props.data.id)
      if (res && res.success) {
        setIsSpin(false)
        modalNotification({
          type: 'success',
          message: 'Success',
          description: res.message
        })
      }
      props.onHide()
    } catch (error) {
      setIsSpin(false)
      setErrorMsg(error.message)
      logger({ 'error:': error })
    }
  }

  const onFinishFailed = (errorInfo) => {
    logger({ 'Failed:': errorInfo })
  }

  const onCancel = () => {
    form.resetFields()
    props.onHide()
  }

  if (!props.show) {
    return <></>
  }

  return (
    <Modal
      title={t('riders.edit_riders')}
      show={props.show}
      onHide={props.onHide}
      size='md'
      aria-labelledby='contained-modal-title-vcenter'
      className='editModal'
      centered
    >
      <Modal.Header>
        <Modal.Title id='contained-modal-title-vcenter'>
          <h2>
            {isEditFormType
              ? t('settings.editCategory')
              : t('settings.addCategory')}
          </h2>
        </Modal.Title>
      </Modal.Header>
      <Modal.Body>
        {props.show ? (
          <AddEditCategoryForm
            setCheckBoxValue={setCheckBoxValue}
            form={form}
            onCancel={onCancel}
            isSpin={isSpin}
            onFinish={onFinish}
            onFinishFailed={onFinishFailed}
            onFileUploaded={onFileUploaded}
            media={
              isEditFormType
                ? props.data.icon
                : 'assets/images/default-userNew.jpg'
            }
            checkedList={checkedList}
            checkedId={checkedId}
            // relatedCategory={props.relatedCategory}
            submitButtonText={
              isEditFormType ? t('common.update') : t('common.save')
            }
            errorMsg={errorMsg}
            isEditForm={isEditFormType}
            handleDropdown={handleDropdown}
          />
        ) : (
          ''
        )}
      </Modal.Body>
    </Modal>
  )
}

AddEditCategoryModal.propTypes = {
  data: PropTypes.object.isRequired,
  show: PropTypes.bool.isRequired,
  onHide: PropTypes.func.isRequired
}
// useEffect(() => {
//   fetchRelatedCategories()
// }, [props.data.fare_setting])

// const fetchRelatedCategories = async () => {
//   console.log(props.data, 'fetchRelatedCategories')
//   try {
//     const queryParams = null
//     if (props?.data.fare_setting?.category_id) {
//       queryParams = {
//         exclude_categories: props.data.fare_setting.category_id
//       }
//     }

//     const data = await getRelatedCategories(queryParams)
//     setRelatedCategory(data.data.rows)
//   } catch (error) {}
// }
