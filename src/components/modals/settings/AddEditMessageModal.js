import React, { useState, useEffect } from 'react'
import { useTranslation } from 'react-i18next'
import { Modal } from 'react-bootstrap'
// import APIrequest from '../../../services/apiRequest'
import modalNotification from '../../../utilities/notifications'
import { Form } from 'antd'
import PropTypes from 'prop-types'
// import ApiEndPoints from '../../../utilities/apiEndPoints'
// import AddEditExecutiveForm from '../../forms/excutive/addEditExecutiveForm.js'
import logger from '../../../utilities/logger'
import { filterDataObj } from '../../../utilities/common'
// import AddEditRidersForm from '../../forms/riders/addEditRidersForm'
// import { updateRiderService } from '../../../services/riders'
// import AddEditCategoryForm from '../../forms/settings/addEditCategoryForm'
import {
  // getRelatedCategories,
  // updateCategoryService,
  // updateMakeService,
  updateMessageService
} from '../../../services/settings'
// import AddEditMakeForm from '../../forms/settings/addEditMakeForm'
import AddEditMessageForm from '../../forms/settings/AddEditMessageForm'
export default function AddEditMessageModal(props) {
  const { t } = useTranslation()
  const [isSpin, setIsSpin] = useState(false)
  const [errorMsg, setErrorMsg] = useState('')
  // const [checkBoxValue, setCheckBoxValue] = useState(null)
  const [isEditFormType, setIsEditFormType] = useState(
    Object.keys(props.data).length > 0
  )
  // const [isImage, setIsImage] = useState()
  // const [relatedCategory, setRelatedCategory] = useState()
  const [form] = Form.useForm()

  useEffect(() => {
    const isEditFormType = Object.keys(props.data).length > 0
    // console.log("isEditFormType",isEditFormType)
    if (props.show && isEditFormType) {
      form.setFieldsValue({
        message: props.data.message,
        sequence_number: props.data.sort_order,
        user_type: props.data.user_type,
        type: props.data.type
      })
    } else {
      form.setFieldsValue({
        message: '',
        sequence_number: '',
        user_type: '',
        type: ''
      })
    }
    setIsEditFormType(isEditFormType)
    setErrorMsg('')
    setIsSpin(false)
  }, [props.show, props.data]) // eslint-disable-line react-hooks/exhaustive-deps

  const handleSequenceNumberChange = (sequenceNumberData) => {
    console.log(sequenceNumberData, 'sequence')
    form.setFieldsValue({
      sequence_number: sequenceNumberData
    })
  }
  const onFinish = async (values) => {
    setIsSpin(true)
    setErrorMsg('')
    try {
      console.log(values, 'values')
      const { filterData } = filterDataObj(values)
      const res = await updateMessageService(filterData, props.data.id)
      if (res && res.success) {
        setIsSpin(false)
        modalNotification({
          type: 'success',
          message: 'Success',
          description: res.message
        })
      }

      props.onHide('edited')
    } catch (error) {
      setIsSpin(false)
      setErrorMsg(error.message)
      logger({ 'error:': error })
    }
  }

  const onFinishFailed = (errorInfo) => {
    logger({ 'Failed:': errorInfo })
  }

  const onCancel = () => {
    form.resetFields()
    props.onHide()
  }

  if (!props.show) {
    return <></>
  }

  return (
    <Modal
      title={t('riders.edit_riders')}
      show={props.show}
      onHide={props.onHide}
      size='md'
      aria-labelledby='contained-modal-title-vcenter'
      className='editModal'
      centered
    >
      <Modal.Header>
        <Modal.Title id='contained-modal-title-vcenter'>
          <h2>
            {isEditFormType
              ? t('settings.editMessage')
              : t('settings.addNewMessage')}
          </h2>
        </Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <AddEditMessageForm
          // setCheckBoxValue={setCheckBoxValue}
          form={form}
          onCancel={onCancel}
          isSpin={isSpin}
          onFinish={onFinish}
          onFinishFailed={onFinishFailed}
          //   onFileUploaded={onFileUploaded}
          media={
            isEditFormType
              ? props.data.icon
              : 'assets/images/default-userNew.jpg'
          }
          initialValues={
            isEditFormType
              ? {}
              : {
                  status: 'active'
                }
          }
          // relatedCategory={relatedCategory}
          submitButtonText={
            isEditFormType ? t('common.update') : t('common.save')
          }
          errorMsg={errorMsg}
          isEditForm={isEditFormType}
          handleSequenceNumberChange={handleSequenceNumberChange}
        />
      </Modal.Body>
    </Modal>
  )
}

AddEditMessageModal.propTypes = {
  data: PropTypes.object.isRequired,
  show: PropTypes.bool.isRequired,
  onHide: PropTypes.func.isRequired
}
