import React, { useState, useEffect } from 'react'
import { useTranslation } from 'react-i18next'
import { Modal } from 'react-bootstrap'
// import APIrequest from '../../../services/apiRequest'
import modalNotification from '../../../utilities/notifications'
import { Form } from 'antd'
import PropTypes from 'prop-types'
// import ApiEndPoints from '../../../utilities/apiEndPoints'
// import AddEditExecutiveForm from '../../forms/excutive/addEditExecutiveForm.js'
import logger from '../../../utilities/logger'
import { filterDataObj } from '../../../utilities/common'
// import AddEditRidersForm from '../../forms/riders/addEditRidersForm'
// import { updateRiderService } from '../../../services/riders'
// import AddEditCategoryForm from '../../forms/settings/addEditCategoryForm'
import { updateModelService } from '../../../services/settings'
import AddEditModelForm from '../../forms/settings/addEditModelForm'
export default function AddEditModelModal(props) {
  const { t } = useTranslation()
  const [isSpin, setIsSpin] = useState(false)
  const [errorMsg, setErrorMsg] = useState('')
  // const [checkBoxValue, setCheckBoxValue] = useState(null)

  const [isEditFormType, setIsEditFormType] = useState(
    Object.keys(props.data).length > 0
  )
  // const [isImage, setIsImage] = useState()
  // const [relatedCategory, setRelatedCategory] = useState()
  const [form] = Form.useForm()

  //
  // const fetchRelatedCategories = async () => {
  //   try {
  //     const queryParams = {
  //       exclude_categories: props.data.fare_setting.category_id
  //     }
  //     const data = await getRelatedCategories(queryParams)
  //     setRelatedCategory(data.data.rows)
  //   } catch (error) {}
  // }
  useEffect(() => {
    // fetchRelatedCategories()
    const isEditFormType = Object.keys(props.data).length > 0
    // console.log("isEditFormType",isEditFormType)
    if (props.show && isEditFormType) {
      form.setFieldsValue({
        name: props.data.name,
        category_id: props.data.category.id,
        brand_id: props.data.brand.id
      })
    } else {
      form.setFieldsValue({
        name: '',
        category_id: '',
        brand_id: ''
      })
    }
    setIsEditFormType(isEditFormType)
    setErrorMsg('')
    setIsSpin(false)
  }, [props.show, props.data]) // eslint-disable-line react-hooks/exhaustive-deps

  // const onFileUploaded = (photo) => {
  //   setIsImage(photo)
  // }

  const onFinish = async (values) => {
    setIsSpin(true)
    setErrorMsg('')
    try {
      console.log(values, 'values')

      const { filterData } = filterDataObj(values)
      // const formData = new FormData()
      // formData.append('name', filterData.name)
      // formData.append('capacity', filterData.capacity)
      // formData.append('icon', filterData.icon)
      // formData.append('related_categories', filterData.related_categories)

      // if (filterData.password) {
      //   formData.append('password', filterData.password)
      // }

      // if (isImage) {
      //   formData.append('icon', isImage, isImage.name)
      // }

      // //   console.log('formData', formData)

      // //   axios.post(
      // //     'https://sunpick-api.codiantdev.com/api/admin/agent/11/edit',
      // //     formData
      // //   )
      const res = await updateModelService(filterData, props.data.id)
      console.log('res.data.message', res.message)
      if (res && res.success) {
        setIsSpin(false)
        modalNotification({
          type: 'success',
          message: 'Success',
          description: res.message
        })
      }

      props.onHide()
    } catch (error) {
      setIsSpin(false)
      setErrorMsg(error.message)
      logger({ 'error:': error })
    }
  }

  const onFinishFailed = (errorInfo) => {
    logger({ 'Failed:': errorInfo })
  }

  const onCancel = () => {
    form.resetFields()
    props.onHide()
  }

  if (!props.show) {
    return <></>
  }

  return (
    <Modal
      title={t('riders.edit_riders')}
      show={props.show}
      onHide={props.onHide}
      size='md'
      aria-labelledby='contained-modal-title-vcenter'
      className='editModal'
      centered
    >
      <Modal.Header>
        <Modal.Title id='contained-modal-title-vcenter'>
          <h2>
            {isEditFormType ? t('settings.editModel') : t('settings.addModel')}
          </h2>
        </Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <AddEditModelForm
          // setCheckBoxValue={setCheckBoxValue}
          form={form}
          onCancel={onCancel}
          isSpin={isSpin}
          onFinish={onFinish}
          onFinishFailed={onFinishFailed}
          // onFileUploaded={onFileUploaded}
          // media={
          //   isEditFormType
          //     ? props.data.icon
          //     : 'assets/images/default-userNew.jpg'
          // }
          initialValues={
            isEditFormType
              ? {}
              : {
                  status: 'active'
                }
          }
          submitButtonText={
            isEditFormType ? t('common.update') : t('common.save')
          }
          errorMsg={errorMsg}
          isEditForm={isEditFormType}
          categoryDropdown={props.categoryDropdown}
          makeDropdown={props.makeDropdown}
        />
      </Modal.Body>
    </Modal>
  )
}

AddEditModelModal.propTypes = {
  data: PropTypes.object.isRequired,
  show: PropTypes.bool.isRequired,
  onHide: PropTypes.func.isRequired
}
