import React, { useState } from 'react'
import { Modal } from 'react-bootstrap'
import { Button } from 'antd'
import PropTypes from 'prop-types'
import LoadingSpinner from '../subComponent/loadingSpinner'
import GlobalLoader from '../subComponent/globalLoader'

export default function ConfirmationAndInfo(props) {
  const [isSpin, setIsSpin] = useState(false)

  const confirmation = async () => {
    // setIsSpin(true)
    try {
      const res = await props.onConfirmation()
      if (res) {
        setIsSpin(false)
        props.onHide()
      }
    } catch (error) {
      setIsSpin(false)
    }
  }

  if (!props.show) {
    return <></>
  }

  return (
    <Modal
      show={props.show}
      onHide={props.onHide}
      size='sm'
      className='editModal'
      aria-labelledby='contained-modal-title-vcenter'
      centered
    >
      <Modal.Header className='justify-content-center text-center'>
        <Modal.Title id='contained-modal-title-vcenter'>
          <h2 className='modal-title text-capitalize'>
            {props.title}
          </h2>
        </Modal.Title>
      </Modal.Header>
      <Modal.Body className=' p-20'>
        {props.showLoading ? (
          <GlobalLoader />
        ) : (
          <>
            <p>{props.message}</p>
            <div className='btn-row text-center'>
              {props.textOnConfirmBtn && (
                <Button
                  disabled={isSpin}
                  className='btn btn-warning width-120 ripple-effect text-uppercase'
                  onClick={() => confirmation()}
                >
                  {isSpin ? <LoadingSpinner /> : props.textOnConfirmBtn}
                </Button>
              )}
              {props.textOnCancelBtn && (
                <Button
                  className='btn btn-cancel width-120 ripple-effect text-uppercase ml-3 '
                  onClick={props.onHide}
                >
                  {props.textOnCancelBtn}
                </Button>
              )}
            </div>
          </>
        )}
      </Modal.Body>
    </Modal>
  )
}

ConfirmationAndInfo.propTypes = {
  show: PropTypes.bool.isRequired,
  onHide: PropTypes.func.isRequired,
  title: PropTypes.string.isRequired,
  message: PropTypes.string.isRequired,
  textOnCancelBtn: PropTypes.string.isRequired,
  textOnConfirmBtn: PropTypes.string,
  onConfirmation: PropTypes.func,
  showLoading: PropTypes.bool
}
