import React, { useState, useEffect } from 'react'
import { useTranslation } from 'react-i18next'
import { Modal } from 'react-bootstrap'
import { changeExecutivePasswordService } from '../../../services/executives'
import modalNotification from '../../../utilities/notifications'
import { Form } from 'antd'
import PropTypes from 'prop-types'
import ChangePassExecutiveForm from '../../forms/excutive/passwordExecutiveForm';
import logger from '../../../utilities/logger'
import {
  filterDataObj
} from '../../../utilities/common'
export default function ChangeExecutiveModal (props) {
  const { t } = useTranslation()
  const [isSpin, setIsSpin] = useState(false)
  const [errorMsg, setErrorMsg] = useState('')
  const [form] = Form.useForm()

  useEffect(() => {
      form.setFieldsValue({
        new_password: '',
        confirm_password: '',
      })
    
    setErrorMsg('')
    setIsSpin(false)
  }, [props.show]) // eslint-disable-line react-hooks/exhaustive-deps

  const onFinish = async values => {
    setIsSpin(true)
    setErrorMsg('')
    try {
      const { filterData } = filterDataObj(values)
      filterData['password'] = filterData.new_password

      const res = await changeExecutivePasswordService(props.data.id,filterData)
      if (res && res.success) {
        setIsSpin(false)
        modalNotification({
          type: 'success',
          message: 'Success',
          description: res.message
        })
      }
 
      props.onHide()
    } catch (error) {
      setIsSpin(false)
      setErrorMsg(error.message)
      logger({ 'error:': error })
    }
  }

  const onFinishFailed = errorInfo => {
    logger({ 'Failed:': errorInfo })
  }

  const onCancel = () => {
    form.resetFields()
    props.onHide()
  }

  if (!props.show) {
    return <></>
  }

  return (
    <Modal
      show={props.show}
      onHide={props.onHide}
      size='md'
      aria-labelledby='contained-modal-title-vcenter'
      className='editModal'
      centered
    >
      <Modal.Header>
        <Modal.Title id='contained-modal-title-vcenter'>
          <h2>{ t('common.changePassword')}</h2>
        </Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <ChangePassExecutiveForm
          form={form}
          onCancel={onCancel}
          isSpin={isSpin}
          onFinish={onFinish}
          onFinishFailed={onFinishFailed}
         
          submitButtonText={ t('common.update') }
          errorMsg={errorMsg}
        />
      </Modal.Body>
    </Modal>
  )
}

ChangeExecutiveModal.propTypes = {
  show: PropTypes.bool.isRequired,
  onHide: PropTypes.func.isRequired,
  data: PropTypes.object.isRequired,
  
}
