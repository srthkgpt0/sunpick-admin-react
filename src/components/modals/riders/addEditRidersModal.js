import React, { useState, useEffect } from 'react'
import { useTranslation } from 'react-i18next'
import { Modal } from 'react-bootstrap'
// import APIrequest from '../../../services/apiRequest'
import modalNotification from '../../../utilities/notifications'
import { Form } from 'antd'
import PropTypes from 'prop-types'
// import ApiEndPoints from '../../../utilities/apiEndPoints'
// import AddEditExecutiveForm from '../../forms/excutive/addEditExecutiveForm.js'
import logger from '../../../utilities/logger'
import { filterDataObj } from '../../../utilities/common'
import AddEditRidersForm from '../../forms/riders/addEditRidersForm'
import { updateRiderService } from '../../../services/riders'
export default function AddEditRidersModal(props) {
  const { t } = useTranslation()
  const [isSpin, setIsSpin] = useState(false)
  const [errorMsg, setErrorMsg] = useState('')
  const [isEditFormType, setIsEditFormType] = useState(
    Object.keys(props.data).length > 0
  )
  // const [isimage, setIsimage] = useState()
  const [form] = Form.useForm()

  useEffect(() => {
    const isEditFormType = Object.keys(props.data).length > 0
    // console.log("isEditFormType",isEditFormType)
    if (props.show && isEditFormType) {
      form.setFieldsValue({
        first_name: props.data.first_name,
        last_name: props.data.last_name,
        photo: props.data.photo,
        phone_number: props.data.phone_number,
        email: props.data.email,
        phone_number_country_code: {
          value: props.data.phone_number_country_code
        }
      })
    } else {
      form.setFieldsValue({
        first_name: '',
        last_name: '',
        photo: '',
        phone_number: '',
        email: '',
        phone_number_country_code: { value: 'select' }
      })
    }
    setIsEditFormType(isEditFormType)
    setErrorMsg('')
    setIsSpin(false)
  }, [props.show, props.data]) // eslint-disable-line react-hooks/exhaustive-deps

  // const onFileUploaded = (photo) => {
  //   setIsimage(photo)
  // }

  const onFinish = async (values) => {
    setIsSpin(true)
    setErrorMsg('')
    try {
      const { filterData } = filterDataObj(values)
      //   console.log(
      //     'phone_number_country_code',
      //     filterData.phone_number_country_code.value,
      //     'ssss',
      //     props.data.phone_number_country_code
      //   )
      //   const formData = new FormData()
      //   formData.append('first_name', filterData.first_name)
      //   formData.append('last_name', filterData.last_name)
      //   formData.append('email', filterData.email)
      //   //   formData.append(
      //   //     'phone_number_country',
      //   //     props.data.phone_number_country
      //   //       ? props.data.phone_number_country
      //   //       : 'ETB'
      //   //   )
      //   //   formData.append(
      //   //     'phone_number_country_code',
      //   //     filterData.phone_number_country_code.value
      //   //       ? filterData.phone_number_country_code.value
      //   //       : props.data.phone_number_country_code
      //   //   )
      //   formData.append('phone_number', filterData.phone_number)

      //   if (filterData.password) {
      //     formData.append('password', filterData.password)
      //   }

      //   if (isimage) {
      //     formData.append('photo', isimage, isimage.name)
      //   }

      //   console.log('formData', isimage)

      // axios.post('https://sunpick-api.codiantdev.com/api/admin/agent/11/edit', formData)
      //   console.log(formData, 'FORMDATA IT IS')
      const res = await updateRiderService(filterData, props.data.id)
      console.log('res.data.message', res.message)
      if (res && res.success) {
        setIsSpin(false)
        modalNotification({
          type: 'success',
          message: 'Success',
          description: res.message
        })
      }

      props.onHide()
    } catch (error) {
      setIsSpin(false)
      setErrorMsg(error.message)
      logger({ 'error:': error })
    }
  }

  const onFinishFailed = (errorInfo) => {
    logger({ 'Failed:': errorInfo })
  }

  const onCancel = () => {
    form.resetFields()
    props.onHide()
  }

  if (!props.show) {
    return <></>
  }

  return (
    <Modal
      title={t('riders.edit_riders')}
      show={props.show}
      onHide={props.onHide}
      size='md'
      aria-labelledby='contained-modal-title-vcenter'
      className='editModal'
      centered
    >
      <Modal.Header>
        <Modal.Title id='contained-modal-title-vcenter'>
          <h2>
            {isEditFormType
              ? t('riders.edit_riders')
              : t('executives.add_executive')}
          </h2>
        </Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <AddEditRidersForm
          form={form}
          onCancel={onCancel}
          isSpin={isSpin}
          onFinish={onFinish}
          onFinishFailed={onFinishFailed}
          // onFileUploaded={onFileUploaded}
          media={
            isEditFormType
              ? props.data.photo
              : 'assets/images/default-userNew.jpg'
          }
          initialValues={
            isEditFormType
              ? {}
              : {
                  status: 'active'
                }
          }
          submitButtonText={
            isEditFormType ? t('common.update') : t('common.save')
          }
          errorMsg={errorMsg}
          isEditForm={isEditFormType}
        />
      </Modal.Body>
    </Modal>
  )
}

AddEditRidersModal.propTypes = {
  data: PropTypes.object.isRequired,
  show: PropTypes.bool.isRequired,
  onHide: PropTypes.func.isRequired
}
