import React, { useState } from 'react'
import { useTranslation } from 'react-i18next'
import { Modal } from 'react-bootstrap'
import modalNotification from '../../../utilities/notifications'
import { Form } from 'antd'
import PropTypes from 'prop-types'
import logger from '../../../utilities/logger'
import { currency_symbol, filterDataObj } from '../../../utilities/common'
import { assignDriverService } from '../../../services/rides/index'

import AssignDriverForm from '../../forms/rides/assignDriverForm'
export default function AssignDriverModal(props) {
  const { t } = useTranslation()
  const [isSpin, setIsSpin] = useState(false)
  const [errorMsg, setErrorMsg] = useState('')
  const [form] = Form.useForm()
  const { data } = props

  const onFinish = async (values) => {
    setIsSpin(true)
    setErrorMsg('')
    try {
      console.log(values, 'values')
      const { filterData } = filterDataObj(values)
      const res = await assignDriverService(filterData, props.ride.id)
      if (res && res.success) {
        setIsSpin(false)
        modalNotification({
          type: 'success',
          message: 'Success',
          description: res.message
        })
      }
      props.onHide()
    } catch (error) {
      setIsSpin(false)
      setErrorMsg(error.message)
      logger({ 'error:': error })
    }
  }

  const onFinishFailed = (errorInfo) => {
    logger({ 'Failed:': errorInfo })
  }

  const onCancel = () => {
    form.resetFields()
    props.onHide()
  }

  if (!props.show) {
    return <></>
  }

  return (
    <Modal
      title={t('rides.assignDriver')}
      show={props.show}
      onHide={props.onHide}
      size='md'
      aria-labelledby='contained-modal-title-vcenter'
      className='editModal'
      centered
    >
      <Modal.Header>
        <Modal.Title id='contained-modal-title-vcenter'>
          <h2>{t('rides.assignDriver')}</h2>
        </Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <div className='modal-body'>
          <div className='ride_user_details'>
            <div
              className='card-body p-0'
              style={{ overflow: 'inherit!important' }}
            >
              <div className='common_border'>
                <div className='profile '>
                  <div className='p_img'>
                    <img
                      src={
                        data?.customer?.photo ||
                        'assets/images/default-userNew.jpg'
                      }
                      className='rounded-circle'
                      alt='user img'
                    />
                  </div>
                  <div className='p_content'>
                    <div className='info'>
                      <h6 className='text-truncate text-capitalize font-heavy'>
                        {`${
                          data?.customer?.first_name
                            ? data?.customer?.first_name
                            : '-'
                        } ${
                          data?.customer?.last_name
                            ? data?.customer?.last_name
                            : '-'
                        }`}
                      </h6>
                    </div>

                    <div className='view_info d-flex align-items-center justify-content-between'>
                      <p className='mb-0 font-book h-14'>
                        Contact No:
                        <span className='font-heavy'>
                          {`${
                            data?.customer?.phone_number_country_code || ''
                          }-${data?.customer?.phone_number || ' '}`}
                        </span>
                      </p>
                    </div>
                  </div>
                </div>
              </div>

              {data?.book_for_someone && data?.passenger_name && (
                <div className='book_for_someone common_border'>
                  <ul className='list-unstyled mb-0'>
                    <li className=' mb-3'>
                      <p className='mb-0'>{data?.passenger_name || '-'}</p>
                    </li>
                    <li>
                      <p className='mb-0'>
                        {data?.passenger_phone_number_country_code
                          ? data?.passenger_phone_number_country_code + '-'
                          : ''}
                        {data?.passenger_mobile || '-'}
                      </p>
                    </li>
                  </ul>
                </div>
              )}

              <div className='scheduled d-flex align-items-center common_border'>
                <div className='d-flex align-items-center'>
                  <i className='icon-timer icon'></i>
                  <div className='scheduled_time'>
                    <h4 className='h-14'>Scheduled on</h4>
                    <p className='mb-0'>{data?.booking_date || ''}</p>
                  </div>
                </div>
              </div>
              <div className='location common_border'>
                <ul className='list-unstyled mb-0'>
                  <li>
                    <h4 className='h-14 '>Pickup Location</h4>
                    <p className='mb-0'>{data?.pick_up_address}</p>
                  </li>
                  {data.way_point.map((wayPoint, index) => (
                    <li key={index}>
                      <h4>Way-Point Location</h4>
                      <p className='mb-0'>{wayPoint?.address}</p>
                    </li>
                  ))}

                  <li>
                    <h4>Drop-off Location</h4>
                    <p className='mb-0'>{data?.drop_off_address} </p>
                  </li>
                </ul>
              </div>
              <div className='fare_info common_border'>
                <div className='row'>
                  <div className='col-sm-4 mb-sm-0'>
                    <h4>Fare</h4>
                    <p className='mb-0'>
                      {`${currency_symbol} ${data?.ride_amount}`}
                    </p>
                  </div>
                  <div className='col-sm-4 mb-3 mb-sm-0'>
                    <h4>Distance</h4>
                    <p className='mb-0'> {data?.ride_km} km</p>
                  </div>
                  <div className='col-sm-4 mb-3 mb-sm-0'>
                    <h4>Time</h4>
                    <p className='mb-0'> {data?.estimate_time} min</p>
                  </div>
                </div>
              </div>
              <div className='car_info common_border '>
                <div className='row'>
                  <div className='col-sm-6 mb-3 mb-sm-0'>
                    <h4>Category</h4>
                    <p className='mb-0 text-capitalize'>{data?.car_category}</p>
                  </div>
                  <div className='col-sm-6 mb-3 mb-sm-0'>
                    <h4>Coupon Code</h4>
                    <p className='mb-0'>{data?.promo_code || '-'}</p>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <AssignDriverForm
          form={form}
          onCancel={onCancel}
          isSpin={isSpin}
          onFinish={onFinish}
          onFinishFailed={onFinishFailed}
          availableDrivers={props.availableDrivers}
          handleRefresh={props.handleRefresh}
          errorMsg={errorMsg}
          ride={props.ride}
          loadingDriverList={props.loadingDriverList}
        />
      </Modal.Body>
    </Modal>
  )
}

AssignDriverModal.propTypes = {
  data: PropTypes.object.isRequired,
  show: PropTypes.bool.isRequired,
  onHide: PropTypes.func.isRequired
}
