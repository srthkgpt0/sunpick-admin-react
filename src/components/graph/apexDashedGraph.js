import React from 'react'
import { Bar } from 'react-chartjs-2'

export default function BarChart(data) {
  const month = []
  const driver = []
  const rider = []
  data.graphData.map((ele) => {
    month.push(ele.month)
    driver.push(ele.driver)
    rider.push(ele.customer)
    return null
  })
  const options = {
    responsive: true,
    legend: {
      display: false
    },
    layout: {
      padding: {
        left: -10
      }
    },
    scales: {
      yAxes: [
        {
          ticks: {
            beginAtZero: true
          }
        }
      ]
      // xAxes: [{
      //     categoryPercentage: 1.0,
      //     barPercentage: 1.0
      // }]
    }
  }
  const dataSet = {
    labels: month,
    datasets: [
      {
        label: 'driver',
        data: driver,
        backgroundColor: 'rgb(64, 153, 255)',
        borderColor: 'rgb(64, 153, 255)',
        borderWidth: 1
      },
      {
        label: 'rider',
        data: rider,
        backgroundColor: 'rgb(255, 83, 112)',
        borderColor: 'rgb(255, 83, 112)',
        borderWidth: 1
      }
    ]
  }
  return (
    <div>
      <Bar height={300} width={1000} data={dataSet} options={options} />
    </div>
  )
}
