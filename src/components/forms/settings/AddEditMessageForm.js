import React from 'react'
import { useTranslation } from 'react-i18next'
import { Form, Button, Alert, Select } from 'antd'
import PropTypes from 'prop-types'
import { LoadingSpinner } from '../../common'
// import { currentTimeStamp } from '../../../utilities/common'
import validation from '../../../utilities/validation'
import TextArea from 'antd/lib/input/TextArea'
// import { useState } from 'react'
// import { default_const_flag } from '../../../utilities/country-flag-list'
// import { getRelatedCategories } from '../../../services/settings'

export default function AddEditMessageForm({
  form,
  onCancel,
  isSpin,
  onFinish,
  onFinishFailed,
  onFileUploaded,
  initialValues = {},
  media,
  submitButtonText,
  errorMsg,
  isEditForm,
  handleSequenceNumberChange
}) {
  const { t } = useTranslation()
  const { Option } = Select
  //   const [prevImage, setPrevImage] = useState()

  //   const handleCheckBoxChange = (checkedValues) => {
  //     setCheckBoxValue(checkedValues.target.value)
  //     console.log(checkedValues)
  //   }
  //   const handleChange = (event) => {
  //     const file = event.target.files[0]
  //     console.log('File')
  //     onFileUploaded(file)
  //     // setIsimage(event.target.files[0])
  //     let reader = new FileReader()
  //     reader.readAsDataURL(file)
  //     reader.onloadend = (e) => {
  //       setPrevImage(reader.result)
  //     }
  //   }
  const sequenceNumber = [1, 2, 3, 4, 5, 6, 7]
  const userType = [
    { name: 'Driver', key: 'driver' },
    { name: 'Customer', key: 'customer' }
  ]
  const messageType = [
    { name: 'Chat', key: 'chat' },
    { name: 'Cancellation', key: 'cancellation' }
  ]
  return (
    <>
      {errorMsg && <Alert message={errorMsg} className='mb-4' type='error' />}
      <Form
        name='messageAddEdit'
        form={form}
        onFinish={onFinish}
        onFinishFailed={onFinishFailed}
        initialValues={initialValues}
      >
        <div className='row'>
          <div className='col-sm-12'>
            <div className='form-group'>
              <Form.Item
                name='message'
                rules={validation?.addMessage?.textTemplate}
              >
                <TextArea rows={4} />
              </Form.Item>
            </div>
          </div>
          <div className='col-sm-12'>
            <div className='form-group'>
              <label>
                Sequence Number <span>*</span>
              </label>
              <Form.Item
                name='sequence_number'
                rules={validation?.addMessage?.sequenceNumber}
              >
                <Select
                  placeholder='Select'
                  onChange={handleSequenceNumberChange}
                >
                  {sequenceNumber.map((sequence, index) => (
                    <Option key={index} id={sequence} value={sequence}>
                      {sequence}
                    </Option>
                  ))}
                </Select>
              </Form.Item>
            </div>
          </div>
          <div className='col-sm-12'>
            <div className='form-group'>
              <label>
                User Type <span>*</span>
              </label>
              <Form.Item
                name='user_type'
                rules={validation?.addMessage?.userType}
              >
                <Select placeholder='Select'>
                  {userType.map((user, index) => (
                    <Option key={index} value={user.key}>
                      {user.name}
                    </Option>
                  ))}
                </Select>
              </Form.Item>
            </div>
          </div>
          <div className='col-sm-12'>
            <div className='form-group'>
              <label>
                Message Type <span>*</span>
              </label>
              <Form.Item
                name='type'
                rules={validation?.addMessage?.messageType}
              >
                <Select placeholder='Select'>
                  {messageType.map((message, index) => (
                    <Option key={index} value={message.key}>
                      {message.name}
                    </Option>
                  ))}
                </Select>
              </Form.Item>
              <div>Note: Please use Enter Key to display text in new line</div>
            </div>
            
          </div>
        </div>

        <div className='form-group btn-row text-center mb-0'>
          <Form.Item>
            <Button
              disabled={isSpin}
              htmlType='submit'
              className='btn btn-warning width-120 ripple-effect text-uppercase'
            >
              {isSpin ? <LoadingSpinner /> : submitButtonText}
            </Button>
            <Button
              htmlType='button'
              onClick={onCancel}
              className='btn btn-cancel width-120 ripple-effect text-uppercase'
            >
              {t('common.cancel')}
            </Button>
          </Form.Item>
        </div>
      </Form>
    </>
  )
}

AddEditMessageForm.propTypes = {
  form: PropTypes.any.isRequired,
  onCancel: PropTypes.func.isRequired,
  isSpin: PropTypes.bool.isRequired,
  onFinish: PropTypes.func.isRequired,
  onFinishFailed: PropTypes.func.isRequired,
  // onFileUploaded: PropTypes.func.isRequired,
  initialValues: PropTypes.object.isRequired,
  media: PropTypes.any.isRequired,
  submitButtonText: PropTypes.string.isRequired,
  errorMsg: PropTypes.string.isRequired
}
