import React, { useEffect } from 'react'
import { useTranslation } from 'react-i18next'
import { Form, Input, Select, Button, Alert, Checkbox } from 'antd'
import PropTypes from 'prop-types'
import { LoadingSpinner } from '../../common'
// import { currentTimeStamp } from '../../../utilities/common'
import validation from '../../../utilities/validation'
import { useState } from 'react'
// import { default_const_flag } from '../../../utilities/country-flag-list'
// import { getRelatedCategories } from '../../../services/settings'

export default function AddEditCategoryForm({
  form,
  onCancel,
  isSpin,
  onFinish,
  onFinishFailed,
  onFileUploaded,
  initialValues = {},
  media,
  submitButtonText,
  errorMsg,
  isEditForm,
  relatedCategory,
  setCheckBoxValue,
  handleDropdown,
  checkedList,
  checkedId
}) {
  const { t } = useTranslation()
  const { Option } = Select
  const [prevImage, setPrevImage] = useState()
  const [onChangeCheckList, setOnChangeCheckList] = useState([])

  const handleCheckBoxChange = (checkedValues) => {
    // setCheckBoxValue(checkedValues.target.value)
    setOnChangeCheckList(checkedValues)
    setCheckBoxValue(checkedValues)
  }
  useEffect(() => {
    setOnChangeCheckList(checkedId)
  }, [checkedId])
  const handleChange = (event) => {
    const file = event.target.files[0]
    onFileUploaded(file)
    // setIsimage(event.target.files[0])
    let reader = new FileReader()
    reader.readAsDataURL(file)
    reader.onloadend = (e) => {
      setPrevImage(reader.result)
    }
  }
  const seatingCapacityArray = ['2', '3', '4', '5', '6', '7']
  return (
    <>
      {errorMsg && <Alert message={errorMsg} className='mb-4' type='error' />}
      <Form
        name='albumAddEdit'
        form={form}
        onFinish={onFinish}
        onFinishFailed={onFinishFailed}
      >
        <div className='form-group'>
          <Form.Item name='icon' rules={validation?.addDriver.uploadImage}>
            <div className='form-group text-center '>
              <div className='uploadimg position-relative'>
                <div className='img'>
                  <img
                    id='uploadimage'
                    src={`${prevImage ? prevImage : media}`}
                    className=''
                    alt='use img'
                  />
                </div>
                <label htmlFor='UploadImage'>
                  <i className='icon-photo_camera'></i>
                </label>

                <Input
                  type='file'
                  id='UploadImage'
                  hidden
                  placeholder='Icon'
                  onChange={(e) => {
                    handleChange(e)
                  }}
                  // defaultValue={`${media}`}
                />
              </div>
            </div>
          </Form.Item>
        </div>

        <div className='row'>
          <div className='col-sm-12'>
            <div className='form-group'>
              <label>
                Category name
                <span className='errorMessage'>*</span>
              </label>
              <Form.Item
                name='name'
                rules={validation?.addCategory?.categoryName}
              >
                <Input type='text' placeholder='Category name' />
              </Form.Item>
            </div>
          </div>
          <div className='col-sm-12'>
            <div className='form-group'>
              <label>
                Seating Capacity
                <span className='errorMessage'>*</span>
              </label>
              <Form.Item
                name='capacity'
                rules={validation.addCategory.capacity}
              >
                <Select onChange={handleDropdown} placeholder='Select'>
                  {seatingCapacityArray?.map((capacity, index) => (
                    <Option key={index} value={capacity}>
                      {capacity}
                    </Option>
                  ))}
                </Select>
              </Form.Item>
            </div>
          </div>
          <div className='col-sm-6'>
            Can Accept Ride Request For:
            <div className='input-group'>
              <div className='input-group-prepend mb-0'>
                {/* <Form.Item
                  name='related_categories'
                  // rules={validation.addCategory.}
                >
                  {/* {console.log(relatedCategory, 'relatedCategory')} */}
                {/* {relatedCategory?.map((relCategory, index) => (
                    <Checkbox
                      onChange={handleCheckBoxChange}
                      value={relCategory.id}
                      key={index}
                    >
                      {relCategory.name}
                    </Checkbox>
                  ))} */}
                {/* {false ? (
                    <Checkbox.Group
                      options={options}
                      defaultValue={['Pear', 'Apple']}
                    />
                  ) : null}
                  {checkedList && checkedId ? (
                    <Checkbox.Group
                      options={checkedList}
                      value={onChangeCheckList}
                      onChange={handleCheckBoxChange}
                    />
                  ) : (
                    ''
                  )} */}
                {/* <Checkbox.Group
                    options={checkedList}
                    value={onChangeCheckList}
                    onChange={handleCheckBoxChange}
                  /> */}
                {/* </Form.Item>  */}
                {/* {false ? (
                  <Form.Item name='related_categories'></Form.Item>
                ) : null} */}
                <Checkbox.Group
                  options={checkedList}
                  value={onChangeCheckList}
                  onChange={handleCheckBoxChange}
                />
              </div>
            </div>
          </div>
        </div>

        <div className='form-group btn-row text-center mb-0'>
          <Form.Item>
            <Button
              disabled={isSpin}
              htmlType='submit'
              className='btn btn-warning width-120 ripple-effect text-uppercase'
            >
              {isSpin ? <LoadingSpinner /> : submitButtonText}
            </Button>
            <Button
              htmlType='button'
              onClick={onCancel}
              className='btn btn-cancel width-120 ripple-effect text-uppercase'
            >
              {t('common.cancel')}
            </Button>
          </Form.Item>
        </div>
      </Form>
    </>
  )
}

AddEditCategoryForm.propTypes = {
  form: PropTypes.any.isRequired,
  onCancel: PropTypes.func.isRequired,
  isSpin: PropTypes.bool.isRequired,
  onFinish: PropTypes.func.isRequired,
  onFinishFailed: PropTypes.func.isRequired,
  onFileUploaded: PropTypes.func.isRequired,
  // initialValues: PropTypes.object.isRequired,
  media: PropTypes.any.isRequired,
  submitButtonText: PropTypes.string.isRequired,
  errorMsg: PropTypes.string.isRequired
}
