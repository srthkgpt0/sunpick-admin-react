import { Button, Form, Input, Select } from 'antd'
import React from 'react'
import { useTranslation } from 'react-i18next'
// import {
//   getCategoryDropdown,
//   getMakeDropdown
// } from '../../../services/settings/index'

function ManageModelFilterForm({
  onFinish,
  onFinishFailed,
  onReset,
  formRef,
  makeDropdown,
  categoryDropdown
}) {
  const { t } = useTranslation()
  // const [categoryDropdown, setCategoryDropdown] = useState(null)
  // const [makeDropdown, setMakeDropdown] = useState(null)
  // console.log(makeDropdown, 'makeDropdown')
  // console.log(categoryDropdown, 'categoryDropdown')

  const { Option } = Select
  // useEffect(() => {
  //   fetchCategoryDropdown()
  //   fetchMakeDropdown()
  // }, [])
  // const fetchCategoryDropdown = async () => {
  //   try {
  //     const res = await getCategoryDropdown()
  //     console.log(res.data.rows, 'fetchCategoryDropdown')
  //     setCategoryDropdown(res.data.rows)
  //   } catch (error) {}
  // }
  // const fetchMakeDropdown = async () => {
  //   try {
  //     const res = await getMakeDropdown()
  //     setMakeDropdown(res.data.rows)
  //     console.log(res.data.rows, 'fetchMakeDropdown')
  //   } catch (error) {}
  // }
  return (
    <div
      className='filter_section full collapse'
      id='driverSearch'
      style={{ display: 'block' }}
      //   style='display: block'
    >
      <div className='container-fluid'>
        <Form
          name='basic'
          onFinish={onFinish}
          onFinishFailed={onFinishFailed}
          ref={formRef}
        >
          <div className='row'>
            <div className='col-sm-6 col-xl'>
              <div className='form-group'>
                <label>Category</label>
                <Form.Item name='category_id'>
                  <Select placeholder='Select'>
                    {categoryDropdown?.map((category, index) => (
                      <Option key={index} value={category.id}>
                        {category.name}
                      </Option>
                    ))}
                  </Select>
                </Form.Item>
              </div>
            </div>
            <div className='col-sm-6  col-xl'>
              <div className='form-group'>
                <label>Make</label>
                <Form.Item name='brand_id'>
                  <Select placeholder='Select'>
                    {makeDropdown?.map((make, index) => (
                      <Option key={index} value={make.id}>
                        {make.name}
                      </Option>
                    ))}
                  </Select>
                </Form.Item>
              </div>
            </div>
            <div className='col-sm-6 col-xl'>
              <div className='form-group'>
                <label>Model</label>
                <Form.Item name='q'>
                  <Input placeholder={t('settings.model')} type='text' />
                </Form.Item>
              </div>
            </div>
            <div className='col-sm-6 col-xl'>
              <div className='btnsubmit'>
                <label className="d-none d-md-block">&nbsp;</label>
                <Form.Item>
                  <Button className='btn btn-warning' htmlType='submit'>
                    {t('common.submit')}
                  </Button>
                  <Button
                    className='btn btn-warning'
                    htmlType='button'
                    onClick={onReset}
                  >
                    {t('common.reset')}
                  </Button>
                </Form.Item>
              </div>
            </div>
          </div>
        </Form>
      </div>
    </div>
  )
}

export default ManageModelFilterForm
