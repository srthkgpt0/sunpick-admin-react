import React from 'react'
import { useTranslation } from 'react-i18next'
import { Form, Input, Button, Alert } from 'antd'
import PropTypes from 'prop-types'
import { LoadingSpinner } from '../../common'
// import { currentTimeStamp } from '../../../utilities/common'
import validation from '../../../utilities/validation'
// import { useState } from 'react'
// import { default_const_flag } from '../../../utilities/country-flag-list'
// import { getRelatedCategories } from '../../../services/settings'

export default function AddEditMakeForm({
  form,
  onCancel,
  isSpin,
  onFinish,
  onFinishFailed,
  onFileUploaded,
  initialValues = {},
  media,
  submitButtonText,
  errorMsg,
  isEditForm
}) {
  const { t } = useTranslation()
  //   const { Option } = Select
  //   const [prevImage, setPrevImage] = useState()

  //   const handleCheckBoxChange = (checkedValues) => {
  //     setCheckBoxValue(checkedValues.target.value)
  //     console.log(checkedValues)
  //   }
  //   const handleChange = (event) => {
  //     const file = event.target.files[0]
  //     console.log('File')
  //     onFileUploaded(file)
  //     // setIsimage(event.target.files[0])
  //     let reader = new FileReader()
  //     reader.readAsDataURL(file)
  //     reader.onloadend = (e) => {
  //       setPrevImage(reader.result)
  //     }
  //   }
  //   const seatingCapacityArray = [2, 3, 4, 5, 6, 7]
  return (
    <>
      {errorMsg && <Alert message={errorMsg} className='mb-4' type='error' />}
      <Form
        name='albumAddEdit'
        form={form}
        onFinish={onFinish}
        onFinishFailed={onFinishFailed}
        initialValues={initialValues}
      >
        <div className='row'>
          <div className='col-sm-12'>
            <div className='form-group'>
              <Form.Item name='name' rules={validation?.addMake?.make}>
                <Input type='text' placeholder='Make' />
              </Form.Item>
            </div>
          </div>
        </div>

        <div className='form-group btn-row text-center mb-0'>
          <Form.Item>
            <Button
              disabled={isSpin}
              htmlType='submit'
              className='btn btn-warning width-120 ripple-effect text-uppercase'
            >
              {isSpin ? <LoadingSpinner /> : submitButtonText}
            </Button>
            <Button
              htmlType='button'
              onClick={onCancel}
              className='btn btn-cancel width-120 ripple-effect text-uppercase'
            >
              {t('common.cancel')}
            </Button>
          </Form.Item>
        </div>
      </Form>
    </>
  )
}

AddEditMakeForm.propTypes = {
  form: PropTypes.any.isRequired,
  onCancel: PropTypes.func.isRequired,
  isSpin: PropTypes.bool.isRequired,
  onFinish: PropTypes.func.isRequired,
  onFinishFailed: PropTypes.func.isRequired,
  // onFileUploaded: PropTypes.func.isRequired,
  initialValues: PropTypes.object.isRequired,
  media: PropTypes.any.isRequired,
  submitButtonText: PropTypes.string.isRequired,
  errorMsg: PropTypes.string.isRequired
}
