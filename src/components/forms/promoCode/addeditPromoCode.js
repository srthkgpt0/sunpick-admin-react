import React from 'react'
import { Form, Input, Button } from 'antd'
import PropTypes from 'prop-types'
import { useTranslation } from 'react-i18next'
import { Select } from 'antd'
import validation from '../../../utilities/validation'
import { useHistory } from 'react-router'
const { Option } = Select
export default function AddEditPromoCodeForm({
  form,
  onFinish,
  onFinishFailed,
  submitButtonText,
  onFromDateChange,
  onToDateChange,
  id
}) {
  const { t } = useTranslation()
  let history = useHistory()

  const discountOption = [
    {
      id: 1,
      label: t('promoCode.fixedDiscount'),
      value: 'fixed'
    },
    {
      id: 2,
      label: t('promoCode.discountPercent'),
      value: 'percent'
    },
    {
      id: 3,
      label: t('promoCode.discountWithFixedPercent'),
      value: 'percent_max_discount'
    }
  ]
  const goBack = () => {
    history.push('/promo-code')
  }
  return (
    <Form
      form={form}
      name='AddEditPromoCodeForm'
      onFinish={onFinish}
      onFinishFailed={onFinishFailed}
    >
      <div className='row'>
        <div className='col-sm-6 col-md-4'>
          <div className='form-group'>
            <label>
              {t('promoCode.promoTitle')}
              <span className='errorMessage'>*</span>
            </label>
            <Form.Item name='title' rules={validation.promoCode.title}>
              <Input type='text' />
            </Form.Item>
          </div>
        </div>
        <div className='col-sm-6 col-md-4'>
          <div className='form-group'>
            <label> {t('promoCode.discountTypes')}</label>
            <Form.Item
              name='discount_type'
              rules={validation.promoCode.discountType}
            >
              <Select>
                {discountOption.map((data, index) => (
                  <Option key={index} value={data.value}>
                    {data.label}
                  </Option>
                ))}
              </Select>
            </Form.Item>
          </div>
        </div>
        <Form.Item
          noStyle
          shouldUpdate={(prevValues, currentValues) =>
            prevValues.discount_type !== currentValues.discount_type
          }
        >
          {({ getFieldValue }) =>
            getFieldValue('discount_type') === 'fixed' ? (
              <div className='col-sm-6 col-md-4'>
                <div className='form-group'>
                  <label>
                    {t('promoCode.maximumDiscount')}
                    <span className='errorMessage'>*</span>
                  </label>
                  <Form.Item
                    name='discount'
                    rules={validation.promoCode.discountPercentage}
                  >
                    <Input type='number' />
                  </Form.Item>
                </div>
              </div>
            ) : null
          }
        </Form.Item>
        <Form.Item
          noStyle
          shouldUpdate={(prevValues, currentValues) =>
            prevValues.discount_type !== currentValues.discount_type
          }
        >
          {({ getFieldValue }) =>
            getFieldValue('discount_type') === 'percent' ? (
              <div className='col-sm-6 col-md-4'>
                <div className='form-group'>
                  <label>
                    {t('promoCode.off')}
                    <span className='errorMessage'>*</span>
                  </label>
                  <Form.Item
                    name='discount'
                    rules={validation.promoCode.discountPercentage}
                  >
                    <Input type='text' placeholder='%Off' />
                  </Form.Item>
                </div>
              </div>
            ) : null
          }
        </Form.Item>

        <Form.Item
          noStyle
          shouldUpdate={(prevValues, currentValues) =>
            prevValues.discount_type !== currentValues.discount_type
          }
        >
          {({ getFieldValue }) =>
            getFieldValue('discount_type') === 'percent_max_discount' ? (
              <>
                <div className='col-sm-6 col-md-4'>
                  <div className='form-group'>
                    <label>
                      {t('promoCode.off')}
                      <span className='errorMessage'>*</span>
                    </label>
                    <Form.Item
                      name='discount'
                      rules={validation.promoCode.discountPercentage}
                    >
                      <Input type='text' placeholder='%Off' />
                    </Form.Item>
                  </div>
                </div>
                <div className='col-sm-6 col-md-4'>
                  <div className='form-group'>
                    <label>
                      {t('promoCode.maximumDiscount')}
                      <span className='errorMessage'>*</span>
                    </label>
                    <Form.Item
                      name='max_discount'
                      //     rules={validation.promoCode.maximumDiscount}
                    >
                      <Input type='number'></Input>
                    </Form.Item>
                  </div>
                </div>
              </>
            ) : null
          }
        </Form.Item>

        <div className='col-sm-6 col-md-4'>
          <div className='form-group'>
            <label>
              {t('promoCode.limitPerUsers')}
              <span className='errorMessage'>*</span>
            </label>
            <Form.Item
              name='limit_per_user'
              rules={validation.promoCode.limitPerUser}
            >
              <Input type='text' />
            </Form.Item>
          </div>
        </div>
        <div className='col-sm-6 col-md-4'>
          <div className='form-group'>
            <label>{t('promoCode.date')}</label>
            <div className='input-group input-daterange'>
              <Form.Item
                name='start_date'
                rules={validation.promoCode.startDate}
              >
                {/* <Space direction="vertical" className='w-100'>
                  <DatePicker placeholder="Start Date"  defaultValue={(id)?moment(form.getFieldValue('start_date')):''} format="YYYY-MM-DD" onChange={onFromDateChange} className='form-control w-100'
                  />
                </Space> */}
                <Input type='date' />
              </Form.Item>
              <div className='input-group-addon border-0 bg-transparent'>
                TO
              </div>
              <Form.Item name='end_date' rules={validation.promoCode.endDate}>
                {/* <Space direction="vertical" >
                  <DatePicker placeholder="End Date"  defaultValue={(id)?moment(form.getFieldValue('end_date')):''}   format="YYYY-MM-DD" onChange={onToDateChange} className='form-control w-100'
                  />
                </Space> */}
                <Input type='date' />
              </Form.Item>
            </div>
          </div>
        </div>
        <div className='col-sm-6 col-md-4'>
          <div className='form-group'>
            <label>
              {t('promoCode.couponCode')}
              <span className='errorMessage'></span>
            </label>
            <Form.Item name='code' rules={validation.promoCode.couponCode}>
              <Input type='text' />
            </Form.Item>
          </div>
        </div>
        <div className='col-sm-6 col-md-4'>
          <div className='form-group'>
            <label>
              {t('promoCode.termsAndCondition')}
              <span className='errorMessage'></span>
            </label>
            <Form.Item
              name='term_condition'
              rules={validation.promoCode.termCondition}
            >
              <Input.TextArea />
            </Form.Item>
          </div>
        </div>
      </div>
      <div>
        <Form.Item className='mb-0'>
          <Button
            //  disabled={isSpin}
            type='primary'
            htmlType='submit'
            className='btn btn-warning'
          >
            {submitButtonText}
            {/* {isSpin ? 
             <div>loading</div>
             : submitButtonText} */}
          </Button>
          <Button
            //  disabled={isSpin}
            type='primary'
            htmlType='button'
            className='btn btn-warning'
            onClick={goBack}
          >
            {'Cancel'}
            {/* {isSpin ? 
             <div>loading</div>
             : submitButtonText} */}
          </Button>
        </Form.Item>
      </div>
    </Form>
  )
}

AddEditPromoCodeForm.propTypes = {
  form: PropTypes.any.isRequired,
  onFinish: PropTypes.func.isRequired,
  onFinishFailed: PropTypes.func.isRequired,
  submitButtonText: PropTypes.string.isRequired,
  onFromDateChange: PropTypes.func.isRequired
}
