import React from 'react'
import { Form, Button } from 'antd'
import PropTypes from 'prop-types'
import { DatePicker, Space, Select } from 'antd'
import { useTranslation } from 'react-i18next'

const { Option } = Select
export default function ManagePromoCodeForm({
  onFinish,
  onFinishFailed,
  onFromDateChange,
  onToDateChange,
  formRef,
  onReset
}) {
  const { t } = useTranslation()
  const statusOptions = [
    {
      id: 1,
      label: t('common.active'),
      value: 'active'
    },
    {
      id: 2,
      label: t('common.inactive'),
      value: 'inactive'
    }
  ]

  const discountOption = [
    {
      id: 1,
      label: t('promoCode.fixed'),
      value: 'fixed'
    },
    {
      id: 2,
      label: t('promoCode.percent'),
      value: 'percent'
    },
    {
      id: 3,
      label: t('promoCode.percentMaxDiscount'),
      value: 'percent_max_discount'
    }
  ]
  return (
    <Form
      name='basic'
      onFinish={onFinish}
      onFinishFailed={onFinishFailed}
      ref={formRef}
    >
      <div className='row'>
        <div className='col-sm-12 col-md-8 col-lg-3'>
          <div className='form-group'>
            <label>{t('promoCode.dateRange')}</label>
            <div className='input-group input-daterange'>
              <Form.Item name='from_date'>
                <Space direction='vertical' className='w-100'>
                  <DatePicker
                    placeholder='From Date'
                    format='YYYY-MM-DD'
                    onChange={onFromDateChange}
                    className='form-control w-100'
                  />
                </Space>
              </Form.Item>
              <div className='input-group-addon'>{t('promoCode.to')}</div>
              <Form.Item name='to_date'>
                <Space direction='vertical' className='w-100'>
                  <DatePicker
                    placeholder='To Date'
                    format='YYYY-MM-DD'
                    onChange={onToDateChange}
                    className='form-control w-100'
                  />
                </Space>
              </Form.Item>
            </div>
          </div>
        </div>
        <div className='col-sm-6 col-md-4 col-xl-3'>
          <div className='form-group'>
            <label>{t('common.status')}</label>
            <Form.Item name='status'>
              <Select placeholder='Select'>
                {statusOptions.map((data, index) => (
                  <Option key={index} value={data.value}>
                    {data.label}
                  </Option>
                ))}
              </Select>
            </Form.Item>
          </div>
        </div>

        <div className='col-sm-6 col-md-4 col-xl-3'>
          <div className='form-group'>
            <label>{t('promoCode.discountType')} </label>
            <Form.Item name='discount_type'>
              <Select placeholder='Select'>
                {discountOption.map((data, index) => (
                  <Option key={index} value={data.value}>
                    {data.label}
                  </Option>
                ))}
              </Select>
            </Form.Item>
          </div>
        </div>

        <div className='col-sm-6 col-md-4 col-lg-3'>
          <div className='btnsubmit'>
            <label className='d-sm-block'>&nbsp;</label>
            <Form.Item>
              <Button className='btn btn-warning' htmlType='submit'>
                {t('common.submit')}
              </Button>
              <Button
                className='btn btn-warning'
                htmlType='button'
                onClick={onReset}
              >
                {t('common.reset')}
              </Button>
            </Form.Item>
          </div>
        </div>
      </div>
    </Form>
  )
}

ManagePromoCodeForm.propTypes = {
  onFinish: PropTypes.func.isRequired,
  onFinishFailed: PropTypes.func.isRequired,
  onFromDateChange: PropTypes.func.isRequired,
  onToDateChange: PropTypes.func.isRequired,
  onReset: PropTypes.func.isRequired,
  formRef: PropTypes.any.isRequired
}
