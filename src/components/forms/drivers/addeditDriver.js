import React from 'react'
import { Form, Input, Button } from 'antd'
import PropTypes from 'prop-types'
import validation from '../../../utilities/validation'
import UploadMedia from '../../image-upload'
import ApiEndPoints from '../../../utilities/apiEndPoints'
import { currentTimeStamp } from '../../../utilities/common'
import { Select } from 'antd'

const { Option } = Select
export default function AddEditDriverForm({
  form,
  isSpin,
  media,
  onFinish,
  onFinishFailed,
  onFileUploaded,
  onFileRemoved,
  submitButtonText,
  onFromDateChange,
  makeList,
  modelList,
  stateList,
  fetchvehicleModel,
  fetchCategory,
  id
}) {
  const vehicleModel = async (value) => {
    try {
      fetchvehicleModel(value)
    } catch (error) {
      //   logger({ 'error:': error })
    }
  }
  const vehicleMake = async (value) => {
    try {
      fetchCategory(value)
    } catch (error) {
      //   logger({ 'error:': error })
    }
  }
  return (
    <Form
      form={form}
      name='AddEditDriverForm'
      onFinish={onFinish}
      onFinishFailed={onFinishFailed}
      // initialValues={{ brand_id: 'Select' }}
    >
      <div className='row'>
        <div className='col-sm-6'>
          <div className='form-group'>
            <label>
              First name
              <span className='errorMessage'>*</span>
            </label>
            <Form.Item
              name='first_name'
              rules={validation.addDriver.first_name}
            >
              <Input type='text' placeholder='First Name' />
            </Form.Item>
          </div>
        </div>
        <div className='col-sm-6'>
          <div className='form-group'>
            <label>
              Last name
              <span className='errorMessage'>*</span>
            </label>
            <Form.Item name='last_name' rules={validation.addDriver.last_name}>
              <Input type='text' placeholder='Last Name' />
            </Form.Item>
          </div>
        </div>
      </div>
      <div className='row'>
        <div className='col-sm-6'>
          <div className='form-group'>
            <label>
              Mobile No.
              <span className='errorMessage'>*</span>
            </label>
            <Form.Item
              name='phone_number'
              rules={validation.addDriver.mobile_number}
            >
              <Input type='text' placeholder='Mobile Number' />
            </Form.Item>
          </div>
        </div>
        <div className='col-sm-6'>
          <div className='form-group'>
            <label>
              Email address
              <span className='errorMessage'>*</span>
            </label>
            <Form.Item name='email' rules={validation.addDriver.email}>
              <Input type='email' placeholder='Email' />
            </Form.Item>
          </div>
        </div>
      </div>
      <div className='row'>
        <div className='col-sm-6'>
          <div className='form-group'>
            <label>
              Vehicle Year
              <span className='errorMessage'>*</span>
            </label>
            <Form.Item
              name='vehicle_year'
              rules={validation.addDriver.vehicle_year}
            >
              <Input type='text' placeholder='Vehicle Year' />
            </Form.Item>
          </div>
        </div>
        <div className='col-sm-6'>
          <div className='form-group'>
            <label>
              Vehicle Registration Number
              <span className='errorMessage'>*</span>
            </label>
            <Form.Item
              name='car_registration_number'
              rules={validation.addDriver.car_registration_number}
            >
              <Input type='text' placeholder='Vehicle Registration Number' />
            </Form.Item>
          </div>
        </div>
      </div>
      <div className='row'>
        <div className='col-sm-6'>
          <div className='form-group'>
            <label>
              Vehicle Make
              <span className='errorMessage'>*</span>
            </label>
            <Form.Item
              name='brand_id'
              rules={validation.addDriver.vehicle_make}
            >
              <Select
                labelInValue
                // defaultValue={{ value: 'Select' }}
                placeholder='Select'
                onChange={vehicleModel}
              >
                {makeList.map((v) => {
                  return (
                    <Option
                      value={v.id}
                      key={`${v.id}${currentTimeStamp()}`}
                      id={v.id}
                    >
                      {v.name}
                    </Option>
                  )
                })}
              </Select>
            </Form.Item>
          </div>
        </div>
        <div className='col-sm-6'>
          <div className='form-group'>
            <label>
              Vehicle Model
              <span className='errorMessage'>*</span>
            </label>
            <Form.Item
              name='model_id'
              rules={validation.addDriver.vehicle_model}
            >
              <Select
                labelInValue
                placeholder='Select'
                // defaultValue={{ value: 'Select' }}
                onChange={vehicleMake}
              >
                {modelList.map((m) => {
                  return (
                    <Option value={m.id} key={`${m.id}${currentTimeStamp()}`}>
                      {m.name}
                    </Option>
                  )
                })}
              </Select>
            </Form.Item>
          </div>
        </div>
      </div>
      <div className='row'>
        <div className='col-sm-6'>
          <div className='form-group'>
            <label>Vehicle Category</label>
            <Form.Item
              name='category_id'
              rules={validation.addDriver.vehicle_category}
            >
              <Input type='text' placeholder='Vehicle Category' readOnly />
            </Form.Item>
          </div>
        </div>
        <div className='col-sm-6'>
          <div className='form-group'>
            <label>
              Date Of Birth
              <span className='errorMessage'>*</span>
            </label>
            <Form.Item
              name='date_of_birth'
              rules={validation.addDriver.date_of_birth}
            >
              {/* <Space direction='vertical' className='w-100'> */}
              {/* <DatePicker
                placeholder='From Date'
                defaultValue={
                  id ? moment(form.getFieldValue('date_of_birth')) : ''
                }
                format='YYYY-MM-DD'
                className='form-control w-100'
                onChange={onFromDateChange}
              /> */}
              {/* </Space> */}
              <Input type='date' />
              {/* <Input placeholder="From Date" /> */}
            </Form.Item>
          </div>
        </div>
      </div>
      <div className='row'>
        <div className='col-sm-6'>
          <div className='form-group'>
            <label>
              Blood Group
              <span className='errorMessage'></span>
            </label>
            <Form.Item name='blood_group'>
              <Input type='text' placeholder='Blood Group' />
            </Form.Item>
          </div>
        </div>
        <div className='col-sm-6'>
          <div className='form-group'>
            <label>
              Emergency Number
              <span className='errorMessage'>*</span>
            </label>
            <Form.Item
              name='emergency_number'
              rules={validation.addDriver.emergency_number}
            >
              <Input type='text' placeholder='Emergency Number' />
            </Form.Item>
          </div>
        </div>
        <div className='col-sm-6'>
          <div className='form-group'>
            <label>
              Emergency Name
              <span className='errorMessage'>*</span>
            </label>
            <Form.Item
              name='emergency_contact_name'
              rules={validation.addDriver.emergency_contact_name}
            >
              <Input type='text' placeholder='Emergency Name' />
            </Form.Item>
          </div>
        </div>
        <div className='col-sm-6'>
          <div className='form-group'>
            <label>
              State
              <span className='errorMessage'>*</span>
            </label>
            <Form.Item name='state' rules={validation.addDriver.state}>
              <Select labelInValue placeholder='Select'>
                {stateList.map((state) => {
                  return (
                    <Option
                      value={state.default_name}
                      key={`${state.id}${currentTimeStamp()}`}
                    >
                      {state.default_name}
                    </Option>
                  )
                })}
              </Select>
            </Form.Item>
          </div>
        </div>
        <div className='col-sm-6'>
          <div className='form-group'>
            <label>
              City
              <span className='errorMessage'>*</span>
            </label>
            <Form.Item name='city' rules={validation.addDriver.city}>
              <Input type='text' placeholder='City' />
            </Form.Item>
          </div>
        </div>
        <div className='col-sm-6'>
          <div className='form-group'>
            <label>
              Postal Code
              <span className='errorMessage'></span>
            </label>
            <Form.Item
              name='postal_code'
              rules={validation.addDriver.postal_code}
            >
              <Input type='text' placeholder='Postal Code' />
            </Form.Item>
          </div>
        </div>
        <div className='col-sm-6'>
          <div className='form-group'>
            <label>
              Address
              <span className='errorMessage'>*</span>
            </label>
            <Form.Item name='address' rules={validation.addDriver.address}>
              <Input type='text' placeholder='Address' />
            </Form.Item>
          </div>
        </div>
      </div>
      <div className='row'>
        <div className='col-sm-6 col-xl-6'>
          <div className='form-group btn-file'>
            <label>
              Profile Picture
              <span className='errorMessage'>*</span>
            </label>
            <Form.Item name='photo' rules={validation.addDriver.profile_image}>
              <UploadMedia
                showWithoutDragger={true}
                actionURL={ApiEndPoints.ADD_DOCUMENT('photo').url}
                onFileUploaded={onFileUploaded}
                onFileRemoved={onFileRemoved}
                media={[
                  form.getFieldValue().photo
                    ? form.getFieldValue().photo
                    : 'assets/images/default-userNew.jpg'
                ]}
                customListType='picture-card'
                mediaType='photo'
              />
            </Form.Item>
          </div>
        </div>
        <div className='col-sm-6 col-xl-6'>
          <div className='form-group btn-file'>
            <label>
              Upload Commercial Insurance
              <span className='errorMessage'>*</span>
            </label>
            <Form.Item
              name='commercial_insurance'
              rules={validation.addDriver.insurance_image}
            >
              <UploadMedia
                showWithoutDragger={true}
                actionURL={
                  ApiEndPoints.ADD_DOCUMENT('commercial_insurance').url
                }
                onFileUploaded={onFileUploaded}
                onFileRemoved={onFileRemoved}
                media={[
                  form.getFieldValue().commercial_insurance
                    ? form.getFieldValue().commercial_insurance
                    : 'assets/images/default-doc.jpg'
                ]}
                customListType='picture-card'
                mediaType='commercial_insurance'
              />
            </Form.Item>
          </div>
        </div>
      </div>
      <div className='row'>
        <div className='col-sm-6 col-xl-6'>
          <div className='form-group btn-file'>
            <label>
              Upload Driver’s license Front Side
              <span className='errorMessage'>*</span>
            </label>
            <Form.Item
              name='license_photo_front'
              rules={validation.addDriver.license_front_image}
            >
              <UploadMedia
                showWithoutDragger={true}
                actionURL={ApiEndPoints.ADD_DOCUMENT('license_photo_front').url}
                onFileUploaded={onFileUploaded}
                onFileRemoved={onFileRemoved}
                media={[
                  form.getFieldValue().license_photo_front
                    ? form.getFieldValue().license_photo_front
                    : 'assets/images/default-doc.jpg'
                ]}
                customListType='picture-card'
                mediaType='license_photo_front'
              />
            </Form.Item>
          </div>
        </div>
        <div className='col-sm-6 col-xl-6'>
          <div className='form-group btn-file'>
            <label>
              Upload Driver’s license Back Side
              <span className='errorMessage'>*</span>
            </label>
            <Form.Item
              name='license_photo_back'
              rules={validation.addDriver.license_back_image}
            >
              <UploadMedia
                showWithoutDragger={true}
                actionURL={ApiEndPoints.ADD_DOCUMENT('license_photo_back').url}
                onFileUploaded={onFileUploaded}
                onFileRemoved={onFileRemoved}
                media={[
                  form.getFieldValue().license_photo_back
                    ? form.getFieldValue().license_photo_back
                    : 'assets/images/default-doc.jpg'
                ]}
                customListType='picture-card'
                mediaType='license_photo_back'
              />
            </Form.Item>
          </div>
        </div>
      </div>
      <div className='row'>
        <div className='col-sm-6 col-xl-6'>
          <div className='form-group btn-file'>
            <label>
              Upload Owner's Tin
              <span className='errorMessage'>*</span>
            </label>
            <Form.Item name='owner_tin' rules={validation.addDriver.owner_tin}>
              <UploadMedia
                showWithoutDragger={true}
                actionURL={ApiEndPoints.ADD_DOCUMENT('owner_tin').url}
                onFileUploaded={onFileUploaded}
                onFileRemoved={onFileRemoved}
                media={[
                  form.getFieldValue().owner_tin
                    ? form.getFieldValue().owner_tin
                    : 'assets/images/default-doc.jpg'
                ]}
                customListType='picture-card'
                mediaType='owner_tin'
              />
            </Form.Item>
          </div>
        </div>
        <div className='col-sm-6 col-xl-6'>
          <div className='form-group btn-file'>
            <label>
              Upload Vehicle Registration
              <span className='errorMessage'>*</span>
            </label>
            <Form.Item
              name='registration_photo'
              rules={validation.addDriver.vehicle_registration_image}
            >
              <UploadMedia
                showWithoutDragger={true}
                actionURL={ApiEndPoints.ADD_DOCUMENT('registration_photo').url}
                onFileUploaded={onFileUploaded}
                onFileRemoved={onFileRemoved}
                media={[
                  form.getFieldValue().registration_photo
                    ? form.getFieldValue().registration_photo
                    : 'assets/images/default-doc.jpg'
                ]}
                customListType='picture-card'
                mediaType='registration_photo'
              />
            </Form.Item>
          </div>
        </div>
      </div>
      <div className='row'>
        <div className='col-sm-6 col-xl-6'>
          <div className='form-group btn-file'>
            <label>
              Upload Driver Employment Agreement
              <span className='errorMessage'>*</span>
            </label>
            <Form.Item
              name='employment_agreement'
              rules={validation.addDriver.employment_agreement}
            >
              <UploadMedia
                showWithoutDragger={true}
                actionURL={
                  ApiEndPoints.ADD_DOCUMENT('employment_agreement').url
                }
                onFileUploaded={onFileUploaded}
                onFileRemoved={onFileRemoved}
                media={[
                  form.getFieldValue().employment_agreement
                    ? form.getFieldValue().employment_agreement
                    : 'assets/images/default-doc.jpg'
                ]}
                customListType='picture-card'
                mediaType='employment_agreement'
              />
            </Form.Item>
          </div>
        </div>
      </div>
      <div>
        <Form.Item>
          <Button
            //  disabled={isSpin}
            type='primary'
            htmlType='submit'
            className='btn btn-warning'
          >
            {submitButtonText}
            {/* {isSpin ? 
             <div>loading</div>
             : submitButtonText} */}
          </Button>
        </Form.Item>
      </div>
    </Form>
  )
}

AddEditDriverForm.propTypes = {
  form: PropTypes.any.isRequired,
  media: PropTypes.any.isRequired,
  onFileUploaded: PropTypes.func.isRequired,
  fetchvehicleModel: PropTypes.func.isRequired,
  onFileRemoved: PropTypes.func.isRequired,
  isSpin: PropTypes.bool.isRequired,
  onFinish: PropTypes.func.isRequired,
  onFinishFailed: PropTypes.func.isRequired,
  submitButtonText: PropTypes.string.isRequired,
  onFromDateChange: PropTypes.func.isRequired
}
