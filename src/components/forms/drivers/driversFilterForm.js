import React from 'react'
import { Form, Input, Button, DatePicker, Space, Select } from 'antd'
import Autocomplete from 'react-google-autocomplete'
import { useTranslation } from 'react-i18next'
import { currentTimeStamp } from '../../../utilities/common'

// const formRef = React.createRef()

function DriversFilterForm({
  onFinish,
  onFinishFailed,
  onReset,
  driverAvailability,
  taxiCategoryList,
  onFromDateChange,
  onToDateChange,
  onAutoComplete,
  formRef
}) {
  const { t } = useTranslation()
  //   const onResetFields = () => {
  //     formRef.current.resetFields()
  //     onReset()
  //   }
  const { Option } = Select
  return (
    <Form
      name='basic'
      onFinish={onFinish}
      onFinishFailed={onFinishFailed}
      ref={formRef}
    //   initialValues={availability : 'Select', category_id : 'Select'}
    >
      <div className='row'>
        <div className='col-sm-6 col-md-4 col-xl-3'>
          <div className='form-group'>
            <label>{t('drivers.driverId')}</label>
            <Form.Item
              name='driver_id'
              rules={[
                {
                  required: false,
                  message: 'Please input your Password!'
                }
              ]}
            >
              <Input placeholder='Driver Id' type='text' />
            </Form.Item>
          </div>
        </div>
        <div className='col-sm-6 col-md-4 col-xl-3'>
          <div className='form-group'>
            <label>{t('common.name')}</label>
            <Form.Item
              name='name'
              rules={[
                {
                  required: false,
                  message: 'Please input your Password!'
                }
              ]}
            >
              <Input placeholder='Name' type='text' />
            </Form.Item>
          </div>
        </div>
        <div className='col-sm-6 col-md-4 col-xl-3'>
          <div className='form-group'>
            <label>{t('common.email')}</label>
            <Form.Item
              name='email'
              rules={[{ required: false, type: 'email' }]}
            >
              <Input placeholder='Email' />
            </Form.Item>
            {/* <input type="text"
                    className="form-control" placeholder="Email" /> */}
          </div>
        </div>
        <div className='col-sm-6 col-md-4 col-xl-3'>
          <div className='form-group'>
            <label>{t('common.mobileNumber')}</label>
            <Form.Item name='phone_number' rules={[{ required: false }]}>
              <Input placeholder='Mobile Number' />
            </Form.Item>
            {/* <input type="text"
                    className="form-control" placeholder="Mobile Number" /> */}
          </div>
        </div>
        <div className='col-sm-12 col-md-8 col-lg-4 col-xl-3'>
          <div className='form-group'>
            <label>{t('drivers.regWithinDateRange')}</label>
            <div className='input-group input-daterange'>
              <Form.Item name='from_date' rules={[{ required: false }]}>
                <Space direction='vertical'>
                  <DatePicker
                    placeholder='From Date'
                    format='YYYY-MM-DD'
                    className='form-control w-100'
                    onChange={onFromDateChange}
                  />
                </Space>
                {/* <Input placeholder="From Date" /> */}
              </Form.Item>
              <div className='input-group-addon'>TO</div>
              <Form.Item name='to_date' rules={[{ required: false }]}>
                <Space direction='vertical'>
                  <DatePicker
                    placeholder='To Date'
                    format='YYYY-MM-DD'
                    className='form-control w-100'
                    onChange={onToDateChange}
                  />
                </Space>
                {/* <Input placeholder="From Date" /> */}
              </Form.Item>
            </div>
          </div>
        </div>
        <div className='col-sm-6 col-md-4 col-xl-3'>
          <div className='form-group'>
            <label>{t('settings.vehicleCategory')}</label>
            <Form.Item name='category_id' rules={[{ required: false }]}>
              <Select labelInValue placeholder='Select'>
                {taxiCategoryList.map((category) => {
                  return (
                    <Option
                      value={category.id}
                      key={`${category.id}${currentTimeStamp()}`}
                    >
                      {category.name}
                    </Option>
                  )
                })}
              </Select>
            </Form.Item>
          </div>
        </div>
        <div className='col-sm-6 col-md-4 col-xl-3'>
          <div className='form-group'>
            <label>{t('drivers.available')}</label>
            <Form.Item name='availability' rules={[{ required: false }]}>
              <Select labelInValue placeholder='Select'>
                {driverAvailability.map((availability, index) => {
                  return (
                    <Option
                      className={availability.textColor}
                      value={`${availability.key}`}
                      key={`${availability.id}${currentTimeStamp()}${index}`}
                    >
                      {availability.value}
                    </Option>
                  )
                })}
              </Select>
            </Form.Item>
          </div>
        </div>
        <div className='col-sm-6 col-md-4 col-xl-3'>
          <div className='form-group'>
            <label>{t('drivers.location')}</label>
            <Form.Item name='auto' rules={[{ required: false }]}>
              <div>
                <Autocomplete
                  className='form-control'
                  onPlaceSelected={onAutoComplete}
                  types={['(regions)']}
                  componentRestrictions={{
                    country: ['ET', 'IN']
                  }}
                />
              </div>
            </Form.Item>
          </div>
        </div>
        <div className='col-sm-6 col-md-4 col-xl-3'>
          <div className='form-group'>
            <label>{t('common.address')}</label>
            <Form.Item name='address'>
              <Input placeholder='Address' />
            </Form.Item>
          </div>
        </div>
        <div className='col-sm-12 text-left text-sm-right'>
          <div className='btnsubmit'>
            <Form.Item>
              <Button className='btn btn-warning' htmlType='submit'>
                {t('common.submit')}
              </Button>
              <Button
                className='btn btn-warning'
                htmlType='button'
                onClick={onReset}
              >
                {t('common.reset')}
              </Button>
            </Form.Item>
            {/* <button type="submit"
                    className="btn btn-warning">Submit</button>
                <button type="button"
                    className="btn btn-warning">Reset</button> */}
          </div>
        </div>
      </div>
    </Form>
  )
}

export default DriversFilterForm
