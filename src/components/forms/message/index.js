import React from 'react'
import { Form, Button } from 'antd'
import PropTypes from 'prop-types'
import validation from '../../../utilities/validation'
import { Select } from 'antd'
import { useTranslation } from 'react-i18next'
import TextArea from 'antd/lib/input/TextArea'

const { Option } = Select
export default function ManageMessageForm({
  onFinish,
  onFinishFailed,
  statusOptions,
  form
}) {
  const { t } = useTranslation()

  return (
    <Form
      form={form}
      name='basic'
      onFinish={onFinish}
      onFinishFailed={onFinishFailed}
    >
      <div className='row'>
        <div className='col-sm-12 col-md-8 col-lg-4'>
          <div className='form-group'>
            <div className='form-group'>
              <label>{t('common.status')}</label>
              <Form.Item name='status' rules={validation.message.status}>
                <Select placeholder='Select'>
                  {statusOptions.map((data, index) => (
                    <Option key={index} value={data.value}>
                      {data.label}
                    </Option>
                  ))}
                </Select>
              </Form.Item>
            </div>
            <div className='form-group'>
              <label>
                {t('common.message')}
                <span className='errorMessage'></span>
              </label>
              <Form.Item name='message' rules={validation.message.messages}>
                <TextArea rows={4} />
              </Form.Item>
            </div>
          </div>
        </div>
      </div>
      <div className='row'>
        <div className='col-sm-12 col-md-4 col-lg-3'>
          <div className='btnsubmit'>
            <label className='d-sm-block'>&nbsp;</label>
            <Form.Item>
              <Button className='btn btn-warning' htmlType='submit'>
                {t('common.send')}
              </Button>
            </Form.Item>
          </div>
        </div>
      </div>
    </Form>
  )
}

ManageMessageForm.propTypes = {
  onFinish: PropTypes.func.isRequired,
  onFinishFailed: PropTypes.func.isRequired,
  statusOptions: PropTypes.array.isRequired
}
