import React from 'react'
import { Form, Input, Button } from 'antd'
import PropTypes from 'prop-types'
import validation from '../../../utilities/validation'
import { DatePicker, Space, Select } from 'antd'
import { useTranslation } from 'react-i18next'

const { Option } = Select
export default function ManageReportForm({
  onFinish,
  onFinishFailed,
  onFromDateChange,
  onToDateChange,
  formRef,
  onReset,
  driverId,
  rideId,
  organizaion
}) {
  const { t } = useTranslation()
  const organizationOption = [
    {
      id: 1,
      label: t('reports.mis'),
      value: 'lta'
    },
    {
      id: 2,
      label: t('reports.cabE'),
      value: 'sixtnc'
    }
  ]
  return (
    <Form
      name='basic'
      onFinish={onFinish}
      onFinishFailed={onFinishFailed}
      ref={formRef}
      initialValues={{ organization: organizationOption[0].value }}
    >
      <div className='row'>
        <div className='col-sm-4 col-md-6 col-lg-4'>
          <div className='form-group'>
            <label>{t('common.dateRange')}</label>
            <div className='input-group input-daterange'>
              <Form.Item name='from_date'>
                <Space direction='vertical' className='w-100'>
                  <DatePicker
                    placeholder='From Date'
                    format='YYYY-MM-DD'
                    onChange={onFromDateChange}
                    className='form-control w-100'
                  />
                </Space>
                {/* <Input placeholder="From Date" /> */}
              </Form.Item>
              <div className='input-group-addon'>{t('reports.to')}</div>
              <Form.Item name='to_date'>
                <Space direction='vertical' className='w-100'>
                  <DatePicker
                    placeholder='To Date'
                    format='YYYY-MM-DD'
                    onChange={onToDateChange}
                    className='form-control w-100'
                  />
                </Space>
                {/* <Input placeholder="From Date" /> */}
              </Form.Item>
            </div>
          </div>
        </div>
        {rideId && (
          <div className='col-sm-4 col-md-6 col-lg-4'>
            <div className='form-group'>
              <label>{t('rides.rideId')}</label>
              <Form.Item name='ride_id' rules={validation.queryParams.ride_id}>
                <Input type='text' placeholder='Ride Id' />
              </Form.Item>
            </div>
          </div>
        )}
        {driverId && (
          <div className='col-sm-4 col-md-6 col-lg-4'>
            <div className='form-group'>
              <label>{t('reports.driverId')}</label>
              <Form.Item
                name='driver_id'
                rules={validation.queryParams.driver_id}
              >
                <Input type='text' placeholder='Driver Id' />
              </Form.Item>
            </div>
          </div>
        )}
        {organizaion && (
          <div className='col-sm-4 col-md-6 col-lg-4'>
            <div className='form-group'>
              <label>{t('reports.organization')}</label>
              <Form.Item name='organization'>
                <Select
                // defaultValue={organizationOption[0].value}
                >
                  {organizationOption.map((data, index) => (
                    <Option key={index} value={data.value}>
                      {data.label}
                    </Option>
                  ))}
                </Select>
              </Form.Item>
            </div>
          </div>
        )}
        <div className='col-sm-4 col-md-6 col-lg-4'>
          <div className='btnsubmit'>
            <label className='d-sm-block'>&nbsp;</label>
            <Form.Item>
              <Button className='btn btn-warning' htmlType='submit'>
                {t('common.submit')}
              </Button>
              <Button
                className='btn btn-warning'
                htmlType='button'
                onClick={onReset}
              >
                {t('common.reset')}
              </Button>
            </Form.Item>
          </div>
        </div>
      </div>
    </Form>
  )
}

ManageReportForm.propTypes = {
  onFinish: PropTypes.func.isRequired,
  onFinishFailed: PropTypes.func.isRequired,
  onFromDateChange: PropTypes.func.isRequired,
  onToDateChange: PropTypes.func.isRequired,
  onReset: PropTypes.func.isRequired,
  formRef: PropTypes.any.isRequired
}
