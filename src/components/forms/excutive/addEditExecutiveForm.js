import React from 'react'
import { useTranslation } from 'react-i18next'
import { Form, Input, Select, Button, Alert } from 'antd'
import PropTypes from 'prop-types'
import { LoadingSpinner } from '../../common'
import { currentTimeStamp } from '../../../utilities/common'
import validation from '../../../utilities/validation'
import { useState } from 'react'
import { default_const_flag } from '../../../utilities/country-flag-list'

export default function AddEditExecutiveForm({
  form,
  onCancel,
  isSpin,
  onFinish,
  onFinishFailed,
  onFileUploaded,
  initialValues = {},
  media,
  submitButtonText,
  errorMsg,
  isEditForm
}) {
  const { t } = useTranslation()
  const { Option } = Select
  const [prevImage, setPrevImage] = useState()
  const handleChanage = (event) => {
    const file = event.target.files[0]
    // console.log('File')
    onFileUploaded(file)
    // setIsimage(event.target.files[0])
    let reader = new FileReader()
    reader.readAsDataURL(file)
    reader.onloadend = (e) => {
      setPrevImage(reader.result)
    }
  }
  const getCountryCodeList = () => {
    return default_const_flag.CountryflagData
  }
  const countryList = getCountryCodeList()

  return (
    <>
      {errorMsg && <Alert message={errorMsg} className='mb-4' type='error' />}
      <Form
        name='albumAddEdit'
        form={form}
        onFinish={onFinish}
        onFinishFailed={onFinishFailed}
        initialValues={initialValues}
      >
        <div className='form-group'>
          <div className='form-group text-center'>
            <Form.Item name='photo' rules={validation.addDriver.uploadImage}>
              <div className='uploadimg position-relative'>
                <div className='img'>
                  <img
                    id='uploadimage'
                    src={`${prevImage ? prevImage : media}`}
                    className='rounded-circle'
                    alt='use img'
                  />
                </div>
                <label htmlFor='UploadImage'>
                  <i className='icon-photo_camera'></i>
                </label>
                <Input
                  type='file'
                  id='UploadImage'
                  placeholder='First name'
                  onChange={(e) => {
                    handleChanage(e)
                  }}
                  hidden
                />
              </div>
            </Form.Item>
          </div>
        </div>

        <div className='row'>
          <div className='col-sm-6'>
            <div className='form-group'>
              <label>
                First name
                <span className='errorMessage'>*</span>
              </label>
              <Form.Item
                name='first_name'
                rules={validation.addDriver.first_name}
              >
                <Input type='text' placeholder='First name' />
              </Form.Item>
            </div>
          </div>
          <div className='col-sm-6'>
            <div className='form-group'>
              <label>
                Last name
                <span className='errorMessage'>*</span>
              </label>
              <Form.Item
                name='last_name'
                rules={validation.addDriver.last_name}
              >
                <Input type='text' placeholder='Last name' />
              </Form.Item>
            </div>
          </div>
          <div className='col-sm-6'>
            <div className='form-group country_code'>
              <label>
                Contacting Person No. <span className='text-danger'>*</span>
              </label>
              <div className='input-group'>
                <div className='input-group-prepend mb-0'>
                  <Form.Item
                    name='phone_number_country_code'
                    rules={validation.addDriver.phone_number_country_code}
                  >
                    <Select labelInValue>
                      {countryList.map((m) => {
                        return (
                          <Option
                            value={m.callingCode}
                            key={`${m.callingCode}${currentTimeStamp()}`}
                          >
                            {m.callingCode}
                          </Option>
                        )
                      })}
                    </Select>
                  </Form.Item>
                </div>
                <Form.Item
                  name='phone_number'
                  rules={validation.addDriver.mobile_number}
                >
                  <Input type='text' placeholder='Mobile No.' />
                </Form.Item>
              </div>
            </div>
          </div>
          <div className='col-sm-6'>
            <div className='form-group'>
              <label>
                Email address
                <span className='errorMessage'>*</span>
              </label>
              <Form.Item name='email' rules={validation.addDriver.email}>
                <Input type='email' placeholder='Email address' />
              </Form.Item>
            </div>
          </div>
          {!isEditForm && (
            <>
              <div className='col-sm-6'>
                <div className='form-group'>
                  <label>
                    Password
                    <span className='errorMessage'>*</span>
                  </label>
                  <Form.Item
                    name='password'
                    rules={validation.changePassword.password}
                  >
                    <Input type='password' placeholder='Password' />
                  </Form.Item>
                </div>
              </div>
              <div className='col-sm-6'>
                <div className='form-group'>
                  <label>
                    Confirm Password
                    <span className='errorMessage'>*</span>
                  </label>
                  <Form.Item
                    name='confirm_password'
                    rules={validation.changePassword.confirmPassword}
                  >
                    <Input type='password' placeholder='Confirm Password' />
                  </Form.Item>
                </div>
              </div>
            </>
          )}
        </div>

        <div className='form-group btn-row text-center mb-0'>
          <Form.Item>
            <Button
              disabled={isSpin}
              htmlType='submit'
              className='btn btn-warning width-120 ripple-effect text-uppercase'
            >
              {isSpin ? <LoadingSpinner /> : submitButtonText}
            </Button>
            <Button
              htmlType='button'
              onClick={onCancel}
              className='btn btn-cancel width-120 ripple-effect text-uppercase'
            >
              {t('common.cancel')}
            </Button>
          </Form.Item>
        </div>
      </Form>
    </>
  )
}

AddEditExecutiveForm.propTypes = {
  form: PropTypes.any.isRequired,
  onCancel: PropTypes.func.isRequired,
  isSpin: PropTypes.bool.isRequired,
  onFinish: PropTypes.func.isRequired,
  onFinishFailed: PropTypes.func.isRequired,
  onFileUploaded: PropTypes.func.isRequired,
  initialValues: PropTypes.object.isRequired,
  media: PropTypes.any.isRequired,
  submitButtonText: PropTypes.string.isRequired,
  errorMsg: PropTypes.string.isRequired
}
