import { Button, Form, Input } from 'antd'
import React from 'react'
// import { useTranslation } from 'react-i18next'
const formRef = React.createRef()
function ExecutivesFilterForm({ onFinish, onFinishFailed, onReset }) {
  // const { t } = useTranslation()
  const onResetFields = () => {
    formRef.current.resetFields()
    onReset()
  }
  return (
    <Form
      name='basic'
      ref={formRef}
      onFinish={onFinish}
      onFinishFailed={onFinishFailed}
    >
      <div className='row'>
        <div className='col-sm-6 col-md-6 col-xl-6'>
          <div className='form-group'>
            <Form.Item name='q'>
              <Input
                placeholder='Search by name, Mobile and Email-id'
                type='text'
              />
            </Form.Item>
          </div>
        </div>
        <div className='col-sm-6 col-md-4 col-lg-3'>
          <div className='btnsubmit'>
            <Form.Item>
              <Button className='btn btn-warning' htmlType='submit'>
                Submit
              </Button>
              <Button
                className='btn btn-warning'
                htmlType='button'
                onClick={onResetFields}
              >
                Reset
              </Button>
            </Form.Item>
          </div>
        </div>
      </div>
    </Form>
  )
}

export default ExecutivesFilterForm
