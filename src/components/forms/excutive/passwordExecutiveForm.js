import React from 'react'
import { useTranslation } from 'react-i18next'
import { Form, Input, Button, Alert } from 'antd'
import PropTypes from 'prop-types'
import { LoadingSpinner } from '../../common'
import validation from '../../../utilities/validation'

export default function ChangePassExecutiveForm({
  form,
  onCancel,
  isSpin,
  onFinish,
  onFinishFailed,
  submitButtonText,
  errorMsg
}) {
  const { t } = useTranslation()

  return (
    <>
      {errorMsg && <Alert message={errorMsg} className='mb-4' type='error' />}
      <Form
        name='albumAddEdit'
        form={form}
        onFinish={onFinish}
        onFinishFailed={onFinishFailed}
      >
        <div className='row'>
          <div className='col-sm-6'>
            <div className='form-group'>
              <label>
                New Password
                <span className='errorMessage'>*</span>
              </label>
              <Form.Item
                name='new_password'
                rules={validation.changePassword.newPassword}
              >
                <Input type='password' placeholder='New Password' />
              </Form.Item>
            </div>
          </div>
          <div className='col-sm-6'>
            <div className='form-group'>
              <label>
                Confirm Password
                <span className='errorMessage'>*</span>
              </label>
              <Form.Item
                name='confirm_password'
                rules={validation.changePassword.confirmNewPassword}
              >
                <Input type='password' placeholder='Last name' />
              </Form.Item>
            </div>
          </div>
        </div>

        <div className='form-group btn-row text-center mb-0'>
          <Form.Item>
            <Button
              disabled={isSpin}
              htmlType='submit'
              className='btn btn-warning width-120 ripple-effect text-uppercase'
            >
              {isSpin ? <LoadingSpinner /> : submitButtonText}
            </Button>
            <Button
              htmlType='button'
              onClick={onCancel}
              className='btn btn-cancel width-120 ripple-effect text-uppercase'
            >
              {t('common.cancel')}
            </Button>
          </Form.Item>
        </div>
      </Form>
    </>
  )
}

ChangePassExecutiveForm.propTypes = {
  form: PropTypes.any.isRequired,
  onCancel: PropTypes.func.isRequired,
  isSpin: PropTypes.bool.isRequired,
  onFinish: PropTypes.func.isRequired,
  onFinishFailed: PropTypes.func.isRequired,
  submitButtonText: PropTypes.string.isRequired,
  errorMsg: PropTypes.string.isRequired
}
