import React from 'react'
import { Form, Input, Button } from 'antd'
import PropTypes from 'prop-types'
import validation from '../../../utilities/validation'
// import UploadMedia from '../../image-upload'
// import ApiEndPoints from '../../../utilities/apiEndPoints'
// import { currentTimeStamp } from '../../../utilities/common'
import { DatePicker, Space, Select } from 'antd'
const { Option } = Select
export default function ManageRiderForm({
  onFinish,
  onFinishFailed,
  onFromDateChange,
  onToDateChange,
  formRef,
  onReset
}) {
  return (
    <Form
      name='basic'
      onFinish={onFinish}
      onFinishFailed={onFinishFailed}
      ref={formRef}
    >
      <div className='row'>
        <div className='col-sm-6 col-md-4 col-xl-3'>
          <div className='form-group'>
            <label>Rider ID</label>
            <Form.Item name='id'>
              <Input placeholder='Rider ID' type='text' />
            </Form.Item>
          </div>
        </div>
        <div className='col-sm-6 col-md-4 col-xl-3'>
          <div className='form-group'>
            <label>Name</label>
            <Form.Item name='name' rules={validation.manageRiders.name}>
              <Input placeholder='Name' type='text' />
            </Form.Item>
          </div>
        </div>
        <div className='col-sm-6 col-md-4 col-xl-3'>
          <div className='form-group'>
            <label>Email</label>
            <Form.Item name='email' rules={validation.manageRiders.email}>
              <Input placeholder='Email' />
            </Form.Item>
            {/* <input type="text"
                className="form-control" placeholder="Email" /> */}
          </div>
        </div>
        <div className='col-sm-6 col-md-4 col-xl-3'>
          <div className='form-group'>
            <label>Mobile Number</label>
            <Form.Item
              name='phone_number'
              rules={validation.manageRiders.mobile_number}
            >
              <Input placeholder='Mobile Number' />
            </Form.Item>
            {/* <input type="text"
                className="form-control" placeholder="Mobile Number" /> */}
          </div>
        </div>
        <div className='col-sm-12 col-md-8 col-lg-4'>
          <div className='form-group'>
            <label>Registered within Date Range</label>
            <div className='input-group input-daterange'>
              <Form.Item name='from_date'>
                <Space direction='vertical' className='w-100'>
                  <DatePicker
                    placeholder='From Date'
                    className='form-control w-100'
                    format='YYYY-MM-DD'
                    onChange={onFromDateChange}
                  />
                </Space>
                {/* <Input placeholder="From Date" /> */}
              </Form.Item>
              <div className='input-group-addon'>TO</div>
              <Form.Item name='to_date'>
                <Space direction='vertical'>
                  <DatePicker
                    placeholder='To Date'
                    className='form-control w-100'
                    format='YYYY-MM-DD'
                    onChange={onToDateChange}
                  />
                </Space>
                {/* <Input placeholder="From Date" /> */}
              </Form.Item>
            </div>
          </div>
        </div>
        <div className='col-sm-6 col-md-4 col-xl-3'>
          <div className='form-group'>
            <label>Account Status</label>
            <Form.Item name='status'>
              <Select labelInValue placeholder='Select'>
                <Option value='pending'>Pending</Option>
                <Option value='active'>Active</Option>
                <Option value='inactive'>Inactive</Option>
              </Select>
            </Form.Item>
          </div>
        </div>

        <div className='col-sm-6 col-md-4 col-lg-3'>
          <div className='btnsubmit'>
            <label className='d-sm-block'>&nbsp;</label>
            <Form.Item>
              <Button className='btn btn-warning' htmlType='submit'>
                Submit
              </Button>
              <Button
                className='btn btn-warning'
                htmlType='button'
                onClick={onReset}
              >
                Reset
              </Button>
            </Form.Item>
          </div>
        </div>
      </div>
    </Form>
  )
}

ManageRiderForm.propTypes = {
  onFinish: PropTypes.func.isRequired,
  onFinishFailed: PropTypes.func.isRequired,
  onFromDateChange: PropTypes.func.isRequired,
  onToDateChange: PropTypes.func.isRequired,
  onReset: PropTypes.func.isRequired,
  formRef: PropTypes.any.isRequired
}
