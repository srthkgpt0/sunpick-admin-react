import React, { useEffect } from 'react'
import { Form, Input, Button, Radio } from 'antd'
import PropTypes from 'prop-types'
import { useTranslation } from 'react-i18next'
import { DatePicker, Space, Select } from 'antd'
import validation from '../../../utilities/validation'
import Autocomplete from 'react-google-autocomplete'
import VechicalListing from '../../../container/manageRides/vechicalList'
import { useHistory } from 'react-router'

const { Option } = Select
export default function AddRideForm({
  form,
  onFinish,
  onFinishFailed,
  onFromDateChange,
  onChange,
  countryOption,
  onCountryCodeSelect,
  onPickUpLocationSelect,
  onDropOffLocationSelect,
  passenger,
  // isFinish,
  // filterData,
  handleSelectOption,
  select,
  fare,
  handlePickUpOnChange,
  handleDropOffOnChange,
  onChangeCountryCode
}) {
  const { t } = useTranslation()
  let history = useHistory()

  const goBack = () => {
    history.push('/rides')
  }

  useEffect(() => {
    form.setFieldsValue({
      booking_type: 'now'
    })
  }, []) // eslint-disable-line react-hooks/exhaustive-deps
  const disabledDate = (current) => {
    return current && current.valueOf() < Date.now();
  }
  const countryCodeSelector = (
    <Form.Item
      name='phone_country_code'
      noStyle
      rules={validation.addDriver.phone_number_country_code}
    >
      <Select onChange={onChangeCountryCode}
        placeholder='Select'
        onSelect={onCountryCodeSelect}
        style={{
          width: 100
        }}
      >
        {countryOption.map((data, index) => (
          <Option key={index} value={data.value}>
            {data.label}
          </Option>
        ))}
      </Select>
    </Form.Item>
  )
  return (
    <Form
      form={form}
      name='AddRideForm'
      onFinish={onFinish}
      onFinishFailed={onFinishFailed}
    >
      <div className='row'>
        <div className='col-sm-6'>
          <div className='form-group'>
            <label>
              {t('rides.travelerNo')}
              <span className='errorMessage'>*</span>
            </label>
            {/* <Form.Item
              name='phone_country_code'
              rules={validation.addDriver.phone_number_country_code}
            >
              <Select onSelect={onCountryCodeSelect}>
                {countryOption.map((data, index) => (
                  <Option key={index} value={data.value}>
                    {data.label}
                  </Option>
                ))}
              </Select>
            </Form.Item> */}
            <Form.Item
              name='phone_number'
              rules={validation.addDriver.mobile_number}
              labelInValue
            >
              <Input
                addonBefore={countryCodeSelector}
                style={{
                  width: '100%'
                }}
                onChange={onChange}
              />
              {/* <Input onChange={onChange} /> */}
            </Form.Item>
          </div>
        </div>
        <div className='col-sm-6'>
          <div className='form-group'>
            <label> {t('rides.travelerName')}</label>
            <Form.Item name='full_name' rules={validation.ride.travelerName}>
              <Input />
            </Form.Item>
          </div>
        </div>
      </div>
      <div className='row'>
        <div className='col-sm-12'>
          <div className='form-group'>
            <label>
              {t('rides.pickupLocation')}
              <span className='errorMessage'>*</span>
            </label>
            <Form.Item
              name='pick_up_location'
              rules={validation.ride.pickUpLocation}
            >
              <div>
                <Autocomplete
                  className='form-control'
                  onPlaceSelected={onPickUpLocationSelect}
                  onChange={handlePickUpOnChange}
                  types={['(regions)']}
                  componentRestrictions={{
                    country: ['ET', 'IN']
                  }}
                />
              </div>
            </Form.Item>
          </div>
        </div>
        <div className='col-sm-12'>
          <div className='form-group'>
            <label>
              {t('rides.dropOffLocation')}
              <span className='errorMessage'>*</span>
            </label>
            <Form.Item
              name='drop_location'
              rules={validation.ride.dropOffLocation}
            >
              <div>
                <Autocomplete
                  className='form-control'
                  onPlaceSelected={onDropOffLocationSelect}
                  onChange={handleDropOffOnChange}
                  types={['(regions)']}
                  componentRestrictions={{
                    country: ['ET', 'IN']
                  }}
                />
              </div>
            </Form.Item>
          </div>
        </div>
      </div>
      <div className='row'>
        <div className='col-sm-6'>
          <div className='form-group'>
            <label>{t('rides.selectBookingType')}</label>
            <div className='input-group input-daterange'>
              {/* <div className="custom-control custom-radio custom-control-inline mt-0"> */}
              <Form.Item name='booking_type'>
                <Radio.Group name='radiogroup' defaultValue='now'>
                  <Radio value='now'>instant</Radio>
                  <Radio value='prebook'>preBook</Radio>
                </Radio.Group>
              </Form.Item>
              {/* </div> */}
            </div>
          </div>
        </div>
      </div>
      <div className='row'>
        <div className='col-sm-12'>
          <div className='row'>
            <div className='col-sm-3'>
              <div className='form-group'>
                <Form.Item
                  noStyle
                  shouldUpdate={(prevValues, currentValues) =>
                    prevValues.booking_type !== currentValues.booking_type
                  }
                >
                  {({ getFieldValue }) =>
                    getFieldValue('booking_type') === 'prebook' ? (
                      <>
                        <Form.Item name='booking_date_time'>
                          <Space direction='vertical' className='w-100'>
                            <DatePicker
                              placeholder='Select date and Time'
                              showTime
                              format='YYYY-MM-DD HH:mm:00'
                              onChange={onFromDateChange}
                              className='form-control w-100'
                              disabledDate={disabledDate}
                            />
                          </Space>
                        </Form.Item>
                      </>
                    ) : null
                  }
                </Form.Item>
              </div>
            </div>
            {/* <div className='col-sm-4'>
              <div className='form-group btnCounter'>
                <label>Select Passenger</label>
                <div className='controller'>
                  <Form.Item>
                    <Button
                      className='icon-add'
                      onClick={() => passengerIncrement()}
                    >
                      +
                    </Button>
                  </Form.Item>
                  <Form.Item>
                    <Button
                      className='icon-remove'
                      onClick={() => passengerDecrement()}
                    >
                      -
                    </Button>
                  </Form.Item>
                  <input
                    type='text'
                    className='form-control'
                    formControlName='passenger_capacity'
                    disabled
                    value={passenger}
                  />
                </div>
              </div>
            </div> */}
          </div>
        </div>
      </div>
      <div className='form-section'>
        <div className='row'>
          <div className='col-md-12'>
            <div className='common-table'>
              <div className='table-responsive'>
                {/* {passenger !== null ? ( */}
                <VechicalListing
                  // isFinish={isFinish}
                  // filterData={filterData}
                  passenger={passenger}
                  handleSelectOption={handleSelectOption}
                ></VechicalListing>
                {/* ) : null} */}
              </div>
            </div>
          </div>
        </div>
        {select && (
          <div className='row'>
            <div className='col-md-6'>
              <div className='form-group'>
                <label>Estimated Fare</label>
                <input
                  disabled
                  className='form-control'
                  type='text'
                  value={fare}
                  readonly
                />
              </div>
            </div>
          </div>
        )}
      </div>
      <div>
        <Form.Item>
          <Button
            type='primary'
            htmlType='button'
            className='btn btn-dark text-uppercase'
            onClick={goBack}
          >
            {'Cancel'}
          </Button>
          <Button
            type='primary'
            htmlType='submit'
            className='btn btn-warning text-uppercase'
          >
            Create Ride
          </Button>
        </Form.Item>
      </div>
    </Form>
  )
}

AddRideForm.propTypes = {
  form: PropTypes.any.isRequired,
  onFinish: PropTypes.func.isRequired,
  onFinishFailed: PropTypes.func.isRequired,
  submitButtonText: PropTypes.string.isRequired,
  onFromDateChange: PropTypes.func.isRequired
}
