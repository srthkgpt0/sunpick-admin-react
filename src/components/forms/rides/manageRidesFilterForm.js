import { Button, Form, Input, Select, DatePicker } from 'antd'
import React from 'react'
import { useTranslation } from 'react-i18next'
import validation from '../../../utilities/validation';

function ManageRidesForm({ onFinish, onFinishFailed, onReset, formRef, onFromDateChange,
  onToDateChange, }) {
  const { t } = useTranslation()
  const { Option } = Select
  const rideStatusOption = [
    {
      id: 1,
      label: 'Requested',
      value: 'requested'
    },
    {
      id: 1,
      label: 'Pending',
      value: 'pending'
    },
    {
      id: 2,
      label: 'Accepted',
      value: 'accepted'
    },
    {
      id: 3,
      label: 'Pre-Accepted',
      value: 'pre-accepted'
    },
    {
      id: 4,
      label: 'Arrived',
      value: 'arrived'
    },
    {
      id: 5,
      label: 'Started',
      value: 'started'
    },
    {
      id: 7,
      label: 'Ended',
      value: 'ended'
    },
    {
      id: 6,
      label: 'Completed',
      value: 'completed'
    },
    {
      id: 6,
      label: 'Assigned',
      value: 'assigned'
    },

    {
      id: 8,
      label: 'Driver Cancelled',
      value: 'driver_cancelled'
    },
    {
      id: 9,
      label: 'Customer Cancelled',
      value: 'customer_cancelled'
    },
    {
      id: 10,
      label: 'Admin Cancelled',
      value: 'admin_cancelled'
    },
    {
      id: 11,
      label: 'Deleted',
      value: 'deleted'
    },
    {
      id: 12,
      label: 'Timeout',
      value: 'timeout'
    }
  ]
  const paymentStatusOption = [
    {
      id: 1,
      label: 'Successful',
      value: 'success'
    },
    {
      id: 2,
      label: 'Failed',
      value: 'failed'
    }
  ]
  const paymentTypeOption = [
    {
      id: 1,
      label: 'Cash',
      value: 'cash'
    },
    {
      id: 2,
      label: 'Credit Card',
      value: 'card'
    },
    {
      id: 3,
      label: 'UPI',
      value: 'upi'
    },
    {
      id: 4,
      label: 'Net Banking',
      value: 'netbanking'
    },
    {
      id: 5,
      label: 'EMI',
      value: 'emi'
    },
    {
      id: 6,
      label: 'Bank Transfer',
      value: 'bank_transfer'
    },
    {
      id: 7,
      label: 'Wallet',
      value: 'wallet'
    }
  ]
  return (
    <Form
      name='basic'
      onFinish={onFinish}
      onFinishFailed={onFinishFailed}
      ref={formRef}
    >
      <div className='row'>
        <div className='col-sm-6 col-md-4 col-xl-3'>
          <div className='form-group'>
            <label>{t('rides.rideId')}</label>
            <Form.Item name='id' rules={validation.queryParams.ride_id}>
              <Input
                placeholder={t('rides.rideId')}
                type='text'
                name='rideId'
              />
            </Form.Item>
          </div>
        </div>
        <div className='col-sm-6 col-md-4 col-xl-3'>
          <div className='form-group'>
            <label>{t('rides.rideNumber')}</label>

            <Form.Item name='ride_number'>
              <Input placeholder={t('rides.rideNumber')} type='text' />
            </Form.Item>
          </div>
        </div>
        <div className='col-sm-6 col-md-4 col-xl-3'>
          <div className='form-group'>
            <label>{t('rides.rider')}</label>

            <Form.Item name='rider'>
              <Input placeholder={t('rides.rider')} type='text' />
            </Form.Item>
          </div>
        </div>
        <div className='col-sm-6 col-md-4 col-xl-3'>
          <div className='form-group'>
            <label>{t('rides.driver')}</label>

            <Form.Item name='driver'>
              <Input placeholder={t('rides.driver')} type='text' />
            </Form.Item>
          </div>
        </div>
        <div className='col-sm-6 col-md-4 col-xl-3'>
          <div className='form-group'>
            <label>{t('rides.rideStatus')}</label>

            <Form.Item name='ride_status'>
              <Select placeholder='Select'>
                {rideStatusOption.map((data, index) => (
                  <Option key={index} value={data.value}>
                    {data.label}
                  </Option>
                ))}
              </Select>
            </Form.Item>
          </div>
        </div>
        <div className='col-sm-6 col-md-4 col-xl-3'>
          <div className='form-group'>
            <label>{t('rides.paymentStatus')}</label>

            <Form.Item name='payment_status'>
              <Select placeholder='Select'>
                {paymentStatusOption.map((data, index) => (
                  <Option key={index} value={data.value}>
                    {data.label}
                  </Option>
                ))}
              </Select>
            </Form.Item>
          </div>
        </div>
        <div className='col-sm-6 col-md-4 col-xl-3'>
          <div className='form-group'>
            <label>{t('rides.paymentType')}</label>

            <Form.Item name='payment_type'>
              <Select placeholder='Select'>
                {paymentTypeOption.map((data, index) => (
                  <Option key={index} value={data.value}>
                    {data.label}
                  </Option>
                ))}
              </Select>
            </Form.Item>
          </div>
        </div>
        <div className='col-sm-6 col-md-4 col-xl-3'>
          <div className='form-group'>
            <label>{t('rides.paymentId')}</label>

            <Form.Item name='charge_id'>
              <Input placeholder={t('rides.paymentId')} type='text' />
            </Form.Item>
          </div>
        </div>
        <div className='col-sm-6 col-md-4 col-xl-3'>
          <div className='form-group'>
            <label>{t('rides.bookWithinDateRange')}</label>
            <div className='input-group input-daterange'>
              <Form.Item name='from_date'>
                <DatePicker
                  placeholder={t('rides.fromDate')}
                  className='form-control w-100'
                  format='YYYY-MM-DD'
                  onChange={onFromDateChange}
                />
              </Form.Item>
              <div className='input-group-addon'>TO</div>
              <Form.Item name='to_date'>
                <DatePicker
                  placeholder={t('rides.toDate')}
                  className='form-control w-100'
                  format='YYYY-MM-DD'
                  onChange={onToDateChange}
                />
              </Form.Item>
            </div>
          </div>
        </div>

        <div className='col-sm-12 col-md-4 col-lg-3'>
          <div className='btnsubmit'>
            <label className='d-sm-block'>&nbsp;</label>
            <Form.Item>
              <Button className='btn btn-warning' htmlType='submit'>
                {t('common.submit')}
              </Button>
              <Button
                className='btn btn-warning'
                htmlType='button'
                onClick={onReset}
              >
                {t('common.reset')}
              </Button>
            </Form.Item>
          </div>
        </div>
      </div>
    </Form>
  )
}

export default ManageRidesForm
