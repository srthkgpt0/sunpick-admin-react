import React from 'react'
import { useTranslation } from 'react-i18next'
import { Form, Button, Alert, Select } from 'antd'
import PropTypes from 'prop-types'
import { LoadingSpinner } from '../../common'
import validation from '../../../utilities/validation'

export default function AssignDriverForm({
  form,
  onCancel,
  isSpin,
  onFinish,
  onFinishFailed,
  errorMsg,
  availableDrivers,
  handleRefresh,
  ride,
  loadingDriverList
}) {
  const { t } = useTranslation()
  const { Option } = Select
  return (
    <>
      {errorMsg && <Alert message={errorMsg} className='mb-4' type='error' />}
      <Form
        name='albumAddEdit'
        form={form}
        onFinish={onFinish}
        onFinishFailed={onFinishFailed}
      >
        <div className='assign_info pb-0 common_border border-0'>
          <h4>Assign Driver</h4>
          <div className='driverlist input-group align-items-start'>
            <div className='w-100'>
              <Form.Item
                name='driver_id'
                rules={validation.ride.availableDriver}
              >
                <Select
                  showSearch
                  placeholder='Select'
                  optionFilterProp='children'
                >
                  {availableDrivers &&
                    availableDrivers?.map((drivers, index) => (
                      <Option key={index} value={drivers.id}>
                        {drivers.name}
                      </Option>
                    ))}
                </Select>
              </Form.Item>
              {(availableDrivers ?? availableDrivers?.length === 0) && (
                <div className='text-danger'>Currently No Driver Available</div>
              )}
            </div>

            <button
              className='btn btn-outline-danger ripple-effect ml-3'
              type='button'
              disabled={loadingDriverList}
              onClick={() => handleRefresh(ride)}
            >
              {loadingDriverList && <span className='fa fa-spinner'></span>}
              {!loadingDriverList && <i className='icon-refresh'></i>}
            </button>
          </div>

          <div className='form-group btn-row  mt-4'>
            <Form.Item>
              <Button
                disabled={isSpin}
                htmlType='submit'
                className='btn btn-warning width-120 ripple-effect text-uppercase'
              >
                {isSpin ? <LoadingSpinner /> : 'Assign'}
              </Button>
              <Button
                htmlType='button'
                onClick={onCancel}
                className='btn btn-cancel width-120 ripple-effect text-uppercase'
              >
                {t('common.cancel')}
              </Button>
            </Form.Item>
          </div>
        </div>
      </Form>
    </>
  )
}

AssignDriverForm.propTypes = {
  form: PropTypes.any.isRequired,
  onCancel: PropTypes.func.isRequired,
  isSpin: PropTypes.bool.isRequired,
  onFinish: PropTypes.func.isRequired,
  onFinishFailed: PropTypes.func.isRequired,
  errorMsg: PropTypes.string.isRequired
}
