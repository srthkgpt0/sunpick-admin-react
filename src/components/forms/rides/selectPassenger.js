export default function SelectPassenger({ passengerIncrement, passengerDecrement }) {
    return (
        <>
            <button onClick={() => passengerIncrement()}>+</button>
            <button onClick={() => passengerDecrement()}>-</button>
        </>
    );
}