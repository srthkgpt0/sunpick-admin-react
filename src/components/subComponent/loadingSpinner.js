import React from 'react'
// import { Spin } from 'antd'
// import { LoadingOutlined } from '@ant-design/icons'

export default function LoadingSpinner() {
  return (
    <div className='text-center'>
      <i className='fa fa-spinner fa-spin fa-2x fa-fw'></i>
    </div>
  )
}
