import React, { PureComponent } from 'react'
import { connect } from 'react-redux'
import { Link } from 'react-router-dom'
import PropTypes from 'prop-types'

class HeaderMenu extends PureComponent {

  constructor(props) {
    super(props);
    // Sets up our
      this.state = {
        show: true
      }


  }
   /// open sidewar
   filterOpen = (event, show) => {
      document.getElementsByTagName("body")[0].classList.add("page-sidebar-closed");
      event.preventDefault();
  };
  
  render() {
    const {
     show
    } = this.state;
    return (
      <div className="side_menu" data-mcs-theme="dark" id="sidemenu">
        <div className="navbar_header">
          <Link className="close-icon d-sm-none" id="closeToggle" to="/" onClick={(e) => this.filterOpen(e, show)}><i className="ti-close"></i></Link>

          <Link to="/dashboard" className="logo"> <img src="assets/images/logo.png" className="img-fluid" alt="logo" /> </Link>
        </div>
        <ul className="list-unstyled menu" data-mcs-theme="light">
          <li className="active" >
              <Link  to="/dashboard"> <i className="ti-pie-chart icon"></i>Dashboard</Link>
          </li>
          <li  >
              <Link  to="/drivers"> <i className="ti-car icon"></i>Drivers</Link>
          </li>
          <li  >
              <Link  to="/riders"> <i className="ti-user icon"></i>Riders</Link>
          </li>
        </ul>
      </div>

    )
  }
}

const mapStateToProps = (state) => {
  return {
    userData: state.auth.userData
  }
}

const mapDispatchToProps = () => {
  return {

  }
}

HeaderMenu.propTypes = {
  userData: PropTypes.object.isRequired
}

export default connect(mapStateToProps, mapDispatchToProps)(HeaderMenu)