import { Menu, Dropdown } from 'antd'
import React, { useState } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { Link } from 'react-router-dom'
import {
  logout,
  selectIsLoggedIn,
  selectUserData
} from '../../redux/auth/authSlice'
import { useTranslation } from 'react-i18next'

export default function Header(props) {
  const isLoggedIn = useSelector(selectIsLoggedIn)
  const userData = useSelector(selectUserData)
  const { t } = useTranslation()
  const [show, setShow] = useState(true)
  const dispatch = useDispatch()
  const filterOpen = (event, show) => {
    if (show === true) {
      setShow(false)
      document
        .getElementsByTagName('body')[0]
        .classList.add('page-sidebar-closed')
    } else {
      setShow(true)
      document
        .getElementsByTagName('body')[0]
        .classList.remove('page-sidebar-closed')
    }

    event.preventDefault()
  }
  const menu = (
    <Menu>
      <div aria-labelledby='dropdownMenuButton'>
        <Menu>
          <Menu.Item key='0'>
            <Link className='dropdown-item' to={'/change-password'}>
              <i className='ti-settings'></i> {t('header.changePassword')}
            </Link>
          </Menu.Item>
          <Menu.Item key='1'>
            <Link
              className='dropdown-item'
              to='/'
              onClick={(e) => {
                e.preventDefault()
                dispatch(logout())
              }}
            >
              <i className='ti-lock'></i> {t('header.logout')}
            </Link>
          </Menu.Item>
        </Menu>
      </div>
    </Menu>
  )

  return (
    <>
      <header id='header'>
        <nav className='header-navbar fixed-top navbar-shadow'>
          <div className='navbar_container'>
            <div className='container-fluid'>
              <div className='navbar'>
                <ul className='nav navbar-nav mr-auto'>
                  <li>
                    <Link
                      id='menu-toggle'
                      className='sidebar_toggle'
                      to='/'
                      onClick={(e) => filterOpen(e, show)}
                    >
                      <i className='material-icons icon'>sort</i>
                    </Link>
                  </li>
                </ul>
                {isLoggedIn && userData && (
                  <div className='dropdown user_setting'>
                    <Dropdown overlay={menu} trigger={['click']}>
                      <button
                        className='btn dropdown-toggle'
                        type='button'
                        id='dropdownMenuButton'
                        onClick={(e) => e.preventDefault()}
                        aria-haspopup='true'
                        aria-expanded='false'
                      >
                        <div className='user_img rounded-circle'>
                          <img
                            src={
                              userData.photo
                                ? userData.photo
                                : 'assets/images/default-userNew.jpg'
                            }
                            className='rounded-circle img-fluid'
                            alt='fff'
                          />
                        </div>

                        {userData.userName && (
                          <span className='user_name'>
                            Hello {userData.userName || ''}
                          </span>
                        )}
                      </button>
                    </Dropdown>
                  </div>
                )}
              </div>
            </div>
          </div>
        </nav>
      </header>
      {/* <HeaderMenu /> */}
    </>
  )
}
