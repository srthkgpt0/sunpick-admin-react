import React, { useEffect } from 'react'


const LoaderComponent = (props) => {
   
   useEffect(() => {

   }, [props]) 

  

   return (
      <div className="text-center"><i className="fa fa-spinner fa-spin fa-2x fa-fw"></i></div>
   )
}

export default LoaderComponent

