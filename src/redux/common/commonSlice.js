import { createSlice } from '@reduxjs/toolkit'
import { masterVendorListService } from '../../services/master'
import logger from '../../utilities/logger'

export const commonSlice = createSlice({
  name: 'common',
  initialState: {
    vendorList: [],
    selectedVendor: ''
  },
  reducers: {
    updateVendorlistInState: (state, action) => {
      return (state = {
        ...state,
        vendorList: action.payload
      })
    },
    updateVendorSelect: (state, action) => {
      return (state = {
        ...state,
        selectedVendor: action.payload
      })
    }
  }
})

export const {
  updateVendorlistInState,
  updateVendorSelect
} = commonSlice.actions

export const updateVendorlist = () => async (dispatch) => {
  try {
    let res = await masterVendorListService()
    dispatch(updateVendorlistInState(res))
  } catch (error) {
    logger(error)
  }
}

export const selectVendorList = (state) => state.common.vendorList

export const selectedVendorInList = (state) => state.common.selectedVendor

export default commonSlice.reducer
