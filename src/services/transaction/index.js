import APIrequest from "../apiRequest"
import ApiEndPoints from "../../utilities/apiEndPoints"
import logger from "../../utilities/logger"
import modalNotification from "../../utilities/notifications"

export const deactivateTransactionService = async ({ bodyData }) => {
  const payload = {
    ...ApiEndPoints.deactivateTransaction,
    bodyData
  }
  try {
    const res = await APIrequest(payload)
    modalNotification({
      type: 'success',
      message: 'Success',
      description: res.message
    })
    return res
  } catch (error) {
    logger(error)
    throw (error)
  }
}

export const activateTransactionService = async ({ bodyData }) => {
  const payload = {
    ...ApiEndPoints.activateTransaction,
    bodyData
  }
  try {
    const res = await APIrequest(payload)
    modalNotification({
      type: 'success',
      message: 'Success',
      description: res.message
    })
    return res
  } catch (error) {
    logger(error)
    throw (error)
  }
}

export const errorDetailTransactionService = async ({ queryParams }) => {
  const payload = {
    ...ApiEndPoints.errorDetailTransaction,
    queryParams
  }
  try {
    return await APIrequest(payload)
  } catch (error) {
    logger(error)
    throw (error)
  }
}

export const fileContentTransactionService = async ({ transactionId }) => {
  const payload = {
    ...ApiEndPoints.fileContentTransaction(transactionId)
  }
  try {
    return await APIrequest(payload)
  } catch (error) {
    logger(error)
    throw (error)
  }
}