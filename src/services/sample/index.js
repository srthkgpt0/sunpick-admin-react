import APIrequest from "../apiRequest"
import logger from "../../utilities/logger"

export const sampleService = async () => {
  const payload = {
    baseURL: 'https://reqres.in/api/products/3'
  }
  try {
    return await APIrequest(payload)
  } catch (error) {
    logger(error)
    throw (error)
  }
}