import APIrequest from "../apiRequest"
import ApiEndPoints from "../../utilities/apiEndPoints"
import logger from "../../utilities/logger"

export const authListService = async (data) => {
  const bodyData = data;
  const payload = {
    ...ApiEndPoints.login,
    bodyData
  }
  try {
    
    return await APIrequest(payload)
  } catch (error) {
    logger(error)
    throw (error)
  }
}
export const changePassService = async (data) => {
  const bodyData = data;
  const payload = {
    ...ApiEndPoints.changePassword,
    bodyData
  }
  try {
    
    return await APIrequest(payload)
  } catch (error) {
    logger(error)
    throw (error)
  }
}
export const logoutAuth = async () => {
 
  const payload = {
    ...ApiEndPoints.logout
  }
  try {
    
    return await APIrequest(payload)
  } catch (error) {
    logger(error)
    throw (error)
  }
}

export const forgetPassService = async (data) => {
  const bodyData = data;
  const payload = {
    ...ApiEndPoints.forgetPassword,
    bodyData
  }
  try {
    
    return await APIrequest(payload)
  } catch (error) {
    logger(error)
    throw (error)
  }
}