import APIrequest from '../apiRequest'
import ApiEndPoints from '../../utilities/apiEndPoints'
import logger from '../../utilities/logger'

export const sendDriverMessage = async ({ bodyData }) => {
  const payload = {
    ...ApiEndPoints.sendDriverMessage,
    bodyData
  }
  try {
    return await APIrequest(payload)
  } catch (error) {
    logger(error)
    throw error
  }
}


export const sendCustomerMessage = async ({ bodyData }) => {
    const payload = {
      ...ApiEndPoints.sendCustomerMessage,
      bodyData
    }
    try {
      return await APIrequest(payload)
    } catch (error) {
      logger(error)
      throw error
    }
  }
  
