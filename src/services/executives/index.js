import APIrequest from '../apiRequest'
import ApiEndPoints from '../../utilities/apiEndPoints'
import logger from '../../utilities/logger'

export const getExecutiveListService = async ({ queryParams }) => {
  const payload = {
    ...ApiEndPoints.getExecutiveList,
    queryParams
  }
  try {
    return await APIrequest(payload)
  } catch (error) {
    logger(error)
    throw error
  }
}

export const deleteUpdateExecutiveStatusService = async (executiveId, body) => {
  let payload
  if (body.status === 'delete') {
    payload = {
      ...ApiEndPoints.deleteExcutiveStatus(executiveId),
      bodyData: body
    }
  } else {
    payload = {
      ...ApiEndPoints.updateExcutiveStatus(executiveId),
      bodyData: body
    }
  }
  try {
    return await APIrequest(payload)
  } catch (error) {
    logger(error)
    throw error
  }
}

export const changeExecutivePasswordService = async (id, filterData) => {
  const payload = {
    ...ApiEndPoints.executiveChangePass(id),
    bodyData: filterData
  }
  try {
    return await APIrequest(payload)
  } catch (error) {
    logger(error)
    throw error
  }
}

export const addEditExecutiveService = async (id, isEditFormType, formData) => {
  let payload = {
    ...ApiEndPoints.addEditExecutive(),
    editData: formData
  }

  if (isEditFormType) {
    payload = {
      ...payload,
      ...ApiEndPoints.addEditExecutive(id)
    }
  }
  try {
    return await APIrequest(payload)
  } catch (error) {
    logger(error)
    throw error
  }
}
