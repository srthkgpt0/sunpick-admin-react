import APIrequest from "../apiRequest"
import ApiEndPoints from "../../utilities/apiEndPoints"
import logger from "../../utilities/logger"

export const driverListService = async ({ queryParams }) => {
  const payload = {
    ...ApiEndPoints.driverList,
    queryParams
  }
  try {
    return await APIrequest(payload)
  } catch (error) {
    logger(error)
    throw (error)
  }
}
export const driverRidesEarningList = async ({ queryParams } , id) => {
  const payload = {
    ...ApiEndPoints.DRIVER_RIDES_EARNING_LIST(id),
    queryParams
  }
  try {
    return await APIrequest(payload)
  } catch (error) {
    logger(error)
    throw (error)
  }
}
export const driverDocumentList = async ({ queryParams } , id) => {
  const payload = {
    ...ApiEndPoints.DRIVER_DOCUMENT_LIST(id),
    queryParams
  }
  try {
    return await APIrequest(payload)
  } catch (error) {
    logger(error)
    throw (error)
  }
}
export const driverDelete = async (id) => {
  const payload = {
    ...ApiEndPoints.DELETE_DRIVER(id),
  }
  try {
    return await APIrequest(payload)
  } catch (error) {
    logger(error)
    throw (error)
  }
}
export const categoryListService = async ({ queryParams }) => {
  const payload = {
    ...ApiEndPoints.categoryList,
    queryParams
  }
  try {
    return await APIrequest(payload)
  } catch (error) {
    logger(error)
    throw (error)
  }
}
export const getMakeList = async ({ queryParams }) => {
  const payload = {
    ...ApiEndPoints.makeList,
    queryParams
  }
  try {
    return await APIrequest(payload)
  } catch (error) {
    logger(error)
    throw (error)
  }
}
export const getDriverDetail = async (id) => {
  const payload = {
    ...ApiEndPoints.GET_DRIVER_DETAIL(id)
  }
  try {
    return await APIrequest(payload)
  } catch (error) {
    logger(error)
    throw (error)
  }
}
export const getModelList = async ({ queryParams }) => {
  const payload = {
    ...ApiEndPoints.modelList,
    queryParams
  }
  try {
    return await APIrequest(payload)
  } catch (error) {
    logger(error)
    throw (error)
  }
}
export const getCategoryList = async ({ queryParams }) => {
  const payload = {
    ...ApiEndPoints.categoryList,
    queryParams
  }
  try {
    return await APIrequest(payload)
  } catch (error) {
    logger(error)
    throw (error)
  }
}
export const getSateList = async ({ queryParams }) => {
  const payload = {
    ...ApiEndPoints.stateList,
    queryParams
  }
  try {
    return await APIrequest(payload)
  } catch (error) {
    logger(error)
    throw (error)
  }
}

export const changeStatusService = async (data,driver_id) => {
  const bodyData = data;
  const payload = {
    ...ApiEndPoints.updateStatus(driver_id),
    bodyData
  }
  try {
    
    return await APIrequest(payload)
  } catch (error) {
    logger(error)
    throw (error)
  }
}
export const addUpdateDriver = async (data,driver_id) => {
  const bodyData = data;
  const payload = {
    ...ApiEndPoints.addUpdateDriver(driver_id),
    bodyData
  }
  try {
    
    return await APIrequest(payload)
  } catch (error) {
    logger(error)
    throw (error)
  }
}
export const addDocument = async (data,type) => {
  const bodyData = data;
  const queryParams = {type:type};
  const payload = {
    ...ApiEndPoints.ADD_DOCUMENT(type),
    queryParams,
    bodyData
  }
  try {
    
    return await APIrequest(payload)
  } catch (error) {
    logger(error)
    throw (error)
  }
}
export const driverAvailabilityService = async () => {
  const data = [
    { key: 0, value: "Not Available", textColor: "red" },
    { key: 1, value: "Available", textColor: "green" },
    { key: 2, value: "On Ride", textColor: "red" }
  ];
  try {
    
    return data
  } catch (error) {
    logger(error)
    throw (error)
  }
}
export const getDriverAvailabilityDetails = (is_available, is_shift_started) => {
  let result = {};
  var element = [
    { key: 0, value: "Not Available", textColor: "red" },
    { key: 1, value: "Available", textColor: "green" },
    { key: 2, value: "On Ride", textColor: "red" }
  ];
  if (is_available === true && is_shift_started === true) {
    result = { value: element[1].value, textColor: element[1].textColor };
  } else if (is_available === false && is_shift_started === true) {
    result = { value: element[2].value, textColor: element[2].textColor };
  } else if (is_shift_started === false) {
    result = { value: element[0].value, textColor: element[0].textColor };
  }
  return result;
}
export const getDriverLocationAvailabilityDetails = (socket_status) => {
  let result = {};
  var element = [{ key: 0, value: "OFF", textColor: "red" },
  { key: 1, value: "ON", textColor: "green" },]
    if (socket_status && socket_status ==='online' ) {
    result = { value: element[1].value, textColor: element[1].textColor };
  } else  {
    result = { value: element[0].value, textColor: element[0].textColor };
  }
  return result;
}

export const getDriverListService = async ({ queryParams }) => {
  const payload = {
    ...ApiEndPoints.getActiveDriverList,
    queryParams
  }
  try {
    return await APIrequest(payload)
  } catch (error) {
    logger(error)
    throw (error)
  }
}

export const driverStatusUpdateService = async (id,status) => {
  // debugger
  let payload;
  if(status === 'delete'){
    payload = {
      ...ApiEndPoints.deleteDriver(id),
      // bodyData: { status }
    }
  } else {
    payload = {
      ...ApiEndPoints.updateStatus(id),
      bodyData: { status }
    }
  }
  try {
    return await APIrequest(payload)
  } catch (error) {
    logger(error)
    throw (error)
  }
}

export const importDriverService = async (type,formData) => {
  const payload = {
    ...ApiEndPoints.importDriver(type),
    editData: formData
  }
  try {
    return await APIrequest(payload)
  } catch (error) {
    logger(error)
    throw (error)
  }
}

