import APIrequest from "../apiRequest"
import ApiEndPoints from "../../utilities/apiEndPoints"
import logger from "../../utilities/logger"

export const promoCodeService = async ({ queryParams }) => {
  const payload = {
    ...ApiEndPoints.getPromoCodeList,
    queryParams
  }
  try {
    return await APIrequest(payload)
  } catch (error) {
    logger(error)
    throw (error)
  }
}

export const promoCodeChangeStatusService = async (id,{ bodyData }) => {
  const payload = {
    ...ApiEndPoints.changePromoStatus(id),
    bodyData
  }
  try {
    return await APIrequest(payload)
  } catch (error) {
    logger(error)
    throw (error)
  }
}


export const promoCodeDetailService = async (id) => {
  const payload = {
    ...ApiEndPoints.GET_PROMOCODE_DETAIL(id)
  }
  try {
    return await APIrequest(payload)
  } catch (error) {
    logger(error)
    throw (error)
  }
}

export const updatePromoCode = async (data,id) => {
  const bodyData = data;
  const payload = {
    ...ApiEndPoints.addEditPromoCode(id),
    bodyData
  }
  try {
    return await APIrequest(payload)
  } catch (error) {
    logger(error)
    throw (error)
  }
}


export const deletePromoCode = async (id) => {
  const payload = {
    ...ApiEndPoints.deletePromoCode(id)
  }
  try {
    return await APIrequest(payload)
  } catch (error) {
    logger(error)
    throw (error)
  }
}