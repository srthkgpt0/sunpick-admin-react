import APIrequest from "../apiRequest"
import ApiEndPoints from "../../utilities/apiEndPoints"
import logger from "../../utilities/logger"

export const masterVendorListService = async () => {
  const payload = {
    ...ApiEndPoints.masterVendorList
  }
  try {
    return await APIrequest(payload)
  } catch (error) {
    logger(error)
    throw (error)
  }
}