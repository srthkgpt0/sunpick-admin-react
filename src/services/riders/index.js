import ApiEndPoints from '../../utilities/apiEndPoints'
import logger from '../../utilities/logger'
import APIrequest from '../apiRequest'

export const getRiderDetailsService = async (id) => {
  const payload = {
    ...ApiEndPoints.getRiderDetails(id)
  }
  try {
    return await APIrequest(payload)
  } catch (error) {
    logger(error)
    throw error
  }
}
export const getRiderListingDetailsService = async (id, { queryParams }) => {
  const payload = {
    ...ApiEndPoints.getRiderListingDetails(id),
    queryParams
  }
  try {
    return await APIrequest(payload)
  } catch (error) {
    logger(error)
    throw error
  }
}
export const updateRiderService = async (filterData, id = {}) => {
  try {
    let payload = {
      ...ApiEndPoints.updateRider(id),
      bodyData: filterData
    }

    return await APIrequest(payload)
  } catch (error) {
    logger(error)
    throw error
  }
}
export const getRiderListingService = async (queryParams) => {
  try {
    let payload = {
      ...ApiEndPoints.getRidersDetails,
      queryParams
    }

    return await APIrequest(payload)
  } catch (error) {
    logger(error)
    throw error
  }
}
export const updateRiderStatusService = async (id,status) => {
  try {
    const payload = {
      ...ApiEndPoints.updateRiderStatus(id),
      bodyData: { status }
    }

    return await APIrequest(payload)
  } catch (error) {
    logger(error)
    throw error
  }
}
