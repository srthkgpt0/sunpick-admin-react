import ApiEndPoints from '../../utilities/apiEndPoints'
import logger from '../../utilities/logger'
import APIrequest from '../apiRequest'

export const getRideListService = async ({ queryParams }) => {
  const payload = {
    ...ApiEndPoints.getRidesList,
    queryParams
  }
  try {
    return await APIrequest(payload)
  } catch (error) {
    logger(error)
    throw error
  }
}

export const getSingleRideDetailService = async (id) => {
  const payload = {
    ...ApiEndPoints.getRideDetails(id)
  }
  try {
    return await APIrequest(payload)
  } catch (error) {
    logger(error)
    throw error
  }
}
export const getAvailableDrivers = async (queryParams) => {
  const payload = {
    ...ApiEndPoints.getAvailableDrivers,
    queryParams
  }
  try {
    return await APIrequest(payload)
  } catch (error) {
    logger(error)
    throw error
  }
}
export const assignDriverService = async (queryParams, id) => {
  const payload = {
    ...ApiEndPoints.assignDriver(id),
    queryParams
  }
  try {
    return await APIrequest(payload)
  } catch (error) {
    logger(error)
    throw error
  }
}

export const getVechicalListService = async ({ queryParams }) => {
  const payload = {
    ...ApiEndPoints.getVechicalList,
    queryParams
  }
  try {
    return await APIrequest(payload)
  } catch (error) {
    logger(error)
    throw error
  }
}

export const createFareCalculationService = async ({ bodyData }) => {
  const payload = {
    ...ApiEndPoints.getFareCalculation,
    bodyData
  }
  try {
    return await APIrequest(payload)
  } catch (error) {
    logger(error)
    throw error
  }
}

export const searchCustomerService = async ({ bodyData }) => {
  const payload = {
    ...ApiEndPoints.searchCustomer,
    bodyData
  }
  try {
    return await APIrequest(payload)
  } catch (error) {
    logger(error)
    throw error
  }
}

export const bookRideService = async ({ bodyData }) => {
  // console.log('book ride body data we have here', bodyData)
  const payload = {
    ...ApiEndPoints.bookingRide,
    bodyData
  }
  try {
    return await APIrequest(payload)
  } catch (error) {
    logger(error)
    throw error
  }
}
