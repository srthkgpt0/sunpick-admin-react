import APIrequest from "../apiRequest"
import ApiEndPoints from "../../utilities/apiEndPoints"
import logger from "../../utilities/logger"

export const dashboardCountService = async ({ queryParams }) => {
  const payload = {
    ...ApiEndPoints.dashboardList,
    queryParams
  }
  try {
    return await APIrequest(payload)
  } catch (error) {
    logger(error)
    throw (error)
  }
}
export const dashboardMapData = async ({ queryParams }) => {
  const payload = {
    ...ApiEndPoints.driverDashList,
    queryParams
  }
  try {
    return await APIrequest(payload)
  } catch (error) {
    logger(error)
    throw (error)
  }
}
export const dashboardListService = async ({ queryParams }) => {
  const payload = {
    ...ApiEndPoints.driverList,
    queryParams
  }
  try {
    return await APIrequest(payload)
  } catch (error) {
    logger(error)
    throw (error)
  }
}
export const getUserGraph = async ({ queryParams }) => {
  const payload = {
    ...ApiEndPoints.dashboardUserGraph,
    queryParams
  }
  try {
    return await APIrequest(payload)
  } catch (error) {
    logger(error)
    throw (error)
  }
}






