import APIrequest from "../apiRequest"
import ApiEndPoints from "../../utilities/apiEndPoints"
import logger from "../../utilities/logger"

export const getTripDataListService = async ({ queryParams }) => {
    const payload = {
      ...ApiEndPoints.getTripData,
      queryParams
    }
    try {
      return await APIrequest(payload)
    } catch (error) {
      logger(error)
      throw (error)
    }
  }

  export const getCompletedRideListService = async ({ queryParams }) => {
    const payload = {
      ...ApiEndPoints.getCompeltedRide,
      queryParams
    }
    try {
      return await APIrequest(payload)
    } catch (error) {
      logger(error)
      throw (error)
    }
  }

  export const getRejectedRideListService = async ({ queryParams }) => {
    const payload = {
      ...ApiEndPoints.getRejectedRides,
      queryParams
    }
    try {
      return await APIrequest(payload)
    } catch (error) {
      logger(error)
      throw (error)
    }
  }

  export const getDriverParticularListService = async ({ queryParams }) => {
    const payload = {
      ...ApiEndPoints.getDriverParticular,
      queryParams
    }
    try {
      return await APIrequest(payload)
    } catch (error) {
      logger(error)
      throw (error)
    }
  }
