// import APIrequest from "../apiRequest"
// import ApiEndPoints from "../../utilities/apiEndPoints"
// import logger from "../../utilities/logger"
import { getSessionStorageToken } from '../../utilities/common'
export const getRideStatusClass = (currentState) => {
  let className
  switch (currentState) {
    case 'customer_cancelled':
      className = 'red'
      break
    case 'success':
      className = 'green'
      break
    case 'accepted':
      className = 'green'
      break
    case 'pending':
      className = 'green'
      break
    case 'offline':
      className = 'red'
      break
    case 'online':
      className = 'green'
      break

    case 'started':
      className = 'blue'
      break
    case 'Not Available':
      className = 'red'
      break
    case 'Available':
      className = 'green'
      break

    case 'admin_cancelled':
      className = 'red'
      break

    case 'completed':
      className = 'green'
      break

    case 'requested':
      className = 'green'
      break

    case 'timeout':
      className = 'yellow'
      break
    case 'assigned':
      className = 'yellow'
      break

    default:
      className = 'red'
      break
  }

  return className
}
export const getPaymentStatusCustomName = (currentState) => {
  switch (currentState) {
    case 'failed':
      currentState = 'Failed'
      break

    case 'success':
      currentState = 'Success'
      break

    case 'pending':
      currentState = 'Pending'
      break

    case 'completed':
      currentState = 'Completed'
      break

    case 'paid':
      currentState = 'Paid'
      break

    default:
      break
  }

  return currentState
}

// getRideStatusCustomName is used for cutom name of ride status
export const getRideStatusCustomName = (currentState) => {
  switch (currentState) {
    case 'customer_cancelled':
      currentState = 'Customer Cancelled'
      break

    case 'accepted':
      currentState = 'Accepted'
      break

    case 'started':
      currentState = 'Started'
      break

    case 'completed':
      currentState = 'Completed'
      break

    case 'requested':
      currentState = 'Requested'
      break

    case 'driver_cancelled':
      currentState = 'Driver Cancelled'
      break

    case 'pre-accepted':
      currentState = 'Pre-Accepted'
      break

    case 'deleted':
      currentState = 'Deleted'
      break

    case 'timeout':
      currentState = 'Timeout'
      break

    case 'pending':
      currentState = 'Pending'
      break
    case 'ended':
      currentState = 'Ended'
      break
    case 'arrived':
      currentState = 'Arrived'
      break
    case 'admin_cancelled':
      currentState = 'Admin Cancelled'
      break
    case 'assigned':
      currentState = 'Assigned'
      break
    default:
      break
  }

  return currentState
}
export const getFile = (url, queryParam) => {
  let reqUrl
  if (queryParam) {
    reqUrl = `${url}?authorization=${'Bearer '}${getSessionStorageToken()}&${queryParam}`
  } else {
    reqUrl = `${url}?authorization=${'Bearer '}${getSessionStorageToken()}`
  }
  window.open(reqUrl)
}
