import APIrequest from '../apiRequest'
import ApiEndPoints from '../../utilities/apiEndPoints'
import logger from '../../utilities/logger'

export const getCategoryListingData = async (queryParams) => {
  const payload = {
    ...ApiEndPoints.getCategoryListingData,
    queryParams
  }
  try {
    return await APIrequest(payload)
  } catch (error) {
    logger(error)
    throw error
  }
}
export const getGlobalSettingDataService = async () => {
  const payload = {
    ...ApiEndPoints.getGlobalSettingsListingData
  }
  try {
    return await APIrequest(payload)
  } catch (error) {
    logger(error)
    throw error
  }
}

export const getModelListingData = async (queryParams) => {
  const payload = {
    ...ApiEndPoints.getModelListingData,
    queryParams
  }
  try {
    return await APIrequest(payload)
  } catch (error) {
    logger(error)
    throw error
  }
}

export const getMakeListingData = async (queryParams) => {
  const payload = {
    ...ApiEndPoints.getMakeListingData,
    queryParams
  }
  try {
    return await APIrequest(payload)
  } catch (error) {
    logger(error)
    throw error
  }
}
export const getFareListingDataService = async () => {
  const payload = {
    ...ApiEndPoints.getFareListingData
  }
  try {
    return await APIrequest(payload)
  } catch (error) {
    logger(error)
    throw error
  }
}
export const getPreDefinedListingService = async (queryParams) => {
  const payload = {
    ...ApiEndPoints.getPreDefinedListingData,
    queryParams
  }
  try {
    return await APIrequest(payload)
  } catch (error) {
    logger(error)
    throw error
  }
}

export const getCategoryDropdown = async () => {
  const payload = {
    ...ApiEndPoints.getCategoryDropdownData
  }
  try {
    return await APIrequest(payload)
  } catch (error) {
    logger(error)
    throw error
  }
}
export const getMakeDropdown = async () => {
  const payload = {
    ...ApiEndPoints.getMakeDropdownData
  }
  try {
    return await APIrequest(payload)
  } catch (error) {
    logger(error)
    throw error
  }
}

export const getRelatedCategories = async (queryParams = null) => {
  const payload = {
    ...ApiEndPoints.getRelatedCategories,
    queryParams
  }
  try {
    return await APIrequest(payload)
  } catch (error) {
    logger(error)
    throw error
  }
}

export const updateCategoryService = async (formData, id = null) => {
  // console.log(id, 'updateCategoryService')
  const payload = {
    ...ApiEndPoints.updateCategory(id),
    editData: formData
  }
  try {
    return await APIrequest(payload)
  } catch (error) {
    logger(error)
    throw error
  }
}
export const updateSettingService = async (bodyData, id = null) => {
  const payload = {
    ...ApiEndPoints.updateGlobalSetting(id),
    bodyData: bodyData
  }
  try {
    return await APIrequest(payload)
  } catch (error) {
    logger(error)
    throw error
  }
}

export const updateMessageService = async (bodyData, id = null) => {
  const payload = {
    ...ApiEndPoints.updateMessage(id),
    bodyData: bodyData
  }
  try {
    return await APIrequest(payload)
  } catch (error) {
    logger(error)
    throw error
  }
}

export const updateModelService = async (bodyData, id = null) => {
  const payload = {
    ...ApiEndPoints.updateModel(id),
    bodyData: bodyData
  }
  try {
    return await APIrequest(payload)
  } catch (error) {
    logger(error)
    throw error
  }
}

export const updateCategoryStatusService = async (id, status) => {
  const payload = {
    ...ApiEndPoints.updateCategoryStatus(id),
    bodyData: { status }
  }
  try {
    return await APIrequest(payload)
  } catch (error) {
    logger(error)
    throw error
  }
}
export const updateFareStatusService = async (id, status) => {
  const payload = {
    ...ApiEndPoints.updateFareStatus(id),
    bodyData: { status }
  }
  try {
    return await APIrequest(payload)
  } catch (error) {
    logger(error)
    throw error
  }
}

export const updateModelStatusService = async (id, status) => {
  const payload = {
    ...ApiEndPoints.updateModelStatus(id),
    bodyData: { status }
  }
  try {
    return await APIrequest(payload)
  } catch (error) {
    logger(error)
    throw error
  }
}

export const updateMakeStatusService = async (id, status) => {
  const payload = {
    ...ApiEndPoints.updateMakeStatus(id),
    bodyData: { status }
  }
  try {
    return await APIrequest(payload)
  } catch (error) {
    logger(error)
    throw error
  }
}
export const updateMakeService = async (bodyData, id = null) => {
  const payload = {
    ...ApiEndPoints.updateMake(id),
    bodyData: bodyData
  }
  try {
    return await APIrequest(payload)
  } catch (error) {
    logger(error)
    throw error
  }
}
export const updateFareSettingService = async (bodyData, id) => {
  const payload = {
    ...ApiEndPoints.updateFareSetting(id),
    bodyData: bodyData
  }
  try {
    return await APIrequest(payload)
  } catch (error) {
    logger(error)
    throw error
  }
}

export const deleteModelService = async (id) => {
  const payload = {
    ...ApiEndPoints.deleteModel(id)
  }
  try {
    return await APIrequest(payload)
  } catch (error) {
    logger(error)
    throw error
  }
}
export const deleteMessageService = async (id) => {
  const payload = {
    ...ApiEndPoints.deleteMessage(id)
  }
  try {
    return await APIrequest(payload)
  } catch (error) {
    logger(error)
    throw error
  }
}

export const deleteCategoryService = async (id) => {
  const payload = {
    ...ApiEndPoints.deleteCategory(id)
  }
  try {
    return await APIrequest(payload)
  } catch (error) {
    logger(error)
    throw error
  }
}
export const deleteMakeService = async (id) => {
  const payload = {
    ...ApiEndPoints.deleteMake(id)
  }
  try {
    return await APIrequest(payload)
  } catch (error) {
    logger(error)
    throw error
  }
}
