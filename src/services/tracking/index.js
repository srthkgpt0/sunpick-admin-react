import ApiEndPoints from '../../utilities/apiEndPoints'
import logger from '../../utilities/logger'
import APIrequest from '../apiRequest'

export const getTrackingListService = async ({ queryParams }) => {
  const payload = {
    ...ApiEndPoints.getTrackingList,
    queryParams
  }
  try {
    return await APIrequest(payload)
  } catch (error) {
    logger(error)
    throw error
  }
}

export const getSingleTrackingDetailService = async (id) => {
    const payload = {
        ...ApiEndPoints.getTrackingDetails(id)
        }
    try {
        return await APIrequest(payload)
    } catch (error) {
        logger(error)
        throw error
    }
}
export const getSingleTrackerService = async (id) => {
    const payload = {
        ...ApiEndPoints.getTrakerDetails(id)
        }
    try {
        return await APIrequest(payload)
    } catch (error) {
        logger(error)
        throw error
    }
}